��    U      �  q   l      0  -   1  1   _     �  4   �  =   �  ?     8   Y  B   �     �     �     	  2   	     G	     Z	     i	     z	     �	  ,   �	     �	     �	  2   �	     (
     ?
  ,   S
     �
  2   �
  2   �
  *   �
  +   &  /   R  )   �     �  "   �     �  $   
  &   /  Y   V     �  	   �  0   �  c   �  �   c     �  $        >      R     s  (   �     �     �  '   �       *   0  $   [     �     �     �     �     �     �     �          "     2  "   A  $   d  $   �     �     �  &   �     �          4     G     V  !   m  !   �     �  :   �     �          $  	   9  6   C  !  z  �   �  �   9     �  8   �  7   %  >   ]  8   �  :   �        '   1     Y  ?   f     �     �     �     �     �  7   �     /     D  7   \     �     �  1   �       1     1   J  #   |  $   �  6   �  +   �     (  '   H  )   p  (   �  )   �  �   �     �  
   �  @   �    %  �   -     �  %   �       (   *     S  :   r  &   �  %   �  ,   �  *   '  B   R  '   �  $   �  '   �  -   
  1   8     j     z     �     �     �     �  ,   �  ,   	   4   6      k      }   +   �      �   5   �      !     (!  "   =!  *   `!  '   �!     �!  G   �!     "       "     A"  	   ["  ?   e"         '      %   2      /            P          
                T   O   I   6           !   ?      F   +   &         7             ,      8   #      M           C   )              D   <   R      (   0              ;   $   E               K                    B          J   :   5   H   S                        	       N   A   1       "      4       G   -   @   3       9   *   >   U   =          .               Q   L    
For bug reporting instructions, please see:
 
For bug reporting instructions, please see:
%s.
 
Go ahead? (y or n)    --help                   Display this information
   -h, --help                      Print this help, then exit
   -n, --no-output                 Do not create an output file
   -o <file>                Place the output into <file>
   -v, --version                   Print version number, then exit
 #%s is a GCC extension #`%s' not supported by %s# %s (GCC) %s
 %s `%s' does not fully implement the `%s' protocol %s at end of input %s before "%s" %s before %s'%c' %s before %s'\x%x' %s is too large %s: %s compiler not installed on this system %s: Not a directory --resource requires -o -keep_private_externs not allowed with -dynamiclib -pipe is not supported -pipe not supported -private_bundle not allowed with -dynamiclib Configured with: %s
 Copyright (C) 2001 Free Software Foundation, Inc.
 Copyright (C) 2002 Free Software Foundation, Inc.
 GNU C does not support -C without using -E GNU C does not support -CC without using -E Generate C header of platform specific features Generate code for GNU runtime environment ISO C forbids `goto *expr;' ISO C forbids an empty source file ISO C forbids nested functions ISO C90 does not support `long long' ISO C90 does not support complex types Internal error: %s (program %s)
Please submit a full bug report.
See %s for instructions. No input file name. Options:
 Please keep this in mind before you report bugs. Please submit a full bug report,
with preprocessed source if appropriate.
See %s for instructions.
 This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 Usage: %s [options] file...
 Usage: gcov [OPTION]... SOURCEFILE

 YYDEBUG not defined `%s' attribute directive ignored `%s' attribute ignored `%s' declared `static' but never defined `%s' defined but not used `%s' is not a valid class name `%s' is not at beginning of declaration `%s' used but never defined `-p' not supported; use `-pg' and gprof(1) `long long long' is too long for GCC can't create %s can't open %s cannot create temporary file cannot specify both -C and -o creating %s declaration of `%D' as %s division by zero empty declaration gcc version %s
 gcov (GCC) %s
 ignoring duplicate directory "%s"
 ignoring invalid multibyte character ignoring nonexistent directory "%s"
 internal error:  invalid option `%s' invalid option `%s' in automata_option language %s not recognized may not use both -m32 and -m64 multi-line comment no input files opening output file %s profiling not supported with -mg
 repeated declaration of unit `%s' syntax error to generate dependencies you must specify either -M or -MM too many input files unit `%s' is not used unterminated comment warning:  wrong number of arguments specified for `%s' attribute Project-Id-Version: gcc 3.3-b20021230
POT-Creation-Date: 2002-12-30 18:56+0000
PO-Revision-Date: 2003-01-06 20:10+0100
Last-Translator: Karl Eichwalder <ke@suse.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Zum Einsenden von Fehlerberichten (auf Englisch) lesen Sie bitte die
folgenden Hinweise; Fehler in der deutschen Übersetzung sind an de@li.org
zu melden:
 
Zum Einsenden von Fehlerberichten (auf Englisch) lesen Sie bitte die Hinweise in:
%s.
Fehler in der deutschen Übersetzung sind an de@li.org zu melden.
 
Fortfahren? (y oder n)    --help                   Diese Informationen anzeigen
   -h, --help                      Diese Hilfe anzeigen
   -n, --no-output                 Keine Ausgabedatei erzeugen
   -o <Datei>               Ausgabe in <Datei> schreiben
   -v, --version                   Versionsnummer anzeigen
 #%s ist eine Erweiterung des GCC #»%s« wird von %s# nicht unterstützt %s (GCC) %s
 %s »%s« implementiert das »%s«-Protokoll nicht vollständig %s am Ende der Eingabe %s vor "%s" %s vor %s'%c' %s vor %s'\x%x' %s ist zu groß %s: %s-Compiler ist auf diesem System nicht installiert %s: Kein Verzeichnis --resource erfordert -o -keep_private_externs ist mit -dynamiclib nicht erlaubt -pipe wird nicht unterstützt -pipe wird nicht unterstützt -private_bundle ist mit -dynamiclib nicht erlaubt Konfiguriert mit: %s
 Copyright © 2001 Free Software Foundation, Inc.
 Copyright © 2002 Free Software Foundation, Inc.
 GNU C unterstützt nicht -C ohne -E GNU C unterstützt nicht -CC ohne -E C-Header mit Plattform-spezifischen Merkmalen erzeugen Code für die GNU-Laufzeitumgebung erzeugen ISO C verbietet »goto *expr;« ISO C erlaubt keine leeren Quelldateien ISO C verbietet verschachtelte Funktionen ISO C90 unterstützt nicht »long long« ISO C90 unterstützt nicht komplexe Typen Interner Fehler: %s (Programm %s)
Bitte senden Sie einen vollständigen Fehlerbericht
auf Englisch ein; Fehler in der deutschen Übersetzung
sind an de@li.org zu melden.
Gehen Sie gemäß den Hinweisen in %s vor. Kein Eingabedateiname, Optionen:
 Bitte bedenken Sie dies, wenn Sie einen Fehlerbericht einsenden. Bitte senden Sie einen vollständigen Fehlerbericht auf Englisch ein;
bearbeiten Sie die Quellen zunächst mit eine Präprozessor, wenn es
dienlich ist.
Fehler in der deutschen Übersetzung sind an de@li.org zu melden.

Gehen Sie gemäß den Hinweisen in %s vor.
 Dies ist freie Software; die Kopierbedingungen stehen in den Quellen. Es
gibt KEINE Garantie; auch nicht für VERKAUFBARKEIT oder FÜR SPEZIELLE ZWECKE.

 Aufruf: %s [Optionen] Datei...
 Aufruf: gcov [OPTION]... QUELLDATEI

 YYDEBUG ist nicht definiert Attribut-Anweisung »%s« wird ignoriert Attribut »%s« wird ignoriert »%s« als »static« deklariert, aber nirgendwo definiert »%s« definiert, aber nicht verwendet »%s« ist kein gültiger Klassenname »%s« ist nicht am Beginn einer Deklaration »%s« verwendet, aber nirgendwo definiert »-p« wird nicht unterstützt; verwenden Sie »-pg« und gprof(1) »long long long« ist für GCC zu lang Es ist nicht möglich %s zu erzeugen Es ist nicht möglich »%s« zu öffnen temporäre Datei konnte nicht angelegt werden -C und -o können nicht zusammen angegeben werden Erzeugen von %s Deklaration von »%D« als %s Teilung durch Null leere Deklaration gcc-Version %s
 gcov (GCC) %s
 doppeltes Verzeichnis »%s« wird ignoriert
 ungültiges Multibyte-Zeichen wird ignoriert nicht vorhandenes Verzeichnis »%s« wird ignoriert
 interner Fehler:  ungültige Option »%s« ungültige Option »%s« in automata_option Sprache %s nicht erkannt -m32 und -m64 können nicht zusammen angegeben werden mehrzeiliger Kommentar keine Eingabedateien Ausgabedatei »%s« wird geöffnet Profiling wird mit -mg nicht unterstützt
 wiederholte Deklaration von Unit »%s« Syntaxfehler Um Abhängigkeiten zu erzeugen müssen Sie entweder -M oder -MM angeben zu viele Eingabedateien Unit »%s« wird nicht verwendet nicht beendeter Kommentar Warnung:  falsche Anzahl an Argumenten für das Attribut »%s« angegeben 