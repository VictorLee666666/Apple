XDrawLine(3X11R5)                                     XDrawLine(3X11R5)

  XXDDrraawwLLiinnee(())

  NNAAMMEE

    XDrawLine(), XDrawLines(), XDrawSegments(), XSegment() - draw lines,
    polygons, and line structure

  SSYYNNOOPPSSIISS

    XDrawLine (Display *display, Drawable d, GC gc, int x1,
               int y1, int x2, int y2)
    XDrawLines (Display *display, Drawable d, GC gc,
                XPoint *points, int npoints, int mode)
    XDrawSegments (Display *display, Drawable d, GC gc,
                   XSegment *segments, int nsegments)

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    mode
        Specifies the coordinate mode. You can pass CoordModeOrigin or
        CoordModePrevious .

    npoints
        Specifies the number of points in the array.

    nsegments
        Specifies the number of segments in the array.

    points
        Specifies an array of points.

    segments
        Specifies an array of segments.

    x1

    y1

    x2

    y2
        Specify the points (x1, y1) and (x2, y2) to be connected.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_DD_rr_aa_ww_LL_ii_nn_ee(3X11R5) function uses the components of the specified GC to
    draw a line between the specified set of points (x1, y1) and (x2, y2). It
    does not perform joining at coincident endpoints. For any given line,
    _XX_DD_rr_aa_ww_LL_ii_nn_ee(3X11R5) does not draw a pixel more than once. If lines
    intersect, the intersecting pixels are drawn multiple times.

    The _XX_DD_rr_aa_ww_LL_ii_nn_ee_ss(3X11R5) function uses the components of the specified GC to
    draw npoints-1 lines between each pair of points (point[i], point[i+1]) in
    the array of XXPPooiinntt structures. It draws the lines in the order listed in
    the array. The lines join correctly at all intermediate points, and if the
    first and last points coincide, the first and last lines also join
    correctly. For any given line, _XX_DD_rr_aa_ww_LL_ii_nn_ee_ss(3X11R5) does not draw a pixel
    more than once. If thin (zero line-width) lines intersect, the
    intersecting pixels are drawn multiple times. If wide lines intersect, the
    intersecting pixels are drawn only once, as though the entire PolyLine
    protocol request were a single, filled shape. CoordModeOrigin treats all
    coordinates as relative to the origin, and CoordModePrevious treats all
    coordinates after the first as relative to the previous point.

    The _XX_DD_rr_aa_ww_SS_ee_gg_mm_ee_nn_tt_ss(3X11R5) function draws multiple, unconnected lines. For
    each segment, _XX_DD_rr_aa_ww_SS_ee_gg_mm_ee_nn_tt_ss(3X11R5) draws a line between (x1, y1) and (x2,
    y2). It draws the lines in the order listed in the array of XXSSeeggmmeenntt
    structures and does not perform joining at coincident endpoints. For any
    given line, XXDDrraawwSSeeggmmeennttss does not draw a pixel more than once. If lines
    intersect, the intersecting pixels are drawn multiple times.

    All three functions use these GC components: function, plane-mask, line-
    width, line-style, cap-style, fill-style, subwindow-mode, clip-x-origin,
    clip-y-origin, and clip-mask. The _XX_DD_rr_aa_ww_LL_ii_nn_ee_ss(3X11R5) function also uses
    the join-style GC component. All three functions also use these GC mode-
    dependent components: foreground, background, tile, stipple, tile-stipple-
    x-origin, tile-stipple-y-origin, dash-offset, and dash-list.

    _XX_DD_rr_aa_ww_LL_ii_nn_ee(3X11R5), _XX_DD_rr_aa_ww_LL_ii_nn_ee_ss(3X11R5), and _XX_DD_rr_aa_ww_SS_ee_gg_mm_ee_nn_tt_ss(3X11R5) can
    generate BadDrawable , BadGC , and BadMatch errors. _XX_DD_rr_aa_ww_LL_ii_nn_ee_ss(3X11R5) can
    also generate a BadValue error.

  SSTTRRUUCCTTUURREESS

    The XXSSeeggmmeenntt structure contains:

    typedef struct {
         short x1, y1, x2, y2;
    } XSegment;

    All x and y members are signed integers. The width and height members are
    16-bit unsigned integers. You should be careful not to generate
    coordinates and sizes out of the 16-bit ranges, because the protocol only
    has 16-bit fields for these values.

  DDIIAAGGNNOOSSTTIICCSS

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        An IInnppuuttOOnnllyy window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_D_r_a_w_A_r_c()

    _X_D_r_a_w_P_o_i_n_t()

    _X_D_r_a_w_R_e_c_t_a_n_g_l_e()

    Xlib

