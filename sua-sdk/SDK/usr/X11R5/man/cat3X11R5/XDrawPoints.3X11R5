XDrawPoints(3X11R5)                                 XDrawPoints(3X11R5)

  XXDDrraawwPPooiinntt(())

  NNAAMMEE

    XDrawPoint(), XDrawPoints(), XPoint() - draw points and points structure

  SSYYNNOOPPSSIISS

    XDrawPoint (Display *display, Drawable d, GC gc, int x,
                int y)
    XDrawPoints (Display *display, Drawable d, GC gc,
                 XPoint *points, int npoints, int mode)

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    mode
        Specifies the coordinate mode. You can pass CoordModeOrigin or
        CoordModePrevious.

    npoints
        Specifies the number of points in the array.

    points
        Specifies an array of points.

    x

    y
        Specify the x and y coordinates where you want the point drawn.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt(3X11R5) function uses the foreground pixel and function
    components of the GC to draw a single point into the specified drawable;
    _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt_ss(3X11R5) draws multiple points this way. CoordModeOrigin treats
    all coordinates as relative to the origin, and CoordModePrevious treats
    all coordinates after the first as relative to the previous point.
    _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt_ss(3X11R5) draws the points in the order listed in the array.

    Both functions use these GC components: function, plane-mask, foreground,
    subwindow-mode, clip-x-origin, clip-y-origin, and clip-mask.

    _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt(3X11R5) can generate BadDrawable, BadGC, and BadMatch errors.
    _XX_DD_rr_aa_ww_PP_oo_ii_nn_tt_ss(3X11R5) can generate BadDrawable, BadGC, BadMatch, and
    BadValue errors.

  SSTTRRUUCCTTUURREESS

    The XXPPooiinntt structure contains:

    typedef struct {
         short x, y;
    } XPoint;

    All x and y members are signed integers. The width and height members are
    16-bit unsigned integers. You should be careful not to generate
    coordinates and sizes out of the 16-bit ranges, because the protocol only
    has 16-bit fields for these values.

  DDIIAAGGNNOOSSTTIICCSS

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadMatch
        An InputOnly window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_D_r_a_w_A_r_c()

    _X_D_r_a_w_L_i_n_e()

    _X_D_r_a_w_R_e_c_t_a_n_g_l_e()

    Xlib

