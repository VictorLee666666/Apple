XListPixmapFormats(3X11R5)                   XListPixmapFormats(3X11R5)

  IImmaaggeeBByytteeOOrrddeerr(())

  NNAAMMEE

    ImageByteOrder(), BitmapBitOrder(), BitmapPad(), BitmapUnit(),
    DisplayHeight(), DisplayHeightMM(), DisplayWidth(), DisplayWidthMM(),
    XListPixmapFormats(), XPixmapFormatValues() - image format functions and
    macros

  SSYYNNOOPPSSIISS

    XPixmapFormatValues *XListPixmapFormats (Display *display,
                                             int *count_return)
    int ImageByteOrder (Display *display)
    int BitmapBitOrder (Display *display)
    int BitmapPad (Display *display)
    int BitmapUnit (Display *display)
    int DisplayHeight (Display *display, int screen_number)
    int DisplayHeightMM (Display *display, int screen_number)
    int DisplayWidth (Display *display, int screen_number)
    int DisplayWidthMM (Display *display, int screen_number)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    count_return
        Returns the number of pixmap formats that are supported by the
        display.

    screen_number
        Specifies the appropriate screen number on the host server.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_LL_ii_ss_tt_PP_ii_xx_mm_aa_pp_FF_oo_rr_mm_aa_tt_ss(3X11R5) function returns an array of
    XXPPiixxmmaappFFoorrmmaattVVaalluueess structures that describe the types of Z format images
    supported by the specified display. If insufficient memory is available,
    _XX_LL_ii_ss_tt_PP_ii_xx_mm_aa_pp_FF_oo_rr_mm_aa_tt_ss(3X11R5) returns NULL. To free the allocated storage for
    the XXPPiixxmmaappFFoorrmmaattVVaalluueess structures, use _XX_FF_rr_ee_ee(3X11R5).

    The _II_mm_aa_gg_ee_BB_yy_tt_ee_OO_rr_dd_ee_rr(3X11R5) macro specifies the required byte order for
    images for each scanline unit in XY format (bitmap) or for each pixel
    value in Z format.

    The _BB_ii_tt_mm_aa_pp_BB_ii_tt_OO_rr_dd_ee_rr(3X11R5) macro returns LSBFirst or MSBFirst to indicate
    whether the leftmost bit in the bitmap as displayed on the screen is the
    least or most significant bit in the unit.

    The _BB_ii_tt_mm_aa_pp_PP_aa_dd(3X11R5) macro returns the number of bits that each scanline
    must be padded.

    The _BB_ii_tt_mm_aa_pp_UU_nn_ii_tt(3X11R5) macro returns the size of a bitmap's scanline unit
    in bits.

    The _DD_ii_ss_pp_ll_aa_yy_HH_ee_ii_gg_hh_tt(3X11R5) macro returns the height of the specified screen
    in pixels.

    The _DD_ii_ss_pp_ll_aa_yy_HH_ee_ii_gg_hh_tt_MM_MM(3X11R5) macro returns the height of the specified
    screen in millimeters.

    The _DD_ii_ss_pp_ll_aa_yy_WW_ii_dd_tt_hh(3X11R5) macro returns the width of the screen in pixels.

    The _DD_ii_ss_pp_ll_aa_yy_WW_ii_dd_tt_hh_MM_MM(3X11R5) macro returns the width of the specified screen
    in millimeters.

  SSTTRRUUCCTTUURREESS

    The XXPPiixxmmaappFFoorrmmaattVVaalluueess structure provides an interface to the pixmap
    format information that is returned at the time of a connection setup. It
    contains:

    typedef struct {
         int depth;
         int bits_per_pixel;
         int scanline_pad;
    } XPixmapFormatValues;

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s()

    _B_l_a_c_k_P_i_x_e_l_O_f_S_c_r_e_e_n()

    _I_s_C_u_r_s_o_r_K_e_y()

    _X_F_r_e_e()

    Xlib

