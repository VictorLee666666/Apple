XDestroyIC(3X11R5)                                   XDestroyIC(3X11R5)

  XXCCrreeaatteeIICC(())

  NNAAMMEE

    XCreateIC(), XDestroyIC(), XIMOfIC() - create, destroy, and obtain the
    input method of an input context

  SSYYNNOOPPSSIISS

    XIC XCreateIC (XIM im, ...)
    void XDestroyIC (XIC ic)
    XIM XIMOfIC (XIC ic)

  AARRGGUUMMEENNTTSS

    ic
        Specifies the input context.

    im
        Specifies the input method.

    ...
        Specifies the variable length argument list to set XIC values.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_CC_rr_ee_aa_tt_ee_II_CC(3X11R5) function creates a context within the specified
    input method.

    Some of the arguments are mandatory at creation time, and the input
    context will not be created if they are not provided. Those arguments are
    the input style and the set of text callbacks (if the input style selected
    requires callbacks). All other input context values can be set later.

    _XX_CC_rr_ee_aa_tt_ee_II_CC(3X11R5) returns a NULL value if no input context could be
    created. A NULL value could be returned for any of the following reasons:
    *     A required argument was not set.
    *     A read-only argument was set (for example, XNFilterEvents).
    *     The argument name is not recognized.
    *     The input method encountered an input method implementation
          dependent error.

    The _XX_CC_rr_ee_aa_tt_ee_II_CC(3X11R5) can generate BadAtom, BadColor, BadPixmap, and
    BadWindow errors.

    _XX_DD_ee_ss_tt_rr_oo_yy_II_CC(3X11R5) destroys the specified input context.

    The _XX_II_MM_OO_ff_II_CC(3X11R5) function returns the input method associated with the
    specified input context.

  DDIIAAGGNNOOSSTTIICCSS

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadPixmap
        A value for a Pixmap argument does not name a defined Pixmap.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_O_p_e_n_I_M()

    _X_S_e_t_I_C_F_o_c_u_s()

    _X_S_e_t_I_C_V_a_l_u_e_s()

    _X_m_b_R_e_s_e_t_I_C()

    Xlib

