XtHasCallbacks(3X11R5)                           XtHasCallbacks(3X11R5)

  XXttCCaallllCCaallllbbaacckkss(())

  NNAAMMEE

    XtCallCallbacks(), XtHasCallbacks() - process callbacks

  SSYYNNOOPPSSIISS

    void XtCallCallbacks (Widget w, String callback_name,
                          XtPointer call_data)
    typedef enum {XtCallbackNoList,
                  XtCallbackHasNone,
                  XtCallbackHasSome} XtCallbackStatus

    XtCallbackStatus XtHasCallbacks (Widget w, String callback_name)

  AARRGGUUMMEENNTTSS

    callback_name
        Specifies the callback list to be executed or checked.

    call_data
        Specifies a callback-list specific data value to pass to each of the
        callback procedure in the list.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_CC_aa_ll_ll_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) function calls each procedure that is
    registered in the specified widget's callback list.

    The _XX_tt_HH_aa_ss_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) function first checks to see if the widget has
    a callback list identified by callback_name. If the callback list does not
    exist, _XX_tt_HH_aa_ss_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) returns XtCallbackNoList. If the callback
    list exists but is empty, it returns XtCallbackHasNone. If the callback
    list exists and has at least one callback registered, it returns
    XtCallbackHasSome.

  SSEEEE AALLSSOO

    _X_t_A_d_d_C_a_l_l_b_a_c_k()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

