XQueryPointer(3X11R5)                             XQueryPointer(3X11R5)

  XXQQuueerryyPPooiinntteerr(())

  NNAAMMEE

    XQueryPointer() - get pointer coordinates

  SSYYNNOOPPSSIISS

    Bool XQueryPointer (Display *display, Window w,
                        Window *root_return, Window *child_return,
                        int *root_x_return, int *root_y_return,
                        int *win_x_return, int *win_y_return,
                        unsigned int *mask_return)

  AARRGGUUMMEENNTTSS

    child_return
        Returns the child window that the pointer is located in, if any.

    display
        Specifies the connection to the X server.

    mask_return
        Returns the current state of the modifier keys and pointer buttons.

    root_return
        Returns the root window that the pointer is in.

    root_x_return

    root_y_return
        Return the pointer coordinates relative to the root window's origin.

    w
        Specifies the window.

    win_x_return

    win_y_return
        Return the pointer coordinates relative to the specified window.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) function returns the root window the pointer is
    logically on and the pointer coordinates relative to the root window's
    origin. If _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) returns False, the pointer is not on the
    same screen as the specified window, and _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) returns
    None to child_return and zero to win_x_return and win_y_return. If
    _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) returns True, the pointer coordinates returned to
    win_x_return and win_y_return are relative to the origin of the specified
    window. In this case, _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) returns the child that
    contains the pointer, if any, or else None to child_return.

    _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) returns the current logical state of the keyboard
    buttons and the modifier keys in mask_return. It sets mask_return to the
    bitwise inclusive OR of one or more of the button or modifier key bitmasks
    to match the current state of the mouse buttons and the modifier keys.

    _XX_QQ_uu_ee_rr_yy_PP_oo_ii_nn_tt_ee_rr(3X11R5) can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_G_e_t_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s()

    _X_Q_u_e_r_y_T_r_e_e()

    Xlib

