XQueryBestStipple(3X11R5)                     XQueryBestStipple(3X11R5)

  XXQQuueerryyBBeessttSSiizzee(())

  NNAAMMEE

    XQueryBestSize(), XQueryBestTile(), XQueryBestStipple() - determine
    efficient sizes

  SSYYNNOOPPSSIISS

    Status XQueryBestSize (Display *display, int class,
                           Drawable which_screen, unsigned int width,
                           unsigned int  height,
                           unsigned int *width_return,
                           unsigned int *height_return)
    Status XQueryBestTile (Display *display, Drawable which_screen,
                           unsigned int width, height,
                           unsigned int *width_return,
                           unsigned int *height_return)
    Status XQueryBestStipple (Display *display, Drawable which_screen,
                              unsigned int width, unsigned int height,
                              unsigned int *width_return,
                              unsigned int *height_return)

  AARRGGUUMMEENNTTSS

    class
        Specifies the class that you are interested in. You can pass
        TileShape, CursorShape, or StippleShape.

    display
        Specifies the connection to the X server.

    width

    height
        Specify the width and height.

    which_screen
        Specifies any drawable on the screen.

    width_return

    height_return
        Return the width and height of the object best supported by the
        display hardware.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_SS_ii_zz_ee(3X11R5) function returns the best or closest size to
    the specified size. For CursorShape, this is the largest size that can be
    fully displayed on the screen specified by which_screen. For TileShape,
    this is the size that can be tiled fastest. For StippleShape, this is the
    size that can be stippled fastest. For CursorShape, the drawable indicates
    the desired screen. For TileShape and StippleShape, the drawable indicates
    the screen and possibly the window class and depth. An InputOnly window
    cannot be used as the drawable for TileShape or StippleShape, or a
    BadMatch error results.

    _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_SS_ii_zz_ee(3X11R5) can generate BadDrawable, BadMatch, and BadValue
    errors.

    The _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_TT_ii_ll_ee(3X11R5) function returns the best or closest size, that
    is, the size that can be tiled fastest on the screen specified by
    which_screen. The drawable indicates the screen and possibly the window
    class and depth. If an InputOnly window is used as the drawable, a
    BadMatch error results.

    _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_TT_ii_ll_ee(3X11R5) can generate BadDrawable and BadMatch errors.

    The _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_SS_tt_ii_pp_pp_ll_ee(3X11R5) function returns the best or closest size,
    that is, the size that can be stippled fastest on the screen specified by
    which_screen. The drawable indicates the screen and possibly the window
    class and depth. If an InputOnly window is used as the drawable, a
    BadMatch error results.

    _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_SS_tt_ii_pp_pp_ll_ee(3X11R5) can generate BadDrawable and BadMatch errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadMatch
        An InputOnly window is used as a Drawable.

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

    BadMatch
        The values do not exist for an InputOnly window.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C()

    _X_S_e_t_A_r_c_M_o_d_e()

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n()

    _X_S_e_t_F_i_l_l_S_t_y_l_e()

    _X_S_e_t_F_o_n_t()

    _X_S_e_t_L_i_n_e_A_t_t_r_i_b_u_t_e_s()

    _X_S_e_t_S_t_a_t_e()

    _X_S_e_t_T_i_l_e()

    Xlib

