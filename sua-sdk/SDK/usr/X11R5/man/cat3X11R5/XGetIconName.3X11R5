XGetIconName(3X11R5)                               XGetIconName(3X11R5)

  XXSSeettWWMMIIccoonnNNaammee(())

  NNAAMMEE

    XSetWMIconName(), XGetWMIconName(), XSetIconName(), XGetIconName() - set
    or read a window's WM_ICON_NAME property

  SSYYNNOOPPSSIISS

    void XSetWMIconName (Display *display, Window w,
                         XTextProperty *text_prop)
    Status XGetWMIconName (Display *display, Window w,
                           XTextProperty *text_prop_return)
    XSetIconName (Display *display, Window w, char *icon_name)
    Status XGetIconName (Display *display, Window w,
                         char **icon_name_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    icon_name
        Specifies the icon name, which should be a null-terminated string.

    icon_name_return
        Returns the window's icon name, which is a null-terminated string.

    text_prop
        Specifies the XXTTeexxttPPrrooppeerrttyy structure to be used.

    text_prop_return
        Returns the XXTTeexxttPPrrooppeerrttyy structure.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_WW_MM_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) convenience function calls
    _XX_SS_ee_tt_TT_ee_xx_tt_PP_rr_oo_pp_ee_rr_tt_yy(3X11R5) to set the WM_ICON_NAME property.

    The _XX_GG_ee_tt_WW_MM_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) convenience function calls
    _XX_GG_ee_tt_TT_ee_xx_tt_PP_rr_oo_pp_ee_rr_tt_yy(3X11R5) to obtain the WM_ICON_NAME property. It returns
    nonzero status on success; otherwise it returns a zero status.

    The _XX_SS_ee_tt_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) function sets the name to be displayed in a
    window's icon.

    _XX_SS_ee_tt_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) function returns the name to be displayed in the
    specified window's icon. If it succeeds, it returns nonzero; otherwise, if
    no icon name has been set for the window, it returns zero. If you never
    assigned a name to the window, _XX_GG_ee_tt_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) sets icon_name_return
    to NULL. If the data returned by the server is in the Latin Portable
    Character Encoding, then the returned string is in the Host Portable
    Character Encoding. Otherwise, the result is implementation dependent.
    When finished with it, a client must free the icon name string using
    _XX_FF_rr_ee_ee(3X11R5).

    _XX_GG_ee_tt_II_cc_oo_nn_NN_aa_mm_ee(3X11R5) can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_ICON_NAME
        The name to be used in an icon.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_F_r_e_e()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

