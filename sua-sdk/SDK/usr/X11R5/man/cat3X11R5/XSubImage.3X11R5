XSubImage(3X11R5)                                     XSubImage(3X11R5)

  XXCCrreeaatteeIImmaaggee(())

  NNAAMMEE

    XCreateImage(), XGetPixel(), XPutPixel(), XSubImage(), XAddPixel(),
    XDestroyImage() - image utilities

  SSYYNNOOPPSSIISS

    XImage *XCreateImage (Display *display, Visual *visual,
                          unsigned int depth, int format,
                          int offset, char *data, unsigned int width,
                          unsigned int height, int bitmap_pad,
                          int bytes_per_line)
    unsigned long XGetPixel (XImage *ximage, int x, int y)
    XPutPixel (XImage *ximage, int x, int y, unsigned long pixel)
    XImage *XSubImage (XImage *ximage, int x, int y,
                       unsigned int subimage_width,
                       unsigned int subimage_height)
    XAddPixel (XImage *ximage, long value)
    XDestroyImage (XImage * ximage)

  AARRGGUUMMEENNTTSS

    bitmap_pad
        Specifies the quantum of a scanline (8, 16, or 32). In other words,
        the start of one scanline is separated in client memory from the start
        of the next scanline by an integer multiple of this many bits.

    bytes_per_line
        Specifies the number of bytes in the client image between the start of
        one scanline and the start of the next.

    data
        Specifies the image data.

    depth
        Specifies the depth of the image.

    display
        Specifies the connection to the X server.

    format
        Specifies the format for the image. You can pass XYBitmap, XYPixmap,
        or ZPixmap.

    height
        Specifies the height of the image, in pixels.

    offset
        Specifies the number of pixels to ignore at the beginning of the
        scanline.

    pixel
        Specifies the new pixel value.

    subimage_height
        Specifies the height of the new subimage, in pixels.

    subimage_width
        Specifies the width of the new subimage, in pixels.

    value
        Specifies the constant value that is to be added.

    visual
        Specifies the VViissuuaall structure.

    width
        Specifies the width of the image, in pixels.

    ximage
        Specifies the image.

    x

    y
        Specify the x and y coordinates.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_CC_rr_ee_aa_tt_ee_II_mm_aa_gg_ee(3X11R5) function allocates the memory needed for an
    XXIImmaaggee structure for the specified display but does not allocate space for
    the image itself. Rather, it initializes the structure byte-order, bit-
    order, and bitmap-unit values from the display and returns a pointer to
    the XXIImmaaggee structure. The red, green, and blue mask values are defined for
    Z format images only and are derived from the VViissuuaall structure passed in.
    Other values also are passed in. The offset permits the rapid displaying
    of the image without requiring each scanline to be shifted into position.
    If you pass a zero value in bytes_per_line, Xlib assumes that the
    scanlines are contiguous in memory and calculates the value of
    bytes_per_line itself.

    Note that when the image is created using _XX_CC_rr_ee_aa_tt_ee_II_mm_aa_gg_ee(3X11R5),
    _XX_GG_ee_tt_II_mm_aa_gg_ee(3X11R5), or _XX_SS_uu_bb_II_mm_aa_gg_ee(3X11R5), the destroy procedure that the
    _XX_DD_ee_ss_tt_rr_oo_yy_II_mm_aa_gg_ee(3X11R5) function calls frees both the image structure and
    the data pointed to by the image structure.

    The basic functions used to get a pixel, set a pixel, create a subimage,
    and add a constant value to an image are defined in the image object. The
    functions in this section are really macro invocations of the functions in
    the image object and are defined in <<XX1111//XXuuttiill..hh>>.

    The _XX_GG_ee_tt_PP_ii_xx_ee_ll(3X11R5) function returns the specified pixel from the named
    image. The pixel value is returned in normalized format (that is, the
    least-significant byte of the long is the least-significant byte of the
    pixel). The image must contain the x and y coordinates.

    The _XX_PP_uu_tt_PP_ii_xx_ee_ll(3X11R5) function overwrites the pixel in the named image
    with the specified pixel value. The input pixel value must be in
    normalized format (that is, the least-significant byte of the long is the
    least-significant byte of the pixel). The image must contain the x and y
    coordinates.

    The _XX_SS_uu_bb_II_mm_aa_gg_ee(3X11R5) function creates a new image that is a subsection of
    an existing one. It allocates the memory necessary for the new XXIImmaaggee
    structure and returns a pointer to the new image. The data is copied from
    the source image, and the image must contain the rectangle defined by x,
    y, subimage_width, and subimage_height.

    The _XX_AA_dd_dd_PP_ii_xx_ee_ll(3X11R5) function adds a constant value to every pixel in an
    image. It is useful when you have a base pixel value from allocating color
    resources and need to manipulate the image to that form.

    The _XX_DD_ee_ss_tt_rr_oo_yy_II_mm_aa_gg_ee(3X11R5) function deallocates the memory associated with
    the XXIImmaaggee structure.

  SSEEEE AALLSSOO

    _X_P_u_t_I_m_a_g_e()

    Xlib

