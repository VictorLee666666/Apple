XDeleteContext(3X11R5)                           XDeleteContext(3X11R5)

  XXSSaavveeCCoonntteexxtt(())

  NNAAMMEE

    XSaveContext(), XFindContext(), XDeleteContext(), XUniqueContext() -
    associative look-up routines

  SSYYNNOOPPSSIISS

    int XSaveContext (Display *display, XID rid, XContext context,
                      XPointer data)
    int XFindContext (Display *display, XID rid, XContext context,
                      XPointer *data_return)
    int XDeleteContext (Display *display, XID rid, XContext context)
    XContext XUniqueContext (void)

  AARRGGUUMMEENNTTSS

    context
        Specifies the context type to which the data belongs.

    data
        Specifies the data to be associated with the window and type.

    data_return
        Returns the data.

    display
        Specifies the connection to the X server.

    rid
        Specifies the resource ID with which the data is associated.

  DDEESSCCRRIIPPTTIIOONN

    If an entry with the specified resource ID and type already exists,
    _XX_SS_aa_vv_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5) overrides it with the specified context. The
    _XX_SS_aa_vv_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5) function returns a nonzero error code if an error has
    occurred and zero otherwise. Possible errors are XCNOMEM (out of memory).

    Because it is a return value, the data is a pointer. The
    _XX_FF_ii_nn_dd_CC_oo_nn_tt_ee_xx_tt(3X11R5) function returns a nonzero error code if an error has
    occurred and zero otherwise. Possible errors are XCNOENT (context-not-
    found).

    The _XX_DD_ee_ll_ee_tt_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5) function deletes the entry for the given
    resource ID and type from the data structure. This function returns the
    same error codes that _XX_FF_ii_nn_dd_CC_oo_nn_tt_ee_xx_tt(3X11R5) returns if called with the same
    arguments. _XX_DD_ee_ll_ee_tt_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5) does not free the data whose address was
    saved.

    The _XX_UU_nn_ii_qq_uu_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5) function creates a unique context type that can
    be used in subsequent calls to _XX_SS_aa_vv_ee_CC_oo_nn_tt_ee_xx_tt(3X11R5).

  SSEEEE AALLSSOO

    Xlib

