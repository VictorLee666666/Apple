XtParseAcceleratorTable(3X11R5)          XtParseAcceleratorTable(3X11R5)

  XXttPPaarrsseeAAcccceelleerraattoorrTTaabbllee(())

  NNAAMMEE

    XtParseAcceleratorTable(), XtInstallAccelerators(),
    XtInstallAllAccelerators() - managing accelerator tables

  SSYYNNOOPPSSIISS

    XtAccelerators XtParseAcceleratorTable (String source)
    void XtInstallAccelerators (Widget destination, Widget source)
    void XtInstallAllAccelerators (Widget destination, Widget source)

  AARRGGUUMMEENNTTSS

    source
        Specifies the accelerator table to compile.

    destination
        Specifies the widget on which the accelerators are to be installed.

    source
        Specifies the widget or the root widget of the widget tree from which
        the accelerators are to come.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_PP_aa_rr_ss_ee_AA_cc_cc_ee_ll_ee_rr_aa_tt_oo_rr_TT_aa_bb_ll_ee(3X11R5) function compiles the accelerator
    table into the opaque internal representation.

    The _XX_tt_II_nn_ss_tt_aa_ll_ll_AA_cc_cc_ee_ll_ee_rr_aa_tt_oo_rr_ss(3X11R5) function installs the accelerators from
    source onto destination by augmenting the destination translations with
    the source accelerators. If the source display_accelerator method is non-
    NULL, _XX_tt_II_nn_ss_tt_aa_ll_ll_AA_cc_cc_ee_ll_ee_rr_aa_tt_oo_rr_ss(3X11R5) calls it with the source widget and a
    string representation of the accelerator table, which indicates that its
    accelerators have been installed and that it should display them
    appropriately. The string representation of the accelerator table is its
    canonical translation table representation.

    The _XX_tt_II_nn_ss_tt_aa_ll_ll_AA_ll_ll_AA_cc_cc_ee_ll_ee_rr_aa_tt_oo_rr_ss(3X11R5) function recursively descends the
    widget tree rooted at source and installs the accelerators of each widget
    encountered onto destination. A common use os to call
    _XX_tt_II_nn_ss_tt_aa_ll_ll_AA_ll_ll_AA_cc_cc_ee_ll_ee_rr_aa_tt_oo_rr_ss(3X11R5) and pass the application main window as
    the source.

  SSEEEE AALLSSOO

    _X_t_P_a_r_s_e_T_r_a_n_s_l_a_t_i_o_n_T_a_b_l_e

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

