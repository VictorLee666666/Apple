XUndefineCursor(3X11R5)                         XUndefineCursor(3X11R5)

  XXDDeeffiinneeCCuurrssoorr(())

  NNAAMMEE

    XDefineCursor(), XUndefineCursor() - define cursors

  SSYYNNOOPPSSIISS

    XDefineCursor (Display *display, Window w, Cursor cursor)
    XUndefineCursor (Display *display, Window w)

  AARRGGUUMMEENNTTSS

    cursor
        Specifies the cursor that is to be displayed or None.

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    If a cursor is set, it will be used when the pointer is in the window. If
    the cursor is None, it is equivalent to XUndefineCursor.

    _XX_DD_ee_ff_ii_nn_ee_CC_uu_rr_ss_oo_rr(3X11R5) can generate BadCursor and BadWindow errors.

    The _XX_UU_nn_dd_ee_ff_ii_nn_ee_CC_uu_rr_ss_oo_rr(3X11R5) function undoes the effect of a previous
    _XX_DD_ee_ff_ii_nn_ee_CC_uu_rr_ss_oo_rr(3X11R5) for this window. When the pointer is in the window,
    the parent's cursor will now be used. On the root window, the default
    cursor is restored.

    _XX_UU_nn_dd_ee_ff_ii_nn_ee_CC_uu_rr_ss_oo_rr(3X11R5) can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadCursor
        A value for a Cursor argument does not name a defined Cursor.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_F_o_n_t_C_u_r_s_o_r()

    _X_R_e_c_o_l_o_r_C_u_r_s_o_r()

    Xlib

