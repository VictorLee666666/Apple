XScreenResourceString(3X11R5)             XScreenResourceString(3X11R5)

  XXRReessoouurrcceeMMaannaaggeerrSSttrriinngg(())

  NNAAMMEE

    XResourceManagerString(), XScreenResourceString() - obtain server resource
    properties

  SSYYNNOOPPSSIISS

    char *XResourceManagerString (Display *display)
    char *XScreenResourceString (Screen *screen)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    screen
        Specifies the screen.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_RR_ee_ss_oo_uu_rr_cc_ee_MM_aa_nn_aa_gg_ee_rr_SS_tt_rr_ii_nn_gg(3X11R5) returns the RESOURCE_MANAGER property
    from the server's root window of screen zero, which was returned when the
    connection was opened using _XX_OO_pp_ee_nn_DD_ii_ss_pp_ll_aa_yy(3X11R5). The property is
    converted from type STRING to the current locale. The conversion is
    identical to that produced by _XX_mm_bb_TT_ee_xx_tt_PP_rr_oo_pp_ee_rr_tt_yy_TT_oo_TT_ee_xx_tt_LL_ii_ss_tt(3X11R5) for a
    singleton STRING property. The returned string is owned by Xlib, and
    should not be freed by the client. Note that the property value must be in
    a format that is acceptable to _XX_rr_mm_GG_ee_tt_SS_tt_rr_ii_nn_gg_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5). If no
    property exists, NULL is returned.

    The _XX_SS_cc_rr_ee_ee_nn_RR_ee_ss_oo_uu_rr_cc_ee_SS_tt_rr_ii_nn_gg(3X11R5) returns the SCREEN_RESOURCES property
    from the root window of the specified screen. The property is converted
    from type STRING to the current locale. The conversion is identical to
    that produced by _XX_mm_bb_TT_ee_xx_tt_PP_rr_oo_pp_ee_rr_tt_yy_TT_oo_TT_ee_xx_tt_LL_ii_ss_tt(3X11R5) for a singleton STRING
    property. Note that the property value must be in a format that is
    acceptable to _XX_rr_mm_GG_ee_tt_SS_tt_rr_ii_nn_gg_DD_aa_tt_aa_bb_aa_ss_ee(3X11R5). If no property exists, NULL is
    returned. The caller is responsible for freeing the returned string, using
    _XX_FF_rr_ee_ee(3X11R5).

  SSEEEE AALLSSOO

    Xlib

