XGetWMNormalHints(3X11R5)                     XGetWMNormalHints(3X11R5)

  XXAAllllooccSSiizzeeHHiinnttss(())

  NNAAMMEE

    XAllocSizeHints(), XSetWMNormalHints(), XGetWMNormalHints(),
    XSetWMSizeHints(), XGetWMSizeHints(), XSizeHints() - allocate size hints
    structure and set or read a window's WM_NORMAL_HINTS property

  SSYYNNOOPPSSIISS

    XSizeHints *XAllocSizeHints (void)
    void XSetWMNormalHints (Display *display, Window w,
                            XSizeHints *nhints)
    Status XGetWMNormalHints (Display *display, Window w,
                              XSizeHints *hints_return,
                              long *supplied_return)
    void XSetWMSizeHints (Display *display, Window w,
                          XSizeHints *hints, Atom property)
    Status XGetWMSizeHints (Display *display, Window w,
                            XSizeHints *hints_return,
                            long *supplied_return,
                            Atom property)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    nhints
        Specifies the size hints for the window in its normal state.

    hints
        Specifies the XXSSiizzeeHHiinnttss structure to be used for setting the
        XA_WM_SIZE_HINTS property.

    hints_return
        Returns the size hints for the window in its normal state.

    property
        Specifies the property name.

    supplied_return
        Returns the hints that were supplied by the user.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_ll_ll_oo_cc_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) function allocates and returns a pointer to a
    XXSSiizzeeHHiinnttss structure. Note that all fields in the XXSSiizzeeHHiinnttss structure are
    initially set to zero. If insufficient memory is available,
    _XX_AA_ll_ll_oo_cc_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) returns NULL. To free the memory allocated to this
    structure, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) function replaces the size nhints for the
    WM_NORMAL_HINTS property on the specified window. If the property does not
    already exist, _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) sets the size nhints for the
    WM_NORMAL_HINTS property on the specified window. The property is stored
    with a type of WM_SIZE_HINTS and a format of 32.

    _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) function returns the size hints stored in
    the WM_NORMAL_HINTS property on the specified window. If the property is
    of type WM_SIZE_HINTS, is of format 32, and is long enough to contain
    either an old (pre-ICCCM) or new size hints structure,
    _XX_GG_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) sets the various fields of the XXSSiizzeeHHiinnttss
    structure, sets the supplied_return argument to the list of fields that
    were supplied by the user (whether or not they contained defined values),
    and returns a nonzero status. Otherwise, it returns a zero status.

    If _XX_GG_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) returns successfully and a pre-ICCCM size
    hints property is read, the supplied_return argument will contain the
    following bits:

    (USPosition|USSize|PPosition|PSize|PMinSize|
     PMaxSize|PResizeInc|PAspect)

    If the property is large enough to contain the base size and window
    gravity fields as well, the supplied_return argument will also contain the
    following bits:

    PBaseSize|PWinGravity

    _XX_GG_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) can generate a BadWindow error.

    The _XX_SS_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) function replaces the size hints for the
    specified property on the named window. If the specified property does not
    already exist, _XX_SS_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) sets the size hints for the
    specified property on the named window. The property is stored with a type
    of WM_SIZE_HINTS and a format of 32. To set a window's normal size hints,
    you can use the _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) function.

    _XX_SS_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) can generate BadAlloc, BadAtom, and BadWindow
    errors.

    The _XX_GG_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) function returns the size hints stored in the
    specified property on the named window. If the property is of type
    WM_SIZE_HINTS, is of format 32, and is long enough to contain either an
    old (pre-ICCCM) or new size hints structure, _XX_GG_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) sets
    the various fields of the XXSSiizzeeHHiinnttss structure, sets the supplied_return
    argument to the list of fields that were supplied by the user (whether or
    not they contained defined values), and returns a nonzero status.
    Otherwise, it returns a zero status. To get a window's normal size hints,
    you can use the _XX_GG_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5) function.

    If _XX_GG_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) returns successfully and a pre-ICCCM size hints
    property is read, the supplied_return argument will contain the following
    bits:

    (USPosition|USSize|PPosition|PSize|PMinSize|
     PMaxSize|PResizeInc|PAspect)

    If the property is large enough to contain the base size and window
    gravity fields as well, the supplied_return argument will also contain the
    following bits:

    PBaseSize|PWinGravity

    _XX_GG_ee_tt_WW_MM_SS_ii_zz_ee_HH_ii_nn_tt_ss(3X11R5) can generate BadAtom and BadWindow errors.

  PPRROOPPEERRTTIIEESS

    WM_NORMAL_HINTS
        Size hints for a window in its normal state. The C type of this
        property is XXSSiizzeeHHiinnttss.

  SSTTRRUUCCTTUURREESS

    The XXSSiizzeeHHiinnttss structure contains:

    Size hints mask bits

    Values

    typedef struct {
         long flags;         marks which fields in this structure are defined
         int x, y;           Obsolete
         int width, height;  Obsolete
         int min_width, min_height;
         int max_width, max_height;
         int width_inc, height_inc;
         struct {
                int x;       numerator
                int y;       denominator
         } min_aspect, max_aspect;
         int base_width, base_height;
         int win_gravity;
    } XSizeHints;

    The x, y, width, and height members are now obsolete and are left solely
    for compatibility reasons. The min_width and min_height members specify
    the minimum window size that still allows the application to be useful.
    The max_width and max_height members specify the maximum window size. The
    width_inc and height_inc members define an arithmetic progression of sizes
    (minimum to maximum) into which the window prefers to be resized. The
    min_aspect and max_aspect members are expressed as ratios of x and y, and
    they allow an application to specify the range of aspect ratios it
    prefers. The base_width and base_height members define the desired size of
    the window. The window manager will interpret the position of the window
    and its border width to position the point of the outer rectangle of the
    overall window specified by the win_gravity member. The outer rectangle of
    the window includes any borders or decorations supplied by the window
    manager. In other words, if the window manager decides to place the window
    where the client asked, the position on the parent window's border named
    by the win_gravity will be placed where the client window would have been
    placed in the absence of a window manager.

    Note that use of the PPAAllllHHiinnttss() macro is highly discouraged.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadAtom
        A value for an Atom argument does not name a defined Atom.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_F_r_e_e()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

