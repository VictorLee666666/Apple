XWMHints(3X11R5)                                       XWMHints(3X11R5)

  XXAAllllooccWWMMHHiinnttss(())

  NNAAMMEE

    XAllocWMHints(), XSetWMHints(), XGetWMHints(), XWMHints() - allocate
    window manager hints structure and set or read a window's WM_HINTS
    property

  SSYYNNOOPPSSIISS

    XWMHints *XAllocWMHints (void)
    XSetWMHints (Display *display, Window w, XWMHints *wmhints)
    XWMHints *XGetWMHints (Display *display, Window w)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    w
        Specifies the window.

    wmhints
        Specifies the XXWWMMHHiinnttss structure to be used.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_ll_ll_oo_cc_WW_MM_HH_ii_nn_tt_ss(3X11R5) function allocates and returns a pointer to a
    XXWWMMHHiinnttss structure. Note that all fields in the XXWWMMHHiinnttss structure are
    initially set to zero. If insufficient memory is available,
    _XX_AA_ll_ll_oo_cc_WW_MM_HH_ii_nn_tt_ss(3X11R5) returns NULL. To free the memory allocated to this
    structure, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_SS_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5) function sets the window manager hints that
    include icon information and location, the initial state of the window,
    and whether the application relies on the window manager to get keyboard
    input.

    _XX_SS_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5) function reads the window manager hints and
    returns NULL if no WM_HINTS property was set on the window or returns a
    pointer to a XXWWMMHHiinnttss structure if it succeeds. When finished with the
    data, free the space used for it by calling XXFFrreeee.

    _XX_GG_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5) can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_HINTS
        Additional hints set by the client for use by the window manager. The
        C type of this property is XXWWMMHHiinnttss.

  SSTTRRUUCCTTUURREESS

    The XXWWMMHHiinnttss structure contains:

    Window manager hints mask bits

    Values

    typedef struct {
         long flags;         marks which fields in this structure are defined
         Bool input;         does this application rely on the window
                             manager to get keyboard input?
         int initial_state;  see below
         Pixmap icon_pixmap; pixmap to be used as icon
         Window icon_window; window to be used as icon
         int icon_x, icon_y; initial position of icon
         Pixmap icon_mask;   pixmap to be used as mask for icon_pixmap
         XID window_group;   id of related window group
         this structure can be extended in the future
    } XWMHints;

    The input member is used to communicate to the window manager the input
    focus model used by the application.
    *     Applications that expect input but never explicitly set focus to any
          of their subwindows (that is, use the push model of focus
          management), such as X Version 10 style applications that use real-
          estate driven focus, should set this member to True.
    *     Similarly, applications that set input focus to their subwindows
          only when it is given to their top-level window by a window manager
          should also set this member to True.
    *     Applications that manage their own input focus by explicitly setting
          focus to one of their subwindows whenever they want keyboard input
          (that is, use the pull model of focus management) should set this
          member to False.
    *     Applications that never expect any keyboard input also should set
          this member to False.

    Pull model window managers should make it possible for push model
    applications to get input by setting input focus to the top-level windows
    of applications whose input member is True. Push model window managers
    should make sure that pull model applications do not break them by
    resetting input focus to PointerRoot when it is appropriate (for example,
    whenever an application whose input member is False sets input focus to
    one of its subwindows).

    The definitions for the initial_state flag are:
    The icon_mask specifies which pixels of the icon_pixmap should be used as
    the icon. This allows for nonrectangular icons. Both icon_pixmap and
    icon_mask must be bitmaps. The icon_window lets an application provide a
    window for use as an icon for window managers that support such use. The
    window_group lets you specify that this window belongs to a group of other
    windows. For example, if a single application manipulates multiple top-
    level windows, this allows you to provide enough information that a window
    manager can iconify all of the windows rather than just the one window.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_F_r_e_e()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

