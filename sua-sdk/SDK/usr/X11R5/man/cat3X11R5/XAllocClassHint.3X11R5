XAllocClassHint(3X11R5)                         XAllocClassHint(3X11R5)

  XXAAllllooccCCllaassssHHiinntt(())

  NNAAMMEE

    XAllocClassHint(), XSetClassHint(), XGetClassHint(), XClassHint() -
    allocate class hints structure and set or read a window's WM_CLASS
    property

  SSYYNNOOPPSSIISS

    XClassHint *XAllocClassHint (void)
    XSetClassHint (Display *display, Window w, XClassHint *class_hints)
    Status XGetClassHint (Display *display, Window w,
                          XClassHint *class_hints_return)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    class_hints
        Specifies the XXCCllaassssHHiinntt structure that is to be used.

    class_hints_return
        Returns the XXCCllaassssHHiinntt structure.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_AA_ll_ll_oo_cc_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) function allocates and returns a pointer to a
    XXCCllaassssHHiinntt structure. Note that the pointer fields in the XXCCllaassssHHiinntt
    structure are initially set to NULL. If insufficient memory is available,
    _XX_AA_ll_ll_oo_cc_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) returns NULL. To free the memory allocated to this
    structure, use _XX_FF_rr_ee_ee(3X11R5).

    The _XX_SS_ee_tt_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) function sets the class hint for the specified
    window. If the strings are not in the Host Portable Character Encoding the
    result is implementation dependent.

    _XX_SS_ee_tt_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) can generate BadAlloc and BadWindow errors.

    The _XX_GG_ee_tt_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) function returns the class hint of the specified
    window to the members of the supplied structure. If the data returned by
    the server is in the Latin Portable Character Encoding, then the returned
    strings are in the Host Portable Character Encoding. Otherwise, the result
    is implementation dependent. It returns nonzero status on success;
    otherwise it returns a zero status. To free res_name and res_class when
    finished with the strings, use _XX_FF_rr_ee_ee(3X11R5) on each individually.

    _XX_GG_ee_tt_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5) can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_CLASS

        Set by application programs to allow window and session managers to
        obtain the application's resources from the resource database.

  SSTTRRUUCCTTUURREESS

    The XXCCllaassssHHiinntt structure contains:

    typedef struct {
         char *res_name;
         char *res_class;
    } XClassHint;

    The res_name member contains the application name, and the res_class
    member contains the application class. Note that the name set in this
    property can differ from the name set as WM_NAME. That is, WM_NAME
    specifies what should be displayed in the title bar and, therefore, can
    contain temporal information (for example, the name of a file currently in
    an editor's buffer). On the other hand, the name specified as part of
    WM_CLASS is the formal name of the application that should be used when
    retrieving the application's resources from the resource database.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc

        The server failed to allocate the requested resource or server memory.

    BadWindow

        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_F_r_e_e()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

