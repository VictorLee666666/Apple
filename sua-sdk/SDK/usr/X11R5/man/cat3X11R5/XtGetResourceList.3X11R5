XtGetResourceList(3X11R5)                     XtGetResourceList(3X11R5)

  XXttGGeettRReessoouurrcceeLLiisstt(())

  NNAAMMEE

    XtGetResourceList() - obtain resource list

  SSYYNNOOPPSSIISS

    void XtGetResourceList (WidgetClass class,
                            XtResourceList *resources_return,
                            Cardinal *num_resources_return)

  AARRGGUUMMEENNTTSS

    num_resources_return
        Specifies a pointer to where to store the number of entries in the
        resource list.

    resources_return
        Specifies a pointer to where to store the returned resource list. The
        caller must free this storage using _XX_tt_FF_rr_ee_ee(3X11R5) when done with it.

    widget_class
        Specifies the widget class.

  DDEESSCCRRIIPPTTIIOONN

    If it is called before the widget class is initialized (that is, before
    the first widget of that class has been created),
    _XX_tt_GG_ee_tt_RR_ee_ss_oo_uu_rr_cc_ee_LL_ii_ss_tt(3X11R5) returns the resource list as specified in the
    widget class record. If it is called after the widget class has been
    initialized, _XX_tt_GG_ee_tt_RR_ee_ss_oo_uu_rr_cc_ee_LL_ii_ss_tt(3X11R5) returns a merged resource list that
    contains the resources for all superclasses.

  SSEEEE AALLSSOO

    _X_t_G_e_t_S_u_b_r_e_s_o_u_r_c_e_s()

    _X_t_O_f_f_s_e_t()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

