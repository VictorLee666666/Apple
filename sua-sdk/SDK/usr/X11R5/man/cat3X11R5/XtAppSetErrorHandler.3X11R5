XtAppSetErrorHandler(3X11R5)               XtAppSetErrorHandler(3X11R5)

  XXttAAppppEErrrroorr(())

  NNAAMMEE

    XtAppError(), XtAppSetErrorHandler(), XtAppSetWarningHandler(),
    XtAppWarning() - low-level error handlers

  SSYYNNOOPPSSIISS

    void XtAppError (XtAppContext app_context, String message)
    void XtAppSetErrorHandler (XtAppContext app_context,
                               XtErrorHandler handler)
    void XtAppSetWarningHandler (XtAppContext app_context,
                                 XtErrorHandler handler)
    void XtAppWarning (XtAppContext app_context, String message)

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    message
        Specifies the nonfatal error message that is to be reported.

    handler
        Specifies the new fatal error procedure, which should not return, or
        the nonfatal error procedure, which usually returns.

    message
        Specifies the message that is to be reported.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_AA_pp_pp_EE_rr_rr_oo_rr(3X11R5) function calls the installed error procedure and
    passes the specified message.

    The _XX_tt_AA_pp_pp_SS_ee_tt_EE_rr_rr_oo_rr_HH_aa_nn_dd_ll_ee_rr(3X11R5) function registers the specified
    procedure, which is called when a fatal error condition occurs.

    The _XX_tt_AA_pp_pp_SS_ee_tt_WW_aa_rr_nn_ii_nn_gg_HH_aa_nn_dd_ll_ee_rr(3X11R5) registers the specified procedure,
    which is called when a nonfatal error condition occurs.

    The _XX_tt_AA_pp_pp_WW_aa_rr_nn_ii_nn_gg(3X11R5) function calls the installed nonfatal error
    procedure and passes the specified message.

  SSEEEE AALLSSOO

    _X_t_A_p_p_G_e_t_E_r_r_o_r_D_a_t_a_b_a_s_e()

    _X_t_A_p_p_E_r_r_o_r_M_s_g()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

