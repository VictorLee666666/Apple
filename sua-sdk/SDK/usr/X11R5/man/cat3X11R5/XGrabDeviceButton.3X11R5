XGrabDeviceButton(3X11R5)                     XGrabDeviceButton(3X11R5)

  XXGGrraabbDDeevviicceeBBuuttttoonn(())

  NNAAMMEE

    XGrabDeviceButton(), XUngrabDeviceButton() - grab/ungrab extension input
    device buttons

  SSYYNNOOPPSSIISS

    XGrabDeviceButton (Display *display, XDevice *device,
                       unsigned int button, unsigned int modifiers,
                       XDevice *modifier_device, Window grab_window,
                       Bool owner_events, unsigned int event_count,
                       XEventClass *event_list, int this_device_mode,
                       int other_devices_mode)
    XUngrabDeviceButton (Display *display, XDevice *device,
                         unsigned int button, unsigned int modifiers,
                         XDevice *modifier_device, Window grab_window)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device that is to be grabbed or released

    button
        Specifies the device button that is to be grabbed or released or
        AnyButton.

    modifiers
        Specifies the set of keymasks or AnyModifier. The mask is the bitwise
        inclusive OR of the valid keymask bits. Valid bits are: Shiftmask,
        LockMask, ControlMask, Mod1Mask, Mod2Mask, Mod3Mask, Mod4Mask,
        Mod5Mask.

    modifier_device
        specifies the device whose modifiers are to be used. If the
        modifier_device specified is NULL, the X keyboard will be used as the
        modifier_device.

    grab_window
        Specifies the grab window.

    owner_events
        Specifies a Boolean value that indicates whether the device events are
        to be reported as usual or reported with respect to the grab window if
        selected by the event list.

    event_count
        Specifies the number of event classes in the event list.

    event_list
        Specifies which events are reported to the client.

    this_device_mode
        Specifies further processing of events from this device. You can pass
        GrabModeSync or GrabModeAsync.

    other_devices_mode
        Specifies further processing of events from all other devices. You can
        pass GrabModeSync or GrabModeAsync.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_GG_rr_aa_bb_DD_ee_vv_ii_cc_ee_BB_uu_tt_tt_oo_nn(3X11R5) request establishes a passive grab. In the
    future, the device is actively grabbed (as for _XX_GG_rr_aa_bb_DD_ee_vv_ii_cc_ee(3X11R5), the
    last-grab time is set to the time at which the button was pressed (as
    transmitted in the DeviceButtonPress event), and the DeviceButtonPress
    event is reported if all of the following conditions are true:
    *     The device is not grabbed, and the specified button is logically
          pressed when the specified modifier keys are logically down on the
          specified modifier device and no other buttons or modifier keys are
          logically down.
    *     Either the grab window is an ancestor of (or is) the focus window,
          OR the grab window is a descendent of the focus window and contains
          the device.
    *     A passive grab on the same button/modifier combination does not
          exist on any ancestor of grab_window.

    The interpretation of the remaining arguments is as for
    _XX_GG_rr_aa_bb_DD_ee_vv_ii_cc_ee(3X11R5). The active grab is terminated automatically when the
    logical state of the device has all buttons released (independent of the
    logical state of the modifier keys).

    Note that the logical state of a device (as seen by client applications)
    might lag the physical state if device event processing is frozen.

    This request overrides all previous grabs by the same client on the same
    button/modifier combinations on the same window. A modifiers of
    AnyModifier is equivalent to issuing the grab request for all possible
    modifier combinations (including the combination of no modifiers). It is
    not required that all modifiers specified have currently assigned
    KeyCodes. A button of AnyButton is equivalent to issuing the request for
    all possible buttons. Otherwise, it is not required that the specified
    button currently be assigned to a physical button.

    A modifier_device of NULL indicates that the X keyboard is to be used as
    the modifier_device.

    If some other client has already issued a _XX_GG_rr_aa_bb_DD_ee_vv_ii_cc_ee_BB_uu_tt_tt_oo_nn(3X11R5) with
    the same button/modifier combination on the same window, a BadAccess error
    results. When using AnyModifier or AnyButton , the request fails
    completely, and a BadAccess error results (no grabs are established) if
    there is a conflicting grab for any combination. XGrabDeviceButton has no
    effect on an active grab.

    XGrabDeviceButton can generate BadClass, BadDevice, BadMatch, BadValue,
    and BadWindow errors.

    The XUngrabDeviceButton request releases the passive grab for a button/
    modifier combination on the specified window if it was grabbed by this
    client. A modifier of AnyModifier is equivalent to issuing the ungrab
    request for all possible modifier combinations, including the combination
    of no modifiers. A button of AnyButton is equivalent to issuing the
    request for all possible buttons. _XX_UU_nn_gg_rr_aa_bb_DD_ee_vv_ii_cc_ee_BB_uu_tt_tt_oo_nn(3X11R5) has no
    effect on an active grab.

    A modifier_device of NULL indicates that the X keyboard should be used as
    the modifier_device.

    _XX_UU_nn_gg_rr_aa_bb_DD_ee_vv_ii_cc_ee_BB_uu_tt_tt_oo_nn(3X11R5) can generate BadDevice, BadMatch, BadValue and
    BadWindow errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client with XXOOppeennIInnppuuttDDeevviiccee(). This
        error might also occur if the specified device is the X keyboard or X
        pointer device.

    BadMatch
        This error might occur if an _XX_GG_rr_aa_bb_DD_ee_vv_ii_cc_ee_BB_uu_tt_tt_oo_nn(3X11R5) request was
        made specifying a device that has no buttons, or specifying a modifier
        device that has no keys.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_w_D_e_v_i_c_e_E_v_e_n_t_s()

    _X_G_r_a_b_D_e_v_i_c_e()

    _X_G_r_a_b_D_e_v_i_c_e_K_e_y()

    Programming With Xlib

