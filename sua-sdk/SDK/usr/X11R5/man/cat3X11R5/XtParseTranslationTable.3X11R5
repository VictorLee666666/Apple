XtParseTranslationTable(3X11R5)          XtParseTranslationTable(3X11R5)

  XXttPPaarrsseeTTrraannssllaattiioonnTTaabbllee(())

  NNAAMMEE

    XtParseTranslationTable(), XtAugmentTranslations(),
    XtOverrideTranslations(), XtUninstallTranslations() - manage translation
    tables

  SSYYNNOOPPSSIISS

    XtTranslations XtParseTranslationTable (String table)
    void XtAugmentTranslations (Widget w, XtTranslations translations)
    void XtOverrideTranslations (Widget w, XtTranslations translations)
    void XtUninstallTranslations (Widget w)

  AARRGGUUMMEENNTTSS

    table
        Specifies the translation table to compile.

    translations
        Specifies the compiled translation table to merge in (must not be
        NULL).

    w
        Specifies the widget into which the new translations are to be merged
        or removed.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_PP_aa_rr_ss_ee_TT_rr_aa_nn_ss_ll_aa_tt_ii_oo_nn_TT_aa_bb_ll_ee(3X11R5) function compiles the translation
    table into the opaque internal representation of type XXttTTrraannssllaattiioonnss. Note
    that if an empty translation table is required for any purpose, one can be
    obtained by calling _XX_tt_PP_aa_rr_ss_ee_TT_rr_aa_nn_ss_ll_aa_tt_ii_oo_nn_TT_aa_bb_ll_ee(3X11R5) and passing an empty
    string.

    The _XX_tt_AA_uu_gg_mm_ee_nn_tt_TT_rr_aa_nn_ss_ll_aa_tt_ii_oo_nn_ss(3X11R5) function nondestructively merges the new
    translations into the existing widget translations. If the new
    translations contain an event or event sequence that already exists in the
    widget's translations, the new translation is ignored.

    The _XX_tt_OO_vv_ee_rr_rr_ii_dd_ee_TT_rr_aa_nn_ss_ll_aa_tt_ii_oo_nn_ss(3X11R5) function destructively merges the new
    translations into the existing widget translations. If the new
    translations contain an event or event sequence that already exists in the
    widget's translations, the new translation is merged in and override the
    widget's translation.

    To replace a widget's translations completely, use _XX_tt_SS_ee_tt_VV_aa_ll_uu_ee_ss(3X11R5) on
    the XtNtranslations resource and specify a compiled translation table as
    the value.

    The _XX_tt_UU_nn_ii_nn_ss_tt_aa_ll_ll_TT_rr_aa_nn_ss_ll_aa_tt_ii_oo_nn_ss(3X11R5) function causes the entire translation
    table for widget to be removed.

  SSEEEE AALLSSOO

    _X_t_A_p_p_A_d_d_A_c_t_i_o_n_s()

    _X_t_C_r_e_a_t_e_P_o_p_u_p_S_h_e_l_l()

    _X_t_P_a_r_s_e_A_c_c_e_l_e_r_a_t_o_r_T_a_b_l_e()

    _X_t_P_o_p_u_p()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

