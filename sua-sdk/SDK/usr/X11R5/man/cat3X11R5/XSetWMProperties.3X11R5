XSetWMProperties(3X11R5)                       XSetWMProperties(3X11R5)

  XXSSeettWWMMPPrrooppeerrttiieess(())

  NNAAMMEE

    XSetWMProperties(), XmbSetWMProperties() - set standard window properties

  SSYYNNOOPPSSIISS

    void XSetWMProperties (Display *display, Window w,
                           XTextProperty *window_name,
                           XTextProperty *icon_name,
                           char **argv, int argc,
                           XSizeHints *normal_hints,
                           XWMHints *wm_hints,
                           XClassHint *class_hints)
    void XmbSetWMProperties (Display *display, Window w,
                             char *window_name, char *icon_name,
                             char *argv[], int argc,
                             XSizeHints *normal_hints,
                             XWMHints *wm_hints,
                             XClassHint *class_hints)

  AARRGGUUMMEENNTTSS

    argc
        Specifies the number of arguments.

    argv
        Specifies the application's argument list.

    class_hints
        Specifies the XXCCllaassssHHiinntt structure to be used.

    display
        Specifies the connection to the X server.

    icon_name
        Specifies the icon name, which should be a null-terminated string.

    normal_hints
        Specifies the size hints for the window in its normal state.

    w
        Specifies the window.

    window_name
        Specifies the window name, which should be a null-terminated string.

    wm_hints
        Specifies the XXWWMMHHiinnttss structure to be used.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) convenience function provides a single
    programming interface for setting those essential window properties that
    are used for communicating with other clients (particularly window and
    session managers).

    If the window_name argument is non-NULL, _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls
    _XX_SS_ee_tt_WW_MM_NN_aa_mm_ee(3X11R5), which in turn, sets the WM_NAME property (see section
    14.1.4). If the icon_name argument is non-NULL, _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5)
    calls _XX_SS_ee_tt_WW_MM_II_cc_oo_nn_NN_aa_mm_ee(3X11R5), which sets the WM_ICON_NAME property (see
    section 14.1.5). If the argv argument is non-NULL,
    _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls _XX_SS_ee_tt_CC_oo_mm_mm_aa_nn_dd(3X11R5), which sets the
    WM_COMMAND property (see section 14.2.1). Note that an argc of zero is
    allowed to indicate a zero-length command. Note also that the hostname of
    this computer is stored using _XX_SS_ee_tt_WW_MM_CC_ll_ii_ee_nn_tt_MM_aa_cc_hh_ii_nn_ee(3X11R5) (see section
    14.2.2).

    If the normal_hints argument is non-NULL, _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls
    _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5), which sets the WM_NORMAL_HINTS property (see
    section 14.1.7). If the wm_hints argument is non-NULL,
    _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls _XX_SS_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5), which sets the
    WM_HINTS property (see section 14.1.6).

    If the class_hints argument is non-NULL, _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls
    _XX_SS_ee_tt_CC_ll_aa_ss_ss_HH_ii_nn_tt(3X11R5), which sets the WM_CLASS property (see section
    14.1.8). If the res_name member in the XXCCllaassssHHiinntt structure is set to the
    NULL pointer and the RESOURCE_NAME environment variable is set, then the
    value of the environment variable is substituted for res_name. If the
    res_name member is NULL, the environment variable is not set, and argv and
    argv[0] are set, then the value of argv[0], stripped of any directory
    prefixes, is substituted for res_name.

    The _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) convenience function provides a simple
    programming interface for setting those essential window properties that
    are used for communicating with other clients (particularly window and
    session managers).

    If the window_name argument is non-NULL, _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) sets
    the WM_NAME property. If the icon_name argument is non-NULL,
    _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) sets the WM_ICON_NAME property. The window_name
    and icon_name arguments are null-terminated strings in the encoding of the
    current locale. If the arguments can be fully converted to the STRING
    encoding, the properties are created with type ``STRING'': otherwise, the
    arguments are converted to Compound Text, and the properties are created
    with type ``COMPOUND_TEXT''.

    If the normal_hints argument is non-NULL, _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls
    _XX_SS_ee_tt_WW_MM_NN_oo_rr_mm_aa_ll_HH_ii_nn_tt_ss(3X11R5), which sets the WM_NORMAL_HINTS property (see
    section 14.1.7). If the wm_hints argument is non-NULL,
    _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) calls _XX_SS_ee_tt_WW_MM_HH_ii_nn_tt_ss(3X11R5), which sets the
    WM_HINTS property (see section 14.1.6).

    If the argv argument is non-NULL, _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) sets the
    WM_COMMAND property from argv and argc. Note that an argc of 0 indicates a
    zero-length command.

    The hostname of this computer is stored using _XX_SS_ee_tt_WW_MM_CC_ll_ii_ee_nn_tt_MM_aa_cc_hh_ii_nn_ee(3X11R5)
    (see section 14.2.2).

    If the class_hints argument is non-NULL, _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) sets
    the WM_CLASS property. If the res_name member in the XXCCllaassssHHiinntt structure
    is set to the NULL pointer and the RESOURCE_NAME environment variable is
    set, the value of the environment variable is substituted for res_name. If
    the res_name member is NULL, the environment variable is not set, and argv
    and argv[0] are set, then the value of argv[0], stripped of any directory
    prefixes, is substituted for res_name.

    It is assumed that the supplied class_hints.res_name and argv, the
    RESOURCE_NAME environment variable, and the hostname of this computer are
    in the encoding of the locale announced for the LC_CTYPE category. (On
    POSIX-compliant systems, the LC_CTYPE, else LANG environment variable).
    The corresponding WM_CLASS, WM_COMMAND, and WM_CLIENT_MACHINE properties
    are typed according to the local host locale announcer. No encoding
    conversion is performed prior to storage in the properties.

    For clients that need to process the property text in a locale,
    _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) sets the WM_LOCALE_NAME property to be the name
    of the current locale. The name is assumed to be in the Host Portable
    Character Encoding, and is converted to STRING for storage in the
    property.

    _XX_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) and _XX_mm_bb_SS_ee_tt_WW_MM_PP_rr_oo_pp_ee_rr_tt_ii_ee_ss(3X11R5) can generate
    BadAlloc and BadWindow errors.

  PPRROOPPEERRTTIIEESS

    WM_CLASS
        Set by application programs to allow window and session managers to
        obtain the application's resources from the resource database.

    WM_CLIENT_MACHINE
        The string name of the computer on which the client application is
        running.

    WM_COMMAND
        The command and arguments, null-separated, used to invoke the
        application.

    WM_HINTS
        Additional hints set by the client for use by the window manager. The
        C type of this property is XXWWMMHHiinnttss.

    WM_ICON_NAME
        The name to be used in an icon.

    WM_NAME
        The name of the application.

    WM_NORMAL_HINTS
        Size hints for a window in its normal state. The C type of this
        property is XXSSiizzeeHHiinnttss.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t()

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e()

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s()

    _X_A_l_l_o_c_W_M_H_i_n_t_s()

    _X_P_a_r_s_e_G_e_o_m_e_t_r_y()

    _X_S_e_t_C_o_m_m_a_n_d()

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t()

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y()

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e()

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s()

    _X_S_e_t_W_M_I_c_o_n_N_a_m_e()

    _X_S_e_t_W_M_N_a_m_e()

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s()

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y()

    Xlib

