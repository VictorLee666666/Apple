XSetDashes(3X11R5)                                   XSetDashes(3X11R5)

  XXSSeettLLiinneeAAttttrriibbuutteess(())

  NNAAMMEE

    XSetLineAttributes(), XSetDashes() - GC convenience routines

  SSYYNNOOPPSSIISS

    XSetLineAttributes (Display *display, GC gc,
                        unsigned int line_width, int line_style,
                        int cap_style, int join_style)
    XSetDashes (Display *display, GC gc, int dash_offset,
                char dash_list[], int n)

  AARRGGUUMMEENNTTSS

    cap_style
        Specifies the line-style and cap-style you want to set for the
        specified GC. You can pass CapNotLast, CapButt, CapRound, or
        CapProjecting.

    dash_list
        Specifies the dash-list for the dashed line-style you want to set for
        the specified GC.

    dash_offset
        Specifies the phase of the pattern for the dashed line-style you want
        to set for the specified GC.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    join_style
        Specifies the line join-style you want to set for the specified GC.
        You can pass JoinMiter, JoinRound, or JoinBevel.

    line_style
        Specifies the line-style you want to set for the specified GC. You can
        pass LineSolid, LineOnOffDash, or LineDoubleDash.

    line_width
        Specifies the line-width you want to set for the specified GC.

    n
        Specifies the number of elements in dash_list.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_LL_ii_nn_ee_AA_tt_tt_rr_ii_bb_uu_tt_ee_ss(3X11R5) function sets the line drawing components
    in the specified GC.

    _XX_SS_ee_tt_LL_ii_nn_ee_AA_tt_tt_rr_ii_bb_uu_tt_ee_ss(3X11R5) can generate BadAlloc, BadGC, and BadValue
    errors.

    The _XX_SS_ee_tt_DD_aa_ss_hh_ee_ss(3X11R5) function sets the dash-offset and dash-list
    attributes for dashed line styles in the specified GC. There must be at
    least one element in the specified dash_list, or a BadValue error results.
    The initial and alternating elements (second, fourth, and so on) of the
    dash_list are the even dashes, and the others are the odd dashes. Each
    element specifies a dash length in pixels. All of the elements must be
    nonzero, or a BadValue error results. Specifying an odd-length list is
    equivalent to specifying the same list concatenated with itself to produce
    an even-length list.

    The dash-offset defines the phase of the pattern, specifying how many
    pixels into the dash-list the pattern should actually begin in any single
    graphics request. Dashing is continuous through path elements combined
    with a join-style but is reset to the dash-offset between each sequence of
    joined lines.

    The unit of measure for dashes is the same for the ordinary coordinate
    system. Ideally, a dash length is measured along the slope of the line,
    but implementations are only required to match this ideal for horizontal
    and vertical lines. Failing the ideal semantics, it is suggested that the
    length be measured along the major axis of the line. The major axis is
    defined as the x axis for lines drawn at an angle of between -45 and +45
    degrees or between 135 and 225 degrees from the x axis. For all other
    lines, the major axis is the y axis.

    _XX_SS_ee_tt_DD_aa_ss_hh_ee_ss(3X11R5) can generate BadAlloc, BadGC, and BadValue errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C()

    _X_Q_u_e_r_y_B_e_s_t_S_i_z_e()

    _X_S_e_t_A_r_c_M_o_d_e()

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n()

    _X_S_e_t_F_i_l_l_S_t_y_l_e()

    _X_S_e_t_F_o_n_t()

    _X_S_e_t_S_t_a_t_e()

    _X_S_e_t_T_i_l_e()

    Xlib

