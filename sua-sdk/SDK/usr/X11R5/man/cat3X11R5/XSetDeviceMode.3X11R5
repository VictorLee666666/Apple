XSetDeviceMode(3X11R5)                           XSetDeviceMode(3X11R5)

  XXSSeettDDeevviicceeMMooddee(())

  NNAAMMEE

    XSetDeviceMode() - change the mode of a device

  SSYYNNOOPPSSIISS

    XSetDeviceMode (Display *display, XDevice *device, int _mode)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device whose mode is to be changed.

    mode
        Specifies the mode. You can pass Absolute, or Relative.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ee(3X11R5) request changes the mode of an input device
    that is capable of reporting either absolute positional information or
    relative motion information. Not all input devices are capable of
    reporting motion data, and not all are capable of changing modes from
    Absolute to Relative.

    _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ee(3X11R5) can generate a BadDevice or BadMode error.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client with XXOOppeennIInnppuuttDDeevviiccee(). This
        error might also occur if some other client has caused the specified
        device to become the X keyboard or X pointer device through the
        _XX_CC_hh_aa_nn_gg_ee_KK_ee_yy_bb_oo_aa_rr_dd_DD_ee_vv_ii_cc_ee(3X11R5) or _XX_CC_hh_aa_nn_gg_ee_PP_oo_ii_nn_tt_ee_rr_DD_ee_vv_ii_cc_ee(3X11R5)
        requests.

    BadMatch
        This error might occur if an _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ee(3X11R5) request is made
        specifying a device that has no valuators and reports no axes of
        motion.

    BadMode
        An invalid mode was specified. This error will also be returned if the
        specified device is not capable of supporting the
        _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ee(3X11R5) request.

  SSEEEE AALLSSOO

    Programming With Xlib

