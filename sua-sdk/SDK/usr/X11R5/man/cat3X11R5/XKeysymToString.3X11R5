XKeysymToString(3X11R5)                         XKeysymToString(3X11R5)

  XXSSttrriinnggTTooKKeeyyssyymm(())

  NNAAMMEE

    XStringToKeysym(), XKeysymToString(), XKeycodeToKeysym(),
    XKeysymToKeycode() - convert keysyms

  SSYYNNOOPPSSIISS

    KeySym XStringToKeysym (char *string)
    char *XKeysymToString (KeySym keysym)
    KeySym XKeycodeToKeysym (Display *display, KeyCode keycode,
                             int index)
    KeyCode XKeysymToKeycode (Display *display, KeySym keysym)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    index
        Specifies the element of KeyCode vector.

    keycode
        Specifies the KeyCode.

    keysym
        Specifies the KeySym that is to be searched for or converted.

    string
        Specifies the name of the KeySym that is to be converted.

  DDEESSCCRRIIPPTTIIOONN

    Standard KeySym names are obtained from <<XX1111//kkeeyyssyymmddeeff..hh>> by removing the
    XK_ prefix from each name. KeySyms that are not part of the Xlib standard
    can also be obtained with this function. Note that the set of KeySyms that
    are available in this manner and the mechanisms by which Xlib obtains them
    is implementation dependent.

    If the keysym name is not in the Host Portable Character Encoding the
    result is implementation dependent. If the specified string does not match
    a valid KeySym, _XX_SS_tt_rr_ii_nn_gg_TT_oo_KK_ee_yy_ss_yy_mm(3X11R5) returns NoSymbol.

    The returned string is in a static area and must not be modified. The
    returned string is in the Host Portable Character Encoding. If the
    specified KeySym is not defined, _XX_KK_ee_yy_ss_yy_mm_TT_oo_SS_tt_rr_ii_nn_gg(3X11R5) returns NULL.

    The _XX_KK_ee_yy_cc_oo_dd_ee_TT_oo_KK_ee_yy_ss_yy_mm(3X11R5) function uses internal Xlib tables and
    returns the KeySym defined for the specified KeyCode and the element of
    the KeyCode vector. If no symbol is defined, _XX_KK_ee_yy_cc_oo_dd_ee_TT_oo_KK_ee_yy_ss_yy_mm(3X11R5)
    returns NoSymbol.

    If the specified KeySym is not defined for any KeyCode,
    _XX_KK_ee_yy_ss_yy_mm_TT_oo_KK_ee_yy_cc_oo_dd_ee(3X11R5) returns zero.

  SSEEEE AALLSSOO

    _X_L_o_o_k_u_p_K_e_y_s_y_m()

    Xlib

