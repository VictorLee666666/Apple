XQueryBestCursor(3X11R5)                       XQueryBestCursor(3X11R5)

  XXRReeccoolloorrCCuurrssoorr(())

  NNAAMMEE

    XRecolorCursor(), XFreeCursor(), XQueryBestCursor() - manipulate cursors

  SSYYNNOOPPSSIISS

    XRecolorCursor (Display *display, Cursor cursor,
                    XColor *foreground_color,
                    XColor *background_color)
    XFreeCursor (Display *display, Cursor cursor)
    Status XQueryBestCursor (Display *display, Drawable d,
                             unsigned int width,
                             unsigned int height,
                             unsigned int *width_return,
                             unsigned int *height_return)

  AARRGGUUMMEENNTTSS

    background_color
        Specifies the RGB values for the background of the source.

    cursor
        Specifies the cursor.

    d
        Specifies the drawable, which indicates the screen.

    display
        Specifies the connection to the X server.

    foreground_color
        Specifies the RGB values for the foreground of the source.

    width

    height
        Specify the width and heightof the cursor that you want the size
        information for.

    width_return

    height_return
        Return the best width and height that is closest to the specified
        width and height.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_RR_ee_cc_oo_ll_oo_rr_CC_uu_rr_ss_oo_rr(3X11R5) function changes the color of the specified
    cursor, and if the cursor is being displayed on a screen, the change is
    visible immediately. Note that the pixel members of the XXCCoolloorr structures
    are ignored, only the RGB values are used.

    _XX_RR_ee_cc_oo_ll_oo_rr_CC_uu_rr_ss_oo_rr(3X11R5) can generate a BadCursor error.

    The _XX_FF_rr_ee_ee_CC_uu_rr_ss_oo_rr(3X11R5) function deletes the association between the
    cursor resource ID and the specified cursor. The cursor storage is freed
    when no other resource references it. The specified cursor ID should not
    be referred to again.

    _XX_FF_rr_ee_ee_CC_uu_rr_ss_oo_rr(3X11R5) can generate a BadCursor error.

    Some displays allow larger cursors than other displays. The
    _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_CC_uu_rr_ss_oo_rr(3X11R5) function provides a way to find out what size
    cursors are actually possible on the display. It returns the largest size
    that can be displayed. Applications should be prepared to use smaller
    cursors on displays that cannot support large ones.

    _XX_QQ_uu_ee_rr_yy_BB_ee_ss_tt_CC_uu_rr_ss_oo_rr(3X11R5) can generate a BadDrawable error.

  DDIIAAGGNNOOSSTTIICCSS

    BadCursor
        A value for a Cursor argument does not name a defined Cursor.

    BadDrawable
        A value for a Drawable argument does not name a defined Window or
        Pixmap.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_C_o_l_o_r_m_a_p()

    _X_C_r_e_a_t_e_F_o_n_t_C_u_r_s_o_r()

    _X_D_e_f_i_n_e_C_u_r_s_o_r()

    Xlib

