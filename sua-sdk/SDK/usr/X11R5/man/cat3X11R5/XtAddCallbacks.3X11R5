XtAddCallbacks(3X11R5)                           XtAddCallbacks(3X11R5)

  XXttAAddddCCaallllbbaacckk(())

  NNAAMMEE

    XtAddCallback(), XtAddCallbacks(), XtRemoveCallback(),
    XtRemoveCallbacks(), XtRemoveAllCallbacks() - add and remove callback
    procedures

  SSYYNNOOPPSSIISS

    void XtAddCallback (Widget w, String callback_name,
                        XtCallbackProc callback,
                        XtPointer client_data)
    void XtAddCallbacks (Widget w, String callback_name,
                         XtCallbackList callbacks)
    void XtRemoveCallback (Widget w, String callback_name,
                           XtCallbackProc callback,
                           XtPointer client_data)
    void XtRemoveCallbacks (Widget w, String callback_name,
                            XtCallbackList callbacks)
    void XtRemoveAllCallbacks (Widget w, String callback_name)

  AARRGGUUMMEENNTTSS

    callback
        Specifies the callback procedure.

    callbacks
        Specifies the null-terminated list of callback procedures and
        corresponding client data.

    callback_name
        Specifies the callback list to which the procedure is to be appended
        or deleted.

    client_data
        Specifies the argument that is to be passed to the specified procedure
        when it is invoked by XtCallbacks or NULL, or the client data to match
        on the registered callback procedures.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_AA_dd_dd_CC_aa_ll_ll_bb_aa_cc_kk(3X11R5) function adds the specified callback procedure
    to the specified widget's callback list.

    The _XX_tt_AA_dd_dd_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) add the specified list of callbacks to the
    specified widget's callback list.

    The _XX_tt_RR_ee_mm_oo_vv_ee_CC_aa_ll_ll_bb_aa_cc_kk(3X11R5) function removes a callback only if both the
    procedure and the client data match.

    The _XX_tt_RR_ee_mm_oo_vv_ee_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) function removes the specified callback
    procedures from the specified widget's callback list.

    The _XX_tt_RR_ee_mm_oo_vv_ee_AA_ll_ll_CC_aa_ll_ll_bb_aa_cc_kk_ss(3X11R5) function removes all the callback
    procedures from the specified widget's callback list.

  SSEEEE AALLSSOO

    _X_t_C_a_l_l_C_a_l_l_b_a_c_k_s()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

