XmbDrawText(3X11R5)                                 XmbDrawText(3X11R5)

  XXmmbbDDrraawwTTeexxtt(())

  NNAAMMEE

    XmbDrawText(), XwcDrawText() - draw text using multiple font sets

  SSYYNNOOPPSSIISS

    void XmbDrawText (Display *display, Drawable d, GC gc,
                      int x, int y, XmbTextItem *items,
                      int nitems)
    void XwcDrawText (Display *display, Drawable d, GC gc,
                      int x, int y, XwcTextItem *items,
                      int nitems)

  AARRGGUUMMEENNTTSS

    d
        Specifies the drawable.

    display
        Specifies the connection to the X server.

    gc
        Specifies the GC.

    items
        Specifies an array of text items.

    nitems
        Specifies the number of text items in the array.

    x

    y
        Specify the x and y coordinates.

  DDEESSCCRRIIPPTTIIOONN

    _XX_mm_bb_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) and _XX_ww_cc_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) allow complex spacing and font
    set shifts between text strings. Each text item is processed in turn, with
    the origin of a text element advanced in the primary draw direction by the
    escapement of the previous text item. A text item delta specifies an
    additional escapement of the text item drawing origin in the primary draw
    direction. A font_set member other than None in an item causes the font
    set to be used for this and subsequent text items in the text_items list.
    Leading text items with font_set member set to None will not be drawn.

    _XX_mm_bb_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) and _XX_ww_cc_DD_rr_aa_ww_TT_ee_xx_tt(3X11R5) do not perform any context-
    dependent rendering between text segments. Clients may compute the drawing
    metrics by passing each text segment to _XX_mm_bb_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) and
    _XX_ww_cc_TT_ee_xx_tt_EE_xx_tt_ee_nn_tt_ss(3X11R5) or _XX_mm_bb_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5) and
    _XX_ww_cc_TT_ee_xx_tt_PP_ee_rr_CC_hh_aa_rr_EE_xx_tt_ee_nn_tt_ss(3X11R5). When the XXFFoonnttSSeett has missing charsets,
    each unavailable character is drawn with the default string returned by
    _XX_CC_rr_ee_aa_tt_ee_FF_oo_nn_tt_SS_ee_tt(3X11R5). The behavior for an invalid codepoint is
    undefined.

  SSTTRRUUCCTTUURREESS

    The XXmmbbTTeexxttIItteemm structure contains:

    typedef struct {
         char *chars;        pointer to string
         int nchars;         number of characters
         int delta;          pixel delta between strings
         XFontSet font_set;  fonts, None means don't change
    } XmbTextItem;

    The XXwwccTTeexxttIItteemm structure contains:

    typedef struct {
         wchar_t *chars;     pointer to wide char string
         int nchars;         number of wide characters
         int delta;          pixel delta between strings
         XFontSet font_set;  fonts, None means don't change
    } XwcTextItem;

  SSEEEE AALLSSOO

    _X_D_r_a_w_I_m_a_g_e_S_t_r_i_n_g()

    _X_D_r_a_w_S_t_r_i_n_g()

    _X_D_r_a_w_T_e_x_t()

    _X_m_b_D_r_a_w_I_m_a_g_e_S_t_r_i_n_g()

    _X_m_b_D_r_a_w_S_t_r_i_n_g()

    Xlib

