XClearArea(3X11R5)                                   XClearArea(3X11R5)

  XXCClleeaarrAArreeaa(())

  NNAAMMEE

    XClearArea(), XClearWindow() - clear area or window

  SSYYNNOOPPSSIISS

    XClearArea (Display *display, Window w, int x, y,
                unsigned int width, height, Bool exposures)
    XClearWindow (Display *display, Window w)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    exposures
        Specifies a Boolean value that indicates if Expose events are to be
        generated.

    w
        Specifies the window.

    width

    height
        Specify the width and height, which are the dimensions of the
        rectangle.

    x

    y
        Specify the x and y coordinates, which are relative to the origin of
        the window and specify the upper-left corner of the rectangle.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_CC_ll_ee_aa_rr_AA_rr_ee_aa(3X11R5) function paints a rectangular area in the specified
    window according to the specified dimensions with the window's background
    pixel or pixmap. The subwindow-mode effectively is ClipByChildren. If
    width is zero, it is replaced with the current width of the window minus
    x. If height is zero, it is replaced with the current height of the window
    minus y. If the window has a defined background tile, the rectangle
    clipped by any children is filled with this tile. If the window has
    background None, the contents of the window are not changed. In either
    case, if exposures is True, one or more Expose events are generated for
    regions of the rectangle that are either visible or are being retained in
    a backing store. If you specify a window whose class is InputOnly, a
    BadMatch error results.

    _XX_CC_ll_ee_aa_rr_AA_rr_ee_aa(3X11R5) can generate BadMatch, BadValue, and BadWindow errors.

    The _XX_CC_ll_ee_aa_rr_WW_ii_nn_dd_oo_ww(3X11R5) function clears the entire area in the specified
    window and is equivalent to

    XClearArea(display, w, 0, 0, 0, 0, False).

    If the window has a defined background tile, the rectangle is tiled with a
    plane-mask of all ones and GGXXccooppyy() function. If the window has background
    None, the contents of the window are not changed. If you specify a window
    whose class is InputOnly, a BadMatch error results.

    _XX_CC_ll_ee_aa_rr_WW_ii_nn_dd_oo_ww(3X11R5) can generate BadMatch and BadWindow errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadMatch
        An InputOnly window is used as a Drawable.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_o_p_y_A_r_e_a()

    Xlib

