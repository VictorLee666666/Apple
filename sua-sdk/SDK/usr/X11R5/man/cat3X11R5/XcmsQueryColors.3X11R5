XcmsQueryColors(3X11R5)                         XcmsQueryColors(3X11R5)

  XXccmmssQQuueerryyCCoolloorr(())

  NNAAMMEE

    XcmsQueryColor(), XcmsQueryColors(), XcmsLookupColor() - obtain color
    values

  SSYYNNOOPPSSIISS

    Status XcmsQueryColor (Display *display, Colormap colormap,
                           XcmsColor *color_in_out,
                           XcmsColorFormat result_format)
    Status XcmsQueryColors (Display *display, Colormap colormap,
                            XcmsColor colors_in_out[],
                            unsigned int ncolors,
                            XcmsColorFormat result_format)
    Status XcmsLookupColor (Display *display, Colormap colormap,
                            char *color_string,
                            XcmsColor *color_exact_return,
                            XcmsColor *color_screen_return,
                            XcmsColorFormat result_format)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    colormap
        Specifies the colormap.

    color_exact_return
        Returns the color specification parsed from the color string or parsed
        from the corresponding string found in a color name database.

    color_in_out
        Specifies the pixel member that indicates the color cell to query, and
        the color specification stored for the color cell is returned in this
        XXccmmssCCoolloorr structure.

    color_screen_return
        Returns the color that can be reproduced on the SSccrreeeenn.

    color_string
        Specifies the color string.

    result_format
        Specifies the color format for the returned color specifications
        (color_screen_return and color_exact_return arguments). If format is
        XcmsUndefinedFormat and the color string contains a numerical color
        specification, the specification is returned in the format used in
        that numerical color specification. If format is XcmsUndefinedFormat
        and the color string contains a color name, the specification is
        returned in the format used to store the color in the database.

    ncolors
        Specifies the number of XXccmmssCCoolloorr structures in the color
        specification array.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_CC_oo_ll_oo_rr(3X11R5) function obtains the RGB value for the pixel
    value in the pixel member of the specified XXccmmssCCoolloorr structure, and then
    converts the value to the target format as specified by the result_format
    argument. If the pixel is not a valid index into the specified colormap, a
    BadValue error results. The _XX_cc_mm_ss_QQ_uu_ee_rr_yy_CC_oo_ll_oo_rr_ss(3X11R5) function obtains the
    RGB values for pixel values in the pixel members of XXccmmssCCoolloorr structures,
    and then converts the values to the target format as specified by the
    result_format argument. If a pixel is not a valid index into the specified
    colormap, a BadValue error results. If more than one pixel is in error,
    the one that gets reported is arbitrary.

    _XX_cc_mm_ss_QQ_uu_ee_rr_yy_CC_oo_ll_oo_rr(3X11R5) and _XX_cc_mm_ss_QQ_uu_ee_rr_yy_CC_oo_ll_oo_rr_ss(3X11R5) can generate BadColor
    and BadValue errors.

    The _XX_cc_mm_ss_LL_oo_oo_kk_uu_pp_CC_oo_ll_oo_rr(3X11R5) function looks up the string name of a color
    with respect to the screen associated with the specified colormap. It
    returns both the exact color values and the closest values provided by the
    screen with respect to the visual type of the specified colormap. The
    values are returned in the format specified by result_format. If the color
    name is not in the Host Portable Character Encoding the result is
    implementation dependent. Use of uppercase or lowercase does not matter.
    _XX_cc_mm_ss_LL_oo_oo_kk_uu_pp_CC_oo_ll_oo_rr(3X11R5) returns XcmsSuccess or XcmsSuccessWithCompression
    if the name is resolved, otherwise it returns XcmsFailure. If
    XcmsSuccessWithCompression is returned, then the color specification in
    color_screen_return is the result of gamut compression.

  DDIIAAGGNNOOSSTTIICCSS

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_c_m_s_A_l_l_o_c_C_o_l_o_r()

    _X_c_m_s_S_t_o_r_e_C_o_l_o_r()

    _X_Q_u_e_r_y_C_o_l_o_r()

    Xlib

