XtAddRawEventHandler(3X11R5)               XtAddRawEventHandler(3X11R5)

  XXttAAddddEEvveennttHHaannddlleerr(())

  NNAAMMEE

    XtAddEventHandler(), XtAddRawEventHandler(), XtRemoveEventHandler(),
    XtRemoveRawEventHandler() - add and remove event handlers

  SSYYNNOOPPSSIISS

    void XtAddEventHandler (Widget w, EventMask event_mask,
                            Boolean nonmaskable, XtEventHandler proc,
                            XtPointer client_data)
    void XtAddRawEventHandler (Widget w, EventMask event_mask,
                               Boolean nonmaskable, XtEventHandler proc,
                               XtPointer client_data)
    void XtRemoveEventHandler (Widget w, EventMask event_mask,
                               Boolean nonmaskable, XtEventHandler proc,
                               XtPointer client_data)
    void XtRemoveRawEventHandler (Widget w, EventMask event_mask,
                                  Boolean nonmaskable, XtEventHandler proc,
                                  XtPointer client_data)

  AARRGGUUMMEENNTTSS

    client_data
        Specifies additional data to be passed to the client's event handler.

    event_mask
        Specifies the event mask for which to call or unregister this
        procedure.

    nonmaskable
        Specifies a Boolean value that indicates whether this procedure should
        be called or removed on the nonmaskable events (GraphicsExpose,
        NoExpose, SelectionClear, SelectionRequest, SelectionNotify,
        ClientMessage, and MappingNotify).

    proc
        Specifies the procedure that is to be added or removed.

    w
        Specifies the widget for which this event handler is being registered.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_AA_dd_dd_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) function registers a procedure with the
    dispatch mechanism that is to be called when an event that matches the
    mask occurs on the specified widget. If the procedure is already
    registered with the same client_data, the specified mask is ORed into the
    existing mask. If the widget is realized, _XX_tt_AA_dd_dd_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) calls
    _XX_SS_ee_ll_ee_cc_tt_II_nn_pp_uu_tt(3X11R5), if necessary.

    The _XX_tt_AA_dd_dd_RR_aa_ww_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) function is similar to
    _XX_tt_AA_dd_dd_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) except that it does not affect the widget's mask
    and never causes an XSelectInput for its events. Note that the widget
    might already have those mask bits set because of other nonraw event
    handlers registered on it.

    The _XX_tt_AA_dd_dd_RR_aa_ww_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) function is similar to
    _XX_tt_AA_dd_dd_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) except that it does not affect the widget's mask
    and never causes an XSelectInput for its events. Note that the widget
    might already have those mask bits set because of other nonraw event
    handlers registered on it.

    The _XX_tt_RR_ee_mm_oo_vv_ee_RR_aa_ww_EE_vv_ee_nn_tt_HH_aa_nn_dd_ll_ee_rr(3X11R5) function stops the specified procedure
    from receiving the specified events. Because the procedure is a raw event
    handler, this does not affect the widget's mask and never causes a call on
    XSelectInput .

  SSEEEE AALLSSOO

    _X_t_A_p_p_N_e_x_t_E_v_e_n_t()

    _X_t_B_u_i_l_d_E_v_e_n_t_M_a_s_k()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

