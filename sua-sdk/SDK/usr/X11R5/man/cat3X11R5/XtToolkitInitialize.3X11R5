XtToolkitInitialize(3X11R5)                 XtToolkitInitialize(3X11R5)

  XXttCCrreeaatteeAApppplliiccaattiioonnCCoonntteexxtt(())

  NNAAMMEE

    XtCreateApplicationContext(), XtDestroyApplicationContext(),
    XtWidgetToApplicationContext(), XtToolkitInitialize() - create, destroy,
    and obtain an application context

  SSYYNNOOPPSSIISS

    XtAppContext XtCreateApplicationContext (void)
    void XtDestroyApplicationContext (XtAppContext app_context)
    XtAppContext XtWidgetToApplicationContext (Widget w)
    void XtToolkitInitialize (void)

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    w
        Specifies the widget .

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_CC_rr_ee_aa_tt_ee_AA_pp_pp_ll_ii_cc_aa_tt_ii_oo_nn_CC_oo_nn_tt_ee_xx_tt(3X11R5) function returns an application
    context, which is an opaque type. Every application must have at least one
    application context.

    The _XX_tt_DD_ee_ss_tt_rr_oo_yy_AA_pp_pp_ll_ii_cc_aa_tt_ii_oo_nn_CC_oo_nn_tt_ee_xx_tt(3X11R5) function destroys the specified
    application context as soon as it is safe to do so. If called from with an
    event dispatch (for example, a callback procedure),
    _XX_tt_DD_ee_ss_tt_rr_oo_yy_AA_pp_pp_ll_ii_cc_aa_tt_ii_oo_nn_CC_oo_nn_tt_ee_xx_tt(3X11R5) does not destroy the application
    context until the dispatch is complete.

    The _XX_tt_WW_ii_dd_gg_ee_tt_TT_oo_AA_pp_pp_ll_ii_cc_aa_tt_ii_oo_nn_CC_oo_nn_tt_ee_xx_tt(3X11R5) function returns the application
    context for the specified widget.

    The semantics of calling _XX_tt_TT_oo_oo_ll_kk_ii_tt_II_nn_ii_tt_ii_aa_ll_ii_zz_ee(3X11R5) more than once are
    undefined.

  SSEEEE AALLSSOO

    _X_t_D_i_s_p_l_a_y_I_n_i_t_i_a_l_i_z_e()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

