XtManageChild(3X11R5)                             XtManageChild(3X11R5)

  XXttMMaannaaggeeCChhiillddrreenn(())

  NNAAMMEE

    XtManageChildren(), XtManageChild(), XtUnmanageChildren(),
    XtUnmanageChild() - manage and unmanage children

  SSYYNNOOPPSSIISS

    typedef Widget *WidgetList

    void XtManageChildren (WidgetList children, Cardinal num_children)
    void XtManageChild (Widget child)
    void XtUnmanageChildren (WidgetList children, Cardinal num_children)
    void XtUnmanageChild (Widget child)

  AARRGGUUMMEENNTTSS

    child
        Specifies the child.

    children
        Specifies a list of child widgets.

    num_children
        Specifies the number of children.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_tt_MM_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5) function performs the following:
    *     Issues an error if the children do not all have the same parent or
          if the parent is not a subclass of compositeWidgetClass.
    *     Returns immediately if the common parent is being destroyed;
          otherwise, for each unique child on the list,
          _XX_tt_MM_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5) ignores the child if it already is managed
          or is being destroyed and marks it if not.
    *     If the parent is realized and after all children have been marked,
          it makes some of the newly managed children viewable:
          *     Calls the change_managed routine of the widgets' parent.
          *     Calls _XX_tt_RR_ee_aa_ll_ii_zz_ee_WW_ii_dd_gg_ee_tt(3X11R5) on each previously unmanaged
                child that is unrealized.
          *     Maps each previously unmanaged child that has map_when_managed
                True.

    Managing children is independent of the ordering of children and
    independent of creating and deleting children. The layout routine of the
    parent should consider children whose managed field is True and should
    ignore all other children. Note that some composite widgets, especially
    fixed boxes, call _XX_tt_MM_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd(3X11R5) from their insert_child procedure.

    If the parent widget is realized, its change_managed procedure is called
    to notify it that its set of managed children has changed. The parent can
    reposition and resize any of its children. It moves each child as needed
    by calling _XX_tt_MM_oo_vv_ee_WW_ii_dd_gg_ee_tt(3X11R5), which first updates the x and y fields
    and then calls _XX_MM_oo_vv_ee_WW_ii_nn_dd_oo_ww(3X11R5) if the widget is realized.

    The _XX_tt_MM_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd(3X11R5) function constructs a WidgetList of length one
    and calls _XX_tt_MM_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5).

    The _XX_tt_UU_nn_mm_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5) function performs the following:
    *     Issues an error if the children do not all have the same parent or
          if the parent is not a subclass of compositeWidgetClass.
    *     Returns immediately if the common parent is being destroyed;
          otherwise, for each unique child on the list,
          _XX_tt_UU_nn_mm_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5) performs the following:
          *     Ignores the child if it already is unmanaged or is being
                destroyed and marks it if not.
          *     If the child is realized, it makes it nonvisible by unmapping
                it.
    *     Calls the change_managed routine of the widgets' parent after all
          children have been marked if the parent is realized.

    _XX_tt_UU_nn_mm_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5) does not destroy the children widgets. Removing
    widgets from a parent's managed set is often a temporary banishment, and,
    some time later, you might manage the children again.

    The _XX_tt_UU_nn_mm_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd(3X11R5) function constructs a widget list of length
    one and calls _XX_tt_UU_nn_mm_aa_nn_aa_gg_ee_CC_hh_ii_ll_dd_rr_ee_nn(3X11R5).

  SSEEEE AALLSSOO

    _X_t_M_a_p_W_i_d_g_e_t()

    _X_t_R_e_a_l_i_z_e_W_i_d_g_e_t()

    X Toolkit Intrinsics - C Language Interface

    Xlib - C Language X Interface

