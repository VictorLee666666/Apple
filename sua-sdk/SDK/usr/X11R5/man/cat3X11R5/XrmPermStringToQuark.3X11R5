XrmPermStringToQuark(3X11R5)               XrmPermStringToQuark(3X11R5)

  XXrrmmUUnniiqquueeQQuuaarrkk(())

  NNAAMMEE

    XrmUniqueQuark(), XrmStringToQuark(), XrmPermStringToQuark(),
    XrmQuarkToString(), XrmStringToQuarkList(), XrmStringToBindingQuarkList()
    - manipulate resource quarks

  SSYYNNOOPPSSIISS

    XrmQuark XrmUniqueQuark (void)

    #define XrmStringToName(string) XrmStringToQuark(string)
    #define XrmStringToClass(string) XrmStringToQuark(string)
    #define XrmStringToRepresentation(string) XrmStringToQuark(string)

    XrmQuark XrmStringToQuark (char *string)

    XrmQuark XrmPermStringToQuark (char *string)

    #define XrmStringToName(string) XrmStringToQuark(string)
    #define XrmStringToClass(string) XrmStringToQuark(string)
    #define XrmStringToRepresentation(string) XrmStringToQuark(string)

    XrmQuark XrmStringToQuark (char *string)

    XrmQuark XrmPermStringToQuark (char *string)
    #define XrmNameToString(name) XrmQuarkToString(name)
    #define XrmClassToString(class) XrmQuarkToString(class)
    #define XrmRepresentationToString(type) XrmQuarkToString(type)

    char *XrmQuarkToString (XrmQuark quark)
    #define XrmStringToNameList(str, name)  XrmStringToQuarkList((str),
    (name))
    #define XrmStringToClassList(str, class) XrmStringToQuarkList((str),
    (class))

    void XrmStringToQuarkList (char *string,
                               XrmQuarkList quarks_return)
    XrmStringToBindingQuarkList (char *string,
                                 XrmBindingList bindings_return,
                                 XrmQuarkList quarks_return)

  AARRGGUUMMEENNTTSS

    bindings_return
        Returns the binding list.

    quark
        Specifies the quark for which the equivalent string is desired.

    quarks_return
        Returns the list of quarks.

    string
        Specifies the string for which a quark or quark list is to be
        allocated.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_rr_mm_UU_nn_ii_qq_uu_ee_QQ_uu_aa_rr_kk(3X11R5) function allocates a quark that is guaranteed
    not to represent any string that is known to the resource manager.

    These functions can be used to convert from string to quark
    representation. If the string is not in the Host Portable Character
    Encoding the conversion is implementation dependent. The string argument
    to _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5) need not be permanently allocated storage.
    _XX_rr_mm_PP_ee_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5) is just like _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5), except
    that Xlib is permitted to assume the string argument is permanently
    allocated, and hence that it can be used as the value to be returned by
    _XX_rr_mm_QQ_uu_aa_rr_kk_TT_oo_SS_tt_rr_ii_nn_gg(3X11R5).

    This function can be used to convert from quark representation to string.
    The string pointed to by the return value must not be modified or freed.
    The returned string is byte-for-byte equal to the original string passed
    to one of the string-to-quark routines. If no string exists for that
    quark, _XX_rr_mm_QQ_uu_aa_rr_kk_TT_oo_SS_tt_rr_ii_nn_gg(3X11R5) returns NULL. For any given quark, if
    _XX_rr_mm_QQ_uu_aa_rr_kk_TT_oo_SS_tt_rr_ii_nn_gg(3X11R5) returns a non-NULL value, all future calls will
    return the same value (identical address).

    These functions can be used to convert from string to quark
    representation. If the string is not in the Host Portable Character
    Encoding the conversion is implementation dependent. The string argument
    to _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5) need not be permanently allocated storage.
    _XX_rr_mm_PP_ee_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5) is just like _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk(3X11R5), except
    that Xlib is permitted to assume the string argument is permanently
    allocated, and hence that it can be used as the value to be returned by
    _XX_rr_mm_QQ_uu_aa_rr_kk_TT_oo_SS_tt_rr_ii_nn_gg(3X11R5).

    The _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_QQ_uu_aa_rr_kk_LL_ii_ss_tt(3X11R5) function converts the null-terminated
    string (generally a fully qualified name) to a list of quarks. Note that
    the string must be in the valid ResourceName format (see section 15.1). If
    the string is not in the Host Portable Character Encoding the conversion
    is implementation dependent.

    A binding list is a list of type XXrrmmBBiinnddiinnggLLiisstt and indicates if
    components of name or class lists are bound tightly or loosely (that is,
    if wildcarding of intermediate components is specified).

    typedef enum {XrmBindTightly, XrmBindLoosely}
                  XrmBinding, *XrmBindingList;

    XrmBindTightly indicates that a period separates the components, and
    XrmBindLoosely indicates that an asterisk separates the components.

    The _XX_rr_mm_SS_tt_rr_ii_nn_gg_TT_oo_BB_ii_nn_dd_ii_nn_gg_QQ_uu_aa_rr_kk_LL_ii_ss_tt(3X11R5) function converts the specified
    string to a binding list and a quark list. If the string is not in the
    Host Portable Character Encoding the conversion is implementation
    dependent. Component names in the list are separated by a period or an
    asterisk character. If the string does not start with period or asterisk,
    a period is assumed. For example, ``*a.b*c'' becomes:

  SSEEEE AALLSSOO

    _X_r_m_G_e_t_R_e_s_o_u_r_c_e()

    _X_r_m_I_n_i_t_i_a_l_i_z_e()

    _X_r_m_M_e_r_g_e_D_a_t_a_b_a_s_e_s()

    _X_r_m_U_n_i_q_u_e_Q_u_a_r_k()

    Xlib

