XSetDeviceModifierMapping(3X11R5)        XSetDeviceModifierMapping(3X11R5)

  XXGGeettDDeevviicceeMMooddiiffiieerrMMaappppiinngg(())

  NNAAMMEE

    XGetDeviceModifierMapping(), XSetDeviceModifierMapping() - query or change
    device modifier mappings

  SSYYNNOOPPSSIISS

    XSetDeviceModifierMapping (Display *display, XDevice *device,
                               XModifierKeymap  *modmap)
    XModifierKeymap *XGetDeviceModifierMapping (Display *display,
                                                XDevice *device)

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device whose modifier mapping is to be queried or
        modified.

    modmap
        Specifies a pointer to the XXMMooddiiffiieerrKKeeyymmaapp structure.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) request specifies the KeyCodes of
    the keys (if any) that are to be used as modifiers for the specified
    device. If it succeeds, the X server generates a DeviceMappingNotify
    event, and _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) returns MappingSuccess. X
    permits at most eight modifier keys. If more than eight are specified in
    the XXMMooddiiffiieerrKKeeyymmaapp structure, a BadLength error results.

    The modifiermap member of the XXMMooddiiffiieerrKKeeyymmaapp structure contains eight
    sets of max_keypermod KeyCodes, one for each modifier in the order Shift,
    Lock, Control, Mod1, Mod2, Mod3, Mod4, and Mod5. Only nonzero KeyCodes
    have meaning in each set, and zero KeyCodes are ignored. In addition, all
    of the nonzero KeyCodes must be in the range specified by min_keycode and
    max_keycode as returned by _XX_LL_ii_ss_tt_II_nn_pp_uu_tt_DD_ee_vv_ii_cc_ee_ss(3X11R5), or a BadValue error
    results. No KeyCode can appear twice in the entire map, or a BadValue
    error results.

    An X server can impose restrictions on how modifiers can be changed, for
    example, if certain keys do not generate up transitions in hardware, if
    auto-repeat cannot be disabled on certain keys, or if multiple modifier
    keys are not supported. If some such restriction is violated, the status
    reply is MappingFailed, and none of the modifiers are changed. If the new
    KeyCodes specified for a modifier differ from those currently defined and
    any (current or new) keys for that modifier are in the logically down
    state, _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) returns MappingBusy, and none of
    the modifiers is changed.

    _XX_SS_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) can generate BadLength, BadDevice,
    BadMatch, BadAlloc, and BadValue errors.

    The _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) request returns a pointer to a newly
    created XXMMooddiiffiieerrKKeeyymmaapp structure that contains the keys being used as
    modifiers. The structure should be freed after use by calling
    XXFFrreeeeMMooddiiffiieerrMMaappppiinngg(). If only zero values appear in the set for any
    modifier, that modifier is disabled.

    _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) can generate BadDevice and BadMatch
    errors.

  SSTTRRUUCCTTUURREESS

    The XXMMooddiiffiieerrKKeeyymmaapp structure contains:

    typedef struct {
         int max_keypermod;
         KeyCode *modifiermap;
    } XModifierKeymap;

  DDIIAAGGNNOOSSTTIICCSS

    BadLength
        More than eight keys were specified in the XXMMooddiiffiieerrKKeeyymmaapp structure.

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client with XXOOppeennIInnppuuttDDeevviiccee(). This
        error can also occur if the specified device is the X keyboard or X
        pointer device.

    BadMatch
        This error can occur if an _XX_GG_ee_tt_DD_ee_vv_ii_cc_ee_MM_oo_dd_ii_ff_ii_ee_rr_MM_aa_pp_pp_ii_nn_gg(3X11R5) or
        XXCChhaannggeeDDeevviicceeMMooddiiffiieerrMMaappppiinngg() request was made specifying a device
        that has no keys.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_S_e_t_D_e_v_i_c_e_B_u_t_t_o_n_M_a_p_p_i_n_g()

    _X_S_e_t_D_e_v_i_c_e_K_e_y_M_a_p()

    Programming With Xlib

