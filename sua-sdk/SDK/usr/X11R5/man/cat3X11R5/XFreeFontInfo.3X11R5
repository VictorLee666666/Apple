XFreeFontInfo(3X11R5)                             XFreeFontInfo(3X11R5)

  XXLLiissttFFoonnttss(())

  NNAAMMEE

    XListFonts(), XFreeFontNames(), XListFontsWithInfo(), XFreeFontInfo() -
    obtain or free font names and information

  SSYYNNOOPPSSIISS

    char **XListFonts (Display * display, char * pattern, int maxnames,
                       int * actual_count_return)
    XFreeFontNames (char *list[])
    char **XListFontsWithInfo (Display *display, char *pattern,
                               int maxnames, int *count_return,
                               XFontStruct **info_return)
    XFreeFontInfo (char **names, XFontStruct *free_info, int actual_count)

  AARRGGUUMMEENNTTSS

    actual_count
        Specifies the actual number of matched font names returned by
        XListFontsWithInfo .

    actual_count_return
        Returns the actual number of font names.

    count_return
        Returns the actual number of matched font names.

    display
        Specifies the connection to the X server.

    info_return
        Returns the font information.

    free_info
        Specifies the font information returned by XListFontsWithInfo .

    list
        Specifies the array of strings you want to free.

    maxnames
        Specifies the maximum number of names to be returned.

    names
        Specifies the list of font names returned by XListFontsWithInfo .

    pattern
        Specifies the null-terminated pattern string that can contain wildcard
        characters.

  DDEESSCCRRIIPPTTIIOONN

    The _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss(3X11R5) function returns an array of available font names
    (as controlled by the font search path; see _XX_SS_ee_tt_FF_oo_nn_tt_PP_aa_tt_hh(3X11R5)) that
    match the string you passed to the pattern argument. The pattern string
    can contain any characters, but each asterisk (*) is a wildcard for any
    number of characters, and each question mark (?) is a wildcard for a
    single character. If the pattern string is not in the Host Portable
    Character Encoding the result is implementation dependent. Use of
    uppercase or lowercase does not matter. Each returned string is null-
    terminated. If the data returned by the server is in the Latin Portable
    Character Encoding, then the returned strings are in the Host Portable
    Character Encoding. Otherwise, the result is implementation dependent. If
    there are no matching font names, _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss(3X11R5) returns NULL. The
    client should call _XX_FF_rr_ee_ee_FF_oo_nn_tt_NN_aa_mm_ee_ss(3X11R5) when finished with the result to
    free the memory.

    The _XX_FF_rr_ee_ee_FF_oo_nn_tt_NN_aa_mm_ee_ss(3X11R5) function frees the array and strings returned
    by _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss(3X11R5) or _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss_WW_ii_tt_hh_II_nn_ff_oo(3X11R5).

    The _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss_WW_ii_tt_hh_II_nn_ff_oo(3X11R5) function returns a list of font names that
    match the specified pattern and their associated font information. The
    list of names is limited to size specified by maxnames. The information
    returned for each font is identical to what _XX_LL_oo_aa_dd_QQ_uu_ee_rr_yy_FF_oo_nn_tt(3X11R5) would
    return except that the per-character metrics are not returned. The pattern
    string can contain any characters, but each asterisk (*) is a wildcard for
    any number of characters, and each question mark (?) is a wildcard for a
    single character. If the pattern string is not in the Host Portable
    Character Encoding the result is implementation dependent. Use of
    uppercase or lowercase does not matter. Each returned string is null-
    terminated. If the data returned by the server is in the Latin Portable
    Character Encoding, then the returned strings are in the Host Portable
    Character Encoding. Otherwise, the result is implementation dependent. If
    there are no matching font names, _XX_LL_ii_ss_tt_FF_oo_nn_tt_ss_WW_ii_tt_hh_II_nn_ff_oo(3X11R5) returns NULL.

    To free only the allocated name array, the client should call
    _XX_FF_rr_ee_ee_FF_oo_nn_tt_NN_aa_mm_ee_ss(3X11R5). To free both the name array and the font
    information array, or to free just the font information array, the client
    should call _XX_FF_rr_ee_ee_FF_oo_nn_tt_II_nn_ff_oo(3X11R5).

    The _XX_FF_rr_ee_ee_FF_oo_nn_tt_II_nn_ff_oo(3X11R5) function frees the font information array. To
    free an XXFFoonnttSSttrruucctt structure without closing the font, call
    _XX_FF_rr_ee_ee_FF_oo_nn_tt_II_nn_ff_oo(3X11R5) with the names argument specified as NULL.

  SSEEEE AALLSSOO

    _X_L_o_a_d_F_o_n_t()

    _X_S_e_t_F_o_n_t_P_a_t_h()

    Xlib

