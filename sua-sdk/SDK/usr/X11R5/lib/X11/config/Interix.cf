/*
 * Set version numbers before making makefiles.
 * 
 * X11r5 only builds using the berkeley libraries and compiler.  Make
 * sure your path includes /bsd43/bin before /bin/ or /usr/bin.
 */

#define OSName            Interix
#define OSMajorVersion    3
#define OSMinorVersion    0


/*****************************************************************************
 *			  Platform-specific parameters                       *
 *****************************************************************************/

#define HasVoidSignalReturn		YES
#define SetTtyGroup			NO

/*****************************************************************************
 *                                                                           *
 *			DEFINE PARAMETERS FOR BUILDING                       *
 *                                                                           *
 *****************************************************************************/

#define HasGcc YES
#define DefaultCCOptions -O
#define StandardDefines -Dopennt
#define ArCmd ar cvr
#define BootstrapCFlags -Dopennt
#define MakeCmd make 
#define DoRanlibCmd 0
#define ExtraLibraries		
#define ConnectionFlags -DTCPCONN
#define ForceNormalLib YES
#define HasSharedLibraries NO
#define BuildFonts NO
#define AvoidNullMakeCommand YES
#define Malloc0ReturnsNull YES

#define BourneShell  /bin/ksh
/* CppCmd - should point to cpp that provides traditional UNIX behaviour */
#define CppCmd       /usr/lib/cpp

#ifndef ProjectRoot
#define ProjectRoot   /usr/X11R5
#endif

#undef i386

/* SystemRoot is used in this file and in InterixLib.rules
 * Normally it has no value. Only for internal builds will it get a value
 */
#define SystemRoot 

/*****************************************************************************
 *                                                                           *
 *	      OVERRIDE ANY CONFIGURATION PARAMETERS FROM Imake.tmpl          *
 *                                                                           *
 *****************************************************************************/

#define SpecialMalloc		NO
#define HasSockets		YES


#define ConstructMFLAGS	NO		/* build MFLAGS from MAKEFLAGS */

#define BUILD_USE_SHLIB 0

#if defined(opennt) && !defined(UseInstalled)
/*
 * Softway Systems internal build environment
 */

/* SystemInclDir is only used for internal builds.
 * It points to the internal header directory
 */
/* INTERIX_CONFIG    --- DO NOT REMOVE THIS LINE */
#define SystemInclDir    to_be_replaced


/* don't use any std include directories from this machine,
 * and use the internal directory where the full set of internal header 
 * files reside.
 */
#define StandardIncludes	-N nostdinc -I SystemInclDir

#undef HasGcc
#undef CcCmd
#define CcCmd cc

#if (BUILD_USE_SHLIB==1)
#define UserLdFlags	-N nostdlibdir -L SystemRoot/usr/lib -Bdynamic
#else
#define UserLdFlags	-N nostdlibdir -L SystemRoot/usr/lib
#endif /*BUILD_USE_SHLIB*/

#endif /* UseInstalled */


#if (BUILD_USE_SHLIB==1)

#if !defined(UserLdFlags)
#define UserLdFlags	-shared -symbolic
#endif /*UserLdFlags*/

#define InstLibFlags	-m 0755

/* shared library support stuff */
#undef HasSharedLibraries 
#define HasSharedLibraries YES
/* #define LnCmd	ln -s  /* don't do this if using CcCmd based on CL */

/* don't need shared versions of the following (they are small enough) */
#define SharedLibXau	NO
#define SharedOldLibX	NO
#define SharedLibXdmcp  NO

/* 
 * define VM addresses of where these .so libraries should be loaded 
 */
#define SharedLibAddrXm		0x77800000	/* libXm */
#define SharedLibAddrX		0x77700000	/* libX11 */
#define SharedLibAddrXt		0x77600000
#define SharedLibAddrXaw	0x77500000
/* SharedLibAddrUil  77400000 - 77500000 */
#define SharedLibAddrXmu	0x77300000
#define SharedLibAddrXMrm	0x77340000
#define SharedLibAddrXext	0x77390000
#define SharedLibAddrXinput	0x773a0000	/* libXi */

/* ?? what about  Xtst ? */

#include <interixLib.rules>

#else /*BUILD_USE_SHLIB*/

#ifndef UserLdFlags
# define UserLdFlags   -static
#endif /*UserLdFlags*/

#endif /*BUILD_USE_SHLIB*/

