errno(3)                                                       errno(3)

  eerrrrnnoo

  NNAAMMEE

    errno - system error numbers and errno values

  SSYYNNOOPPSSIISS

    #include <errno.h>

  DDEESSCCRRIIPPTTIIOONN

    This section provides an overview of the system error returns.

  EERRRROORRSS

    Nearly all of the system calls provide an error number in the external
    variable errno, which is defined as:

    extern int errno

    When a system call detects an error, it returns an integer value
    indicating failure (usually -1) and sets the variable errno accordingly.
    This allows interpretation of the failure on receiving a -1 and to take
    action accordingly. Successful calls never set errno; once set, it remains
    until another error occurs. It should only be examined after an error.
    Note that a number of system calls overload the meanings of these error
    numbers, and that the meanings must be interpreted according to the type
    and circumstances of the call.

    The following is a complete list of the errors and their names as given in
    <<ssyyss//eerrrrnnoo..hh>>. They are listed in alphabetical order by symbolic name. All
    of these values can be assigned to errno.

    [E2BIG]
        Arg list too long.
        The number of bytes used for the argument and environment list of the
        new process exceeded the current limit of {ARG_MAX} in <<lliimmiittss..hh>>.

    [EACCES]
        Permission denied.
        An attempt was made to access a file in a way forbidden by its file
        access permissions.

    [EADDRINUSE]
        Address already in use.
        Normally, each address can only be used once.

    [EADDRNOTAVAIL]
        Address cannot be assigned.
        Normally results from an attempt to create a socket with an address
        not on this computer.

    [EAFNOSUPPORT]
        Address family not supported by protocol family.
        The address was incompatible with the requested protocol.

    [EAGAIN] | [EWOULDBLOCK]
        Resource temporarily unavailable.
        This is a temporary condition and later calls to the same routine may
        complete normally.

    [EALREADY]
        Operation already in progress.
        An operation was attempted on a non-blocking object that already had
        an operation in progress.

    [EBADF]
        Bad file descriptor.
        A file descriptor argument was out of range, referred to no open file,
        or a read (write) request was made to a file that was only open for
        writing (reading).

    [EBUSY]
        Resource busy.
        An attempt to use a system resource which was in use at the time in a
        manner which would have conflicted with the request.

    [ECHILD]
        No child processes.
        A _ww_aa_ii_tt(2) or _ww_aa_ii_tt_pp_ii_dd(2) function was executed by a process that had no
        existing or unwaited-for child processes.

    [ECONNABORTED]
        Software caused connection abort.
        A connection abort was caused internal to your host computer.

    [ECONNREFUSED]
        Connection refused.
        No connection could be made because the target computer actively
        refused it. This usually results from trying to connect to a service
        that is inactive on the foreign host.

    [ECONNRESET]
        Connection reset by peer.
        A connection was forcibly closed by a peer. This normally results from
        a loss of the connection on the remote socket due to a time-out or a
        reboot.

    [EDEADLK] | [EDEADLOCK]
        Resource deadlock avoided.
        An attempt was made to lock a system resource that would have resulted
        in a deadlock situation.

    [EDESTADDRREQ]
        Destination address required.
        An operation on a socket requires a destination address.

    [EDOM]
        Numerical argument out of domain.
        A numerical input argument was outside the defined domain of the
        mathematical function.

    [EDQUOT]
        Disk quota exceeded.
        One of the following failed because the user's quota of disk space was
        exhausted: a write to an ordinary file, the creation of a directory or
        the creation of a directory entry.

    [EEXIST]
        File exists.
        An existing file was mentioned in an inappropriate context, for
        instance, as the new link name in a _ll_ii_nn_kk(2) function.

    [EFAULT]
        Bad address.
        The system detected an invalid address in attempting to use an
        argument of a call.

    [EFBIG]
        File too large.
        The size of a file exceeded the maximum size, defined for Windows as
        232-1 bytes.

    [EFTYPE]
        Inappropriate file type or format.
        An operation was attempted on a file whose type or format would not
        support it.

    [EHOSTDOWN]
        Host is down.
        A socket operation failed because the destination host was down.

    [EHOSTNOTFOUND]
        Host could not be found.
        A socket operation was attempted to a host that could not be located.

    [EHOSTUNREACH]
        No route to host.
        A socket operation was attempted to an unreachable host.

    [EIDRM]
        IPC object has been deleted.
        The IPC identifier passed to an IPC call is invalid because it has
        been or is in the process of being deleted. This is most commonly seen
        with shared memory segments that have been marked for deletion but
        have not yet been detached from all processes; the error occurs when
        the IPC identifier is used to try to attach. The timing window for the
        same error is much smaller for message queues and semaphore sets, so
        the error is much less likely.

    [EILSEQ]
        Invalid multi-byte character.
        The call contained an multi-byte character that was invalid in the
        current locale.

    [EINPROGRESS]
        Operation now in progress.
        An operation that takes a long time to complete, such as a _cc_oo_nn_nn_ee_cc_tt(2),
        was attempted on a non-blocking object.

    [EINTR]
        Interrupted function call.
        An asynchronous signal (such as SIGINT) was caught by the process
        during the execution of an interruptible function. If the signal
        handler performs a normal return, the interrupted function call will
        seem to have returned the error condition.

    [EINVAL]
        Invalid argument.
        Some invalid argument was supplied. (For example, specifying an
        undefined signal to a _ss_ii_gg_nn_aa_ll(2) or _kk_ii_ll_ll(2) function).

    [EIO]
        Input/output error.
        Some physical input or output error occurred. This error will not be
        reported until a subsequent operation on the same file descriptor and
        may be lost (over written) by any subsequent errors.

    [EISCONN]
        Socket is already connected.
        A connect request was made on an already connected socket; or, a
        sendto or sendmsg request on a connected socket specified a
        destination when already connected.

    [EISDIR]
        Is a directory.
        An attempt was made to open a directory with write mode specified.

    [ELOOP]
        Too many levels of symbolic links.
        A path name lookup involved more than 8 levels of symbolic links.
        (This number is defined as MAXSYMLINKS in <<lliimmiittss..hh>>.)

    [EMFILE]
        Too many open files.
        The number of files open exceeds {OPEN_MAX}.

    [EMLINK]
        Too many links.
        Maximum allowable hard links to a single file has been exceeded (limit
        depends upon the file system; a POSIX-conformant file system supports
        at least 8). You can check the maximum number of links allowed on the
        system with the command ggeettccoonnff LLIINNKK__MMAAXX

    [EMSGSIZE]
        Message too long.
        Message sent on a socket was too long for an internal buffer or
        another limit.

    [ENAMETOOLONG]
        File name too long.
        File name exceeded {PATH_MAX} or a component of a file name exceeded
        {NAME_MAX}. For truly portable software, it may not be safe to rely on
        hard-coded values of {NAME_MAX} and {PATH_MAX}; on a networked system,
        the values may change depending on the file system. A safer way to
        check for the values on the current file system is to use _pp_aa_tt_hh_cc_oo_nn_ff(2)
        or _ff_pp_aa_tt_hh_cc_oo_nn_ff(2) before constructing a file name.

    [ENETDOWN]
        Network is down.
        A socket operation encountered a dead network.

    [ENETRESET]
        Network dropped connection on reset.
        The host you were connected to crashed and rebooted.

    [ENETUNREACH]
        Network is unreachable.
        A socket operation was attempted to an unreachable network.

    [ENFILE]
        Too many open files in system.
        Maximum number of file descriptors allowable on the system has been
        reached and a requests for an open cannot be satisfied until at least
        one has been closed.

    [ENOBUFS]
        No buffer space available.
        An operation on a socket or pipe was not performed because the system
        lacked sufficient buffer space or because a queue was full.

    [ENODATA]
        No message available.
        No data message was available.

    [ENODEV]
        Operation not supported by device.
        An attempt was made to apply an inappropriate function to a device,
        for example, trying to read a write-only device such as a printer.

    [ENOENT]
        No such file or directory.
        A component of a specified path name did not exist, or the path name
        was an empty string.

    [ENOEXEC]
        Exec format error.
        A request was made to execute a file that, although it has the
        appropriate permissions, was not in the format required for an
        executable file.

    [ENOLCK]
        No locks available.
        No locks were available.

    [ENOMEM]
        Cannot allocate memory.
        The new process image required more memory than was allowed by the
        hardware or by system-imposed memory management constraints.

    [ENOMSG]
        No message of the desired type.
        An IPC message queue error. The _mm_ss_gg_gg_ee_tt(2) call was called in non-
        blocking mode, and there are no messages in the queue.

    [ENOPROTOOPT]
        Protocol not available.
        A bad option or level was specified for _gg_ee_tt_ss_oo_cc_kk_oo_pp_tt(2) or
        _ss_ee_tt_ss_oo_cc_kk_oo_pp_tt(2).

    [ENOSPC]
        Device out of space.
        A _ww_rr_ii_tt_ee(2) to an ordinary file, the creation of a directory, or the
        creation of a directory entry failed because no more disk blocks were
        available on the file system, or the allocation of a file serial
        number for a newly created file failed because no more numbers were
        available on the file system.

    [ENOSYS]
        Function not implemented or Operation not applicable.
        Attempted a system call that was not available on this system, or that
        does not apply. For example, attempting to create a directory that
        belongs to one of the Subsystem for UNIX-based Applications virtual file systems will fail with this
        error.

    [ENOTBLK]
        Not a block device.
        A block device operation was attempted on a non-block device or file.

    [ENOTCONN]
        Socket is not connected.
        An request to send or receive data was disallowed because the socket
        was not connected and (when sending on a datagram socket) no address
        was supplied.

    [ENOTDIR]
        Not a directory.
        A component of the specified path name existed, but it was not a
        directory, when a directory was expected.

    [ENOTEMPTY]
        Directory not empty.
        An operation such as an unlink was attempted on a directory that was
        not empty.

    [ENOTSOCK]
        Not a socket.
        A socket function was attempted on a descriptor that was not a socket.

    [ENOTSUP]
        Not supported.
        The function was called with an argument type or value that is not
        supported.

    [ENOTTY]
        Inappropriate I/O control operation for device.
        A control function was attempted for a file or special device for
        which the operation was inappropriate.

    [ENXIO]
        No such device or address.
        Input or output on a special file referred to a device that did not
        exist, or made a request beyond the limits of the device. This error
        may also occur when, for example, a tape is not on-line or no disk is
        loaded in a drive.

    [EOPNOTSUPP]
        Operation not supported on socket.
        The type of socket (either address family or protocol) does not
        support the operation.

    [EPERM]
        Operation not permitted.
        An attempt was made to perform an operation limited to processes with
        appropriate privileges or to the owner of a file or other resources.

    [EPFNOSUPPORT]
        Protocol family not supported.
        The protocol family has not been configured or no implementation for
        it exists.

    [EPIPE]
        Broken pipe.
        A write on a socket, pipe or FIFO for which there is no process to
        read the data.

    [EPROCLIM]
        Too many processes.
        The operation would cause the system to exceed the limit on the number
        of processes.

    [EPROTO]
        Protocol error.
        The protocol encountered an unrecoverable error for that endpoint.

    [EPROTONOSUPPORT]
        Protocol not supported.
        The system does not support the specified protocol.

    [EPROTOTYPE]
        Protocol wrong for socket.
        The requested protocol doesn't support the semantics of the socket.

    [ERANGE]
        Numerical result out of range.
        A numerical result of the function was too large to fit in the
        available space (perhaps exceeded precision).

    [EROFS]
        Read-only file system.
        An attempt was made to modify a file or directory was made on a file
        system that was read-only at the time.

    [ESHUTDOWN]
        Cannot send after socket shutdown.
        A request to send data was disallowed because the socket had already
        been shut down.

    [ESOCKTNOSUPPORT]
        Socket type not supported.
        The system does not support the specified socket type.

    [ESPIPE]
        Illegal seek.
        An _ll_ss_ee_ee_kk(2) function was issued on a socket, pipe or FIFO.

    [ESRCH]
        No such process.
        No process could be found corresponding to that specified by the given
        process ID.

    [ETIMEDOUT]
        Operation timed out.
        A connect or send request failed because the connected party did not
        properly respond after a period of time. (The time-out period is
        dependent on the communication protocol.)

    [ETOOMANYREFS]
        Too many references.
        There were too many references; the system could not splice (System
        V). This normally means that a kernel-level resource has run out.

    [ETXTBUSY]
        Unknown error.
        On Subsystem for UNIX-based Applications, this is an unknown error. Traditionally, it means that an
        attempt was made to execute a pure-procedure program that is currently
        open for writing, or an attempt was made to open for writing a pure-
        procedure program while it was being executed.

    [EUSERS]
        Too many users.
        Traditionally, this means that the quota system ran out of table
        entries. Because Winsock does not produce this error, and the Subsystem for UNIX-based Applications
        socket implementation is based on Winsock, this error is unlikely to
        appear.

    [EXDEV]
        Improper link.
        A hard link to a file on another file system was attempted.

  UUSSAAGGEE NNOOTTEESS

    The eerrrrnnoo function is thread safe only if you define the _REENTRANT
    compile-time macro.

    The eerrrrnnoo function is not async-signal safe.

