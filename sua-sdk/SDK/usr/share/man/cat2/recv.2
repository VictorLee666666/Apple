RECV(2) 		  FreeBSD System Calls Manual		       RECV(2)

NAME
     recv, recvfrom, recvmsg -- receive a message from a socket

LIBRARY
     Standard C Library (libc, -lc)

SYNOPSIS
     #include <sys/types.h>
     #include <sys/socket.h>

     ssize_t
     recv(int s, void *buf, size_t len, int flags);

     ssize_t
     recvfrom(int s, void * restrict buf, size_t len, int flags,
	 struct sockaddr * restrict from, socklen_t * restrict fromlen);

     ssize_t
     recvmsg(int s, struct msghdr *msg, int flags);

DESCRIPTION
     The rreeccvvffrroomm() and rreeccvvmmssgg() system calls are used to receive messages
     from a socket, and may be used to receive data on a socket whether or not
     it is connection-oriented.

     If from is not a null pointer and the socket is not connection-oriented,
     the source address of the message is filled in.  The fromlen argument is
     a value-result argument, initialized to the size of the buffer associated
     with from, and modified on return to indicate the actual size of the
     address stored there.

     The rreeccvv() function is normally used only on a connected socket (see
     connect(2)) and is identical to rreeccvvffrroomm() with a null pointer passed as
     its from argument.  As it is redundant, it may not be supported in future
     releases.

     All three routines return the length of the message on successful comple-
     tion.  If a message is too long to fit in the supplied buffer, excess
     bytes may be discarded depending on the type of socket the message is
     received from (see socket(2)).

     If no messages are available at the socket, the receive call waits for a
     message to arrive, unless the socket is nonblocking (see fcntl(2)) in
     which case the value -1 is returned and the external variable errno set
     to EAGAIN.  The receive calls normally return any data available, up to
     the requested amount, rather than waiting for receipt of the full amount
     requested; this behavior is affected by the socket-level options
     SO_RCVLOWAT and SO_RCVTIMEO described in getsockopt(2).

     The select(2) system call may be used to determine when more data arrive.

     The flags argument to a rreeccvv() function is formed by or'ing one or more
     of the values:

	   MSG_OOB	   process out-of-band data
	   MSG_PEEK	   peek at incoming message
	   MSG_WAITALL	   wait for full request or error
	   MSG_DONTWAIT    do not block

     The MSG_OOB flag requests receipt of out-of-band data that would not be
     received in the normal data stream.  Some protocols place expedited data
     at the head of the normal data queue, and thus this flag cannot be used
     with such protocols.  The MSG_PEEK flag causes the receive operation to
     return data from the beginning of the receive queue without removing that
     data from the queue.  Thus, a subsequent receive call will return the
     same data.  The MSG_WAITALL flag requests that the operation block until
     the full request is satisfied.  However, the call may still return less
     data than requested if a signal is caught, an error or disconnect occurs,
     or the next data to be received is of a different type than that
     returned.	The MSG_DONTWAIT flag requests the call to return when it
     would block otherwise.  If no data is available, errno is set to EAGAIN.
     This flag is not available in strict ANSI or C99 compilation mode.

     The rreeccvvmmssgg() system call uses a msghdr structure to minimize the number
     of directly supplied arguments.  This structure has the following form,
     as defined in <sys/socket.h>:

     struct msghdr {
	     caddr_t msg_name;	     /* optional address */
	     u_int   msg_namelen;    /* size of address */
	     struct  iovec *msg_iov; /* scatter/gather array */
	     u_int   msg_iovlen;     /* # elements in msg_iov */
	     caddr_t msg_control;    /* ancillary data, see below */
	     u_int   msg_controllen; /* ancillary data buffer len */
	     int     msg_flags;      /* flags on received message */
     };

     Here msg_name and msg_namelen specify the destination address if the
     socket is unconnected; msg_name may be given as a null pointer if no
     names are desired or required.  The msg_iov and msg_iovlen arguments
     describe scatter gather locations, as discussed in read(2).  The
     msg_control argument, which has length msg_controllen, points to a buffer
     for other protocol control related messages or other miscellaneous ancil-
     lary data.  The messages are of the form:

     struct cmsghdr {
	     u_int   cmsg_len;	     /* data byte count, including hdr */
	     int     cmsg_level;     /* originating protocol */
	     int     cmsg_type;      /* protocol-specific type */
     /* followed by
	     u_char  cmsg_data[]; */
     };

     As an example, one could use this to learn of changes in the data-stream
     in XNS/SPP, or in ISO, to obtain user-connection-request data by request-
     ing a rreeccvvmmssgg() with no data buffer provided immediately after an
     accept() system call.

     Open file descriptors are now passed as ancillary data for AF_UNIX domain
     sockets, with cmsg_level set to SOL_SOCKET and cmsg_type set to
     SCM_RIGHTS.

     Process credentials can also be passed as ancillary data for AF_UNIX
     domain sockets using a cmsg_type of SCM_CREDS.  In this case, cmsg_data
     should be a structure of type cmsgcred, which is defined in
     <sys/socket.h> as follows:

     struct cmsgcred {
	     pid_t   cmcred_pid;	     /* PID of sending process */
	     uid_t   cmcred_uid;	     /* real UID of sending process */
	     uid_t   cmcred_euid;	     /* effective UID of sending process */
	     gid_t   cmcred_gid;	     /* real GID of sending process */
	     short   cmcred_ngroups;	     /* number or groups */
	     gid_t   cmcred_groups[CMGROUP_MAX];     /* groups */
     };

     The kernel will fill in the credential information of the sending process
     and deliver it to the receiver.

     The msg_flags field is set on return according to the message received.
     MSG_EOR indicates end-of-record; the data returned completed a record
     (generally used with sockets of type SOCK_SEQPACKET).  MSG_TRUNC indi-
     cates that the trailing portion of a datagram was discarded because the
     datagram was larger than the buffer supplied.  MSG_CTRUNC indicates that
     some control data were discarded due to lack of space in the buffer for
     ancillary data.  MSG_OOB is returned to indicate that expedited or out-
     of-band data were received.

RETURN VALUES
     These calls return the number of bytes received, or -1 if an error
     occurred.

ERRORS
     The calls fail if:

     [EBADF]		The argument s is an invalid descriptor.

     [ECONNRESET]	The remote socket end is forcibly closed.

     [ENOTCONN] 	The socket is associated with a connection-oriented
			protocol and has not been connected (see connect(2)
			and accept(2)).

     [ENOTSOCK] 	The argument s does not refer to a socket.

     [EMSGSIZE] 	The rreeccvvmmssgg() system call was used to receive rights
			(file descriptors) that were in flight on the connec-
			tion.  However, the receiving program did not have
			enough free file descriptor slots to accept the them.
			In this case the descriptors are closed, any pending
			data can be returned by another call to rreeccvvmmssgg().

     [EAGAIN]		The socket is marked non-blocking, and the receive
			operation would block, or a receive timeout had been
			set, and the timeout expired before data were
			received.

     [EINTR]		The receive was interrupted by delivery of a signal
			before any data were available.

     [EFAULT]		The receive buffer pointer(s) point outside the
			process's address space.

SEE ALSO
     fcntl(2), getsockopt(2), read(2), select(2), socket(2)

HISTORY
     The rreeccvv() function appeared in 4.2BSD.


USAGE NOTES

     The rreeccvv function is thread safe.
     The rreeccvv function is nnoott async-signal safe.

