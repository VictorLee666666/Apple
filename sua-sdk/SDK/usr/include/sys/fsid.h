/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 *
 * <fsid.h>	-- Fstyp() FS name manifest constants.
 */ 

#ifndef _SYS_FSID_H
#define _SYS_FSID_H 1

#if defined(__cplusplus)
extern "C" {
#endif

#define DEV  	"dev"
#define PROC 	"proc"
#define NET  	"net"
#define LFS	"lfs"
#define NFS	"nfs"
#define CIFS	"cifs"

#ifdef __cplusplus
}
#endif

#endif /* _SYS_FSID_H */
