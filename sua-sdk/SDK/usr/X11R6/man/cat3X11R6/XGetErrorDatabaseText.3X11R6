XGetErrorDatabaseText(3X11R6)             XGetErrorDatabaseText(3X11R6)

  XXSSeettEErrrroorrHHaannddlleerr

  NNAAMMEE

    XSetErrorHandler, XGetErrorText, XDisplayName, XSetIOErrorHandler,
    XGetErrorDatabaseText - default error handlers

  SSYYNNTTAAXX

    int (*XSetErrorHandler(handler))()
          int (*handler)(Display *, XErrorEvent *)

    XGetErrorText(display, code, buffer_return, length)
          Display *display;
          int code;
          char *buffer_return;
          int length;

    char *XDisplayName(string)
          char *string;

    int (*XSetIOErrorHandler(handler))()
          int (*handler)(Display *);

    XGetErrorDatabaseText(display, name, message, default_string,
    buffer_return, length)
          Display *display;
          char *name, *message;
          char *default_string;
          char *buffer_return;
          int length;

  AARRGGUUMMEENNTTSS

    buffer_return
        Returns the error description.

    code
        Specifies the error code for which you want to obtain a description.

    default_string
        Specifies the default error message if none is found in the database.

    display
        Specifies the connection to the X server.

    handler
        Specifies the program's supplied error handler.

    length
        Specifies the size of the buffer.

    message
        Specifies the type of the error message.

    name
        Specifies the name of the application.

    string
        Specifies the character string.

  DDEESSCCRRIIPPTTIIOONN

    Xlib generally calls the program's supplied error handler whenever an
    error is received. It is not called on BadName errors from OpenFont,
    LookupColor, or AllocNamedColor protocol requests or on BadFont errors
    from a QueryFont protocol request. These errors generally are reflected
    back to the program through the procedural interface. Because this
    condition is not assumed to be fatal, it is acceptable for your error
    handler to return; the returned value is ignored. However, the error
    handler should not call any functions (directly or indirectly) on the
    display that will generate protocol requests or that will look for input
    events. The previous error handler is returned.

    The XXGGeettEErrrroorrTTeexxtt function copies a null-terminated string describing the
    specified error code into the specified buffer. The returned text is in
    the encoding of the current locale. It is recommended that you use this
    function to obtain an error description because extensions to Xlib may
    define their own error codes and error strings.

    The XXDDiissppllaayyNNaammee function returns the name of the display that
    XXOOppeennDDiissppllaayy would attempt to use. If a NULL string is specified,
    XXDDiissppllaayyNNaammee looks in the environment for the display and returns the
    display name that XXOOppeennDDiissppllaayy would attempt to use. This makes it easier
    to report to the user precisely which display the program attempted to
    open when the initial connection attempt failed.

    The XXSSeettIIOOEErrrroorrHHaannddlleerr sets the fatal I/O error handler. Xlib calls the
    program's supplied error handler if any sort of system call error occurs
    (for example, the connection to the server was lost). This is assumed to
    be a fatal condition, and the called routine should not return. If the I/
    O error handler does return, the client process exits.

    Note that the previous error handler is returned.

    The XXGGeettEErrrroorrDDaattaabbaasseeTTeexxtt function returns a null-terminated message (or
    the default message) from the error message database. Xlib uses this
    function internally to look up its error messages. The text in the
    default_string argument is assumed to be in the encoding of the current
    locale, and the text stored in the buffer_return argument is in the
    encoding of the current locale.

    The name argument should generally be the name of your application. The
    message argument should indicate which type of error message you want. If
    the name and message are not in the Host Portable Character Encoding, the
    result is implementation-dependent. Xlib uses three predefined
    "application names" to report errors. In these names, uppercase and
    lowercase matter.

    XProtoError
        The protocol error number is used as a string for the message
        argument.

    XlibMessage
        These are the message strings that are used internally by the library.

    XRequest
        For a core protocol request, the major request protocol number is used
        for the message argument. For an extension request, the extension name
        (as given by InitExtension) followed by a period (.) and the minor
        request protocol number is used for the message argument. If no string
        is found in the error database, the default_string is returned to the
        buffer argument.

  SSEEEE AALLSSOO

    _X_O_p_e_n_D_i_s_p_l_a_y(3X11R6)

    _X_S_y_n_c_h_r_o_n_i_z_e(3X11R6)

    Xlib - C Language X Interface

