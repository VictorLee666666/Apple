XInstallColormap(3X11R6)                       XInstallColormap(3X11R6)

  XXIInnssttaallllCCoolloorrmmaapp

  NNAAMMEE

    XInstallColormap, XUninstallColormap, XListInstalledColormaps - control
    colormaps

  SSYYNNTTAAXX

    XInstallColormap(display, colormap)
          Display *display;
          Colormap colormap;

    XUninstallColormap(display, colormap)
          Display *display;
          Colormap colormap;

    Colormap *XListInstalledColormaps(display, w, num_return)
          Display *display;
          Window w;
          int *num_return;

  AARRGGUUMMEENNTTSS

    colormap
        Specifies the colormap.

    display
        Specifies the connection to the X server.

    num_return
        Returns the number of currently installed colormaps.

    w
        Specifies the window that determines the screen.

  DDEESSCCRRIIPPTTIIOONN

    The XXIInnssttaallllCCoolloorrmmaapp function installs the specified colormap for its
    associated screen. All windows associated with this colormap immediately
    display with true colors. You associated the windows with this colormap
    when you created them by calling XXCCrreeaatteeWWiinnddooww, XXCCrreeaatteeSSiimmpplleeWWiinnddooww,
    XXCChhaannggeeWWiinnddoowwAAttttrriibbuutteess, or XXSSeettWWiinnddoowwCCoolloorrmmaapp.

    If the specified colormap is not already an installed colormap, the X
    server generates a ColormapNotify event on each window that has that
    colormap. In addition, for every other colormap that is installed as a
    result of a call to XXIInnssttaallllCCoolloorrmmaapp, the X server generates a
    ColormapNotify event on each window that has that colormap.

    XXIInnssttaallllCCoolloorrmmaapp can generate a BadColor error.

    The XXUUnniinnssttaallllCCoolloorrmmaapp function removes the specified colormap from the
    required list for its screen. As a result, the specified colormap might be
    uninstalled, and the X server might implicitly install or uninstall
    additional colormaps. Which colormaps get installed or uninstalled is
    server dependent except that the required list must remain installed.

    If the specified colormap becomes uninstalled, the X server generates a
    ColormapNotify event on each window that has that colormap. In addition,
    for every other colormap that is installed or uninstalled as a result of a
    call to XXUUnniinnssttaallllCCoolloorrmmaapp, the X server generates a ColormapNotify event
    on each window that has that colormap.

    XXUUnniinnssttaallllCCoolloorrmmaapp can generate a BadColor error.

    The XXLLiissttIInnssttaalllleeddCCoolloorrmmaappss function returns a list of the currently
    installed colormaps for the screen of the specified window. The order of
    the colormaps in the list is not significant and is no explicit indication
    of the required list. When the allocated list is no longer needed, free it
    by using XXFFrreeee.

    XXLLiissttIInnssttaalllleeddCCoolloorrmmaappss can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_C_r_e_a_t_e_C_o_l_o_r_m_a_p(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w(3X11R6)

    _X_F_r_e_e(3X11R6)

    Xlib - C Language X Interface

