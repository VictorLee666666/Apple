XWarpPointer(3X11R6)                               XWarpPointer(3X11R6)

  XXWWaarrppPPooiinntteerr

  NNAAMMEE

    XWarpPointer - move pointer

  SSYYNNTTAAXX

    XWarpPointer(display, src_w, dest_w, src_x, src_y, src_width, src_height,
    dest_x,
                    dest_y)
            Display *display;
            Window src_w, dest_w;
            int src_x, src_y;
            unsigned int src_width, src_height;
            int dest_x, dest_y;

  AARRGGUUMMEENNTTSS

    dest_w
        Specifies the destination window or None.
    dest_x
    dest_y
        Specify the x and y coordinates within the destination window.

    display
        Specifies the connection to the X server.
    src_x
    src_y
    src_width
    src_height
        Specify a rectangle in the source window.

    src_w
        Specifies the source window or None.

  DDEESSCCRRIIPPTTIIOONN

    If dest_w is None, XXWWaarrppPPooiinntteerr moves the pointer by the offsets (dest_x,
    dest_y) relative to the current position of the pointer. If dest_w is a
    window, XXWWaarrppPPooiinntteerr moves the pointer to the offsets (dest_x, dest_y)
    relative to the origin of dest_w. However, if src_w is a window, the move
    only takes place if the window src_w contains the pointer and if the
    specified rectangle of src_w contains the pointer.

    The src_x and src_y coordinates are relative to the origin of src_w. If
    src_height is zero, it is replaced with the current height of src_w minus
    src_y. If src_width is zero, it is replaced with the current width of
    src_w minus src_x.

    There is seldom any reason for calling this function. The pointer should
    normally be left to the user. If you do use this function, however, it
    generates events just as if the user had instantaneously moved the pointer
    from one position to another. Note that you cannot use XXWWaarrppPPooiinntteerr to
    move the pointer outside the confine_to window of an active pointer grab.
    An attempt to do so will only move the pointer as far as the closest edge
    of the confine_to window.

    XXWWaarrppPPooiinntteerr can generate a BadWindow error.

  DDIIAAGGNNOOSSTTIICCSS

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_S_e_t_I_n_p_u_t_F_o_c_u_s(3X11R6)

    Xlib - C Language X Interface

