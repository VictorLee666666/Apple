XwcTextPerCharExtents(3X11R6)             XwcTextPerCharExtents(3X11R6)

  XXmmbbTTeexxttPPeerrCChhaarrEExxtteennttss

  NNAAMMEE

    XmbTextPerCharExtents, XwcTextPerCharExtents - obtain per-character
    information for a text string

  SSYYNNTTAAXX

    Status XmbTextPerCharExtents(font_set, string, num_bytes,
    ink_array_return,
              logical_array_return, array_size, num_chars_return, overall_ink_return,
              overall_logical_return)
          XFontSet font_set;
          char *string;
          int num_bytes;
          XRectangle *ink_array_return;
          XRectangle *logical_array_return;
          int array_size;
          int *num_chars_return;
          XRectangle *overall_ink_return;
          XRectangle *overall_logical_return;

    Status XwcTextPerCharExtents(font_set, string, num_wchars,
    ink_array_return,
              logical_array_return, array_size, num_chars_return, overall_ink_return,
              overall_logical_return)
          XFontSet font_set;
          wchar_t *string;
          int num_wchars;
          XRectangle *ink_array_return;
          XRectangle *logical_array_return;
          int array_size;
          int *num_chars_return;
          XRectangle *overall_ink_return;
          XRectangle *overall_logical_return;

  AARRGGUUMMEENNTTSS

    array_size
        Specifies the size of ink_array_return and logical_array_return. The
        caller must pass in arrays of this size.

    font_set
        Specifies the font set.

    ink_array_return
        Returns the ink dimensions for each character.

    logical_array_return
        Returns the logical dimensions for each character.

    num_bytes
        Specifies the number of bytes in the string argument.

    num_chars_return
        Returns the number of characters in the string argument.

    num_wchars
        Specifies the number of characters in the string argument.

    overall_ink_return
        Returns the overall ink extents of the entire string.

    overall_logical_return
        Returns the overall logical extents of the entire string.

    string
        Specifies the character string.

  DDEESSCCRRIIPPTTIIOONN

    The XXmmbbTTeexxttPPeerrCChhaarrEExxtteennttss and XXwwccTTeexxttPPeerrCChhaarrEExxtteennttss functions return the
    text dimensions of each character of the specified text, using the fonts
    loaded for the specified font set. Each successive element of
    ink_array_return and logical_array_return is set to the successive
    character's drawn metrics, relative to the drawing origin of the string
    and one rectangle for each character in the supplied text string. The
    number of elements of ink_array_return and logical_array_return that have
    been set is returned to num_chars_return.

    Each element of ink_array_return is set to the bounding box of the
    corresponding character's drawn foreground color. Each element of
    logical_array_return is set to the bounding box that provides minimum
    spacing to other graphical features for the corresponding character. Other
    graphical features should not intersect any of the logical_array_return
    rectangles.

    Note that an XXRReeccttaannggllee represents the effective drawing dimensions of the
    character, regardless of the number of font glyphs that are used to draw
    the character or the direction in which the character is drawn. If
    multiple characters map to a single character glyph, the dimensions of all
    the XRectangles of those characters are the same.

    When the XFontSet has missing charsets, metrics for each unavailable
    character are taken from the default string returned by XXCCrreeaatteeFFoonnttSSeett so
    that the metrics represent the text as it will actually be drawn. The
    behavior for an invalid codepoint is undefined.

    If the array_size is too small for the number of characters in the
    supplied text, the functions return zero and num_chars_return is set to
    the number of rectangles required. Otherwise, the functions return a
    nonzero value.

    If the overall_ink_return or overall_logical_return argument is non-NULL,
    XXmmbbTTeexxttPPeerrCChhaarrEExxtteennttss and XXwwccTTeexxttPPeerrCChhaarrEExxtteennttss return the maximum extent
    of the string's metrics to overall_ink_return or overall_logical_return,
    as returned by XXmmbbTTeexxttEExxtteennttss or XXwwccTTeexxttEExxtteennttss.

  SSEEEE AALLSSOO

    _X_m_b_T_e_x_t_E_s_c_a_p_e_m_e_n_t(3X11R6)

    _X_m_b_T_e_x_t_E_x_t_e_n_t_s(3X11R6)

    Xlib - C Language X Interface

