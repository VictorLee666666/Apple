XtCallbackNonexclusive(3X11R6)           XtCallbackNonexclusive(3X11R6)

  XXttPPooppuupp

  NNAAMMEE

    XtPopup, XtPopupSpringLoaded, XtCallbackNone, XtCallbackNonexclusive,
    XtCallbackExclusive - map a pop-up

  SSYYNNTTAAXX

    void XtPopup(popup_shell, grab_kind)
          Widget popup_shell;
          XtGrabKind grab_kind;

    void XtPopupSpringLoaded(popup_shell)
          Widget popup_shell;

    void XtCallbackNone(w, client_data, call_data)
          Widget w;
          XtPointer client_data;
          XtPointer call_data;

    void XtCallbackNonexclusive(w, client_data, call_data)
          Widget w;
          XtPointer client_data;
          XtPointer call_data;

    void XtCallbackExclusive(w, client_data, call_data)
          Widget w;
          XtPointer client_data;
          XtPointer call_data;

    void MenuPopup(shell_name)
          String shell_name;

  AARRGGUUMMEENNTTSS

    call_data
        Specifies the callback data, which is not used by this procedure.

    client_data
        Specifies the pop-up shell.

    grab_kind
        Specifies the way in which user events should be constrained.

    popup_shell
        Specifies the widget shell.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The XXttPPooppuupp function performs the following:

    *     Calls XXttCChheecckkSSuubbccllaassss to ensure popup_shell is a subclass of Shell.
    *     Generates an error if the shell's popped_up field is already True.
    *     Calls the callback procedures on the shell's popup_callback list.
    *     Sets the shell popped_up field to True, the shell spring_loaded
          field to False, and the shell grab_kind field from grab_kind.
    *     If the shell's create_popup_child field is non-NULL, XXttPPooppuupp calls
          it with popup_shell as the parameter.
    *     If grab_kind is either XtGrabNonexclusive or XtGrabExclusive, it
          calls:
          XtAddGrab(popup_shell, (grab_kind == XtGrabExclusive), False)
    *     Calls XXttRReeaalliizzeeWWiiddggeett with popup_shell specified.
    *     Calls XXMMaappWWiinnddooww with popup_shell specified.

    The XXttPPooppuuppSSpprriinnggLLooaaddeedd function performs exactly as XXttPPooppuupp except that
    it sets the shell spring_loaded field to True and always calls XXttAAddddGGrraabb
    with exclusive True and spring_loaded True.

    The XXttCCaallllbbaacckkNNoonnee, XXttCCaallllbbaacckkNNoonneexxcclluussiivvee, and XXttCCaallllbbaacckkEExxcclluussiivvee
    functions call XXttPPooppuupp with the shell specified by the client data
    argument and grab_kind set as the name specifies. XXttCCaallllbbaacckkNNoonnee,
    XXttCCaallllbbaacckkNNoonneexxcclluussiivvee, and XXttCCaallllbbaacckkEExxcclluussiivvee specify XtGrabNone,
    XtGrabNonexclusive, and XtGrabExclusive, respectively. Each function then
    sets the widget that executed the callback list to be insensitive by using
    XXttSSeettSSeennssiittiivvee. Using these functions in callbacks is not required. In
    particular, an application must provide customized code for callbacks that
    create pop-up shells dynamically or that must do more than desensitizing
    the button.

    MenuPopup is known to the translation manager, which must perform special
    actions for spring-loaded pop-ups. Calls to MenuPopup in a translation
    specification are mapped into calls to a nonexported action procedure, and
    the translation manager fills in parameters based on the event specified
    on the left-hand side of a translation.

    If MenuPopup is invoked on ButtonPress (possibly with modifiers), the
    translation manager pops up the shell with grab_kind set to
    XtGrabExclusive and spring_loaded set to True. If MenuPopup is invoked on
    EnterWindow (possibly with modifiers), the translation manager pops up the
    shell with grab_kind set to XtGrabNonexclusive and spring_loaded set to
    False. Otherwise, the translation manager generates an error. When the
    widget is popped up, the following actions occur:
    *     Calls XXttCChheecckkSSuubbccllaassss to ensure popup_shell is a subclass of Shell.
    *     Generates an error if the shell's popped_up field is already True.
    *     Calls the callback procedures on the shell's popup_callback list.
    *     Sets the shell popped_up field to True and the shell grab_kind and
          spring_loaded fields appropriately.
    *     If the shell's create_popup_child field is non-NULL, it is called
          with popup_shell as the parameter.
    *     Calls:
          XtAddGrab(popup_shell, (grab_kind == XtGrabExclusive),
          spring_loaded)
    *     Calls XXttRReeaalliizzeeWWiiddggeett with popup_shell specified.
    *     Calls XXMMaappWWiinnddooww with popup_shell specified.

    (Note that these actions are the same as those for XXttPPooppuupp.) MenuPopup
    tries to find the shell by searching the widget tree starting at the
    parent of the widget in which it is invoked. If it finds a shell with the
    specified name in the pop-up children of that parent, it pops up the shell
    with the appropriate parameters. Otherwise, it moves up the parent chain
    as needed. If MenuPopup gets to the application widget and cannot find a
    matching shell, it generates an error.

  SSEEEE AALLSSOO

    _X_t_C_r_e_a_t_e_P_o_p_u_p_S_h_e_l_l(3X11R6)

    _X_t_P_o_p_d_o_w_n(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

