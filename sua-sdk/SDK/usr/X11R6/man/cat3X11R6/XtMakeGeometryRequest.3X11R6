XtMakeGeometryRequest(3X11R6)             XtMakeGeometryRequest(3X11R6)

  XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt

  NNAAMMEE

    XtMakeGeometryRequest, XtMakeResizeRequest - make geometry manager request

  SSYYNNTTAAXX

    XtGeometryResult XtMakeGeometryRequest(w, request, reply_return)
          Widget w;
          XtWidgetGeometry *request;
          XtWidgetGeometry *reply_return;

    XtGeometryResult XtMakeResizeRequest(w, width, height, width_return,
    height_return)
          Widget w;
          Dimension width, height;
          Dimension *width_return, *height_return

  AARRGGUUMMEENNTTSS

    reply_return
        Returns the allowed widget size or may be NULL if the requesting
        widget is not interested in handling XtGeometryAlmost.

    request
        Specifies the desired widget geometry (size, position, border width,
        and stacking order).

    w
        Specifies the widget that is making the request.
    width_return
    height_return
        Return the allowed widget width and height.

  DDEESSCCRRIIPPTTIIOONN

    Depending on the condition, XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt performs the following:

    *     If the widget is unmanaged or the widget's parent is not realized,
          it makes the changes and returns XtGeometryYes.
    *     If the parent is not a subclass of compositeWidgetClass or the
          parent's geometry_manager is NULL, it issues an error.
    *     If the widget's being_destroyed field is True, it returns
          XtGeometryNo.
    *     If the widget x, y, width, height and border_width fields are all
          equal to the requested values, it returns XtGeometryYes; otherwise,
          it calls the parent's geometry_manager procedure with the given
          parameters.
    *     If the parent's geometry manager returns XtGeometryYes and if
          XtCWQueryOnly is not set in the request_mode and if the widget is
          realized, XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt calls the XXCCoonnffiigguurreeWWiinnddooww Xlib
          function to reconfigure the widget's window (set its size, location,
          and stacking order as appropriate).
    *     If the geometry manager returns XtGeometryDone, the change has been
          approved and actually has been done. In this case,
          XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt does no configuring and returns XtGeometryYes.
          XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt never returns XtGeometryDone.

    Otherwise, XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt returns the resulting value from the
    parent's geometry manager.

    Children of primitive widgets are always unmanaged; thus,
    XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt always returns XtGeometryYes when called by a child
    of a primitive widget.

    The XXttMMaakkeeRReessiizzeeRReeqquueesstt function, a simple interface to
    XXttMMaakkeeGGeeoommeettrryyRReeqquueesstt, creates a XtWidgetGeometry structure and specifies
    that width and height should change. The geometry manager is free to
    modify any of the other window attributes (position or stacking order) to
    satisfy the resize request. If the return value is XtGeometryAlmost,
    width_return and height_return contain a compromise width and height. If
    these are acceptable, the widget should immediately make an
    XXttMMaakkeeRReessiizzeeRReeqquueesstt and request that the compromise width and height be
    applied. If the widget is not interested in XtGeometryAlmost replies, it
    can pass NULL for width_return and height_return.

  SSEEEE AALLSSOO

    _X_t_C_o_n_f_i_g_u_r_e_W_i_d_g_e_t(3X11R6)

    _X_t_Q_u_e_r_y_G_e_o_m_e_t_r_y(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

