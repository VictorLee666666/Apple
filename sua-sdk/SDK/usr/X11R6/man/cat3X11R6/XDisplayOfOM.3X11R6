XDisplayOfOM(3X11R6)                               XDisplayOfOM(3X11R6)

  XXOOppeennOOMM

  NNAAMMEE

    XOpenOM, XCloseOM, XSetOMValues, XGetOMValues, XDisplayOfOM, XLocaleOfOM -
    open output methods

  SSYYNNTTAAXX

    XOM XOpenOM(display, db, res_name, res_class)
          Display *display;
          XrmDatabase db;
          char *res_name;
          char *res_class;

    Status XCloseOM(om)
          XOM om;

    char * XSetOMValues(om, ...)
          XOM om; 

    char * XGetOMValues(om, ...)
          XOM om; 

    Display * XDisplayOfOM(om)
           XOM om;

    char * XLocaleOfOM(om)
          XOM om; 

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    db
        Specifies a pointer to the resource database.

    res_name
        Specifies the full resource name of the application.

    res_class
        Specifies the full class name of the application.

    om
        Specifies the output method.

    ...
        Specifies the variable length argument list to set or get XOM values.

  DDEESSCCRRIIPPTTIIOONN

    The XXOOppeennOOMM function opens an output method matching the current locale
    and modifiers specification. The current locale and modifiers are bound to
    the output method when XXOOppeennOOMM is called. The locale associated with an
    output method cannot be changed.

    The specific output method to which this call will be routed is identified
    on the basis of the current locale and modifiers. XXOOppeennOOMM will identify a
    default output method corresponding to the current locale. That default
    can be modified using XXSSeettLLooccaalleeMMooddiiffiieerrss to set the output method
    modifier.

    The db argument is the resource database to be used by the output method
    for looking up resources that are private to the output method. It is not
    intended that this database be used to look up values that can be set as
    OC values in an output context. If db is NULL, no database is passed to
    the output method.

    The res_name and res_class arguments specify the resource name and class
    of the application. They are intended to be used as prefixes by the output
    method when looking up resources that are common to all output contexts
    that may be created for this output method. The characters used for
    resource names and classes must be in the X Portable Character Set. The
    resources looked up are not fully specified if res_name or res_class is
    NULL.

    The res_name and res_class arguments are not assumed to exist beyond the
    call to XXOOppeennOOMM. The specified resource database is assumed to exist for
    the lifetime of the output method.

    XXOOppeennOOMM returns NULL if no output method could be opened.

    The XXCClloosseeOOMM function closes the specified output method.

    The XXSSeettOOMMVVaalluueess function presents a variable argument list programming
    interface for setting properties or features of the specified output
    method. This function returns NULL if it succeeds; otherwise, it returns
    the name of the first argument that could not be obtained.

    No standard arguments are currently defined by Xlib.

    The XXGGeettOOMMVVaalluueess function presents a variable argument list programming
    interface for querying properties or features of the specified output
    method. This function returns NULL if it succeeds; otherwise, it returns
    the name of the first argument that could not be obtained.

    The XXDDiissppllaayyOOffOOMM function returns the display associated with the
    specified output method.

    The XXLLooccaalleeOOffOOMM returns the locale associated with the specified output
    method.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_O_C(3X11R6)

    _X_C_r_e_a_t_e_F_o_n_t_S_e_t(3X11R6)

    Xlib - C Language X Interface

