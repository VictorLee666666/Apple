XtGetActionList(3X11R6)                         XtGetActionList(3X11R6)

  XXttGGeettAAccttiioonnLLiisstt

  NNAAMMEE

    XtGetActionList - obtain class action list

  SSYYNNTTAAXX

    void XtGetActionList(widget_class, actions_return, num_actions_return)
          WidgetClass widget_class;
          XtActionList* actions_return;
          Cardinal* num_actions_return;

  AARRGGUUMMEENNTTSS

    widget_class
        Specifies the widget class whose actions are to be returned.

    actions_return
        Returns the action list.

    num_actions_return
        Returns the number of action procedures declared by the class.

  DDEESSCCRRIIPPTTIIOONN

    XXttGGeettAAccttiioonnLLiisstt returns the action table defined by the specified widget
    class. This table does not include actions defined by the superclasses. If
    widget_class is not initialized, or is not coreWidgetClass or a subclass
    thereof, or if the class does not define any actions, *actions_return will
    be NULL and *num_actions_return will be zero. If *actions_return is non-
    NULL the client is responsible for freeing the table using XXttFFrreeee when it
    is no longer needed.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

