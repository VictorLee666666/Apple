XSelectionClearEvent(3X11R6)               XSelectionClearEvent(3X11R6)

  XXSSeelleeccttiioonnCClleeaarrEEvveenntt

  NNAAMMEE

    XSelectionClearEvent - SelectionClear event structure

  SSTTRRUUCCTTUURREESS

    The structure for SelectionClear events contains:

    typedef struct {
         int type;                /* SelectionClear */
         unsigned long serial;    /* # of last request processed by server */
         Bool send_event;         /* true if this came from a SendEvent
    request */
         Display *display;        /* Display the event was read from */
         Window window;
         Atom selection;
         Time time;
    } XSelectionClearEvent;

    When you receive this event, the structure members are set as follows.

    The type member is set to the event type constant name that uniquely
    identifies it. For example, when the X server reports a GraphicsExpose
    event to a client application, it sends an XXGGrraapphhiiccssEExxppoosseeEEvveenntt structure
    with the type member set to GraphicsExpose. The display member is set to a
    pointer to the display the event was read on. The send_event member is set
    to True if the event came from a SendEvent protocol request. The serial
    member is set from the serial number reported in the protocol but expanded
    from the 16-bit least-significant bits to a full 32-bit value. The window
    member is set to the window that is most useful to toolkit dispatchers.

    The selection member is set to the selection atom. The time member is set
    to the last change time recorded for the selection. The window member is
    the window that was specified by the current owner (the owner losing the
    selection) in its XXSSeettSSeelleeccttiioonnOOwwnneerr call.

  SSEEEE AALLSSOO

    _X_A_n_y_E_v_e_n_t(3X11R6)

    _X_B_u_t_t_o_n_E_v_e_n_t(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_E_v_e_n_t(3X11R6)

    _X_C_i_r_c_u_l_a_t_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_o_l_o_r_m_a_p_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_E_v_e_n_t(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_C_r_o_s_s_i_n_g_E_v_e_n_t(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w_E_v_e_n_t(3X11R6)

    _X_E_r_r_o_r_E_v_e_n_t(3X11R6)

    _X_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_F_o_c_u_s_C_h_a_n_g_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_p_h_i_c_s_E_x_p_o_s_e_E_v_e_n_t(3X11R6)

    _X_G_r_a_v_i_t_y_E_v_e_n_t(3X11R6)

    _X_K_e_y_m_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_E_v_e_n_t(3X11R6)

    _X_M_a_p_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_P_r_o_p_e_r_t_y_E_v_e_n_t(3X11R6)

    _X_R_e_p_a_r_e_n_t_E_v_e_n_t(3X11R6)

    _X_R_e_s_i_z_e_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_E_v_e_n_t(3X11R6)

    _X_S_e_l_e_c_t_i_o_n_R_e_q_u_e_s_t_E_v_e_n_t(3X11R6)

    _X_S_e_t_S_e_l_e_c_t_i_o_n_O_w_n_e_r(3X11R6)

    _X_U_n_m_a_p_E_v_e_n_t(3X11R6)

    Xlib - C Language X Interface

