XtScreenDatabase(3X11R6)                       XtScreenDatabase(3X11R6)

  XXttDDiissppllaayyIInniittiiaalliizzee

  NNAAMMEE

    XtDisplayInitialize, XtOpenDisplay, XtDatabase, XtScreenDatabase,
    XtCloseDisplay - initialize, open, or close a display

  SSYYNNTTAAXX

    void XtDisplayInitialize(app_context, display, application_name,
    application_class,
                           options, num_options, argc, argv)
          XtAppContext app_context;
          Display *display;
          String application_name;
          String application_class;
          XrmOptionDescRec *options;
          Cardinal num_options;
          int *argc;
          String *argv;

    Display *XtOpenDisplay(app_context, display_string, application_name,
    application_class,
                           options, num_options, argc, argv)
           XtAppContext app_context;
           String display_string;
           String application_name;
           String application_class;
           XrmOptionDescRec *options;
           Cardinal num_options;
           int *argc;
           String *argv;

    void XtCloseDisplay(display)
          Display *display;

    XrmDatabase XtDatabase(display)
          Display *display;

    XrmDatabase XtScreenDatabase(screen)
          Screen* screen;

  AARRGGUUMMEENNTTSS

    argc
        Specifies a pointer to the number of command line parameters.

    argv
        Specifies the command line parameters.

    app_context
        Specifies the application context.

    application_class
        Specifies the class name of this application, which usually is the
        generic name for all instances of this application.

    application_name
        Specifies the name of the application instance.

    display
        Specifies the display. Note that a display can be in at most one
        application context.

    num_options
        Specifies the number of entries in the options list.

    options
        Specifies how to parse the command line for any application-specific
        resources. The options argument is passed as a parameter to
        XXrrmmPPaarrsseeCCoommmmaanndd. For further information, see Xlib - C Language X
        Interface.

    screen
        Specifies the screen whose resource database is to be returned.

  DDEESSCCRRIIPPTTIIOONN

    The XXttDDiissppllaayyIInniittiiaalliizzee function builds the resource database, calls the
    Xlib XXrrmmPPaarrsseeCCoommmmaanndd function to parse the command line, and performs
    other per display initialization. After XXrrmmPPaarrsseeCCoommmmaanndd has been called,
    argc and argv contain only those parameters that were not in the standard
    option table or in the table specified by the options argument. If the
    modified argc is not zero, most applications simply print out the modified
    argv along with a message listing the allowable options. On UNIX-based
    systems, the application name is usually the final component of argv[0].
    If the synchronize resource is True for the specified application,
    XXttDDiissppllaayyIInniittiiaalliizzee calls the Xlib XXSSyynncchhrroonniizzee function to put Xlib into
    synchronous mode for this display connection. If the reverseVideo resource
    is True, the Intrinsics exchange XtDefaultForeground and
    XtDefaultBackground for widgets created on this display. (See Section
    9.6.1).

    The XXttOOppeennDDiissppllaayy function calls XXOOppeennDDiissppllaayy the specified display name.
    If display_string is NULL, XXttOOppeennDDiissppllaayy uses the current value of the -
    display option specified in argv and if no display is specified in argv,
    uses the user's default display (on UNIX-based systems, this is the value
    of the DISPLAY environment variable).

    If this succeeds, it then calls XXttDDiissppllaayyIInniittiiaalliizzee and pass it the opened
    display and the value of the -name option specified in argv as the
    application name. If no name option is specified, it uses the application
    name passed to XXttOOppeennDDiissppllaayy. If the application name is NULL, it uses the
    last component of argv[0]. XXttOOppeennDDiissppllaayy returns the newly opened display
    or NULL if it failed.

    XXttOOppeennDDiissppllaayy is provided as a convenience to the application programmer.

    The XXttCClloosseeDDiissppllaayy function closes the specified display as soon as it is
    safe to do so. If called from within an event dispatch (for example, a
    callback procedure), XXttCClloosseeDDiissppllaayy does not close the display until the
    dispatch is complete. Note that applications need only call XXttCClloosseeDDiissppllaayy
    if they are to continue executing after closing the display; otherwise,
    they should call XXttDDeessttrrooyyAApppplliiccaattiioonnCCoonntteexxtt or just exit.

    The XXttDDaattaabbaassee function returns the fully merged resource database that
    was built by XXttDDiissppllaayyIInniittiiaalliizzee associated with the display that was
    passed in. If this display has not been initialized by
    XXttDDiissppllaayyIInniittiiaalliizzee, the results are not defined.

    The XXttSSccrreeeennDDaattaabbaassee function returns the fully merged resource database
    associated with the specified screen. If the screen does not belong to a
    Display initilized by XXttDDiissppllaayyIInniittiiaalliizzee, the results are undefined.

  SSEEEE AALLSSOO

    _X_t_A_p_p_C_r_e_a_t_e_S_h_e_l_l(3X11R6)

    _X_t_C_r_e_a_t_e_A_p_p_l_i_c_a_t_i_o_n_C_o_n_t_e_x_t(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

