XtSetSensitive(3X11R6)                           XtSetSensitive(3X11R6)

  XXttSSeettSSeennssiittiivvee

  NNAAMMEE

    XtSetSensitive, XtIsSensitive - set and check a widget's sensitivity state

  SSYYNNTTAAXX

    void XtSetSensitive(w, sensitive)
          Widget w;
          Boolean sensitive;

    Boolean XtIsSensitive(w)
         Widget w;

  AARRGGUUMMEENNTTSS

    sensitive
        Specifies a Boolean value that indicates whether the widget should
        receive keyboard and pointer events.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The XXttSSeettSSeennssiittiivvee function first calls XXttSSeettVVaalluueess on the current widget
    with an argument list specifying that the sensitive field should change to
    the new value. It then recursively propagates the new value down the
    managed children tree by calling XXttSSeettVVaalluueess on each child to set the
    ancestor_sensitive to the new value if the new values for sensitive and
    the child's ancestor_sensitive are not the same.

    XXttSSeettSSeennssiittiivvee calls XXttSSeettVVaalluueess to change sensitive and
    ancestor_sensitive. Therefore, when one of these changes, the widget's
    set_values procedure should take whatever display actions are needed (for
    example, greying out or stippling the widget).

    XXttSSeettSSeennssiittiivvee maintains the invariant that if parent has either sensitive
    or ancestor_sensitive False, then all children have ancestor_sensitive
    False.

    The XXttIIssSSeennssiittiivvee function returns True or False to indicate whether or
    not user input events are being dispatched. If both core.sensitive and
    core.ancestor_sensitive are True, XXttIIssSSeennssiittiivvee returns True; otherwise,
    it returns False.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

