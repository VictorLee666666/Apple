XtGetResourceList(3X11R6)                     XtGetResourceList(3X11R6)

  XXttGGeettRReessoouurrcceeLLiisstt

  NNAAMMEE

    XtGetResourceList, XtGetConstraintResourceList - obtain resource list

  SSYYNNTTAAXX

    void XtGetResourceList(class, resources_return, num_resources_return);
          WidgetClass class;
          XtResourceList *resources_return;
          Cardinal *num_resources_return;

    void XtGetConstraintResourceList(class, resources_return,
    num_resources_return);
          WidgetClass class;
          XtResourceList *resources_return;
          Cardinal *num_resources_return;

  AARRGGUUMMEENNTTSS

    num_resources_return
        Specifies a pointer to where to store the number of entries in the
        resource list.

    resources_return
        Specifies a pointer to where to store the returned resource list. The
        caller must free this storage using XXttFFrreeee when done with it.

    widget_class
        Specifies the widget class.

  DDEESSCCRRIIPPTTIIOONN

    If XXttGGeettRReessoouurrcceeLLiisstt is called before the widget class is initialized
    (that is, before the first widget of that class has been created),
    XXttGGeettRReessoouurrcceeLLiisstt returns the resource list as specified in the widget
    class record. If it is called after the widget class has been initialized,
    XXttGGeettRReessoouurrcceeLLiisstt returns a merged resource list that contains the
    resources for all superclasses. The list returned by XXttGGeettRReessoouurrcceeLLiisstt
    should be freed using XXttFFrreeee when it is no longer needed.

    If XXttGGeettCCoonnssttrraaiinnttRReessoouurrcceeLLiisstt is called before the widget class is
    initialized (that is, before the first widget of that class has been
    created), XXttGGeettCCoonnssttrraaiinnttRReessoouurrcceeLLiisstt returns the resource list as
    specified in the widget class Constraint part record. If it is called
    after the widget class has been initialized, XXttGGeettCCoonnssttrraaiinnttRReessoouurrcceeLLiisstt
    returns a merged resource list that contains the Constraint resources for
    all superclasses. If the specified class is not a subclass of
    constraintWidgetClass, *resources_return is set to NULL and
    *num_resources_return is set to zero. The list returned by
    XXttGGeettCCoonnssttrraaiinnttRReessoouurrcceeLLiisstt should be freed using XXttFFrreeee when it is no
    longer needed.

  SSEEEE AALLSSOO

    _X_t_G_e_t_S_u_b_r_e_s_o_u_r_c_e_s(3X11R6)

    _X_t_O_f_f_s_e_t(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

