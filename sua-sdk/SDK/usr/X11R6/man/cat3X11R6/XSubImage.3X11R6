XSubImage(3X11R6)                                     XSubImage(3X11R6)

  XXCCrreeaatteeIImmaaggee

  NNAAMMEE

    XInitImage, XCreateImage, XGetPixel, XPutPixel, XSubImage, XAddPixel,
    XDestroyImage - image utilities

  SSYYNNTTAAXX

    Status XInitImage(image)
          XImage *image;

    XImage *XCreateImage(display, visual, depth, format, offset, data, width,
    height, bitmap_pad,
                            bytes_per_line)
          Display *display;
          Visual *visual;
          unsigned int depth;
          int format;
          int offset;
          char *data;
          unsigned int width;
          unsigned int height;
          int bitmap_pad;
          int bytes_per_line;

    unsigned long XGetPixel(ximage, x, y)
          XImage *ximage;
          int x;
          int y;

    XPutPixel(ximage, x, y, pixel)
          XImage *ximage;
          int x;
          int y;
          unsigned long pixel;

    XImage *XSubImage(ximage, x, y, subimage_width, subimage_height)
          XImage *ximage;
          int x;
          int y;
          unsigned int subimage_width;
          unsigned int subimage_height;

    XAddPixel(ximage, value)
          XImage *ximage;
          long value;

    XDestroyImage(ximage)
            XImage *ximage; 

  AARRGGUUMMEENNTTSS

    bitmap_pad
        Specifies the quantum of a scanline (8, 16, or 32). In other words,
        the start of one scanline is separated in client memory from the start
        of the next scanline by an integer multiple of this many bits.

    bytes_per_line
        Specifies the number of bytes in the client image between the start of
        one scanline and the start of the next.

    data
        Specifies the image data.

    depth
        Specifies the depth of the image.

    display
        Specifies the connection to the X server.

    format
        Specifies the format for the image. You can pass XYBitmap, XYPixmap,
        or ZPixmap.

    height
        Specifies the height of the image, in pixels.

    offset
        Specifies the number of pixels to ignore at the beginning of the
        scanline.

    pixel
        Specifies the new pixel value.

    subimage_height
        Specifies the height of the new subimage, in pixels.

    subimage_width
        Specifies the width of the new subimage, in pixels.

    value
        Specifies the constant value that is to be added.

    visual
        Specifies the Visual structure.

    width
        Specifies the width of the image, in pixels.

    ximage
        Specifies the image.
    x
    y
        Specify the x and y coordinates.

  DDEESSCCRRIIPPTTIIOONN

    The XXIInniittIImmaaggee function initializes the internal image manipulation
    routines of an image structure, based on the values of the various
    structure members. All fields other than the manipulation routines must
    already be initialized. If the bytes_per_line member is zero, XXIInniittIImmaaggee
    will assume the image data is contiguous in memory and set the
    bytes_per_line member to an appropriate value based on the other members;
    otherwise, the value of bytes_per_line is not changed. All of the
    manipulation routines are initialized to functions that other Xlib image
    manipulation functions need to operate on the type of image specified by
    the rest of the structure.

    This function must be called for any image constructed by the client
    before passing it to any other Xlib function. Image structures created or
    returned by Xlib do not need to be initialized in this fashion.

    This function returns a nonzero status if initialization of the structure
    is successful. It returns zero if it detected some error or inconsistency
    in the structure, in which case the image is not changed.

    The XXCCrreeaatteeIImmaaggee function allocates the memory needed for an XImage
    structure for the specified display but does not allocate space for the
    image itself. Rather, it initializes the structure byte-order, bit-order,
    and bitmap-unit values from the display and returns a pointer to the
    XImage structure. The red, green, and blue mask values are defined for Z
    format images only and are derived from the Visual structure passed in.
    Other values also are passed in. The offset permits the rapid displaying
    of the image without requiring each scanline to be shifted into position.
    If you pass a zero value in bytes_per_line, Xlib assumes that the
    scanlines are contiguous in memory and calculates the value of
    bytes_per_line itself.

    Note that when the image is created using XXCCrreeaatteeIImmaaggee, XXGGeettIImmaaggee, or
    XXSSuubbIImmaaggee, the destroy procedure that the XXDDeessttrrooyyIImmaaggee function calls
    frees both the image structure and the data pointed to by the image
    structure.

    The basic functions used to get a pixel, set a pixel, create a subimage,
    and add a constant value to an image are defined in the image object. The
    functions in this section are really macro invocations of the functions in
    the image object and are defined in <X11/Xutil.h>.

    The XXGGeettPPiixxeell function returns the specified pixel from the named image.
    The pixel value is returned in normalized format (that is, the least
    significant byte of the long is the least significant byte of the pixel).
    The image must contain the x and y coordinates.

    The XXPPuuttPPiixxeell function overwrites the pixel in the named image with the
    specified pixel value. The input pixel value must be in normalized format
    (that is, the least significant byte of the long is the least significant
    byte of the pixel). The image must contain the x and y coordinates.

    The XXSSuubbIImmaaggee function creates a new image that is a subsection of an
    existing one. It allocates the memory necessary for the new XImage
    structure and returns a pointer to the new image. The data is copied from
    the source image, and the image must contain the rectangle defined by x,
    y, subimage_width, and subimage_height.

    The XXAAddddPPiixxeell function adds a constant value to every pixel in an image.
    It is useful when you have a base pixel value from allocating color
    resources and need to manipulate the image to that form.

    The XXDDeessttrrooyyIImmaaggee function deallocates the memory associated with the
    XImage structure.

  SSEEEE AALLSSOO

    _X_P_u_t_I_m_a_g_e(3X11R6)

    Xlib - C Language X Interface

