XtGetGC(3X11R6)                                         XtGetGC(3X11R6)

  XXttGGeettGGCC

  NNAAMMEE

    XtGetGC, XtReleaseGC - obtain and destroy a sharable GC

  SSYYNNTTAAXX

    GC XtGetGC(w, value_mask, values)
          Widget w;
          XtGCMask value_mask;
          XGCValues *values;

    void XtReleaseGC(w, gc)
          Widget w;
          GC gc;

  AARRGGUUMMEENNTTSS

    gc
        Specifies the GC to be deallocated.

    values
        Specifies the actual values for this GC.

    value_mask
        Specifies which fields of the values are specified.

    w
        Specifies the widget.

  DDEESSCCRRIIPPTTIIOONN

    The XXttGGeettGGCC function returns a sharable, read-only GC. The parameters to
    this function are the same as those for XXCCrreeaatteeGGCC except that a widget is
    passed instead of a display. XXttGGeettGGCC shares only GCs in which all values
    in the GC returned by XXCCrreeaatteeGGCC are the same. In particular, it does not
    use the value_mask provided to determine which fields of the GC a widget
    considers relevant. The value_mask is used only to tell the server which
    fields should be filled in with widget data and which it should fill in
    with default values. For further information about value_mask and values,
    see XXCCrreeaatteeGGCC in the Xlib - C Language X Interface.

    The XXttRReelleeaasseeGGCC function deallocate the specified shared GC.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

