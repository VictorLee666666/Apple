XGetDeviceButtonMapping(3X11R6)          XGetDeviceButtonMapping(3X11R6)

  XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg

  NNAAMMEE

    XSetDeviceButtonMapping, XGetDeviceButtonMapping - query or change device
    button mappings

  SSYYNNTTAAXX

    int XSetDeviceButtonMapping(display, device, map, nmap)
          Display *display;
          XDevice *device;
          unsigned char map[];
          int nmap;
    int XGetDeviceButtonMapping(display, device, map_return, nmap)
          Display *display;
          XDevice *device;
          unsigned char map_return[];
          int nmap;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device whose button mapping is to be queried or changed.

    map
        Specifies the mapping list.

    map_return
        Returns the mapping list.

    nmap
        Specifies the number of items in the mapping list.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg request sets the mapping of the specified
    device. If it succeeds, the X server generates a DeviceMappingNotify
    event, and XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg returns MappingSuccess. Element map[i]
    defines the logical button number for the physical button i+1. The length
    of the list must be the same as XXGGeettDDeevviicceeBBuuttttoonnMMaappppiinngg would return, or a
    BadValue error results. A zero element disables a button, and elements are
    not restricted in value by the number of physical buttons. However, no two
    elements can have the same nonzero value, or a BadValue error results. If
    any of the buttons to be altered are logically in the down state,
    XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg returns MappingBusy, and the mapping is not
    changed.

    XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg can generate BadDevice, BadMatch, and BadValue
    errors.

    The XXGGeettDDeevviicceeBBuuttttoonnMMaappppiinngg request returns the current mapping of the
    specified device. Buttons are numbered starting from one.
    XXGGeettDDeevviicceeBBuuttttoonnMMaappppiinngg returns the number of physical buttons actually on
    the device. The nominal mapping for a device is map[i]=i+1. The nmap
    argument specifies the length of the array where the device mapping is
    returned, and only the first nmap elements are returned in map_return.

    XXGGeettDDeevviicceeBBuuttttoonnMMaappppiinngg can generate BadDevice or BadMatch errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist
        or has not been opened by this client via XOpenInputDevice. This error
        may also occur if the specified device is the X keyboard or X pointer
        device.

    BadMatch
        This error may occur if an XXGGeettDDeevviicceeBBuuttttoonnMMaappppiinngg or
        XXSSeettDDeevviicceeBBuuttttoonnMMaappppiinngg request was made specifying a device that has
        no buttons.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_D_e_v_i_c_e_K_e_y_M_a_p_p_i_n_g(3X11R6)

    Programming With Xlib

