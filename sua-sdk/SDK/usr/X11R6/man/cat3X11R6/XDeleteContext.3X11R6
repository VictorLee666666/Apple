XDeleteContext(3X11R6)                           XDeleteContext(3X11R6)

  XXSSaavveeCCoonntteexxtt

  NNAAMMEE

    XSaveContext, XFindContext, XDeleteContext, XUniqueContext - associative
    look-up routines

  SSYYNNTTAAXX

    int XSaveContext(display, rid, context, data)
          Display *display; 
          XID rid;
          XContext context;
          XPointer data;

    int XFindContext(display, rid, context, data_return)
          Display *display;
          XID rid;
          XContext context;
          XPointer *data_return;    

    int XDeleteContext(display, rid, context)
          Display *display;
          XID rid;
          XContext context;

    XContext XUniqueContext()

  AARRGGUUMMEENNTTSS

    context
        Specifies the context type to which the data belongs.

    data
        Specifies the data to be associated with the window and type.

    data_return
        Returns the data.

    display
        Specifies the connection to the X server.

    rid
        Specifies the resource ID with which the data is associated.

  DDEESSCCRRIIPPTTIIOONN

    If an entry with the specified resource ID and type already exists,
    XXSSaavveeCCoonntteexxtt overrides it with the specified context. The XXSSaavveeCCoonntteexxtt
    function returns a nonzero error code if an error has occurred and zero
    otherwise. Possible errors are XCNOMEM (out of memory).

    Because it is a return value, the data is a pointer. The XXFFiinnddCCoonntteexxtt
    function returns a nonzero error code if an error has occurred and zero
    otherwise. Possible errors are XCNOENT (context-not-found).

    The XXDDeelleetteeCCoonntteexxtt function deletes the entry for the given resource ID
    and type from the data structure. This function returns the same error
    codes that XXFFiinnddCCoonntteexxtt returns if called with the same arguments.
    XXDDeelleetteeCCoonntteexxtt does not free the data whose address was saved.

    The XXUUnniiqquueeCCoonntteexxtt function creates a unique context type that may be used
    in subsequent calls to XXSSaavveeCCoonntteexxtt.

  SSEEEE AALLSSOO

    Xlib - C Language X Interface

