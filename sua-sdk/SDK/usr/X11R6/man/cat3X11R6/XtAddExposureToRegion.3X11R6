XtAddExposureToRegion(3X11R6)             XtAddExposureToRegion(3X11R6)

  XXttAAddddEExxppoossuurreeTTooRReeggiioonn

  NNAAMMEE

    XtAddExposureToRegion - merge exposure events into a region

  SSYYNNTTAAXX

    void XtAddExposureToRegion(event, region)
         XEvent *event;
         Region region;

  AARRGGUUMMEENNTTSS

    event
        Specifies a pointer to the Expose or GraphicsExpose event.

    region
        Specifies the region object (as defined in <X11/Xutil.h>).

  DDEESSCCRRIIPPTTIIOONN

    The XXttAAddddEExxppoossuurreeTTooRReeggiioonn function computes the union of the rectangle
    defined by the exposure event and the specified region. Then, it stores
    the results back in region. If the event argument is not an Expose or
    GraphicsExpose event, XXttAAddddEExxppoossuurreeTTooRReeggiioonn returns without an error and
    without modifying region.

    This function is used by the exposure compression mechanism (see Section
    7.9.3).

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

