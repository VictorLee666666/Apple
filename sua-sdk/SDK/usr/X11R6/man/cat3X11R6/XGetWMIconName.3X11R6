XGetWMIconName(3X11R6)                           XGetWMIconName(3X11R6)

  XXSSeettWWMMIIccoonnNNaammee

  NNAAMMEE

    XSetWMIconName, XGetWMIconName, XSetIconName, XGetIconName - set or read a
    window's WM_ICON_NAME property

  SSYYNNTTAAXX

    void XSetWMIconName(display, w, text_prop)
          Display *display;
          Window w;
          XTextProperty *text_prop;

    Status XGetWMIconName(display, w, text_prop_return)
          Display *display;
          Window w;
          XTextProperty *text_prop_return;

    XSetIconName(display, w, icon_name)
          Display *display;
          Window w;
          char *icon_name;

    Status XGetIconName(display, w, icon_name_return)
          Display *display;
          Window w;
          char **icon_name_return;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    icon_name
        Specifies the icon name, which should be a null-terminated string.

    icon_name_return
        Returns the window's icon name, which is a null-terminated string.

    text_prop
        Specifies the XXTTeexxttPPrrooppeerrttyy structure to be used.

    text_prop_return
        Returns the XXTTeexxttPPrrooppeerrttyy structure.

    w
        Specifies the window.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettWWMMIIccoonnNNaammee convenience function calls XXSSeettTTeexxttPPrrooppeerrttyy to set the
    WM_ICON_NAME property.

    The XXGGeettWWMMIIccoonnNNaammee convenience function calls XXGGeettTTeexxttPPrrooppeerrttyy to obtain
    the WM_ICON_NAME property. It returns a nonzero status on success;
    otherwise, it returns a zero status.

    The XXSSeettIIccoonnNNaammee function sets the name to be displayed in a window's
    icon.

    XXSSeettIIccoonnNNaammee can generate BadAlloc and BadWindow errors.

    The XXGGeettIIccoonnNNaammee function returns the name to be displayed in the
    specified window's icon. If it succeeds, it returns a nonzero status;
    otherwise, if no icon name has been set for the window, it returns zero.
    If you never assigned a name to the window, XXGGeettIIccoonnNNaammee sets
    icon_name_return to NULL. If the data returned by the server is in the
    Latin Portable Character Encoding, then the returned string is in the Host
    Portable Character Encoding. Otherwise, the result is implementation-
    dependent. When finished with it, a client must free the icon name string
    using XXFFrreeee.

    XXGGeettIIccoonnNNaammee can generate a BadWindow error.

  PPRROOPPEERRTTIIEESS

    WM_ICON_NAME
        The name to be used in an icon.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_l_a_s_s_H_i_n_t(3X11R6)

    _X_A_l_l_o_c_I_c_o_n_S_i_z_e(3X11R6)

    _X_A_l_l_o_c_S_i_z_e_H_i_n_t_s(3X11R6)

    _X_A_l_l_o_c_W_M_H_i_n_t_s(3X11R6)

    _X_F_r_e_e(3X11R6)

    _X_S_e_t_C_o_m_m_a_n_d(3X11R6)

    _X_S_e_t_T_r_a_n_s_i_e_n_t_F_o_r_H_i_n_t(3X11R6)

    _X_S_e_t_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    _X_S_e_t_W_M_C_l_i_e_n_t_M_a_c_h_i_n_e(3X11R6)

    _X_S_e_t_W_M_C_o_l_o_r_m_a_p_W_i_n_d_o_w_s(3X11R6)

    _X_S_e_t_W_M_N_a_m_e(3X11R6)

    _X_S_e_t_W_M_P_r_o_p_e_r_t_i_e_s(3X11R6)

    _X_S_e_t_W_M_P_r_o_t_o_c_o_l_s(3X11R6)

    _X_S_t_r_i_n_g_L_i_s_t_T_o_T_e_x_t_P_r_o_p_e_r_t_y(3X11R6)

    Xlib - C Language X Interface

