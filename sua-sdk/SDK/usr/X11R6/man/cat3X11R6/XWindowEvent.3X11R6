XWindowEvent(3X11R6)                               XWindowEvent(3X11R6)

  XXNNeexxttEEvveenntt

  NNAAMMEE

    XNextEvent, XPeekEvent, XWindowEvent, XCheckWindowEvent, XMaskEvent,
    XCheckMaskEvent, XCheckTypedEvent, XCheckTypedWindowEvent - select events
    by type

  SSYYNNTTAAXX

    XNextEvent(display, event_return)
          Display *display;
          XEvent *event_return;

    XPeekEvent(display, event_return)
          Display *display;
          XEvent *event_return;

    XWindowEvent(display, w, event_mask, event_return)
          Display *display;
          Window w;
          long event_mask;
          XEvent *event_return;

    Bool XCheckWindowEvent(display, w, event_mask, event_return)
          Display *display;
          Window w;
          long event_mask;
          XEvent *event_return;

    XMaskEvent(display, event_mask, event_return)
          Display *display;
          long event_mask;
          XEvent *event_return;

    Bool XCheckMaskEvent(display, event_mask, event_return)
          Display *display;
          long event_mask;
          XEvent *event_return;

    Bool XCheckTypedEvent(display, event_type, event_return)
          Display *display;
          int event_type;
          XEvent *event_return;

    Bool XCheckTypedWindowEvent(display, w, event_type, event_return)
          Display *display;
          Window w;
          int event_type;
          XEvent *event_return;

  AARRGGUUMMEENNTTSS

  DDEESSCCRRIIPPTTIIOONN

    The XXNNeexxttEEvveenntt function copies the first event from the event queue into
    the specified XXEEvveenntt structure and then removes it from the queue. If the
    event queue is empty, XXNNeexxttEEvveenntt flushes the output buffer and blocks
    until an event is received.

    The XXPPeeeekkEEvveenntt function returns the first event from the event queue, but
    it does not remove the event from the queue. If the queue is empty,
    XXPPeeeekkEEvveenntt flushes the output buffer and blocks until an event is
    received. It then copies the event into the client-supplied XXEEvveenntt
    structure without removing it from the event queue.

    The XXWWiinnddoowwEEvveenntt function searches the event queue for an event that
    matches both the specified window and event mask. When it finds a match,
    XXWWiinnddoowwEEvveenntt removes that event from the queue and copies it into the
    specified XXEEvveenntt structure. The other events stored in the queue are not
    discarded. If a matching event is not in the queue, XXWWiinnddoowwEEvveenntt flushes
    the output buffer and blocks until one is received.

    The XXCChheecckkWWiinnddoowwEEvveenntt function searches the event queue and then the
    events available on the server connection for the first event that matches
    the specified window and event mask. If it finds a match,
    XXCChheecckkWWiinnddoowwEEvveenntt removes that event, copies it into the specified XXEEvveenntt
    structure, and returns True. The other events stored in the queue are not
    discarded. If the event you requested is not available, XXCChheecckkWWiinnddoowwEEvveenntt
    returns False, and the output buffer will have been flushed.

    The XXMMaasskkEEvveenntt function searches the event queue for the events associated
    with the specified mask. When it finds a match, XXMMaasskkEEvveenntt removes that
    event and copies it into the specified XXEEvveenntt structure. The other events
    stored in the queue are not discarded. If the event you requested is not
    in the queue, XXMMaasskkEEvveenntt flushes the output buffer and blocks until one is
    received.

    The XXCChheecckkMMaasskkEEvveenntt function searches the event queue and then any events
    available on the server connection for the first event that matches the
    specified mask. If it finds a match, XXCChheecckkMMaasskkEEvveenntt removes that event,
    copies it into the specified XXEEvveenntt structure, and returns True. The other
    events stored in the queue are not discarded. If the event you requested
    is not available, XXCChheecckkMMaasskkEEvveenntt returns False, and the output buffer
    will have been flushed.

    The XXCChheecckkTTyyppeeddEEvveenntt function searches the event queue and then any events
    available on the server connection for the first event that matches the
    specified type. If it finds a match, XXCChheecckkTTyyppeeddEEvveenntt removes that event,
    copies it into the specified XXEEvveenntt structure, and returns True. The other
    events in the queue are not discarded. If the event is not available,
    XXCChheecckkTTyyppeeddEEvveenntt returns False, and the output buffer will have been
    flushed.

    The XXCChheecckkTTyyppeeddWWiinnddoowwEEvveenntt function searches the event queue and then any
    events available on the server connection for the first event that matches
    the specified type and window. If it finds a match, XXCChheecckkTTyyppeeddWWiinnddoowwEEvveenntt
    removes the event from the queue, copies it into the specified XXEEvveenntt
    structure, and returns True. The other events in the queue are not
    discarded. If the event is not available, XXCChheecckkTTyyppeeddWWiinnddoowwEEvveenntt returns
    False, and the output buffer will have been flushed.

  SSEEEE AALLSSOO

    _X_A_n_y_E_v_e_n_t(3X11R6)

    _X_I_f_E_v_e_n_t(3X11R6)

    _X_P_u_t_B_a_c_k_E_v_e_n_t(3X11R6)

    _X_S_e_n_d_E_v_e_n_t(3X11R6)

    Xlib - C Language X Interface

