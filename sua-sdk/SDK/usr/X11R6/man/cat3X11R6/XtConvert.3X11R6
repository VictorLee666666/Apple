XtConvert(3X11R6)                                     XtConvert(3X11R6)

  XXttCCoonnvveerrtt

  NNAAMMEE

    XtConvert, XtDirectConvert - invoke resource converters

  SSYYNNTTAAXX

    void XtConvert(w, from_type, from, to_type, to_return)
        Widget w;
        String from_type;
        XrmValuePtr from;
        String to_type;
        XrmValuePtr to_return;

    void XtDirectConvert(converter, args, num_args, from, to_return)
        XtConverter converter;
        XrmValuePtr args;
        Cardinal num_args;
        XrmValuePtr from;
        XrmValuePtr to_return;

  AARRGGUUMMEENNTTSS

    args
        Specifies the argument list that contains the additional arguments
        needed to perform the conversion (often NULL).

    converter
        Specifies the conversion procedure that is to be called.

    from
        Specifies the value to be converted.

    from_type
        Specifies the source type.

    num_args
        Specifies the number of additional arguments (often zero).

    to_type
        Specifies the destination type.

    to_return
        Returns the converted value.

    w
        Specifies the widget to use for additional arguments (if any are
        needed).

  DDEESSCCRRIIPPTTIIOONN

    The XXttCCoonnvveerrtt function looks up the type converter registered to convert
    from_type to to_type, computes any additional arguments needed, and then
    calls XXttDDiirreeccttCCoonnvveerrtt. XXttCCoonnvveerrtt has been replaced by XXttCCoonnvveerrttAAnnddSSttoorree.

    The XXttDDiirreeccttCCoonnvveerrtt function looks in the converter cache to see if this
    conversion procedure has been called with the specified arguments. If so,
    it returns a descriptor for information stored in the cache; otherwise, it
    calls the converter and enters the result in the cache.

    Before calling the specified converter, XXttDDiirreeccttCCoonnvveerrtt sets the return
    value size to zero and the return value address to NULL. To determine if
    the conversion was successful, the client should check to_return.address
    for non-NULL. XXttDDiirreeccttCCoonnvveerrtt has been replaced by XXttCCaallllCCoonnvveerrtteerr.

  SSEEEE AALLSSOO

    _X_t_A_p_p_A_d_d_C_o_n_v_e_r_t_e_r(3X11R6)

    _X_t_S_t_r_i_n_g_C_o_n_v_e_r_s_i_o_n_W_a_r_n_i_n_g(3X11R6)

    _X_t_C_o_n_v_e_r_t_A_n_d_S_t_o_r_e(3X11R6)

    _X_t_C_a_l_l_C_o_n_v_e_r_t_e_r(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

