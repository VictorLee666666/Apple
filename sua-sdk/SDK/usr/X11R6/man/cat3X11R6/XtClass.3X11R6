XtClass(3X11R6)                                         XtClass(3X11R6)

  XXttCCllaassss

  NNAAMMEE

    XtClass, XtSuperclass, XtIsSubclass, XtCheckSubclass, XtIsObject,
    XtIsRectObj, XtIsWidget, XtIsComposite, XtIsConstraint, XtIsShell,
    XtIsOverrideShell, XtIsWMShell, XtIsVendorShell, XtIsTransientShell,
    XtIsTopLevelShell, XtIsApplicationShell, XtIsSessionShell - obtain and
    verify a widget's class

  SSYYNNTTAAXX

    WidgetClass XtClass(w)
          Widget w;

    WidgetClass XtSuperclass(w)
          Widget w;

    Boolean XtIsSubclass(w, widget_class)
          Widget w;
          WidgetClass widget_class;

    void XtCheckSubclass(widget, widget_class, message)
          Widget widget;
          WidgetClass widget_class;
          String message;

    Boolean XtIsObject(w)
          Widget w;

    Boolean XtIsRectObj(w)
          Widget w;

    Boolean XtIsWidget(w)
          Widget w;

    Boolean XtIsComposite(w)
          Widget w;

    Boolean XtIsConstraint(w)
          Widget w;

    Boolean XtIsShell(w)
          Widget w;

    Boolean XtIsOverrideShell(w)
          Widget w;

    Boolean XtIsWMShell(w)
          Widget w;

    Boolean XtIsVendorShell(w)
          Widget w;

    Boolean XtIsTransientShell(w)
          Widget w;

    Boolean XtIsTopLevelShell(w)
          Widget w;

    Boolean XtIsApplicationShell(w)
          Widget w;

    Boolean XtIsSessionShell(w)
          Widget w;

  AARRGGUUMMEENNTTSS

    w
        Specifies the widget.

    widget_class
        Specifies the widget class.

    message
        Specifies the message that is to be used.

  DDEESSCCRRIIPPTTIIOONN

    The XXttCCllaassss function returns a pointer to the widget's class structure.

    The XXttSSuuppeerrccllaassss function returns a pointer to the widget's superclass
    class structure.

    The XXttIIssSSuubbccllaassss function returns True if the class of the specified
    widget is equal to or is a subclass of the specified class. The widget's
    class can be any number of subclasses down the chain and need not be an
    immediate subclass of the specified class. Composite widgets that need to
    restrict the class of the items they contain can use XXttIIssSSuubbccllaassss to find
    out if a widget belongs to the desired class of objects.

    The XXttCChheecckkSSuubbccllaassss macro determines if the class of the specified widget
    is equal to or is a subclass of the specified widget class. The widget can
    be any number of subclasses down the chain and need not be an immediate
    subclass of the specified widget class. If the specified widget is not a
    subclass, XXttCChheecckkSSuubbccllaassss constructs an error message from the supplied
    message, the widget's actual class, and the expected class and calls
    XXttEErrrroorrMMssgg. XXttCChheecckkSSuubbccllaassss should be used at the entry point of exported
    routines to ensure that the client has passed in a valid widget class for
    the exported operation.

    XXttCChheecckkSSuubbccllaassss is only executed when the widget has been compiled with
    the compiler symbol DEBUG defined; otherwise, it is defined as the empty
    string and generates no code.

    To test if a given widget belongs to a subclass of an Intrinsics-defined
    class, the Intrinsics defines macros or functions equivalent to
    XXttIIssSSuubbccllaassss for each of the built-in classes. These procedures are
    XXttIIssOObbjjeecctt, XXttIIssRReeccttOObbjj, XXttIIssWWiiddggeett, XXttIIssCCoommppoossiittee, XXttIIssCCoonnssttrraaiinntt,
    XXttIIssSShheellll, XXttIIssOOvveerrrriiddeeSShheellll, XXttIIssWWMMSShheellll, XXttIIssVVeennddoorrSShheellll,
    XXttIIssTTrraannssiieennttSShheellll, XXttIIssTTooppLLeevveellSShheellll, XXttIIssAApppplliiccaattiioonnSShheellll, and
    XXttIIssSSeessssiioonnSShheellll.

  SSEEEE AALLSSOO

    _X_t_A_p_p_E_r_r_o_r_M_s_g(3X11R6)

    _X_t_D_i_s_p_l_a_y(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

