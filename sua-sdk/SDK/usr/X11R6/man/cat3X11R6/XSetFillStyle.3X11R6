XSetFillStyle(3X11R6)                             XSetFillStyle(3X11R6)

  XXSSeettFFiillllSSttyyllee

  NNAAMMEE

    XSetFillStyle, XSetFillRule - GC convenience routines

  SSYYNNTTAAXX

    XSetFillStyle(display, gc, fill_style)
          Display *display;
          GC gc;
          int fill_style;

    XSetFillRule(display, gc, fill_rule)
          Display *display;
          GC gc;
          int fill_rule;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    fill_rule
        Specifies the fill-rule you want to set for the specified GC. You can
        pass EvenOddRule or WindingRule.

    fill_style
        Specifies the fill-style you want to set for the specified GC. You can
        pass FillSolid, FillTiled, FillStippled, or FillOpaqueStippled.

    gc
        Specifies the GC.

  DDEESSCCRRIIPPTTIIOONN

    The XXSSeettFFiillllSSttyyllee function sets the fill-style in the specified GC.

    XXSSeettFFiillllSSttyyllee can generate BadAlloc, BadGC, and BadValue errors.

    The XXSSeettFFiillllRRuullee function sets the fill-rule in the specified GC.

    XXSSeettFFiillllRRuullee can generate BadAlloc, BadGC, and BadValue errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadGC
        A value for a GContext argument does not name a defined GContext.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_G_C(3X11R6)

    _X_Q_u_e_r_y_B_e_s_t_S_i_z_e(3X11R6)

    _X_S_e_t_A_r_c_M_o_d_e(3X11R6)

    _X_S_e_t_C_l_i_p_O_r_i_g_i_n(3X11R6)

    _X_S_e_t_F_o_n_t(3X11R6)

    _X_S_e_t_L_i_n_e_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_S_e_t_S_t_a_t_e(3X11R6)

    _X_S_e_t_T_i_l_e(3X11R6)

    Xlib - C Language X Interface

