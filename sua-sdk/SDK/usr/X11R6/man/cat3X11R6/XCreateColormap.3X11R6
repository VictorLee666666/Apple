XCreateColormap(3X11R6)                         XCreateColormap(3X11R6)

  XXCCrreeaatteeCCoolloorrmmaapp

  NNAAMMEE

    XCreateColormap, XCopyColormapAndFree, XFreeColormap, XColor - create,
    copy, or destroy colormaps and color structure

  SSYYNNTTAAXX

    Colormap XCreateColormap(display, w, visual, alloc)
          Display *display;
          Window w;
          Visual *visual;
          int alloc;

    Colormap XCopyColormapAndFree(display, colormap)
          Display *display;
          Colormap colormap;

    XFreeColormap(display, colormap)
          Display *display;
          Colormap colormap;

  AARRGGUUMMEENNTTSS

    alloc
        Specifies the colormap entries to be allocated. You can pass AllocNone
        or AllocAll.

    colormap
        Specifies the colormap that you want to create, copy, set, or destroy.

    display
        Specifies the connection to the X server.

    visual
        Specifies a visual type supported on the screen. If the visual type is
        not one supported by the screen, a BadMatch error results.

    w
        Specifies the window on whose screen you want to create a colormap.

  DDEESSCCRRIIPPTTIIOONN

    The XXCCrreeaatteeCCoolloorrmmaapp function creates a colormap of the specified visual
    type for the screen on which the specified window resides and returns the
    colormap ID associated with it. Note that the specified window is only
    used to determine the screen.

    The initial values of the colormap entries are undefined for the visual
    classes GrayScale, PseudoColor, and DirectColor. For StaticGray,
    StaticColor, and TrueColor, the entries have defined values, but those
    values are specific to the visual and are not defined by X. For
    StaticGray, StaticColor, and TrueColor, alloc must be AllocNone, or a
    BadMatch error results. For the other visual classes, if alloc is
    AllocNone, the colormap initially has no allocated entries, and clients
    can allocate them. For information about the visual types, see section
    3.1.

    If alloc is AllocAll, the entire colormap is allocated writable. The
    initial values of all allocated entries are undefined. For GrayScale and
    PseudoColor, the effect is as if an XXAAllllooccCCoolloorrCCeellllss call returned all
    pixel values from zero to N - 1, where N is the colormap entries value in
    the specified visual. For DirectColor, the effect is as if an
    XXAAllllooccCCoolloorrPPllaanneess call returned a pixel value of zero and red_mask,
    green_mask, and blue_mask values containing the same bits as the
    corresponding masks in the specified visual. However, in all cases, none
    of these entries can be freed by using XXFFrreeeeCCoolloorrss.

    XXCCrreeaatteeCCoolloorrmmaapp can generate BadAlloc, BadMatch, BadValue, and BadWindow
    errors.

    The XXCCooppyyCCoolloorrmmaappAAnnddFFrreeee function creates a colormap of the same visual
    type and for the same screen as the specified colormap and returns the new
    colormap ID. It also moves all of the client's existing allocation from
    the specified colormap to the new colormap with their color values intact
    and their read-only or writable characteristics intact and frees those
    entries in the specified colormap. Color values in other entries in the
    new colormap are undefined. If the specified colormap was created by the
    client with alloc set to AllocAll, the new colormap is also created with
    AllocAll, all color values for all entries are copied from the specified
    colormap, and then all entries in the specified colormap are freed. If the
    specified colormap was not created by the client with AllocAll, the
    allocations to be moved are all those pixels and planes that have been
    allocated by the client using XXAAllllooccCCoolloorr, XXAAllllooccNNaammeeddCCoolloorr,
    XXAAllllooccCCoolloorrCCeellllss, or XXAAllllooccCCoolloorrPPllaanneess and that have not been freed since
    they were allocated.

    XXCCooppyyCCoolloorrmmaappAAnnddFFrreeee can generate BadAlloc and BadColor errors.

    The XXFFrreeeeCCoolloorrmmaapp function deletes the association between the colormap
    resource ID and the colormap and frees the colormap storage. However, this
    function has no effect on the default colormap for a screen. If the
    specified colormap is an installed map for a screen, it is uninstalled
    (see XXUUnniinnssttaallllCCoolloorrmmaapp). If the specified colormap is defined as the
    colormap for a window (by XXCCrreeaatteeWWiinnddooww, XXSSeettWWiinnddoowwCCoolloorrmmaapp, or
    XXCChhaannggeeWWiinnddoowwAAttttrriibbuutteess), XXFFrreeeeCCoolloorrmmaapp changes the colormap associated
    with the window to None and generates a ColormapNotify event. X does not
    define the colors displayed for a window with a colormap of None.

    XXFFrreeeeCCoolloorrmmaapp can generate a BadColor error.

  SSTTRRUUCCTTUURREESS

    The XXCCoolloorr structure contains:

    typedef struct {
         unsigned long pixel;/* pixel value */
         unsigned short red, green, blue;/* rgb values */
         char flags;         /* DoRed, DoGreen, DoBlue */
         char pad;
    } XColor;

    The red, green, and blue values are always in the range 0 to 65535
    inclusive, independent of the number of bits actually used in the display
    hardware. The server scales these values down to the range used by the
    hardware. Black is represented by (0,0,0), and white is represented by
    (65535,65535,65535). In some functions, the flags member controls which of
    the red, green, and blue members is used and can be the inclusive OR of
    zero or more of DoRed, DoGreen, and DoBlue.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadMatch
        An InputOnly window is used as a Drawable.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_A_l_l_o_c_C_o_l_o_r(3X11R6)

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_C_r_e_a_t_e_W_i_n_d_o_w(3X11R6)

    _X_Q_u_e_r_y_C_o_l_o_r(3X11R6)

    _X_S_t_o_r_e_C_o_l_o_r_s(3X11R6)

    Xlib - C Language X Interface

