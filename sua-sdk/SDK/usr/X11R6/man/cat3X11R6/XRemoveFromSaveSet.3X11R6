XRemoveFromSaveSet(3X11R6)                   XRemoveFromSaveSet(3X11R6)

  XXCChhaannggeeSSaavveeSSeett

  NNAAMMEE

    XChangeSaveSet, XAddToSaveSet, XRemoveFromSaveSet - change a client's save
    set

  SSYYNNTTAAXX

    XChangeSaveSet(display, w, change_mode)
          Display *display;
          Window w;
          int change_mode;

    XAddToSaveSet(display, w)
          Display *display;
          Window w;

    XRemoveFromSaveSet(display, w)
          Display *display;
          Window w;

  AARRGGUUMMEENNTTSS

    change_mode
        Specifies the mode. You can pass SetModeInsert or SetModeDelete.

    display
        Specifies the connection to the X server.

    w
        Specifies the window that you want to add or delete from the client's
        save-set.

  DDEESSCCRRIIPPTTIIOONN

    Depending on the specified mode, XXCChhaannggeeSSaavveeSSeett either inserts or deletes
    the specified window from the client's save-set. The specified window must
    have been created by some other client, or a BadMatch error results.

    XXCChhaannggeeSSaavveeSSeett can generate BadMatch, BadValue, and BadWindow errors.

    The XXAAddddTTooSSaavveeSSeett function adds the specified window to the client's save-
    set. The specified window must have been created by some other client, or
    a BadMatch error results.

    XXAAddddTTooSSaavveeSSeett can generate BadMatch and BadWindow errors.

    The XXRReemmoovveeFFrroommSSaavveeSSeett function removes the specified window from the
    client's save-set. The specified window must have been created by some
    other client, or a BadMatch error results.

    XXRReemmoovveeFFrroommSSaavveeSSeett can generate BadMatch and BadWindow errors.

  DDIIAAGGNNOOSSTTIICCSS

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_R_e_p_a_r_e_n_t_W_i_n_d_o_w(3X11R6)

    Xlib - C Language X Interface

