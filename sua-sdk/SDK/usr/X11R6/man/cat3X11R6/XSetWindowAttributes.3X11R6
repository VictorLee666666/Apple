XSetWindowAttributes(3X11R6)               XSetWindowAttributes(3X11R6)

  XXCCrreeaatteeWWiinnddooww

  NNAAMMEE

    XCreateWindow, XCreateSimpleWindow, XSetWindowAttributes - create windows
    and window attributes structure

  SSYYNNTTAAXX

    Window XCreateWindow(display, parent, x, y, width, height, border_width,
    depth,
                           class, visual, valuemask, attributes)
          Display *display;
          Window parent;
          int x, y; 
          unsigned int width, height;
          unsigned int border_width;
          int depth;
          unsigned int class;
          Visual *visual
          unsigned long valuemask;
          XSetWindowAttributes *attributes;

    Window XCreateSimpleWindow(display, parent, x, y, width, height,
    border_width,
                                 border, background)
          Display *display;
          Window parent;
          int x, y;
          unsigned int width, height;
          unsigned int border_width;
          unsigned long border;
          unsigned long background;

  AARRGGUUMMEENNTTSS

  DDEESSCCRRIIPPTTIIOONN

    The XXCCrreeaatteeWWiinnddooww function creates an unmapped subwindow for a specified
    parent window, returns the window ID of the created window, and causes the
    X server to generate a CreateNotify event. The created window is placed on
    top in the stacking order with respect to siblings.

    The coordinate system has the X axis horizontal and the Y axis vertical
    with the origin [0, 0] at the upper-left corner. Coordinates are integral,
    in terms of pixels, and coincide with pixel centers. Each window and
    pixmap has its own coordinate system. For a window, the origin is inside
    the border at the inside, upper-left corner.

    The border_width for an InputOnly window must be zero, or a BadMatch error
    results. For class InputOutput, the visual type and depth must be a
    combination supported for the screen, or a BadMatch error results. The
    depth need not be the same as the parent, but the parent must not be a
    window of class InputOnly, or a BadMatch error results. For an InputOnly
    window, the depth must be zero, and the visual must be one supported by
    the screen. If either condition is not met, a BadMatch error results. The
    parent window, however, may have any depth and class. If you specify any
    invalid window attribute for a window, a BadMatch error results.

    The created window is not yet displayed (mapped) on the user's display. To
    display the window, call XXMMaappWWiinnddooww. The new window initially uses the
    same cursor as its parent. A new cursor can be defined for the new window
    by calling XXDDeeffiinneeCCuurrssoorr. The window will not be visible on the screen
    unless it and all of its ancestors are mapped and it is not obscured by
    any of its ancestors.

    XXCCrreeaatteeWWiinnddooww can generate BadAlloc BadColor, BadCursor, BadMatch,
    BadPixmap, BadValue, and BadWindow errors.

    The XXCCrreeaatteeSSiimmpplleeWWiinnddooww function creates an unmapped InputOutput subwindow
    for a specified parent window, returns the window ID of the created
    window, and causes the X server to generate a CreateNotify event. The
    created window is placed on top in the stacking order with respect to
    siblings. Any part of the window that extends outside its parent window is
    clipped. The border_width for an InputOnly window must be zero, or a
    BadMatch error results. XXCCrreeaatteeSSiimmpplleeWWiinnddooww inherits its depth, class, and
    visual from its parent. All other window attributes, except background and
    border, have their default values.

    XXCCrreeaatteeSSiimmpplleeWWiinnddooww can generate BadAlloc, BadMatch, BadValue, and
    BadWindow errors.

  SSTTRRUUCCTTUURREESS

    The XSetWindowAttributes structure contains:

    /* Window attribute value mask bits */
    #define     CWBackPixmap           (1L<<0)

    #define     CWBackPixel            (1L<<1)

    #define     CWBorderPixmap         (1L<<2)

    #define     CWBorderPixel          (1L<<3)

    #define     CWBitGravity           (1L<<4)

    #define     CWWinGravity           (1L<<5)

    #define     CWBackingStore         (1L<<6)

    #define     CWBackingPlanes        (1L<<7)

    #define     CWBackingPixel         (1L<<8)

    #define     CWOverrideRedirect     (1L<<9)

    #define     CWSaveUnder            (1L<<10)

    #define     CWEventMask            (1L<<11)

    #define     CWDontPropagate        (1L<<12)

    #define     CWColormap             (1L<<13)

    #define     CWCursor               (1L<<14)

    /* Values */

    typedef struct {
         Pixmap background_pixmap;/* background, None, or ParentRelative */
         unsigned long background_pixel;/* background pixel */
         Pixmap border_pixmap;    /* border of the window or CopyFromParent */
         unsigned long border_pixel;/* border pixel value */
         int bit_gravity;         /* one of bit gravity values */
         int win_gravity;         /* one of the window gravity values */
         int backing_store;       /* NotUseful, WhenMapped, Always */
         unsigned long backing_planes;/* planes to be preserved if possible */
         unsigned long backing_pixel;/* value to use in restoring planes */
         Bool save_under;         /* should bits under be saved? (popups) */
         long event_mask;         /* set of events that should be saved */
         long do_not_propagate_mask;/* set of events that should not propagate
    */
         Bool override_redirect;  /* boolean value for override_redirect */
         Colormap colormap;       /* color map to be associated with window */
         Cursor cursor;           /* cursor to be displayed (or None) */
    } XSetWindowAttributes;

    For a detailed explanation of the members of this structure, see Xlib - C
    Language X Interface.

  DDIIAAGGNNOOSSTTIICCSS

    BadAlloc
        The server failed to allocate the requested resource or server memory.

    BadColor
        A value for a Colormap argument does not name a defined Colormap.

    BadCursor
        A value for a Cursor argument does not name a defined Cursor.

    BadMatch
        The values do not exist for an InputOnly window.

    BadMatch
        Some argument or pair of arguments has the correct type and range but
        fails to match in some other way required by the request.

    BadPixmap
        A value for a Pixmap argument does not name a defined Pixmap.

    BadValue
        Some numeric value falls outside the range of values accepted by the
        request. Unless a specific range is specified for an argument, the
        full range defined by the argument's type is accepted. Any argument
        defined as a set of alternatives can generate this error.

    BadWindow
        A value for a Window argument does not name a defined Window.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_W_i_n_d_o_w_A_t_t_r_i_b_u_t_e_s(3X11R6)

    _X_C_o_n_f_i_g_u_r_e_W_i_n_d_o_w(3X11R6)

    _X_D_e_f_i_n_e_C_u_r_s_o_r(3X11R6)

    _X_D_e_s_t_r_o_y_W_i_n_d_o_w(3X11R6)

    _X_M_a_p_W_i_n_d_o_w(3X11R6)

    _X_R_a_i_s_e_W_i_n_d_o_w(3X11R6)

    _X_U_n_m_a_p_W_i_n_d_o_w(3X11R6)

    Xlib - C Language X Interface

