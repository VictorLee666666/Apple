XcmsCIELuvQueryMaxL(3X11R6)                 XcmsCIELuvQueryMaxL(3X11R6)

  XXccmmssCCIIEELLuuvvQQuueerryyMMaaxxCC

  NNAAMMEE

    XcmsCIELuvQueryMaxC, XcmsCIELuvQueryMaxL, XcmsCIELuvQueryMaxLC,
    XcmsCIELuvQueryMinL - obtain the CIE L*u*v* coordinates

  SSYYNNTTAAXX

    Status XcmsCIELuvQueryMaxC(ccc, hue_angle, L_star, color_return)
          XcmsCCC ccc;
          XcmsFloat hue_angle;
          XcmsFloat L_star;
          XcmsColor *color_return;

    Status XcmsCIELuvQueryMaxL(ccc, hue_angle, chroma, color_return)
          XcmsCCC ccc;
          XcmsFloat hue_angle;
          XcmsFloat chroma;
          XcmsColor *color_return;

    Status XcmsCIELuvQueryMaxLC(ccc, hue_angle, color_return)
          XcmsCCC ccc;
          XcmsFloat hue_angle;
          XcmsColor *color_return;

    Status XcmsCIELuvQueryMinL(ccc, hue_angle, chroma, color_return)
          XcmsCCC ccc;
          XcmsFloat hue_angle;
          XcmsFloat chroma;
          XcmsColor *color_return;

  AARRGGUUMMEENNTTSS

    ccc
        Specifies the CCC. Note that the CCC's Client White Point and White
        Point Adjustment procedures are ignored.

    chroma
        Specifies the chroma at which to find maximum lightness (MaxL) or
        minimum lightness (MinL).

    color_return
        Returns the CIE L*u*v* coordinates of maximum chroma (MaxC and MaxLC),
        maximum lightnes (MaxL), or minimum lightness (MinL) displayable by
        the screen for the given hue angle and lightness (MaxC), hue angle and
        chroma (MaxL and MinL), or hue angle (MaxLC). The white point
        associated with the returned color specification is the Screen White
        Point. The value returned in the pixel member is undefined.

    hue_angle
        Specifies the hue angle (in degrees) at which to find maximum chroma
        (MaxC and MaxLC), maximum lightness (MaxL), or minimum lightness
        (MinL).

    L_star
        Specifies the lightness (L*) at which to find maximum chroma (MaxC) or
        maximum lightness (MaxL).

  DDEESSCCRRIIPPTTIIOONN

    The XXccmmssCCIIEELLuuvvQQuueerryyMMaaxxCC function, given a hue angle and lightness, finds
    the point of maximum chroma displayable by the screen. It returns this
    point in CIE L*u*v* coordinates.

    The XXccmmssCCIIEELLuuvvQQuueerryyMMaaxxLL function, given a hue angle and chroma, finds the
    point in CIE L*u*v* color space of maximum lightness (L*) displayable by
    the screen. It returns this point in CIE L*u*v* coordinates. An
    XcmsFailure return value usually indicates that the given chroma is beyond
    maximum for the given hue angle.

    The XXccmmssCCIIEELLuuvvQQuueerryyMMaaxxLLCC function, given a hue angle, finds the point of
    maximum chroma displayable by the screen. It returns this point in CIE
    L*u*v* coordinates.

    The XXccmmssCCIIEELLuuvvQQuueerryyMMiinnLL function, given a hue angle and chroma, finds the
    point of minimum lightness (L*) displayable by the screen. It returns this
    point in CIE L*u*v* coordinates. An XcmsFailure return value usually
    indicates that the given chroma is beyond maximum for the given hue angle.

  SSEEEE AALLSSOO

    _X_c_m_s_C_I_E_L_a_b_Q_u_e_r_y_M_a_x_C(3X11R6)

    _X_c_m_s_T_e_k_H_V_C_Q_u_e_r_y_M_a_x_C(3X11R6)

    _X_c_m_s_Q_u_e_r_y_B_l_a_c_k(3X11R6)

    Xlib - C Language X Interface

