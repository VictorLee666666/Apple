XtIsManaged(3X11R6)                                 XtIsManaged(3X11R6)

  XXttMMaannaaggeeCChhiillddrreenn

  NNAAMMEE

    XtManageChildren, XtManageChild, XtUnmanageChildren, XtUnmanageChild,
    XtChangeManagedSet, XtIsManaged - manage and unmanage children

  SSYYNNTTAAXX

    typedef Widget *WidgetList;
    void XtManageChildren(children, num_children)
          WidgetList children;
          Cardinal num_children;

    void XtManageChild(child)
          Widget child;

    void XtUnmanageChildren(children, num_children)
          WidgetList children;
          Cardinal num_children;

    void XtUnmanageChild(child)
          Widget child;

    void XtChangeManagedSet(unmanage_children, num_unmanage_children,
    manage_children, num_manage_children, post_unmanage_pre_manage_hook,
    client_data)
          WidgetList unmanage_children;
          Cardinal num_unmanage_children;
          WidgetList manage_children;
          Cardinal num_manage_children;
          XtCSMProc post_unmanage_pre_manage_hook;
          XtPointer client_data;

    Boolean XtIsManaged(widget)
          Widget widget

  AARRGGUUMMEENNTTSS

    child
        Specifies the child.

    children
        Specifies a list of child widgets.

    num_children
        Specifies the number of children.

    widget
        Specifies the widget.

    manage_children
        Specifies the list of widget children to add to the managed set.

    num_manage_children
        Specifies the number of entries in the manage_children list.

    unmanage_children
        Specifies the list of widget children to remove from the managed set.

    num_unmanage_children
        Specifies the number of entries in the unmanage_children list.

    post_unmanage_pre_manage_hook
        Specifies the post unmanage, pre manage hook procedure to invoke.

    client_data
        Specifies the client data to be passed to the hook precedure.

  DDEESSCCRRIIPPTTIIOONN

    The XXttMMaannaaggeeCChhiillddrreenn function performs the following:

    *     Issues an error if the children do not all have the same parent or
          if the parent is not a subclass of compositeWidgetClass.
    *     Returns immediately if the common parent is being destroyed;
          otherwise, for each unique child on the list, XXttMMaannaaggeeCChhiillddrreenn
          ignores the child if it already is managed or is being destroyed and
          marks it if not.
    *     If the parent is realized and after all children have been marked,
          it makes some of the newly managed children viewable:
          *     Calls the change_managed routine of the widgets' parent.
          *     Calls XXttRReeaalliizzeeWWiiddggeett on each previously unmanaged child that
                is unrealized.
          *     Maps each previously unmanaged child that has map_when_managed
                True.

    Managing children is independent of the ordering of children and
    independent of creating and deleting children. The layout routine of the
    parent should consider children whose managed field is True and should
    ignore all other children. Note that some composite widgets, especially
    fixed boxes, call XXttMMaannaaggeeCChhiilldd from their insert_child procedure.

    If the parent widget is realized, its change_managed procedure is called
    to notify it that its set of managed children has changed. The parent can
    reposition and resize any of its children. It moves each child as needed
    by calling XXttMMoovveeWWiiddggeett, which first updates the x and y fields and then
    calls XXMMoovveeWWiinnddooww if the widget is realized.

    The XXttMMaannaaggeeCChhiilldd function constructs a WidgetList of length one and calls
    XXttMMaannaaggeeCChhiillddrreenn.

    The XXttUUnnmmaannaaggeeCChhiillddrreenn function performs the following:
    *     Issues an error if the children do not all have the same parent or
          if the parent is not a subclass of compositeWidgetClass.
    *     Returns immediately if the common parent is being destroyed;
          otherwise, for each unique child on the list, XXttUUnnmmaannaaggeeCChhiillddrreenn
          performs the following:
          *     Ignores the child if it already is unmanaged or is being destroyed
                and marks it if not.
          *     If the child is realized, it makes it nonvisible by unmapping it.
    *     Calls the change_managed routine of the widgets' parent after all
          children have been marked if the parent is realized.

    XXttUUnnmmaannaaggeeCChhiillddrreenn does not destroy the children widgets. Removing widgets
    from a parent's managed set is often a temporary banishment, and, some
    time later, you may manage the children again.

    The XXttUUnnmmaannaaggeeCChhiilldd function constructs a widget list of length one and
    calls XXttUUnnmmaannaaggeeCChhiillddrreenn.

    The XXttCChhaannggeeMMaannaaggeeddSSeett function performs the following:
    *     Issues an error if the widgets specified in the manage_children and
          the unmanage_children lists to no all have the same parent, or if
          that parent is not a subclass of compositeWidgetClass.
    *     Returns immediately if the common parent is being destroyed.
    *     If no CompositeClassExtension is defined, or a
          CompositeClassExtension is defined but with an
          allows_change_managed_set field with a value of False, and
          XXttCChhaannggeeMMaannaaggeeddSSeett was invoked with a non-NULL
          post_unmanage_pre_manage_hook procedure then XXttCChhaannggeeMMaannaaggeeddSSeett
          performs the following:
          *     Calls XXttUUnnmmaannaaggeeCChhiillddrreenn (unmanage_children,
                num_unmanage_children).
          *     Calls the post_unmanage_pre_manage_hook specified.
          *     Calls XXttMMaannaaggeeCChhiillddrreenn (manage_children, num_manage_children)
                and then returns immediately.
    *     Otherwise, if a CompositeClassExtension is defined with an
          allows_change_managed_set field with a value of True, or if no
          CompositeClassExtension is defined, and XXttCChhaannggeeMMaannaaggeeddSSeettwas
          post_unmanage_pre_manage_hook procedure, then the following is
          performed:
          *     For each child on the unmanage_children list; if the child is
                already unmanaged or is being destroyed it is ignored,
                otherwise it is marked as being unmanaged and if it is
                realized it is made nonvisible by being unmapped.
          *     If the post_unmanage_pre_manage_hook procdedure is non-NULL
                then it is invoked as specified.
          *     For each child on the manage_children list; if the child is
                already managed or it is being destroyed it is ignored,
                otherwise it is marked as managed
    *     If the parent is realized and after all children have been marked,
          the change_managed method of the parent is invoked and subsequently
          some of the newly managed children are made viewable by:
          *     Calling XXttRReeaalliizzeeWWiiddggeett on each of the previously unmanaged child
                that is unrealized.
          *     Mapping each previously unmanaged child that has map_when_managed
                True.

    The XXttIIssMMaannaaggeedd function returns True if the specified widget is of class
    RectObj or any subclass thereof and is managed, or False otherwise.

  SSEEEE AALLSSOO

    _X_t_M_a_p_W_i_d_g_e_t(3X11R6)

    _X_t_R_e_a_l_i_z_e_W_i_d_g_e_t(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

