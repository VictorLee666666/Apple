XCloseDisplay(3X11R6)                             XCloseDisplay(3X11R6)

  XXOOppeennDDiissppllaayy

  NNAAMMEE

    XOpenDisplay, XCloseDisplay - connect or disconnect to X server

  SSYYNNTTAAXX

    Display *XOpenDisplay(display_name)
          char *display_name;

    XCloseDisplay(display)
          Display *display;

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    display_name
        Specifies the hardware display name, which determines the display and
        communications domain to be used. On a POSIX-conformant system, if the
        display_name is NULL, it defaults to the value of the DISPLAY
        environment variable.

  DDEESSCCRRIIPPTTIIOONN

    The XXOOppeennDDiissppllaayy function returns a Display structure that serves as the
    connection to the X server and that contains all the information about
    that X server. XXOOppeennDDiissppllaayy connects your application to the X server
    through TCP or DECnet communications protocols, or through some local
    inter-process communication protocol. If the hostname is a host machine
    name and a single colon (:) separates the hostname and display number,
    XXOOppeennDDiissppllaayy connects using TCP streams. If the hostname is not specified,
    Xlib uses whatever it believes is the fastest transport. If the hostname
    is a host machine name and a double colon (::) separates the hostname and
    display number, XXOOppeennDDiissppllaayy connects using DECnet. A single X server can
    support any or all of these transport mechanisms simultaneously. A
    particular Xlib implementation can support many more of these transport
    mechanisms.

    If successful, XXOOppeennDDiissppllaayy returns a pointer to a Display structure,
    which is defined in <X11/Xlib.h>. If XXOOppeennDDiissppllaayy does not succeed, it
    returns NULL. After a successful call to XXOOppeennDDiissppllaayy, all of the screens
    in the display can be used by the client. The screen number specified in
    the display_name argument is returned by the DDeeffaauullttSSccrreeeenn macro (or the
    XDefaultScreen function). You can access elements of the Display and
    Screen structures only by using the information macros or functions. For
    information about using macros and functions to obtain information from
    the Display structure, see section 2.2.1.

    The XXCClloosseeDDiissppllaayy function closes the connection to the X server for the
    display specified in the Display structure and destroys all windows,
    resource IDs (Window, Font, Pixmap, Colormap, Cursor, and GContext), or
    other resources that the client has created on this display, unless the
    close-down mode of the resource has been changed (see XXSSeettCClloosseeDDoowwnnMMooddee).
    Therefore, these windows, resource IDs, and other resources should never
    be referenced again or an error will be generated. Before exiting, you
    should call XXCClloosseeDDiissppllaayy explicitly so that any pending errors are
    reported as XXCClloosseeDDiissppllaayy performs a final XXSSyynncc operation.

    XXCClloosseeDDiissppllaayy can generate a BadGC error.

  SSEEEE AALLSSOO

    _A_l_l_P_l_a_n_e_s(3X11R6)

    _X_F_l_u_s_h(3X11R6)

    _X_S_e_t_C_l_o_s_e_D_o_w_n_M_o_d_e(3X11R6)

    Xlib - C Language X Interface

