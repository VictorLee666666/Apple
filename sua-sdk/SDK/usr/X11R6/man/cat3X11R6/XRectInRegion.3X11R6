XRectInRegion(3X11R6)                             XRectInRegion(3X11R6)

  XXEEmmppttyyRReeggiioonn

  NNAAMMEE

    XEmptyRegion, XEqualRegion, XPointInRegion, XRectInRegion - determine if
    regions are empty or equal

  SSYYNNTTAAXX

    Bool XEmptyRegion(r)
          Region r;

    Bool XEqualRegion(r1, r2)
          Region r1, r2;

    Bool XPointInRegion(r, x, y)
          Region r;
          int x, y;

    int XRectInRegion(r, x, y, width, height)
          Region r;
          int x, y; 
          unsigned int width, height;

  AARRGGUUMMEENNTTSS

    r
        Specifies the region.
    r1
    r2
        Specify the two regions.
    width
    height
        Specify the width and height, which define the rectangle.
    x
    y
        Specify the x and y coordinates, which define the point or the
        coordinates of the upper-left corner of the rectangle.

  DDEESSCCRRIIPPTTIIOONN

    The XXEEmmppttyyRReeggiioonn function returns True if the region is empty.

    The XXEEqquuaallRReeggiioonn function returns True if the two regions have the same
    offset, size, and shape.

    The XXPPooiinnttIInnRReeggiioonn function returns True if the point (x, y) is contained
    in the region r.

    The XXRReeccttIInnRReeggiioonn function returns RectangleIn if the rectangle is
    entirely in the specified region, RectangleOut if the rectangle is
    entirely out of the specified region, and RectanglePart if the rectangle
    is partially in the specified region.

  SSEEEE AALLSSOO

    _X_C_r_e_a_t_e_R_e_g_i_o_n(3X11R6)

    _X_I_n_t_e_r_s_e_c_t_R_e_g_i_o_n(3X11R6)

    Xlib - C Language X Interface

