XtSetTypeConverter(3X11R6)                   XtSetTypeConverter(3X11R6)

  XXttAAppppSSeettTTyyppeeCCoonnvveerrtteerr

  NNAAMMEE

    XtAppSetTypeConverter, XtSetTypeConverter - register resource converter

  SSYYNNTTAAXX

    void XtAppSetTypeConverter(app_context, from_type, to_type, converter,
    convert_args, num_args, cache_type, destructor)
        XtAppContext app_context;
        String from_type;
        String to_type;
        XtConverter converter;
        XtConvertArgList convert_args;
        Cardinal num_args;
        XtCacheType cache_type;
        XtDestructor destructor;

    void XtSetTypeConverter(from_type, to_type, converter, convert_args,
    num_args, cache_type, destructor)
        String from_type;
        String to_type;
        XtConverter converter;
        XtConvertArgList convert_args;
        Cardinal num_args;
        XtCacheType cache_type;
        XtDestructor destructor;

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    converter
        Specifies the type converter procedure.

    convert_args
        Specifies how to compute the additional arguments to the converter or
        NULL.

    from_type
        Specifies the source type.

    num_args
        Specifies the number of additional arguments to the converter or zero.

    to_type
        Specifies the destination type.

    cache_type
        Specifies whether or not resources produced by this converter are
        sharable or display-specific and when they should be freed.

    destructor
        Specifies a destroy procedure for resources produced by this
        conversion, or NULL if no addional action is required to deallocate
        resources produced by the converter.

  DDEESSCCRRIIPPTTIIOONN

    XXttSSeettTTyyppeeCCoonnvveerrtteerr registers the specified type converter and destructor
    in all application contexts created by the calling process, including any
    future application contexts that may be created. XXttAAppppSSeettTTyyppeeCCoonnvveerrtteerr
    registers the specified type converter in the single application context
    specified. If the same from_type and to_type are specified in multiple
    calls to either function, the most recent overrides the previous ones.

  SSEEEE AALLSSOO

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

