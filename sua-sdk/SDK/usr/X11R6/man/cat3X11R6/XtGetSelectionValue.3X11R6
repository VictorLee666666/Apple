XtGetSelectionValue(3X11R6)                 XtGetSelectionValue(3X11R6)

  XXttGGeettSSeelleeccttiioonnVVaalluuee

  NNAAMMEE

    XtGetSelectionValue, XtGetSelectionValues - obtain selection values

  SSYYNNTTAAXX

    void XtGetSelectionValue(w, selection, target, callback, client_data,
    time)
          Widget w;
          Atom selection;
          Atom target;
          XtSelectionCallbackProc callback;
          XtPointer client_data;
          Time time;

    void XtGetSelectionValues(w, selection, targets, count, callback,
    client_data, time)
          Widget w;
          Atom selection;
          Atom *targets;
          int count;
          XtSelectionCallbackProc callback;
          XtPointer client_data;
          Time time;

  AARRGGUUMMEENNTTSS

    callback
        Specifies the callback procedure that is to be called when the
        selection value has been obtained.

    client_data
        Specifies the argument that is to be passed to the specified procedure
        when it is called.

    client_data
        Specifies the client data (one for each target type) that is passed to
        the callback procedure when it is called for that target.

    count
        Specifies the length of the targets and client_data lists.

    selection
        Specifies the particular selection desired (that is, primary or
        secondary).

    target
        Specifies the type of the information that is needed about the
        selection.

    targets
        Specifies the types of information that is needed about the selection.

    time
        Specifies the timestamp that indicates when the selection value is
        desired.

    w
        Specifies the widget that is making the request.

  DDEESSCCRRIIPPTTIIOONN

    The XXttGGeettSSeelleeccttiioonnVVaalluuee function requests the value of the selection that
    has been converted to the target type. The specified callback will be
    called some time after XXttGGeettSSeelleeccttiioonnVVaalluuee is called; in fact, it may be
    called before or after XXttGGeettSSeelleeccttiioonnVVaalluuee returns.

    The XXttGGeettSSeelleeccttiioonnVVaalluueess function is similar to XXttGGeettSSeelleeccttiioonnVVaalluuee except
    that it takes a list of target types and a list of client data and obtains
    the current value of the selection converted to each of the targets. The
    effect is as if each target were specified in a separate call to
    XXttGGeettSSeelleeccttiioonnVVaalluuee. The callback is called once with the corresponding
    client data for each target. XXttGGeettSSeelleeccttiioonnVVaalluueess does guarantee that all
    the conversions will use the same selection value because the ownership of
    the selection cannot change in the middle of the list, as would be when
    calling XXttGGeettSSeelleeccttiioonnVVaalluuee repeatedly.

  SSEEEE AALLSSOO

    _X_t_A_p_p_G_e_t_S_e_l_e_c_t_i_o_n_T_i_m_e_o_u_t(3X11R6)

    _X_t_O_w_n_S_e_l_e_c_t_i_o_n(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

