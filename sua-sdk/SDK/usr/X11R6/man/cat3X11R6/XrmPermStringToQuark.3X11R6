XrmPermStringToQuark(3X11R6)               XrmPermStringToQuark(3X11R6)

  XXrrmmUUnniiqquueeQQuuaarrkk

  NNAAMMEE

    XrmUniqueQuark, XrmStringToQuark, XrmPermStringToQuark, XrmQuarkToString,
    XrmStringToQuarkList, XrmStringToBindingQuarkList - manipulate resource
    quarks

  SSYYNNTTAAXX

    XrmQuark XrmUniqueQuark()

    #define XrmStringToName(string) XrmStringToQuark(string)
    #define XrmStringToClass(string) XrmStringToQuark(string)
    #define XrmStringToRepresentation(string) XrmStringToQuark(string)
    XrmQuark XrmStringToQuark(string)
         char *string;
    XrmQuark XrmPermStringToQuark(string)
         char *string;

    #define XrmStringToName(string) XrmStringToQuark(string)
    #define XrmStringToClass(string) XrmStringToQuark(string)
    #define XrmStringToRepresentation(string) XrmStringToQuark(string)
    XrmQuark XrmStringToQuark(string)
         char *string;
    XrmQuark XrmPermStringToQuark(string)
         char *string;

    #define XrmNameToString(name) XrmQuarkToString(name)
    #define XrmClassToString(class) XrmQuarkToString(class)
    #define XrmRepresentationToString(type) XrmQuarkToString(type)
    char *XrmQuarkToString(quark)
         XrmQuark quark;

    #define XrmStringToNameList(str, name)  XrmStringToQuarkList((str),
    (name))
    #define XrmStringToClassList(str,class) XrmStringToQuarkList((str),
    (class))
    void XrmStringToQuarkList(string, quarks_return)
         char *string;
         XrmQuarkList quarks_return;

    XrmStringToBindingQuarkList(string, bindings_return, quarks_return)
         char *string;
         XrmBindingList bindings_return;
         XrmQuarkList quarks_return;

  AARRGGUUMMEENNTTSS

    bindings_return
        Returns the binding list.

    quark
        Specifies the quark for which the equivalent string is desired.

    quarks_return
        Returns the list of quarks.

    string
        Specifies the string for which a quark or quark list is to be
        allocated.

  DDEESSCCRRIIPPTTIIOONN

    The XXrrmmUUnniiqquueeQQuuaarrkk function allocates a quark that is guaranteed not to
    represent any string that is known to the resource manager.

    These functions can be used to convert from string to quark
    representation. If the string is not in the Host Portable Character
    Encoding, the conversion is implementation-dependent. The string argument
    to XXrrmmSSttrriinnggTTooQQuuaarrkk need not be permanently allocated storage.
    XXrrmmPPeerrmmSSttrriinnggTTooQQuuaarrkk is just like XXrrmmSSttrriinnggTTooQQuuaarrkk, except that Xlib is
    permitted to assume the string argument is permanently allocated, and,
    hence, that it can be used as the value to be returned by
    XXrrmmQQuuaarrkkTTooSSttrriinngg.

    For any given quark, if XXrrmmSSttrriinnggTTooQQuuaarrkk returns a non-NULL value, all
    future calls will return the same value (identical address).

    These functions can be used to convert from quark representation to
    string. The string pointed to by the return value must not be modified or
    freed. The returned string is byte-for-byte equal to the original string
    passed to one of the string-to-quark routines. If no string exists for
    that quark, XXrrmmQQuuaarrkkTTooSSttrriinngg returns NULL. For any given quark, if
    XXrrmmQQuuaarrkkTTooSSttrriinngg returns a non-NULL value, all future calls will return
    the same value (identical address).

    These functions can be used to convert from string to quark
    representation. If the string is not in the Host Portable Character
    Encoding, the conversion is implementation-dependent. The string argument
    to XXrrmmSSttrriinnggTTooQQuuaarrkk need not be permanently allocated storage.
    XXrrmmPPeerrmmSSttrriinnggTTooQQuuaarrkk is just like XXrrmmSSttrriinnggTTooQQuuaarrkk, except that Xlib is
    permitted to assume the string argument is permanently allocated, and,
    hence, that it can be used as the value to be returned by
    XXrrmmQQuuaarrkkTTooSSttrriinngg.

    For any given quark, if XXrrmmSSttrriinnggTTooQQuuaarrkk returns a non-NULL value, all
    future calls will return the same value (identical address).

    The XXrrmmSSttrriinnggTTooQQuuaarrkkLLiisstt function converts the null-terminated string
    (generally a fully qualified name) to a list of quarks. The caller must
    allocate sufficient space for the quarks list before calling
    XXrrmmSSttrriinnggTTooQQuuaarrkkLLiisstt. Note that the string must be in the valid
    ResourceName format (see section 15.1). If the string is not in the Host
    Portable Character Encoding, the conversion is implementation-dependent.

    A binding list is a list of type XrmBindingList and indicates if
    components of name or class lists are bound tightly or loosely (that is,
    if wildcarding of intermediate components is specified).

    typedef enum {XrmBindTightly, XrmBindLoosely} XrmBinding, *XrmBindingList;

    XrmBindTightly indicates that a period separates the components, and
    XrmBindLoosely indicates that an asterisk separates the components.

    The XXrrmmSSttrriinnggTTooBBiinnddiinnggQQuuaarrkkLLiisstt function converts the specified string to
    a binding list and a quark list. The caller must allocate sufficient space
    for the quarks list and the binding list before calling
    XXrrmmSSttrriinnggTTooBBiinnddiinnggQQuuaarrkkLLiisstt. If the string is not in the Host Portable
    Character Encoding the conversion is implementation-dependent. Component
    names in the list are separated by a period or an asterisk character. If
    the string does not start with period or asterisk, a period is assumed.
    For example, "*a.b*c" becomes:

    quarks  a      b       c

    bindings       loose   tightloose

  SSEEEE AALLSSOO

    _X_r_m_G_e_t_R_e_s_o_u_r_c_e(3X11R6)

    _X_r_m_I_n_i_t_i_a_l_i_z_e(3X11R6)

    _X_r_m_M_e_r_g_e_D_a_t_a_b_a_s_e_s(3X11R6)

    _X_r_m_P_u_t_R_e_s_o_u_r_c_e(3X11R6)

    Xlib - C Language X Interface

