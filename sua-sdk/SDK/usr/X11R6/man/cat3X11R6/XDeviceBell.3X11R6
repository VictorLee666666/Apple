XDeviceBell(3X11R6)                                 XDeviceBell(3X11R6)

  XXDDeevviicceeBBeellll

  NNAAMMEE

    XDeviceBell - ring a bell on a device supported through the input
    extension

  SSYYNNTTAAXX

    Status XDeviceBell(display, device, feedbackclass, feedbackid, percent)
          Display *display;
          XDevice *device; 
          XID *feedbackclass; 
          XID *feedbackid; 
          int *percent; 

  AARRGGUUMMEENNTTSS

    display
        Specifies the connection to the X server.

    device
        Specifies the device with which the bell is associated.

    feedbackclass
        Specifies the class of the feedback with which the bell is associated.

    feedbackid
        Specifies the id of the feedback with which the bell is associated.

    percent
        Specifies the volume in the range -100 to 100 at which the bell should
        be rung.

  DDEESSCCRRIIPPTTIIOONN

    The XXDDeevviicceeBBeellll request causes the server to ring a bell on the specified
    feedback of the specified device, if possible. The specified volume is
    relative to the base volume for the bell. If an invalid device is
    specified, a BadDevice error will be returned. The feedbackclass and
    feedbackid parameters contain values returned by an XXGGeettFFeeeeddbbaacckkCCoonnttrrooll
    request and uniquely identify the bell to ring. If a feedbackclass is
    specified that does not support a bell, or if a nonexistent feedbackid is
    specified, or a percent value is specified that is not in the range -100
    to 100, a BadValue error will be returned.

    The volume at which the bell is rung when the percent argument is
    nonnegative is:
        base - [(base * percent) / 100] + percent

    The volume at which the bell rings when the percent argument is negative
    is:
        base + [(base * percent) / 100]

    To change the base volume of the bell, use XXCChhaannggeeFFeeeeddbbaacckkCCoonnttrrooll.

    XXDDeevviicceeBBeellll can generate a BadDevice or a BadValue error.

  DDIIAAGGNNOOSSTTIICCSS

    BadDevice
        An invalid device was specified. The specified device does not exist,
        or has not been opened by this client via XOpenInputDevice.

    BadValue
        An invalid feedbackclass, feedbackid, or percent value was specified.

  SSEEEE AALLSSOO

    _X_C_h_a_n_g_e_F_e_e_d_b_a_c_k_C_o_n_t_r_o_l(3X11R6),

    _X_B_e_l_l(3X11R6)

    Programming With Xlib

