XtInstallAllAccelerators(3X11R6)         XtInstallAllAccelerators(3X11R6)

  XXttPPaarrsseeAAcccceelleerraattoorrTTaabbllee

  NNAAMMEE

    XtParseAcceleratorTable, XtInstallAccelerators, XtInstallAllAccelerators -
    managing accelerator tables

  SSYYNNTTAAXX

    XtAccelerators XtParseAcceleratorTable(source)
          String source;

    void XtInstallAccelerators(destination, source)
          Widget destination;
          Widget source;

    void XtInstallAllAccelerators(destination, source)
          Widget destination;
          Widget source;

  AARRGGUUMMEENNTTSS

    source
        Specifies the accelerator table to compile.

    destination
        Specifies the widget on which the accelerators are to be installed.

    source
        Specifies the widget or the root widget of the widget tree from which
        the accelerators are to come.

  DDEESSCCRRIIPPTTIIOONN

    The XXttPPaarrsseeAAcccceelleerraattoorrTTaabbllee function compiles the accelerator table into
    the opaque internal representation.

    The XXttIInnssttaallllAAcccceelleerraattoorrss function installs the accelerators from source
    onto destination by augmenting the destination translations with the
    source accelerators. If the source display_accelerator method is non-NULL,
    XXttIInnssttaallllAAcccceelleerraattoorrss calls it with the source widget and a string
    representation of the accelerator table, which indicates that its
    accelerators have been installed and that it should display them
    appropriately. The string representation of the accelerator table is its
    canonical translation table representation.

    The XXttIInnssttaallllAAllllAAcccceelleerraattoorrss function recursively descends the widget tree
    rooted at source and installs the accelerators of each widget encountered
    onto destination. A common use os to call XXttIInnssttaallllAAllllAAcccceelleerraattoorrss and
    pass the application main window as the source.

  SSEEEE AALLSSOO

    _X_t_P_a_r_s_e_T_r_a_n_s_l_a_t_i_o_n_T_a_b_l_e(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

