XtNoticeSignal(3X11R6)                           XtNoticeSignal(3X11R6)

  XXttAAppppAAddddSSiiggnnaall

  NNAAMMEE

    XtAppAddSignal, XtRemoveSignal, XtNoticeSignal - register and remove a
    signal source

  SSYYNNTTAAXX

    XtSignalId XtAppAddSignal(app_context, proc, client_data)
          XtAppContext app_context;
          XtSignalCallbackProc proc;
          XtPointer client_data;

    void XtRemoveSignal(id)
          XtSignalId id;

    void XtNoticeSignal(id)
          XtSignalId id)

  AARRGGUUMMEENNTTSS

    app_context
        Specifies the application context.

    client_data
        Specifies the argument that is to be passed to the specified procedure
        when a signal has been raised.

    id
        Specifies the ID returned from the corresponding XXttAAppppAAddddSSiiggnnaall call.

    proc
        Specifies the procedure that is to be called when the signal has been
        raised.

  DDEESSCCRRIIPPTTIIOONN

    The XXttAAppppAAddddSSiiggnnaall function initiates a mechanism for handling signals
    within the context of the Intrinsics. Prior to establishing an operating
    system dependent signal handler the application may call XXttAAppppAAddddSSiiggnnaall
    and store the returned id in a place accessible to the signal handler.

    Upon receipt of a signal from the operating system, the application may
    call XXttNNoottiicceeSSiiggnnaall passing the id returned by the call to XXttAAppppAAddddSSiiggnnaall.

    XXttNNoottiicceeSSiiggnnaall is the only Intrinsics function that can safely be called
    from a signal handler. If XXttNNoottiicceeSSiiggnnaall is called multiple times before
    the Intrinsics are able to invoke the registered callback, the callback is
    only called once. Logically the Intrinsics maintain "pending" for each
    registered callback. This flag is initially False and is set to True by
    XXttNNoottiicceeSSiiggnnaall; the Intrinsics invoke the callback whenever the flag is
    True, and the flag is set to False just before the callback is invoked.

    The XXttRReemmoovveeSSiiggnnaall function is called to remove the specified Intrinsics
    signal handler. The client should disable the source of the signal before
    calling XXttRReemmoovveeSSiiggnnaall.

  SSEEEE AALLSSOO

    _X_t_A_p_p_A_d_d_T_i_m_e_O_u_t(3X11R6)

    _X_t_A_p_p_A_d_d_I_n_p_u_t(3X11R6)

    _X_t_A_p_p_A_d_d_W_o_r_k_P_r_o_c(3X11R6)

    X Toolkit Intrinsics - C Language Interface
    Xlib - C Language X Interface

