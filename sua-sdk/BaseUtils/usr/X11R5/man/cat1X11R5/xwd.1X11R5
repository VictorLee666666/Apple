xwd(1X11R5)                                                 xwd(1X11R5)

  xxwwdd

  NNAAMMEE

    xwd - dump an image of an X window

  SSYYNNOOPPSSIISS

    xwd [[-add value]] [[-debug]] [[-display display]]
        [[-frame]] [[-help]] [[-icmap]] [[-nobdrs]]
        [[-out file]] [[-root || -id id || -name name]]
        [[-screen]] [[-xy]]

  DDEESSCCRRIIPPTTIIOONN

    The xxwwdd(1X11R5) utility is an X Window System window dumping utility. The
    xxwwdd(1X11R5) utility allows X users to store window images in a specially
    formatted dump file. This file can then be read by various other X
    utilities for redisplay, printing, editing, formatting, archiving, image
    processing, and so on. The target window is selected by clicking the
    pointer in the desired window. The keyboard bell is rung once at the
    beginning of the dump and twice when the dump is completed.

  OOPPTTIIOONNSS
    --aadddd value
        This option specifies an signed value to be added to every pixel.

    --ffrraammee
        This option indicates that the window manager frame should be included
        when manually selecting a window.
    --ddiissppllaayy display
        This argument allows you to specify the server to connect to; see
        _X(5X11R5).
    --iidd id
        This option indicates that the window with the specified resource
        identifier (id) should be selected for the window dump, without
        requiring the user to select a window with the pointer.

    --iiccmmaapp
        Usually, the colormap of the chosen window is used to obtain red-
        green-blue (RGB) values. This option forces the first installed
        colormap of the screen to be used instead.

    --hheellpp
        Print out the 'Usage:' command syntax summary.
    --nnaammee name
        This option indicates that the window with the specified WM_NAME
        property should be selected for the window dump, without requiring the
        user to select a window with the pointer.

    --nnoobbddrrss
        This argument specifies that the window dump should not include the
        pixels that compose the X window border. This is useful in situations
        where you want to include the window contents in a document as an
        illustration.
    --oouutt file
        This argument allows the user to explicitly specify the output file on
        the command line. The default is to output to standard out.

    --rroooott
        This option indicates that the root window should be selected for the
        window dump without requiring the user to select a window with the
        pointer.

    --ssccrreeeenn
        This option indicates that the GetImage request used to obtain the
        image should be done on the root window rather than directly on the
        specified window. In this way, you can obtain pieces of other windows
        that overlap the specified window, and more importantly, you can
        capture menus or other popups that are independent windows but appear
        over the specified window.

    --xxyy
        This option applies to color displays only. It selects 'XY' format
        dumping instead of the default 'Z' format.

  EENNVVIIRROONNMMEENNTT VVAARRIIAABBLLEESS

    DISPLAY
        Contains default host and display number.

  FFIILLEESS

    XXWWDDFFiillee..hh
        X Window Dump File format definition file.

  CCOOPPYYRRIIGGHHTT

    Copyright 1988, Massachusetts Institute of Technology.

    See _X(5X11R5) for a full statement of rights and permissions.

  AAUUTTHHOORRSS

    Tony Della Fera, Digital Equipment Corp., MIT Project Athena
    William F. Wyatt, Smithsonian Astrophysical Observatory

  SSEEEE AALLSSOO

    _l_p(1)

    _x_w_u_d(1X11R5)

    _X(5X11R5)

