maze(1X11R5)                                               maze(1X11R5)

  mmaazzee

  NNAAMMEE

    maze - an automated X11 demonstration that repeatedly creates and solves a
    random maze

  SSYYNNOOPPSSIISS

    maze [[-S]] [[-r]] [[-g geometry]] [[-d display]]

  DDEESSCCRRIIPPTTIIOONN

    The mmaazzee(1X11R5) program creates a "random" maze and then solves it with
    graphical feedback.

    The mmaazzee(1X11R5) program accepts the following options:

    --SS
        Full screen window option.

    --rr
        Reverse video option.
    --gg geometry
        Specifies the window geometry to be used.
    --dd display
        Specifies the display to be used.

    The following list describes the current functionality of various mouse
    button clicks:

    LLeeffttBBuuttttoonn
        Clears the window and restarts maze.

    MMiiddddlleeBBuuttttoonn
        Toggles the maze program, first click -> stop, then click -> continue.

    RRiigghhttBBuuttttoonn
        Kills maze.

  LLIIMMIITTAATTIIOONNSS

    No color support.

    Expose events force a restart of the maze.

    Currently, mouse actions are based on "raw" values [ Button1, Button2 and
    Button3 ] from the ButtonPress event.

    Does not use pointer mapping.

  CCOOPPYYRRIIGGHHTT

    Copyright 1988 by Sun Microsystems, Inc. Mountain View, CA.

    All Rights Reserved

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted, provided
    that the above copyright notice appear in all copies and that both that
    copyright notice and this permission notice appear in supporting
    documentation, and that the names of Sun or MIT not be used in advertising
    or publicity pertaining to distribution of the software without specific
    prior written permission. Sun and M.I.T. make no representations about the
    suitability of this software for any purpose. It is provided "as is"
    without any express or implied warranty.

    SUN DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE. IN NO EVENT SHALL SUN BE LIABLE FOR ANY SPECIAL, INDIRECT OR
    CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
    USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

  AAUUTTHHOORR((ss))

    Richard Hess [ X11 extensions ] {...}!uunet!cimshop!rhess
    Consilium, Mountain View, CA

    Dave Lemke [ X11 version ] lemke@sun.COM
    Sun MicroSystems, Mountain View, CA

    Martin Weiss [ SunView version ]
    Sun MicroSystems, Mountain View, CA

