proxymngr(1X11R6)                                     proxymngr(1X11R6)

  PPRROOXXYYMMNNGGRR

  NNAAMMEE

    proxymngr - proxy manager service

  SSYYNNOOPPSSIISS

    proxymngr [[-config filename]] [[-timeout seconds]] [[-retries #]] [[-verbose]]

  DDEESSCCRRIIPPTTIIOONN

    The proxy manager (proxymngr) is responsible for resolving requests from
    xfindproxy (and other similar clients), starting new proxies when
    appropriate, and keeping track of all of the available proxy services. The
    proxy manager strives to reuse existing proxies whenever possible.

    There are two types of proxies that the proxy manager deals with, managed
    and unmanaged proxies.

    A managed proxy is a proxy that is started "on demand" by the proxy
    manager.

    An unmanaged proxy, on the other hand, is started either at system boot
    time, or manually by a system administrator. The proxy manager is made
    aware of its existence, but no attempt is made by the proxy manager to
    start unmanaged proxies.

    The command line options that can be specified to pprrooxxyymmnnggrr are:

    --ccoonnffiigg
        Used to override the default proxymngr config file. See below for more
        details about the config file.

    --ttiimmeeoouutt
        Sets the number of seconds between attempts made by the proxy manager
        to find an unmanaged proxy. The default is 10.

    --rreettrriieess
        Sets the maximum number of retries made by the proxy manager to find
        an an unmanaged proxy. The default is 3.

  PPrrooxxyy MMaannaaggeerr CCoonnffiigg FFiillee

    The proxy manager maintains a local configuration file describing the
    proxy services available. This configuration file is installed in /usr/
    X11R6.4/lib/X11/proxymngr/pmconfig during the installation of proxymngr.
    The location of the configuration file can be overwritten using the --
    ccoonnffiigg command line option.

    Aside from lines starting with an exclamation point for comments, each
    line of the configuration file describes either an unmanaged or managed
    proxy service.

    For unmanaged proxies, the format is:
        <service-name> unmanaged <proxy-address>

    service-name is the name of the unmanaged proxy service, and must not
    contain any spaces, for example "XFWP". service-name is case insenstive.

    proxy-address is the network address of the unmanaged proxy. The format of
    the address is specific to the service-name. For example, for the "XFWP"
    service, the proxy-address might be "firewall.x.org:100".

    If there is more than one entry in the config file with the same unmanaged
    service-name, the proxy manager will try to use the proxies in the order
    presented in the config file.

    For managed proxies, the format is:
        <service-name> managed <command-to-start-proxy>

    service-name is the name of the managed proxy service, and must not
    contain any spaces, for example "LBX". service-name is case insensitive.

    command-to-start-proxy is the command executed by the proxy manager to
    start a new instance of the proxy. If command-to-start-proxy contains
    spaces, the complete command should be surrounded by single quotes. If
    desired, command-to-start-proxy can be used to start a proxy on a remote
    machine. The specifics of the remote execution method used to do this is
    not specified here.

  EEXXAAMMPPLLEE

    Here is a sample configuration file:

        ! proxy manager config file
        !
        ! Each line has the format:
        !    <serviceName> managed <startCommand>
        !        or
        !    <serviceName> unmanaged <proxyAddress>
        !
        lbx managed /usr/X11R6.4/bin/lbxproxy
        !
        ! substitute site-specific info
        xfwp unmanaged firewall:4444

  PPRROOXXYY MMAANNAAGGEERR DDEETTAAIILLSS

    When the proxy manager gets a request from xfindproxy (or another similar
    client), its course of action will depend on the service-name in question.

    For a managed proxy service, the proxy manager will find out if any of the
    already running proxies for this service can handle a new request. If not,
    the proxy manager will attempt to start up a new instance of the proxy
    (using the command-to-start-proxy found in the config file). If that
    fails, an error will be returned to the caller.

    For an unmanaged proxy service, the proxy manager will look in the config
    file to find all unmanaged proxies for this service. If there is more than
    one entry in the config file with the same unmanaged service-name, the
    proxy manager will try to use the proxies in the order presented in the
    config file. If none of the unmanaged proxies can satisfy the request, the
    proxy manager will timeout for a configurable amount of time (specified by
    --ttiimmeeoouutt or default of 10) and reattempt to find an unmanaged proxy
    willing to satisfy the request. The number of retries can be specified by
    the --rreettrriieess argument, or a default of 3 will be used. If the retries
    fail, the proxy manager has no choice but to return an error to the caller
    (since the proxy manager can not start unmanaged proxy services).

  BBUUGGSS

    proxy manager listen port should be configurable.

    --ttiimmeeoouutt and --rreettrriieess is not implemented in proxymngr.

    proxymngr does not utilize the "options" and "host" fields in the proxy
    management protocol GetProxyAddr request.

  SSEEEE AALLSSOO

    Proxy Management Protocol spec V1.0

  AAUUTTHHOORR

    Ralph Mor, X Consortium

