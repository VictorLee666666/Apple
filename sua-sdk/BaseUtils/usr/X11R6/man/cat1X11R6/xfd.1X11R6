xfd(1X11R6)                                                 xfd(1X11R6)

  XXFFDD

  NNAAMMEE

    xfd - display all the characters in an X font

  SSYYNNOOPPSSIISS

    xfd [[-option......]] -fn fontname

  DDEESSCCRRIIPPTTIIOONN

    The xfd utility creates a window containing the name of the font being
    displayed, a row of command buttons, several lines of text for displaying
    character metrics, and a grid containing one glyph per cell. The
    characters are shown in increasing order from left to right, top to
    bottom. The first character displayed at the top left will be character
    number 0 unless the --ssttaarrtt option has been supplied in which case the
    character with the number given in the --ssttaarrtt option will be used.

    The characters are displayed in a grid of boxes, each large enough to hold
    any single character in the font. Each character glyph is drawn using the
    PolyText16 request (used by the Xlib routine XXDDrraawwSSttrriinngg1166). If the --bbooxx
    option is given, a rectangle will be drawn around each character, showing
    where an ImageText16 request (used by the Xlib routine XXDDrraawwIImmaaggeeSSttrriinngg1166)
    would cause background color to be displayed.

    The origin of each glyph is normally set so that the character is drawn in
    the upper left hand corner of the grid cell. However, if a glyph has a
    negative left bearing or an unusually large ascent, descent, or right
    bearing (as is the case with cursor font), some character may not appear
    in their own grid cells. The --cceenntteerr option may be used to force all
    glyphs to be centered in their respective cells.

    All the characters in the font may not fit in the window at once. To see
    the next page of glyphs, press the Next button at the top of the window.
    To see the previous page, press Prev. To exit xfd, press Quit.

    Individual character metrics (index, width, bearings, ascent and descent)
    can be displayed at the top of the window by clicking on the desired
    character.

    The font name displayed at the top of the window is the full name of the
    font, as determined by the server. See xlsfonts for ways to generate lists
    of fonts, as well as more detailed summaries of their metrics and
    properties.

  OOPPTTIIOONNSS

    xfd accepts all of the standard toolkit command line options along with
    the additional options listed below:
    --ffnn font
        This option specifies the font to be displayed. This can also be set
        with the FontGrid ffoonntt resource. A font must be specified.

    --bbooxx
        This option indicates that a box should be displayed outlining the
        area that would be filled with background color by an ImageText
        request. This can also be set with the FontGrid bbooxxCChhaarrss resource. The
        default is False.

    --cceenntteerr
        This option indicates that each glyph should be centered in its grid.
        This can also be set with the FontGrid cceenntteerrCChhaarrss resource. The
        default is False.
    --ssttaarrtt number
        This option specifies the glyph index of the upper left hand corner of
        the grid. This is used to view characters at arbitrary locations in
        the font. This can also be set with the FontGrid ssttaarrttCChhaarr resource.
        The default is 0.
    --bbcc color
        This option specifies the color to be used if ImageText boxes are
        drawn. This can also be set with the FontGrid bbooxxCCoolloorr resource.
    --rroowwss numrows
        This option specifies the number of rows in the grid. This can also be
        set with the FontGrid cceellllRRoowwss resource.
    --ccoolluummnnss numcols
        This option specifies the number of columns in the grid. This can also
        be set with the FontGrid cceellllCCoolluummnnss resource.

  WWIIDDGGEETTSS

    In order to specify resources, it is useful to know the widgets which
    compose xfd. In the notation below, indentation indicates hierarchical
    structure. The widget class name is given first, followed by the widget
    instance name. The application class name is Xfd.

    Xfd  xfd
         Paned  pane
              Label  fontname
              Box  box
                   Command  quit
                   Command  prev
                   Command  next
              Label  select
              Label  metrics
              Label  range
              Label  start
              Form  form
                   FontGrid  grid

  FFOONNTTGGRRIIDD RREESSOOUURRCCEESS

    The FontGrid widget is an application-specific widget, and a subclass of
    the Simple widget in the Athena widget set. The effects and instance names
    of this widget's resources are given in the OOPPTTIIOONNSS section. Capitalize
    the first letter of the resource instance name to get the corresponding
    class name.

  AAPPPPLLIICCAATTIIOONN SSPPEECCIIFFIICC RREESSOOUURRCCEESS

    The instance names of the application specific resources are given below.
    Capitalize the first letter of the resource instance name to get the
    corresponding class name. These resources are unlikely to be interesting
    unless you are localizing xfd for a different language.

    sseelleeccttFFoorrmmaatt
        Specifies a printf-style format string used to display information
        about the selected character. The default is "character 0x%02x%02x
        (%u,%u) (%#o,%#o)". The arguments that will come after the format
        string are char.byte1, char.byte2, char.byte1, char.byte2, char.byte1,
        char.byte2. char.byte1 is byte 1 of the selected character. char.byte2
        is byte 2 of the selected character.

    mmeettrriiccssFFoorrmmaatt
        Specifies a printf-style format string used to display character
        metrics. The default is "width %d; left %d, right %d; ascent %d,
        descent %d (font %d, %d)". The arguments that will come after the
        format string are the character metrics width, lbearing, rbearing,
        character ascent, character descent, font ascent, and font descent.

    rraannggeeFFoorrmmaatt
        Specifies a printf-style format string used to display the range of
        characters currently being displayed. The default is "range:
        0x%02x%02x (%u,%u) thru 0x%02x%02x (%u,%u)". The arguments that will
        come after the format string are the following fields from the
        XFontStruct that is returned from opening the font: min_byte1,
        min_char_or_byte2, min_byte1, min_char_or_byte2, max_byte1,
        max_char_or_byte2, max_byte1, max_char_or_byte2.

    ssttaarrttFFoorrmmaatt
        Specifies a printf-style format string used to display information
        about the character at the upper left corner of the font grid. The
        default is "upper left: 0x%04x (%d,%d)". The arguments that will come
        after the format string are the new character, the high byte of the
        new character, and the low byte of the new character.

    nnoocchhaarrFFoorrmmaatt
        Specifies a printf-style format string to display when the selected
        character does not exist. The default is "no such character 0x%02x%02x
        (%u,%u) (%#o,%#o)". The arguments that will come after the format
        string are the same as for the sseelleeccttFFoorrmmaatt resource.

  SSEEEE AALLSSOO

    _X(5X11R5)

    _x_l_s_f_o_n_t_s(1X11R6)

    _x_r_d_b(1X11R6)

    X Logical Font Description Conventions

  BBUUGGSS

    The program should skip over pages full of non-existent characters.

  AAUUTTHHOORR

    Jim Fulton, MIT X Consortium; previous program of the same name by Mark
    Lillibridge, MIT Project Athena.

