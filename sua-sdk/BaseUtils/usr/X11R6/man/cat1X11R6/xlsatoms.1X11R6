xlsatoms(1X11R6)                                       xlsatoms(1X11R6)

  XXLLSSAATTOOMMSS

  NNAAMMEE

    xlsatoms - list interned atoms defined on server

  SSYYNNOOPPSSIISS

    xlsatoms [[-options......]]

  DDEESSCCRRIIPPTTIIOONN

    Xlsatoms lists the interned atoms. By default, all atoms starting from 1
    (the lowest atom value defined by the protocol) are listed until unknown
    atom is found. If an explicit range is given, xlsatoms will try all atoms
    in the range, regardless of whether or not any are undefined.

  OOPPTTIIOONNSS

    --ddiissppllaayy dpy
        This option specifies the X server to which to connect.
    --ffoorrmmaatt string
        This option specifies a printf-style string used to list each atom
        <value,name> pair, printed in that order (value is an unsigned long
        and name is a char *). Xlsatoms will supply a newline at the end of
        each line. The default is %ld\t%s.
    --rraannggee [low]-[high]
        This option specifies the range of atom values to check. If low is not
        given, a value of 1 assumed. If high is not given, xlsatoms will stop
        at the first undefined atom at or above low.
    --nnaammee string
        This option specifies the name of an atom to list. If the atom does
        not exist, a message will be printed on the standard error.

  SSEEEE AALLSSOO

    _X(5X11R5)

    _X_s_e_r_v_e_r(5)

    _x_p_r_o_p(1X11R6)

  EENNVVIIRROONNMMEENNTT

    DDIISSPPLLAAYY
        to get the default host and display to use.

  AAUUTTHHOORR

    Jim Fulton, MIT X Consortium

