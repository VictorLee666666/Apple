smproxy(1X11R6)                                         smproxy(1X11R6)

  ssmmpprrooxxyy

  NNAAMMEE

    smproxy - Session Manager Proxy

  SSYYNNOOPPSSIISS

    smproxy [[-clientId id]] [[-restore saveFile]]

  OOPPTTIIOONNSS
    --cclliieennttIIdd id
        Specifies the session ID used by smproxy in the previous session.
    --rreessttoorree saveFile
        Specifies the file used by smproxy to save state in the previous
        session.

  DDEESSCCRRIIPPTTIIOONN

    smproxy allows X applications that do not support X11R6 session management
    to participate in an X11R6 session.

    In order for smproxy to act as a proxy for an X application, one of the
    following must be true:

    - The application maps a top level window containing the WWMM__CCLLIIEENNTT__LLEEAADDEERR
    property. This property provides a pointer to the client leader window
    which contains the WWMM__CCLLAASSSS, WWMM__NNAAMMEE, WWMM__CCOOMMMMAANNDD, and WWMM__CCLLIIEENNTT__MMAACCHHIINNEE
    properties.

    or ...

    - The application maps a top level window which does not contain the
    WWMM__CCLLIIEENNTT__LLEEAADDEERR property. However, this top level window contains the
    WWMM__CCLLAASSSS, WWMM__NNAAMMEE, WWMM__CCOOMMMMAANNDD, and WWMM__CCLLIIEENNTT__MMAACCHHIINNEE properties.

    An application that support the WWMM__SSAAVVEE__YYOOUURRSSEELLFF protocol will receive a
    WWMM__SSAAVVEE__YYOOUURRSSEELLFF client message each time the session manager issues a
    checkpoint or shutdown. This allows the application to save state. If an
    application does not support the WWMM__SSAAVVEE__YYOOUURRSSEELLFF protocol, then the proxy
    will provide enough information to the session manager to restart the
    application (using WWMM__CCOOMMMMAANNDD), but no state will be restored.

  SSEEEE AALLSSOO

    _x_s_m(1X11R6)

  AAUUTTHHOORR

    Ralph Mor, X Consortium

