x11perfcomp(1X11R6)                                 x11perfcomp(1X11R6)

  xx1111ppeerrffccoommpp

  NNAAMMEE

    x11perfcomp - X11 server performance comparison program

  SSYYNNOOPPSSIISS

    x11perfcomp [[-r||-ro]] [[-l label_file]] files

  DDEESSCCRRIIPPTTIIOONN

    The xx1111ppeerrffccoommpp(1X11R6) program merges the output of several
    xx1111ppeerrff(1X11R6) runs into a nice tabular format. It takes the results in
    each file, fills in any missing test results if necessary, and for each
    test, shows the objects/second rate of each server. If invoked with the --
    rr or --rroo options, it shows the relative performance of each server to the
    first server.

    Normally, xx1111ppeerrffccoommpp(1X11R6) uses the first file specified to determine
    which specific tests it should report on. Some servers might fail to
    perform all tests. In this case, xx1111ppeerrffccoommpp(1X11R6) automatically
    substitutes in a rate of 0.0 objects/second. Because the first file
    determines which tests to report on, this file must contain a superset of
    the tests reported in the other files or xx1111ppeerrffccoommpp(1X11R6) will fail.

    You can provide an explicit list of tests to report on by using the --
    ll switch to specify a file of labels. You can create a label file by using
    the -label option in xx1111ppeerrff(1X11R6).

  OOPPTTIIOONNSS

    The xx1111ppeerrffccoommpp(1X11R6) utility accepts the following options:

    --rr
        Specifies that the output should also include relative server
        performance.

    --rroo
        Specifies that the output should include only relative server
        performance.
    --ll label_file
        Specifies a label file to use.

  XX DDEEFFAAUULLTTSS

    There are no X defaults used by this program.

  SSEEEE AALLSSOO

    _X(5X11R5)

    _x_1_1_p_e_r_f(1X11R6)

