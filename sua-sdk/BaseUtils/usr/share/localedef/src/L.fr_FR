#
# French Language Locale for France
# Source: RAP
# Address: Sankt Jo//rgens Alle 8
#    DK-1615 Ko//benhavn V, Danmark
# Contact: Keld Simonsen
# Email: Keld.Simonsen@dkuug.dk
# Tel: +45 - 31226543
# Fax: +45 - 33256543
# Language: fr
# Territory: FR
# Revision: 4.3
# Date: 1996-10-15
# Users: general
# Repertoiremap: mnemonic,ds
# Charset: ISO-8859-1
# Distribution and use is free, also
# for commercial purposes.

# $Id: fr_FR,v 1.1 2000/03/29 21:33:55 jeffcope Exp $

LC_MESSAGES
yesexpr	"<left-square-bracket><o><O><y><Y><right-square-bracket>\
	<period><asterisk>"
noexpr  "<left-square-bracket><n><N><right-square-bracket><period><asterisk>"
END LC_MESSAGES

LC_MONETARY
int_curr_symbol           "<F><R><F><space>"
currency_symbol           "<F>"
mon_decimal_point         "<comma>"
mon_thousands_sep         "<space>"
mon_grouping              3
positive_sign             ""
negative_sign             "<hyphen>"
int_frac_digits           2
frac_digits               2
p_cs_precedes             0
p_sep_by_space            1
n_cs_precedes             0
n_sep_by_space            1
p_sign_posn               1
n_sign_posn               1
END LC_MONETARY

LC_NUMERIC
decimal_point             "<comma>"
thousands_sep             "<period>"
grouping                  3
END LC_NUMERIC

LC_TIME
abday   "<d><i><m>";"<l><u><n>";\
        "<m><a><r>";"<m><e><r>";\
        "<j><e><u>";"<v><e><n>";\
        "<s><a><m>"
day     "<d><i><m><a><n><c><h><e>";\
        "<l><u><n><d><i>";\
        "<m><a><r><d><i>";\
        "<m><e><r><c><r><e><d><i>";\
        "<j><e><u><d><i>";\
        "<v><e><n><d><r><e><d><i>";\
        "<s><a><m><e><d><i>"
abmon   "<j><a><n>";"<f><e-acute><v>";\
        "<m><a><r>";"<a><v><r>";\
        "<m><a><i>";"<j><u><n>";\
        "<j><u><i>";"<a><o><u-circumflex>";\
        "<s><e><p>";"<o><c><t>";\
        "<n><o><v>";"<d><e-acute><c>"
mon     "<j><a><n><v><i><e><r>";\
        "<f><e-acute><v><r><i><e><r>";\
        "<m><a><r><s>";\
        "<a><v><r><i><l>";\
        "<m><a><i>";\
        "<j><u><i><n>";\
        "<j><u><i><l><l><e><t>";\
        "<a><o><u-circumflex><t>";\
        "<s><e><p><t><e><m><b><r><e>";\
        "<o><c><t><o><b><r><e>";\
        "<n><o><v><e><m><b><r><e>";\
        "<d><e-acute><c><e><m><b><r><e>"     
d_t_fmt "<percent-sign><a><space><percent-sign><d><space><percent-sign>\
<b><space><percent-sign><Y><space><percent-sign><T><space><percent-sign><Z>"
d_fmt   "<percent-sign><d><period><percent-sign><m><period><percent-sign><Y>"
t_fmt   "<percent-sign><T>"
am_pm   "";""
t_fmt_ampm ""
END LC_TIME


LC_CTYPE

digit   <zero>;<one>;<two>;<three>;<four>;\
        <five>;<six>;<seven>;<eight>;<nine>

xdigit  <zero>;<one>;<two>;<three>;<four>;\
        <five>;<six>;<seven>;<eight>;<nine>;\
        <A>;<B>;<C>;<D>;<E>;<F>;\
        <a>;<b>;<c>;<d>;<e>;<f>

blank   <space>;<tab>;<NS>

space   <space>;<newline>;<vertical-tab>;<form-feed>;\
        <carriage-return>;<tab>;<NS>

upper   <A>;<B>;<C>;<D>;<E>;<F>;<G>;\
        <H>;<I>;<J>;<K>;<L>;<M>;<N>;\
        <O>;<P>;<Q>;<R>;<S>;<T>;<U>;\
        <V>;<W>;<X>;<Y>;<Z>;<A-grave>;<A-acute>;\
        <A-circumflex>;<A-tilde>;<A-umlaut>;<A-ring>;<AE>;<C-cedilla>;\
        <E-grave>;<E-acute>;<E-circumflex>;<E-umlaut>;<I-grave>;<I-acute>;\
        <I-cicumflex>;<I-umlaut>;<Eth>;<N-tilde>;<O-grave>;<O-acute>;\
        <O-circumflex>;<O-tilde>;<O-umlaut>;<O-slash>;<U-grave>;\
        <U-acute>;<U-circumflex>;<U-umlaut>;<Y-acute>;<Thorn>

lower   <a>;<b>;<c>;<d>;<e>;<f>;<g>;\
        <h>;<i>;<j>;<k>;<l>;<m>;<n>;\
        <o>;<p>;<q>;<r>;<s>;<t>;<u>;\
        <v>;<w>;<x>;<y>;<z>;<ss>;<a-grave>;\
        <a-acute>;<a-circumflex>;<a-tilde>;<a-umlaut>;<a-ring>;<ae>;\
        <c-cedilla>;<e-grave>;<e-acute>;<e-circumflex>;<e-umlaut>;<i-grave>;\
        <i-acute>;<i-circumflex>;<i-umlaut>;<eth>;<n-tilde>;<o-grave>;\
        <o-acute>;<o-circumflex>;<o-tilde>;<o-umlaut>;<o-slash>;\
        <u-grave>;<u-acute>;<u-circumflex>;<u-umlaut>;<y-acute>;<thorn>;\
        <y-umlaut>

alpha	<A>;...;<Z>; <a>;...;<z>;\
	<soft-hyphen>; \
	<A-grave>;...;<O-umlaut>; <O-slash>;...;<Thorn>; <ss>; \
	<a-grave>;...;<o-umlaut>; <o-slash>;...;<thorn>; <y-umlaut>
#-alpha   <A>;<B>;<C>;<D>;<E>;<F>;<G>;\
#-        <H>;<I>;<J>;<K>;<L>;<M>;<N>;\
#-        <O>;<P>;<Q>;<R>;<S>;<T>;<U>;\
#-        <V>;<W>;<X>;<Y>;<Z>;<a>;<b>;\
#-        <c>;<d>;<e>;<f>;<g>;<h>;<i>;\
#-        <j>;<k>;<l>;<m>;<n>;<o>;<p>;\
#-        <q>;<r>;<s>;<t>;<u>;<v>;<w>;\
#-        <x>;<y>;<z>;<soft-hyphen>;<A-grave>;<A-acute>;\
#-        <A-circumflex>;<A-tilde>;<A-umlaut>;<A-ring>;<AE>;<C-cedilla>;\
#-        <E-grave>;<E-acute>;<E-circumflex>;<E-umlaut>;<I-grave>;<I-acute>;\
#-        <I-cicumflex>;<I-umlaut>;<Eth>;<N-tilde>;<O-grave>;<O-acute>;\
#-        <O-circumflex>;<O-tilde>;<O-umlaut>;<O-slash>;<U-grave>;\
#-        <U-acute>;<U-circumflex>;<U-umlaut>;<Y-acute>;<Thorn>;<ss>;\
#-        <a-grave>;<a-acute>;<a-circumflex>;<a-tilde>;<a-umlaut>;<a-ring>;\
#-        <ae>;<c-cedilla>;<e-grave>;<e-acute>;<e-circumflex>;<e-umlaut>;\
#-        <i-grave>;<i-acute>;<i-circumflex>;<i-umlaut>;<eth>;<n-tilde>;\
#-        <o-grave>;<o-acute>;<o-circumflex>;<o-tilde>;<o-umlaut>;\
#-        <o-slash>;<u-grave>;<u-acute>;<u-circumflex>;<u-umlaut>;\
#-        <y-acute>;<thorn>;<y-umlaut>

cntrl   <NUL>;<SOH>;<STX>;<ETX>;<EOT>;<ENQ>;\
        <ACK>;<alert>;<backspace>;<tab>;<newline>;<vertical-tab>;\
        <form-feed>;<carriage-return>;<SO>;<SI>;<DLE>;<DC1>;\
        <DC2>;<DC3>;<DC4>;<NAK>;<SYN>;<ETB>;\
        <CAN>;<EM>;<SUB>;<ESC>;<IS4>;<IS3>;\
        <IS2>;<IS1>;<DEL>;<PAD>;<HOP>;<BPH>;\
        <NBH>;<IND>;<NEL>;<SSA>;<ESA>;<HTS>;\
        <HTJ>;<VTS>;<PLD>;<PLU>;<RI>;<SS2>;\
        <SS3>;<DCS>;<PU1>;<PU2>;<STS>;<CCH>;\
        <MW>;<SPA>;<EPA>;<SOS>;<SGCI>;<SCI>;\
        <CSI>;<ST>;<OSC>;<PM>;<APC>

punct   <exclamation-mark>;<quotation-mark>;<number-sign>;\
	<dollar-sign>;<percent-sign>;<ampersand>;<apostrophe>;\
        <left-parenthesis>;<right-parenthesis>;<asterisk>;\
	<plus-sign>;<comma>;<hyphen>;<period>;\
        <slash>;<colon>;<semicolon>;<less-than-sign>;<equals-sign>;\
	<greater-than-sign>;<question-mark>;\
        <commercial-at>;<left-square-bracket>;<backslash>;\
	<right-square-bracket>;<circumflex>;\
        <underscore>;<grave-accent>;<left-curly-bracket>;\
	<vertical-line>;<right-curly-bracket>;<tilde>;\
        <inv-exclamation-mark>;<cent-sign>;<sterling>;\
	<currency>;<yen>;<broken-bar>;\
        <section>;<umlaut>;<copyright>;\
	<feminine>;<left-guillemot>;<not-sign>;\
        <registered>;<macron-accent>;<degree-sign>;\
	<plus-or-minus>;<super-two>;<super-three>;\
        <acute-accent>;<micro-sign>;<paragraph>;\
	<middle-dot>;<cedilla>;<super-one>;\
        <masculine>;<right-guillemot>;\
	<one-quarter>;<one-half>;<three-quarters>;\
        <inv-question-mark>;<multiply>;<divide>

tolower (<A>,<a>);(<A-grave>,<a-grave>);\
        (<A-acute>,<a-acute>);\
        (<A-umlaut>,<a-umlaut>);\
        (<A-circumflex>,<a-circumflex>);\
        (<A-tilde>,<a-tilde>);\
        (<A-ring>,<a-ring>);\
        (<AE>,<ae>);\
        (<B>,<b>);\
        (<C>,<c>);\
        (<C-cedilla>,<c-cedilla>);\
        (<D>,<d>);\
        (<Eth>,<eth>);\
        (<E>,<e>);(<E-grave>,<e-grave>);\
        (<E-acute>,<e-acute>);\
        (<E-umlaut>,<e-umlaut>);\
        (<E-circumflex>,<e-circumflex>);\
        (<F>,<f>);\
        (<G>,<g>);\
        (<H>,<h>);\
        (<I>,<i>);\
        (<I-grave>,<i-grave>);\
        (<I-acute>,<i-acute>);\
        (<I-umlaut>,<i-umlaut>);\
        (<I-cicumflex>,<i-circumflex>);\
        (<J>,<j>);\
        (<K>,<k>);\
        (<L>,<l>);\
        (<M>,<m>);\
        (<N>,<n>);\
        (<N-tilde>,<n-tilde>);\
        (<O>,<o>);(<O-grave>,<o-grave>);\
        (<O-acute>,<o-acute>);\
        (<O-slash>,<o-slash>);\
        (<O-umlaut>,<o-umlaut>);\
        (<O-circumflex>,<o-circumflex>);\
        (<O-tilde>,<o-tilde>);\
        (<P>,<p>);\
        (<Q>,<q>);\
        (<R>,<r>);\
        (<S>,<s>);\
        (<T>,<t>);\
        (<Thorn>,<thorn>);\
        (<U>,<u>);(<U-grave>,<u-grave>);\
        (<U-acute>,<u-acute>);\
        (<U-umlaut>,<u-umlaut>);\
        (<U-circumflex>,<u-circumflex>);\
        (<V>,<v>);\
        (<W>,<w>);\
        (<X>,<x>);\
        (<Y>,<y>);\
        (<Y-acute>,<y-acute>);\
        (<Z>,<z>)

toupper (<a>,<A>);(<a-grave>,<A-grave>);\
        (<a-acute>,<A-acute>);\
        (<a-umlaut>,<A-umlaut>);\
        (<a-circumflex>,<A-circumflex>);\
        (<a-tilde>,<A-tilde>);\
        (<a-ring>,<A-ring>);\
        (<ae>,<AE>);\
        (<b>,<B>);\
        (<c>,<C>);\
        (<c-cedilla>,<C-cedilla>);\
        (<d>,<D>);\
        (<eth>,<Eth>);\
        (<e>,<E>);(<e-grave>,<E-grave>);\
        (<e-acute>,<E-acute>);\
        (<e-umlaut>,<E-umlaut>);\
        (<e-circumflex>,<E-circumflex>);\
        (<f>,<F>);\
        (<g>,<G>);\
        (<h>,<H>);\
        (<i>,<I>);\
        (<i-grave>,<I-grave>);\
        (<i-acute>,<I-acute>);\
        (<i-umlaut>,<I-umlaut>);\
        (<i-circumflex>,<I-cicumflex>);\
        (<j>,<J>);\
        (<k>,<K>);\
        (<l>,<L>);\
        (<m>,<M>);\
        (<n>,<N>);\
        (<n-tilde>,<N-tilde>);\
        (<o>,<O>);(<o-grave>,<O-grave>);\
        (<o-acute>,<O-acute>);\
        (<o-slash>,<O-slash>);\
        (<o-umlaut>,<O-umlaut>);\
        (<o-circumflex>,<O-circumflex>);\
        (<o-tilde>,<O-tilde>);\
        (<p>,<P>);\
        (<q>,<Q>);\
        (<r>,<R>);\
        (<s>,<S>);\
        (<t>,<T>);\
        (<thorn>,<Thorn>);\
        (<u>,<U>);(<u-grave>,<U-grave>);\
        (<u-acute>,<U-acute>);\
        (<u-umlaut>,<U-umlaut>);\
        (<u-circumflex>,<U-circumflex>);\
        (<v>,<V>);\
        (<w>,<W>);\
        (<x>,<X>);\
        (<y>,<Y>);\
        (<y-acute>,<Y-acute>);\
        (<z>,<Z>)

END LC_CTYPE


LC_COLLATE

# Base collation scheme: 1994-03-22

# Ordering algorithm:
#  1. Spaces and hyphen (but not soft
#     hyphen) before punctuation
#     characters, punctuation characters
#     before numbers,
#     numbers before letters.
#  2. Letters with diacritical marks are
#     members of equivalence classes
#  3. A distinction is made with regards
#     to case as noted below.
#  4. Special characters are ignored 
#     when comparing letters, but then
#     they are considered
#  5. The alphabets are sorted in order
#     of first appearance in ISO 10646:
#     Latin, Greek, Cyrillic, etc.
#
# According to Greek specifications,
# the steps 2. and 3. above are reversed
# for the Greek script

# collating symbols

# The collating symbol naming is
# mostly taken from ISO 10646-1,
# for example the case and accent
# names are from this standard.

collating-symbol <CAPITAL>
collating-symbol <CAPITAL-SMALL>
collating-symbol <SMALL-CAPITAL>
collating-symbol <SMALL>

# <CAPITAL-SMALL> and <SMALL-CAPITAL>
# are for cases like Danish <A><a>
# and Spanish <C><h> being treated
# as one letter.

# The <a8> ...... <z8> collating 
# symbols have defined weights as
# the last character in a group of
# Latin letters. They are used 
# to specify deltas by locales using
# a locale as the default ordering
# and by "replace-after" statements
# specifying the changed placement
# in an ordering of a character.

collating-symbol <a8>
collating-symbol <b8>
collating-symbol <c8>
collating-symbol <d8>
collating-symbol <e8>
collating-symbol <f8>
collating-symbol <g8>
collating-symbol <h8>
collating-symbol <i8>
collating-symbol <j8>
collating-symbol <k8>
collating-symbol <l8>
collating-symbol <m8>
collating-symbol <n8>
collating-symbol <o8>
collating-symbol <p8>
collating-symbol <q8>
collating-symbol <r8>
collating-symbol <s8>
collating-symbol <t8>
collating-symbol <u8>
collating-symbol <v8>
collating-symbol <w8>
collating-symbol <x8>
collating-symbol <y8>
collating-symbol <z8>

collating-symbol <NONE>
collating-symbol <ACUTE>
collating-symbol <ACUTE+DOT>
collating-symbol <GRAVE>
collating-symbol <DOUBLE-GRAVE>
collating-symbol <BREVE>
collating-symbol <BREVE+ACUTE>
collating-symbol <BREVE+GRAVE>
collating-symbol <BREVE+MACRON>
collating-symbol <BREVE+HOOK>
collating-symbol <BREVE+TILDE>
collating-symbol <BREVE+DOT-BELOW>
collating-symbol <BREVE-BELOW>
collating-symbol <INVERTED-BREVE>
collating-symbol <CIRCUMFLEX>
collating-symbol <CIRCUMFLEX+ACUTE>
collating-symbol <CIRCUMFLEX+GRAVE>
collating-symbol <CIRCUMFLEX+HOOK>
collating-symbol <CIRCUMFLEX+TILDE>
collating-symbol <CIRCUMFLEX+DOT-BELOW>
collating-symbol <CARON>
collating-symbol <CARON+DIAERESIS>
collating-symbol <CARON+DOT>
collating-symbol <RING>
collating-symbol <RING+ACUTE>
collating-symbol <RING-BELOW>
collating-symbol <DIAERESIS>
collating-symbol <DIAERESIS+MACRON>
collating-symbol <DIAERESIS+ACUTE>
collating-symbol <DIAERESIS+GRAVE>
collating-symbol <DIAERESIS+CARON>
collating-symbol <DOUBLE-ACUTE>
collating-symbol <HOOK>
collating-symbol <TILDE>
collating-symbol <TILDE+ACUTE>
collating-symbol <TILDE+DIAERESIS>
collating-symbol <TILDE-BELOW>
collating-symbol <DOT>
collating-symbol <DOT-BELOW>
collating-symbol <DOT+DOT-BELOW>
collating-symbol <STROKE>
collating-symbol <STROKE+ACUTE>
collating-symbol <CEDILLA>
collating-symbol <CEDILLA+ACUTE>
collating-symbol <CEDILLA+GRAVE>
collating-symbol <CEDILLA+BREVE>
collating-symbol <OGONEK>
collating-symbol <OGONEK+MACRON>
collating-symbol <MACRON>
collating-symbol <MACRON+ACUTE>
collating-symbol <MACRON+GRAVE>
collating-symbol <MACRON+DIAERESIS>
collating-symbol <MACRON+DIAERESIS-BELOW>
collating-symbol <MACRON+DOT>
collating-symbol <MACRON+DOT-BELOW>
collating-symbol <MACRON+CIRCUMFLEX>
collating-symbol <LINE-BELOW>
collating-symbol <HORN>
collating-symbol <HORN+ACUTE>
collating-symbol <HORN+GRAVE>
collating-symbol <HORN+HOOK>
collating-symbol <HORN+TILDE>
collating-symbol <HORN+DOT-BELOW>
collating-symbol <PRECEDED-BY-APOSTROPHE>
collating-symbol <GREEK>
collating-symbol <TONOS>
collating-symbol <DIALYTICA>
collating-symbol <DIALYTICA+TONOS>
collating-symbol <CYRILLIC>
collating-symbol <HIRAGANA>
collating-symbol <KATAKANA>
collating-symbol <SPECIAL>

# letter;accent;case;specials

order_start forward;backward;forward;position

# <CAPITAL> or <SMALL> letters first:

<CAPITAL>
<CAPITAL-SMALL>
<SMALL-CAPITAL>
<SMALL>

# Accents:

<NONE>
<ACUTE>
<ACUTE+DOT>
<GRAVE>
<DOUBLE-GRAVE>
<BREVE>
<BREVE+ACUTE>
<BREVE+GRAVE>
<BREVE+MACRON>
<BREVE+HOOK>
<BREVE+TILDE>
<BREVE+DOT-BELOW>
<BREVE-BELOW>
<INVERTED-BREVE>
<CIRCUMFLEX>
<CIRCUMFLEX+ACUTE>
<CIRCUMFLEX+GRAVE>
<CIRCUMFLEX+HOOK>
<CIRCUMFLEX+TILDE>
<CIRCUMFLEX+DOT-BELOW>
<CARON>
<CARON+DIAERESIS>
<CARON+DOT>
<RING>
<RING+ACUTE>
<RING-BELOW>
<DIAERESIS>
<DIAERESIS+MACRON>
<DIAERESIS+ACUTE>
<DIAERESIS+GRAVE>
<DIAERESIS+CARON>
<DOUBLE-ACUTE>
<HOOK>
<TILDE>
<TILDE+ACUTE>
<TILDE+DIAERESIS>
<TILDE-BELOW>
<DOT>
<DOT-BELOW>
<DOT+DOT-BELOW>
<STROKE>
<STROKE+ACUTE>
<CEDILLA>
<CEDILLA+ACUTE>
<CEDILLA+GRAVE>
<CEDILLA+BREVE>
<OGONEK>
<OGONEK+MACRON>
<MACRON>
<MACRON+ACUTE>
<MACRON+GRAVE>
<MACRON+DIAERESIS>
<MACRON+DIAERESIS-BELOW>
<MACRON+DOT>
<MACRON+DOT-BELOW>
<MACRON+CIRCUMFLEX>
<LINE-BELOW>
<HORN>
<HORN+ACUTE>
<HORN+GRAVE>
<HORN+HOOK>
<HORN+TILDE>
<HORN+DOT-BELOW>
<PRECEDED-BY-APOSTROPHE>
<GREEK>
<TONOS>
<DIALYTICA>
<DIALYTICA+TONOS>
<CYRILLIC>
<HIRAGANA>
<KATAKANA>
<SPECIAL>

<NS>	<NS>;<NS>;<NS>;IGNORE
<space>	IGNORE;IGNORE;IGNORE;<space>
<tab>	IGNORE;IGNORE;IGNORE;<tab>
<vertical-tab>	IGNORE;IGNORE;IGNORE;<vertical-tab>
<carriage-return>	IGNORE;IGNORE;IGNORE;<carriage-return>
<newline>	IGNORE;IGNORE;IGNORE;<newline>
<form-feed>	IGNORE;IGNORE;IGNORE;<form-feed>
<underscore>	IGNORE;IGNORE;IGNORE;<underscore>
<macron-accent>	IGNORE;IGNORE;IGNORE;<macron-accent>
<soft-hyphen>	IGNORE;IGNORE;IGNORE;<soft-hyphen>
<hyphen>	IGNORE;IGNORE;IGNORE;<hyphen>
<comma>	IGNORE;IGNORE;IGNORE;<comma>
<semicolon>	IGNORE;IGNORE;IGNORE;<semicolon>
<colon>	IGNORE;IGNORE;IGNORE;<colon>
<exclamation-mark>	IGNORE;IGNORE;IGNORE;<exclamation-mark>
<inv-exclamation-mark>	IGNORE;IGNORE;IGNORE;<inv-exclamation-mark>
<question-mark>	IGNORE;IGNORE;IGNORE;<question-mark>
<inv-question-mark>	IGNORE;IGNORE;IGNORE;<inv-question-mark>
<slash>	IGNORE;IGNORE;IGNORE;<slash>
<period>	IGNORE;IGNORE;IGNORE;<period>
<acute-accent>	IGNORE;IGNORE;IGNORE;<acute-accent>
<grave-accent>	IGNORE;IGNORE;IGNORE;<grave-accent>
<circumflex>	IGNORE;IGNORE;IGNORE;<circumflex>
<umlaut>	IGNORE;IGNORE;IGNORE;<umlaut>
<tilde>	IGNORE;IGNORE;IGNORE;<tilde>
<middle-dot>	IGNORE;IGNORE;IGNORE;<middle-dot>
<cedilla>	IGNORE;IGNORE;IGNORE;<cedilla>
<apostrophe>	IGNORE;IGNORE;IGNORE;<apostrophe>
<quotation-mark>	IGNORE;IGNORE;IGNORE;<quotation-mark>
<left-guillemot>	IGNORE;IGNORE;IGNORE;<left-guillemot>
<right-guillemot>	IGNORE;IGNORE;IGNORE;<right-guillemot>
<left-parenthesis>	IGNORE;IGNORE;IGNORE;<left-parenthesis>
<right-parenthesis>	IGNORE;IGNORE;IGNORE;<right-parenthesis>
<left-square-bracket>	IGNORE;IGNORE;IGNORE;<left-square-bracket>
<right-square-bracket>	IGNORE;IGNORE;IGNORE;<right-square-bracket>
<left-curly-bracket>	IGNORE;IGNORE;IGNORE;<left-curly-bracket>
<right-curly-bracket>	IGNORE;IGNORE;IGNORE;<right-curly-bracket>
<section>	IGNORE;IGNORE;IGNORE;<section>
<paragraph>	IGNORE;IGNORE;IGNORE;<paragraph>
<copyright>	IGNORE;IGNORE;IGNORE;<copyright>
<registered>	IGNORE;IGNORE;IGNORE;<registered>
<commercial-at>	IGNORE;IGNORE;IGNORE;<commercial-at>
<currency>	IGNORE;IGNORE;IGNORE;<currency>
<cent-sign>	IGNORE;IGNORE;IGNORE;<cent-sign>
<dollar-sign>	IGNORE;IGNORE;IGNORE;<dollar-sign>
<sterling>	IGNORE;IGNORE;IGNORE;<sterling>
<yen>	IGNORE;IGNORE;IGNORE;<yen>
<asterisk>	IGNORE;IGNORE;IGNORE;<asterisk>
<backslash>	IGNORE;IGNORE;IGNORE;<backslash>
<ampersand>	IGNORE;IGNORE;IGNORE;<ampersand>
<number-sign>	IGNORE;IGNORE;IGNORE;<number-sign>
<percent-sign>	IGNORE;IGNORE;IGNORE;<percent-sign>
<plus-sign>	IGNORE;IGNORE;IGNORE;<plus-sign>
<plus-or-minus>	IGNORE;IGNORE;IGNORE;<plus-or-minus>
<divide>	IGNORE;IGNORE;IGNORE;<divide>
<multiply>	IGNORE;IGNORE;IGNORE;<multiply>
<less-than-sign>	IGNORE;IGNORE;IGNORE;<less-than-sign>
<equals-sign>	IGNORE;IGNORE;IGNORE;<equals-sign>
<greater-than-sign>	IGNORE;IGNORE;IGNORE;<greater-than-sign>
<not-sign>	IGNORE;IGNORE;IGNORE;<not-sign>
<vertical-line>	IGNORE;IGNORE;IGNORE;<vertical-line>
<broken-bar>	IGNORE;IGNORE;IGNORE;<broken-bar>
<degree-sign>	IGNORE;IGNORE;IGNORE;<degree-sign>
<micro-sign>	IGNORE;IGNORE;IGNORE;<micro-sign>
UNDEFINED	IGNORE;IGNORE;IGNORE

<zero>	<zero>;<zero>;IGNORE;IGNORE
<one-quarter>	<zero>;<one-quarter>;IGNORE;IGNORE
<one-half>	<zero>;<one-half>;IGNORE;IGNORE
<three-quarters>	<zero>;<three-quarters>;IGNORE;IGNORE
<one>	<one>;<one>;IGNORE;IGNORE
<two>	<two>;<two>;IGNORE;IGNORE
<three>	<three>;<three>;IGNORE;IGNORE
<four>	<four>;<four>;IGNORE;IGNORE
<five>	<five>;<five>;IGNORE;IGNORE
<six>	<six>;<six>;IGNORE;IGNORE
<seven>	<seven>;<seven>;IGNORE;IGNORE
<eight>	<eight>;<eight>;IGNORE;IGNORE
<nine>	<nine>;<nine>;IGNORE;IGNORE
<super-one>	<one>;<super-one>;IGNORE;IGNORE
<super-two>	<two>;<super-two>;IGNORE;IGNORE
<super-three>	<three>;<super-three>;IGNORE;IGNORE
<A>	<A>;<NONE>;<CAPITAL>;IGNORE
<a>	<A>;<NONE>;<SMALL>;IGNORE
<feminine>	<A>;<NONE>;<feminine>;IGNORE
<A-acute>	<A>;<ACUTE>;<CAPITAL>;IGNORE
<a-acute>	<A>;<ACUTE>;<SMALL>;IGNORE
<A-grave>	<A>;<GRAVE>;<CAPITAL>;IGNORE
<a-grave>	<A>;<GRAVE>;<SMALL>;IGNORE
<A-circumflex>	<A>;<CIRCUMFLEX>;<CAPITAL>;IGNORE
<a-circumflex>	<A>;<CIRCUMFLEX>;<SMALL>;IGNORE
<A-ring>	<A>;<RING>;<CAPITAL>;IGNORE
<a-ring>	<A>;<RING>;<SMALL>;IGNORE
<A-umlaut>	<A>;<DIAERESIS>;<CAPITAL>;IGNORE
<a-umlaut>	<A>;<DIAERESIS>;<SMALL>;IGNORE
<A-tilde>	<A>;<TILDE>;<CAPITAL>;IGNORE
<a-tilde>	<A>;<TILDE>;<SMALL>;IGNORE
<AE>	"<A><E>";"<AE><AE>";"<CAPITAL><CAPITAL>";IGNORE
<ae>	"<A><E>";"<AE><AE>";"<SMALL><SMALL>";IGNORE
<B>	<B>;<NONE>;<CAPITAL>;IGNORE
<b>	<B>;<NONE>;<SMALL>;IGNORE
<C>	<C>;<NONE>;<CAPITAL>;IGNORE
<c>	<C>;<NONE>;<SMALL>;IGNORE
<C-cedilla>	<C>;<CEDILLA>;<CAPITAL>;IGNORE
<c-cedilla>	<C>;<CEDILLA>;<SMALL>;IGNORE
<D>	<D>;<NONE>;<CAPITAL>;IGNORE
<d>	<D>;<NONE>;<SMALL>;IGNORE
<E>	<E>;<NONE>;<CAPITAL>;IGNORE
<e>	<E>;<NONE>;<SMALL>;IGNORE
<E-acute>	<E>;<ACUTE>;<CAPITAL>;IGNORE
<e-acute>	<E>;<ACUTE>;<SMALL>;IGNORE
<E-grave>	<E>;<GRAVE>;<CAPITAL>;IGNORE
<e-grave>	<E>;<GRAVE>;<SMALL>;IGNORE
<E-circumflex>	<E>;<CIRCUMFLEX>;<CAPITAL>;IGNORE
<e-circumflex>	<E>;<CIRCUMFLEX>;<SMALL>;IGNORE
<E-umlaut>	<E>;<DIAERESIS>;<CAPITAL>;IGNORE
<e-umlaut>	<E>;<DIAERESIS>;<SMALL>;IGNORE
<F>	<F>;<NONE>;<CAPITAL>;IGNORE
<f>	<F>;<NONE>;<SMALL>;IGNORE
<G>	<G>;<NONE>;<CAPITAL>;IGNORE
<g>	<G>;<NONE>;<SMALL>;IGNORE
<H>	<H>;<NONE>;<CAPITAL>;IGNORE
<h>	<H>;<NONE>;<SMALL>;IGNORE
<I>	<I>;<NONE>;<CAPITAL>;IGNORE
<i>	<I>;<NONE>;<SMALL>;IGNORE
<I-acute>	<I>;<ACUTE>;<CAPITAL>;IGNORE
<i-acute>	<I>;<ACUTE>;<SMALL>;IGNORE
<I-grave>	<I>;<GRAVE>;<CAPITAL>;IGNORE
<i-grave>	<I>;<GRAVE>;<SMALL>;IGNORE
<I-cicumflex>	<I>;<CIRCUMFLEX>;<CAPITAL>;IGNORE
<i-circumflex>	<I>;<CIRCUMFLEX>;<SMALL>;IGNORE
<I-umlaut>	<I>;<DIAERESIS>;<CAPITAL>;IGNORE
<i-umlaut>	<I>;<DIAERESIS>;<SMALL>;IGNORE
<J>	<J>;<NONE>;<CAPITAL>;IGNORE
<j>	<J>;<NONE>;<SMALL>;IGNORE
<K>	<K>;<NONE>;<CAPITAL>;IGNORE
<k>	<K>;<NONE>;<SMALL>;IGNORE
<L>	<L>;<NONE>;<CAPITAL>;IGNORE
<l>	<L>;<NONE>;<SMALL>;IGNORE
<M>	<M>;<NONE>;<CAPITAL>;IGNORE
<m>	<M>;<NONE>;<SMALL>;IGNORE
<N>	<N>;<NONE>;<CAPITAL>;IGNORE
<n>	<N>;<NONE>;<SMALL>;IGNORE
<N-tilde>	<N>;<TILDE>;<CAPITAL>;IGNORE
<n-tilde>	<N>;<TILDE>;<SMALL>;IGNORE
<O>	<O>;<NONE>;<CAPITAL>;IGNORE
<o>	<O>;<NONE>;<SMALL>;IGNORE
<masculine>	<O>;<NONE>;<masculine>;IGNORE
<O-acute>	<O>;<ACUTE>;<CAPITAL>;IGNORE
<o-acute>	<O>;<ACUTE>;<SMALL>;IGNORE
<O-grave>	<O>;<GRAVE>;<CAPITAL>;IGNORE
<o-grave>	<O>;<GRAVE>;<SMALL>;IGNORE
<O-circumflex>	<O>;<CIRCUMFLEX>;<CAPITAL>;IGNORE
<o-circumflex>	<O>;<CIRCUMFLEX>;<SMALL>;IGNORE
<O-umlaut>	<O>;<DIAERESIS>;<CAPITAL>;IGNORE
<o-umlaut>	<O>;<DIAERESIS>;<SMALL>;IGNORE
<O-tilde>	<O>;<TILDE>;<CAPITAL>;IGNORE
<o-tilde>	<O>;<TILDE>;<SMALL>;IGNORE
<O-slash>	<O>;<STROKE>;<CAPITAL>;IGNORE
<o-slash>	<O>;<STROKE>;<SMALL>;IGNORE
<P>	<P>;<NONE>;<CAPITAL>;IGNORE
<p>	<P>;<NONE>;<SMALL>;IGNORE
<Q>	<Q>;<NONE>;<CAPITAL>;IGNORE
<q>	<Q>;<NONE>;<SMALL>;IGNORE
<R>	<R>;<NONE>;<CAPITAL>;IGNORE
<r>	<R>;<NONE>;<SMALL>;IGNORE
<S>	<S>;<NONE>;<CAPITAL>;IGNORE
<s>	<S>;<NONE>;<SMALL>;IGNORE
<T>	<T>;<NONE>;<CAPITAL>;IGNORE
<t>	<T>;<NONE>;<SMALL>;IGNORE
<Thorn>	"<T><H>";"<Thorn><Thorn>";"<CAPITAL><CAPITAL>";IGNORE
<thorn>	"<T><H>";"<Thorn><Thorn>";"<SMALL><SMALL>";IGNORE
<U>	<U>;<NONE>;<CAPITAL>;IGNORE
<u>	<U>;<NONE>;<SMALL>;IGNORE
<U-acute>	<U>;<ACUTE>;<CAPITAL>;IGNORE
<u-acute>	<U>;<ACUTE>;<SMALL>;IGNORE
<U-grave>	<U>;<GRAVE>;<CAPITAL>;IGNORE
<u-grave>	<U>;<GRAVE>;<SMALL>;IGNORE
<U-circumflex>	<U>;<CIRCUMFLEX>;<CAPITAL>;IGNORE
<u-circumflex>	<U>;<CIRCUMFLEX>;<SMALL>;IGNORE
<U-umlaut>	<U>;<DIAERESIS>;<CAPITAL>;IGNORE
<u-umlaut>	<U>;<DIAERESIS>;<SMALL>;IGNORE
<V>	<V>;<NONE>;<CAPITAL>;IGNORE
<v>	<V>;<NONE>;<SMALL>;IGNORE
<W>	<W>;<NONE>;<CAPITAL>;IGNORE
<w>	<W>;<NONE>;<SMALL>;IGNORE
<X>	<X>;<NONE>;<CAPITAL>;IGNORE
<x>	<X>;<NONE>;<SMALL>;IGNORE
<Y>	<Y>;<NONE>;<CAPITAL>;IGNORE
<y>	<Y>;<NONE>;<SMALL>;IGNORE
<Y-acute>	<Y>;<ACUTE>;<CAPITAL>;IGNORE
<y-acute>	<Y>;<ACUTE>;<SMALL>;IGNORE
<y-umlaut>	<Y>;<DIAERESIS>;<SMALL>;IGNORE
<Z>	<Z>;<NONE>;<CAPITAL>;IGNORE
<z>	<Z>;<NONE>;<SMALL>;IGNORE

order_end

END LC_COLLATE
