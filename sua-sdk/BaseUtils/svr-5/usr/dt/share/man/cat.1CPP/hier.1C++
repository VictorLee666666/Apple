

hier(1C++)                                             hier(1C++)

SSyynnooppssiiss
       hier [ -Aacikmnstuv ] [ -b boxName] [ -hheight ] [ -L lan-
       guage ]
            [ -o base ] [ -wwidth ] [ cpp options ] file ...

DDeessccrriippttiioonn
       hier prints the inheritance hierarchy for the  C++  source
       code  contained, directly or indirectly, in the given col-
       lection of files.  While the source  code  is  being  pro-
       cessed,  templates  are   treated similar to classes, with
       every option applicable to class also applicable  to  tem-
       plate.   The  output  construct name for template contains
       the argument names enclosed by  angle  brackets.   In  the
       absence of any -L options, the standard output consists of
       lines of the form
           A protection B

       where A and B are class names, protection is one of  "pub-
       lic", "private", or "protected", and  A inherits from B in
       the stated way.  Virtual inheritance is denoted by
           A protection B virtual

       (The format is intended to make it easier for post-proces-
       sors to parse.)

       Notice  that  all files, including those directly or indi-
       rectly included  by the specified files, are searched  for
       inheritance (but see -t below).

   OOuuttppuutt llaanngguuaaggeess
       If  graphic  output is desired, the -L option can be used.
       The output language can  be  any  of   "ps"  (postscript),
       "dag",  "pic",  "tex", or "dvi".   Multiple -L options are
       allowed.  hier  places its output for these  languages  in
       the file(s)
        hierout. ext, where ext is the name of the language.

       If any of the input files to hier  is dag source, (presum-
       ably produced by a previous run of hier),  then the inher-
       itance  graph  contained  therein is incorporated into the
       current  graph.   It is up to the  user  to  make  certain
       that the settings of the   a, i,  and n options which were
       used to produce the input dag source  match  the   current
       settings.

       TeX output produced by hier  can be included in any tex or
       latex document by the following  sequence of commands:
           \input{hierout}
           \centerline{\box\graph}

       where hierout.tex is the TeX file produced by hier.

       Additional options to hier  are as follows:

                                                                1

hier(1C++)                                             hier(1C++)

       -c, -s,  -u    Print  information  for  classes  (structs,
       unions).   If  none  of  these  switches  is selected, the
       default is  -c.

       -n    Also print the names of isolated  classes   (classes
       with  no  predecessors  or  successors  in the inheritance
       graph).

       -t    Only print inheritances directly  contained  in  the
       files  specified on the  command line  (i.e., do not print
       inheritances only contained in included files).    For  .i
       files,  only  print  inheritances  for the first file men-
       tioned in a #line  directive,  as well as anything preced-
       ing that line.

       The  following options apply only to graphic output speci-
       fied by -L:

       -A    Print ascii output on the standard output  in  addi-
       tion to any output(s)  specified by -L.

       -a     Differentiate  aggregate  types  in the inheritance
       graph by drawing classes as  ellipses, structs  as  boxes,
       and  unions  as  diamonds.   (Normally,  all  are drawn as
       ellipses.)

       -i    Do not differentiate  type  of  inheritance  in  the
       inheritance  graph.   (Normally,  private   inheritance is
       drawn as a dashed arrow.)

       -k    Draw inheritance  edges  backwards,  that  is,  from
       derived  class to base class  rather than from  base class
       to derived class.

       -m    (With -L dag.)  In the dag output, insert  cpp-style
       comments  showing  the  file name  and  line number  where
       each of the edges in the  inheritance  graph  is  defined.
       (But see the Warnings  below.)

       -v     Orient the inheritance graph vertically rather than
       horizontally.

       -b boxName     (With -L tex.)  Make   boxName, rather than
       "graph", be the name of the box defined by the TeX code.

       -w  width,  -hheight   Scale  the graph width inches wide,
       height inches high.  width and height should be dimension-
       less  inches,  e.g.,  -h5.2.   The  default  graph size is
       8.5x10.

       -o base     Use base,  rather than   hierout,  as the base
       name for all output files.  base may be any arbitrary path
       name; any extension on base is ignored.

                                                                2

hier(1C++)                                             hier(1C++)

EExxaammppllee
            hier -L tex -L dvi *.h

       produces page-sized TeX and dvi versions  of  the  inheri-
       tance  hierarchy  for  the  classes  defined in the header
       files in the current directory.  The outputs are on  hier-
       out.tex and hierout.dvi, respectively.  (Notice that hier-
       out.dvi is not what one would get by directly running  tex
       on hierout.tex.)

NNootteess
       dag is an AT&T proprietary tool.

RReeffeerreenncceess
WWaarrnniinnggss
       The  comments inserted by the -m option are not treated as
       comments by dag(1), and  will  have  to  be  stripped  out
       (e.g., with sed(1))  before further processing.

BBuuggss
       Depending  on the capabilities of your document production
       environment, dashed lines in .dvi files may be rendered as
       solid lines.

       Printing the word "virtual" over arrows in order to denote
       virtual inheritance is ugly.

       Some people think hier's notion of "backwards" (see the -k
       option)  is backwards.

                                                                3

