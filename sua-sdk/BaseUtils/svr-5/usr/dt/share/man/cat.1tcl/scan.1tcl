

scan(1tcl)                                             scan(1tcl)

SSyynnooppssiiss
       scan string formatString varName [varName ...]

DDeessccrriippttiioonn
       This  command  parses  fields  from an input string in the
       same fashion as the ANSI C sscanf procedure and returns  a
       count of the number of fields successfully parsed.  string
       gives the input to be parsed and format  string  indicates
       how  to  parse  it,  using  %  conversion specifiers as in
       sscanf.  Each varName gives the name of a variable; when a
       field  is scanned from string the result is converted back
       into a string and assigned to the corresponding  variable.

   DDeettaaiillss oonn ssccaannnniinngg
       scan   operates   by   scanning  string  and  formatString
       together.  If the next  character  in  formatString  is  a
       blank or tab then it is ignored.  Otherwise, if it isn't a
       % character then it must match  the  next  non-white-space
       character  of  string.  When a % is encountered in format-
       String, it indicates the start of a conversion  specifier.
       A  conversion specifier contains three fields after the %:
       a *, which indicates that the converted  value  is  to  be
       discarded  instead  of  assigned  to  a variable; a number
       indicating a maximum field width; and a conversion charac-
       ter.  All of these fields are optional except for the con-
       version character.

       When scan finds a conversion specifier in formatString, it
       first skips any white-space characters in string.  Then it
       converts the next input characters according to  the  con-
       version  specifier  and  stores the result in the variable
       given by the next argument to scan.  The following conver-
       sion  characters  are supported: The input field must be a
       decimal integer.  It is read in and the value is stored in
       the variable as a decimal string.  The input field must be
       an octal integer. It is read in and the value is stored in
       the variable as a decimal string.  The input field must be
       a hexadecimal integer. It is read  in  and  the  value  is
       stored  in  the  variable  as  a decimal string.  A single
       character is read in and its binary value is stored in the
       variable  as a decimal string.  Initial white space is not
       skipped in this case, so the input field may be  a  white-
       space  character.   This  conversion is different from the
       ANSI standard in that the input field always consists of a
       single character and no field width may be specified.  The
       input field consists of all the characters up to the  next
       white-space  character;  the  characters are copied to the
       variable.  The input field must be a floating-point number
       consisting of an optional sign, a string of decimal digits
       possibly containing a decimal point, and an optional expo-
       nent  consisting of an e or E followed by an optional sign
       and a string of decimal digits.  It is read in and  stored
       in  the  variable  as  a floating-point string.  The input
       field consists of any number of characters in chars.   The

                                                                1

scan(1tcl)                                             scan(1tcl)

       matching  string  is stored in the variable.  If the first
       character between the brackets is a ] then it  is  treated
       as  part  of chars rather than the closing bracket for the
       set.  The input field consists of any number of characters
       not  in chars.  The matching string is stored in the vari-
       able.  If the character immediately following the ^ is a ]
       then  it  is  treated  as  part of the set rather than the
       closing bracket for the set.   The  number  of  characters
       read from the input for a conversion is the largest number
       that makes sense for that particular conversion (for exam-
       ple,  as  many  decimal digits as possible for %d, as many
       octal digits as possible for %o, and so  on).   The  input
       field  for  a  given  conversion  terminates either when a
       white-space character is encountered or when  the  maximum
       field width has been reached, whichever comes first.  If a
       * is present in the conversion specifier then no  variable
       is assigned and the next scan argument is not consumed.

   DDiiffffeerreenncceess ffrroomm AANNSSII ssssccaannff
       The behavior of the scan command is the same as the behav-
       ior of the ANSI C sscanf procedure except for the  follow-
       ing  differences:  %p and %n conversion specifiers are not
       currently supported.  For %c conversions a single  charac-
       ter  value is converted to a decimal string, which is then
       assigned to the corresponding varName; no field width  may
       be  specified  for this conversion.  The l, h, and L modi-
       fiers are ignored;  integer values are always converted as
       if  there  were  no  modifier  present and real values are
       always converted as if the l modifier were  present  (i.e.
       type double is used for the internal representation).

                                                                2

