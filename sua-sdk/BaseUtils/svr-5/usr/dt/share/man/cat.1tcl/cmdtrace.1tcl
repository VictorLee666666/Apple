

cmdtrace(1tcl)                                     cmdtrace(1tcl)

SSyynnooppssiiss
       cmdtrace level | on [noeval] [notruncate] [procs] [fileid]
       | off | depth

DDeessccrriippttiioonn
       Prints a trace statement  for  all  commands  executed  at
       depth  of  level  or below (1 is the top level).  If on is
       specified, all commands at any level are traced.

       The most common use of this command is to  enable  tracing
       to  a file during the development.  If a failure occurs, a
       trace is then available when needed.  Command tracing will
       slow  down  the execution of code, so it should be removed
       when code is debugged.  The following command will  enable
       tracing  to  a file for the remainder of the program: cmd-
       trace on [open cmd.log w]

   OOppttiioonnss
       Causes arguments to be printed unevaluated.  If noeval  is
       specified,  the  arguments  are printed before evaluation.
       Otherwise, they are printed afterwards.

       If the command line is longer than 60  characters,  it  is
       truncated  to  60 and a ... is postpended to indicate that
       there was more output than was displayed.  If an evaluated
       argument contains a space, the entire argument is enclosed
       within braces ({}) to allow the reader to  visually  sepa-
       rate  the arguments from each other.  Disables the trunca-
       tion of commands and  evaluated  arguments.   Enables  the
       tracing  of  procedure  calls only.  Commands that are not
       procedure calls (that is, calls to commands that are writ-
       ten  in C, C++ or some object-compatible language) are not
       traced if the procs option is specified.  This  option  is
       particularly  useful  for  greatly  reducing the output of
       cmdtrace while debugging.  This is a file id  as  returned
       by  the open command.  If specified, then the trace output
       will be written to the file rather than stdout.   A  stdio
       buffer  flush  is done after every line is written so that
       the trace may be monitored externally  or  provide  useful
       information  for debugging problems that cause core dumps.
       Turns off all tracing.  Returns the current maximum  trace
       level, or zero if trace is disabled.

                                                                1

