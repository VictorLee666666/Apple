

SaDisplay(3tlib)                                 SaDisplay(3tlib)

SSyynnooppssiiss
       SaDisplayErrorStacks  dialogName  errorStacks  help [call-
       Back]  SaDisplayErrorInfo  dialogName  topErrorText   help
       [callback]
       SaDisplayNoAuths dialogName manager callback [host]
       SaDisplayNotRoot dialogName manager callback [host]

DDeessccrriippttiioonn
       These provide a common user interface for presenting error
       stacks and other common error messages  such  as  insuffi-
       cient authorization.

       The  Error  functions display an error box showing the top
       level error text from the stack. They  provide  a  Details
       button  for  viewing  the  complete stack.  This secondary
       dialog also provides a Save button for logging  the  error
       stack  to  a  file  for future reference. An optional Help
       button can be included where relevant help attachments are
       available.  In  the case of multiple error stacks, each is
       displayed in a loop, one at a  time,  after  the  previous
       error dialog is closed.

       SaDisplayErrorInfo  provides  for displaying a simple text
       string rather than a fully formed error stack.  It creates
       a  standard stack which contains the topErrorText and then
       calls SaDisplayErrorStacks.  This  results  in  consistent
       error presentation regardless of its source.

       The  second group of functions provide a common error mes-
       sage for standard problems that should be handled  consis-
       tently  across  all  admin applications.  SaDisplayNoAuths
       applies when the user does  not  have  required  subsystem
       and/or  kernel  authorizations.   SaDisplayNotRoot applies
       when the user's UID is not 0 but needs to be for the  par-
       ticular application.

   AArrgguummeennttss
       &VTcl;  widget name for the new dialog.  list of 1 or more
       error stacks.  set to "HELP" to include a Help  button  on
       the error dialog box.  optional application callback to be
       called when the  error  dialog  is  closed.   simple  text
       string  versus  a  fully  formed error stack.  application
       title which will be included in the error text.   optional
       host  name. Defaults to the local host name. Used when the
       error resulted from a remote administration action.

RReeffeerreenncceess
NNoottiicceess
       All application callbacks are auto locked. See &VTcl;  and
       -autoLock.

                                                                1

