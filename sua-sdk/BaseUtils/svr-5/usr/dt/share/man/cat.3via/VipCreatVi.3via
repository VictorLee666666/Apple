

VipCreateVi(3via)                               VipCreateVi(3via)

SSyynnooppssiiss
       cc  [flag  ...]  file  ...   -via  [library] ...  #include
       <vipl.h>

       VIP_RETURN VipCreateVi(VIP_NIC_HANDLE NicHandle,
            VIP_VI_ATTRIBUTES  *ViAttribs,  VIP_CQ_HANDLE  SendC-
       QHandle,
            VIP_CQ_HANDLE RecvCQHandle, VIP_VI_HANDLE *ViHandle);

DDeessccrriippttiioonn
       The VipCreateVi routine creates an instance of  a  Virtual
       Interface  (VI) for the specified NIC. The ViAttribs input
       parameter specifies the  initial  attributes  for  the  VI
       instance to be created.

       The  SendCQHandle  and RecvCQHandle parameters are used to
       associate the VI's work queues (send and receive)  with  a
       completion  queue.  If one or both of the VI's work queues
       are successfully associated with a completion  queue,  the
       calling  process  cannot  wait  on that queue (or on those
       queues) via calls to the or routines.

       When a new VI instance is created, it begins in  the  Idle
       state.

       For  details  of the possible VI states, and for a list of
       the supported VI attributes, refer to the manual page.

   AArrgguummeennttss
       The VI NIC handle.  The initial attributes to set for  the
       new VI.  The handle of a completion queue. If a valid han-
       dle is specified, the send work queue of the VI is associ-
       ated  with that completion queue. If set to NULL, the send
       queue is not associated with any  completion  queue.   The
       handle  of a completion queue. If a valid handle is speci-
       fied, the receive work queue of the VI is associated  with
       that  completion  queue. If set to NULL, the receive queue
       is not associated with any completion queue.   The  handle
       for the newly created VI instance.

RReettuurrnn vvaalluueess
       The   operation  completed  successfully.   An  error  was
       detected that was caused by insufficient  resources.   One
       of the input parameters was invalid.  The specified relia-
       bility level attribute was invalid or not supported.   The
       specified  maximum  transfer size attribute was invalid or
       not supported.  The specified quality of service attribute
       was  invalid  or  not supported.  The specified protection
       tag attribute was invalid or not supported.  The specified
       attributes  requested  support  for  Remote  Direct Memory
       Access (RDMA) Read, but the VI provider does  not  support
       it.   For  a full list of the supported VI attributes, see
       the <vipl.h> header file and the manual page.

                                                                1

VipCreateVi(3via)                               VipCreateVi(3via)

RReeffeerreenncceess

                                                                2

