

VipSendWait(3via)                               VipSendWait(3via)

SSyynnooppssiiss
       cc  [flag  ...]  file  ...   -via  [library] ...  #include
       <vipl.h>

       VIP_RETURN VipSendWait(VIP_VI_HANDLE  ViHandle,  VIP_ULONG
       TimeOut,
            VIP_DESCRIPTOR **DescriptorPtr);

DDeessccrriippttiioonn
       The  VipSendWait  routine checks whether the descriptor on
       the head of a VI's send queue has  been  marked  as  being
       complete.  If  the  send  has completed, the descriptor is
       removed from the head of the queue, and the address of the
       descriptor is immediately returned. A VIP_DESCRIPTOR_ERROR
       is  returned  if  the  operation  completed  with   errors
       returned  in the descriptor's status. If the send queue is
       empty, DescriptorPtr is set to  NULL  and  a  VIP_DESCRIP-
       TOR_ERROR is returned.

       If  the  descriptor  on the head of the queue has not been
       marked as complete, VipSendWait blocks the calling process
       until  the descriptor is so-marked, or until the specified
       timeout has expired.

       VipSendWait cannot be used to block on a send  queue  that
       has  been associated with a completion queue. See for more
       details.

   AArrgguummeennttss
       The instance of a VI.  The count, in milliseconds,  before
       control is passed back to the calling process. This should
       be set to VIP_INFINITE if no  timeout  is  required.   The
       address of the descriptor that has completed.

RReettuurrnn vvaalluueess
       A  completed  descriptor  was found on the send queue.  If
       the send queue is empty, the descriptor is  set  to  NULL.
       Otherwise,  a  completed  descriptor  is  returned with an
       error completion status.  The VI handle was invalid.   The
       timeout  expired  and  no  completed descriptor was found.
       The send queue is associated with a completion queue.

RReeffeerreenncceess

                                                                1

