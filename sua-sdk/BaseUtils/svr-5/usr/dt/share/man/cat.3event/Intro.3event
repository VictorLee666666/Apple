

Intro(3event)                                       Intro(3event)

SSyynnttaaxx
       cc  . . .  -levent

       #include  <libevent.h>

DDeessccrriippttiioonn
       This section contains manual pages for the routines in the
       libevent library. These routines support event handling.

FFiilleess
       LIBDIRlibevent.a libevent library (archive)

RReettuurrnn vvaalluueess
       For functions that return a floating-point  value,  if  an
       error occurs, the value of errno will be one of the values
       represented by  the  manifest  constants  EDOM  or  ERANGE
       (defined  in  math.h).   EDOM typically indicates a domain
       error: one of the input values was not in  the  domain  of
       the  function.   ERANGE typically indicates a range error:
       the calculated result was either too big or too  small  to
       be  represented by the data type returned by the function.

       Functions that result in a range  error  typically  return
       zero  for  underflow or a value that will compare equal to
       +-HUGE_VAL for overflow.  HUGE_VAL is defined  in  math.h.
       On  systems that support IEEE infinity, HUGE_VAL is infin-
       ity.

       If the system supports IEEE NaN (not-a-number),  functions
       that  result in a domain error typically return NaN.  Such
       functions may also raise one of  the  IEEE  floating-point
       exceptions.   On  systems  that  do  not support IEEE NaN,
       functions that result in a domain error  typically  return
       zero.

       The  error  behavior  for  programs  compiled with the -Xt
       (transitional) compilation mode is different (see In  that
       case,  these  functions  typically  return zero instead of
       IEEE NaN for domain errors and a value that  will  compare
       equal to +-HUGE instead of +-HUGE_VAL for overflows.  HUGE
       is defined in math.h.

DDeeffiinniittiioonnss
       A character (except a multibyte character; see is any  bit
       pattern  able to fit into a byte on the machine.  The null
       character is a character with value 0, conventionally rep-
       resented  in the C language as \0.  A character array is a
       sequence of characters.  A null-terminated character array
       (a  string) is a sequence of characters, the last of which
       is the null character.  The null  string  is  a  character
       array  containing  only the terminating null character.  A
       NULL pointer is the value that is obtained  by  casting  0
       into  a  pointer.   C  guarantees that this value will not
       match that of any legitimate pointer,  so  many  functions

                                                                1

Intro(3event)                                       Intro(3event)

       that  return  pointers  return  NULL to indicate an error.
       The macro NULL is defined in stdio.h.  Types of  the  form
       size_t are defined in the appropriate header files.

RReeffeerreenncceess
SSttaannddaarrddss ccoonnffoorrmmaannccee
       The  libevent  library  is  not part of any currently sup-
       ported standard; it is an extension of AT&T System V  pro-
       vided by the Santa Cruz Operation.

                                                                2

