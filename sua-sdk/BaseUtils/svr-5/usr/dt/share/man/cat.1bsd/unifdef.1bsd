

unifdef(1bsd)                                       unifdef(1bsd)

SSyynnooppssiiss
       /usr/ucb/unifdef [-clt] [-Dname] [-Uname] [-iDname]
            [-iUname] . . . [file]

DDeessccrriippttiioonn
       unifdef removes ifdefed lines from a file while otherwise
       leaving the file alone.  It is smart enough to deal with
       the nested ifdefs, comments, single and double quotes of C
       syntax, but it does not do any including or interpretation
       of macros.  Neither does it strip out comments, though it
       recognizes and ignores them.  You specify which symbols
       you want defined with -D options, and which you want unde-
       fined with -U options.  Lines within those ifdefs will be
       copied to the output, or removed, as appropriate.  Any
       ifdef, ifndef, else, and endif lines associated with file
       will also be removed.

       ifdefs involving symbols you do not specify are untouched
       and copied out along with their associated ifdef, else,
       and endif lines.

       If an ifdefX occurs nested inside another ifdefX, then the
       inside ifdef is treated as if it were an unrecognized sym-
       bol.  If the same symbol appears in more than one argu-
       ment, only the first occurrence is significant.

       unifdef copies its output to the standard output and will
       take its input from the standard input if no file argument
       is given.

       The following options are available: Complement the normal
       operation.  Lines that would have been removed or blanked
       are retained, and vice versa.  Replace ``lines removed''
       lines with blank lines.  Plain text option.  unifdef
       refrains from attempting to recognize comments and single
       and double quotes.  Ignore, but print out, lines associ-
       ated with the defined symbol name.  If you use ifdefs to
       delimit non-C lines, such as comments or code which is
       under construction, then you must tell unifdef which sym-
       bols are used for that purpose so that it will not try to
       parse for quotes and comments within them.  Ignore, but
       print out, lines associated with the undefined symbol
       name.

RReeffeerreenncceess
DDiiaaggnnoossttiiccss
       Inappropriate else or endif.

       Exit status is 0 if output is exact copy of input, 1 if
       not, 2 if unifdef encounters problems.

                     BSD System Compatibility                   1

