

wcswidth(3C)                                         wcswidth(3C)

SSyynnooppssiiss
       #include <wchar.h>

       int wcswidth(const wchar_t *pwcs, size_t n);

DDeessccrriippttiioonn
       wcswidth  determines  the  number of column printing posi-
       tions needed for up to  n  wide  characters  in  the  wide
       string  pwcs.   Fewer  than n wide characters will be pro-
       cessed only if a null wide character is encountered before
       n wide characters in pwcs.

   RReettuurrnn vvaalluueess
       wcswidth returns either zero if pwcs is pointing to a null
       wide character code, or the  number  of  column  positions
       occupied  by the wide character string pointed to by pwcs.
       wcswidth returns -1 if any wide character code in the wide
       character  string  pointed  to  by pwcs is not a printable
       wide character.

EExxaammpplleess
       This  example  function,  when  passed  a  wide  character
       string, calculates the number of column positions required
       and  prints  a  diagnostic  message.   #include  <wchar.h>
       #include <stdio.h>

       . . .

       int  print_width(const  wchar_t  *pwcs)  {      int width;
            size_t len;

            len  =   wcslen(pwcs);        if   (len   >   0)    {
                 width   =  wcswidth  (pwcs,  len);            if
       (width == -1)               (void)  printf("non  printable
       character\n");              else                    (void)
       printf("Wide string  width=%d\n",width);            return
       (1);      }

            (void) printf("zero length wide character string\n");
            return (0);

       }

RReeffeerreenncceess

                                                                1

