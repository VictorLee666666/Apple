

localeconv(3C)                                     localeconv(3C)

SSyynnooppssiiss
       include <locale.h>

       struct lconv *localeconv (void);

DDeessccrriippttiioonn
       localeconv  sets  the  components  of  an object with type
       struct lconv (defined in locale.h) with the values  appro-
       priate  for the formatting of numeric quantities (monetary
       and otherwise) according  to  the  rules  of  the  current
       locale  [see The definition of struct lconv is given below
       (the values for the fields in the C locale  are  given  in
       comments):  char  *decimal_point;          /*  "." */ char
       *thousands_sep;          /* ""  (zero  length  string)  */
       char *grouping;       /* "" */ char *int_curr_symbol;   /*
       ""   */   char   *currency_symbol;   /*   ""    */    char
       *mon_decimal_point; /*   ""  */  char  *mon_thousands_sep;
            /* ""  */  char  *mon_grouping;      /*  ""  */  char
       *positive_sign;          /*        ""        */       char
       *negative_sign;          /*       ""        */        char
       int_frac_digits;         /*      CHAR_MAX      */     char
       frac_digits;        /*       CHAR_MAX       */        char
       p_cs_precedes;      /*        CHAR_MAX       */       char
       p_sep_by_space;          /*     CHAR_MAX      */      char
       n_cs_precedes;      /*        CHAR_MAX       */       char
       n_sep_by_space;          /*     CHAR_MAX      */      char
       p_sign_posn;        /*        CHAR_MAX       */       char
       n_sign_posn;        /* CHAR_MAX */

       The members of the structure with type char * are strings,
       any  of  which  (except decimal_point) can point to "", to
       indicate that the value is not available  in  the  current
       locale  or  is of zero length.  The members with type char
       are nonnegative numbers, any  of  which  can  be  CHAR_MAX
       (defined in the limits.h header file) to indicate that the
       value is not available in the current locale.  The members
       are the following:

       The  decimal-point  character  used to format non-monetary
       quantities.  The character used to separate groups of dig-
       its  to the left of the decimal-point character in format-
       ted non-monetary quantities.  A string in which each  ele-
       ment  is  taken as an integer that indicates the number of
       digits that comprise the current group in a formatted non-
       monetary  quantity.   The  elements of grouping are inter-
       preted according to the following: No further grouping  is
       to be performed.  The previous element is to be repeatedly
       used for the remainder of the digits.  The  value  is  the
       number  of  digits  that  comprise the current group.  The
       next element is examined to determine the size of the next
       group  of  digits  to  the left of the current group.  The
       international currency symbol applicable  to  the  current
       locale,  left-justified  within  a  four-character  space-
       padded field.  The character sequences should  match  with

                                                                1

localeconv(3C)                                     localeconv(3C)

       those  specified in: ISO 4217:1987 Codes for the Represen-
       tation of Currency and Funds.  The local  currency  symbol
       applicable  to the current locale.  The decimal point used
       to format monetary quantities.  The separator  for  groups
       of  digits  to  the left of the decimal point in formatted
       monetary quantities.  A string in which  each  element  is
       taken  as  an  integer that indicates the number of digits
       that comprise the current group in  a  formatted  monetary
       quantity.   The  elements  of mon_grouping are interpreted
       according to the  rules  described  under  grouping.   The
       string  used  to  indicate  a nonnegative-valued formatted
       monetary quantity.  The string used to  indicate  a  nega-
       tive-valued  formatted  monetary  quantity.  The number of
       fractional digits (those  to  the  right  of  the  decimal
       point)  to  be  displayed  in an internationally formatted
       monetary quantity.  The number of fractional digits (those
       to  the  right  of the decimal point) to be displayed in a
       formatted monetary quantity.  Set to 1 or 0  if  the  cur-
       rency_symbol  respectively  precedes or succeeds the value
       for a nonnegative formatted monetary quantity.  Set  to  1
       or 0 if the currency_symbol respectively is or is not sep-
       arated by a space from the value for a nonnegative format-
       ted monetary quantity.  Set to 1 or 0 if the currency_sym-
       bol respectively precedes or succeeds the value for a neg-
       ative  formatted  monetary quantity.  Set to 1 or 0 if the
       currency_symbol respectively is or is not separated  by  a
       space  from  the  value  for a negative formatted monetary
       quantity.  Set to a value indicating  the  positioning  of
       the  positive_sign  for  a  nonnegative formatted monetary
       quantity.  The value of p_sign_posn is interpreted accord-
       ing  to  the  following: Parentheses surround the quantity
       and currency_symbol.  The sign string precedes  the  quan-
       tity  and  currency_symbol.   The sign string succeeds the
       quantity and currency_symbol.  currency_symbol.  The  sign
       string immediately succeeds the currency_symbol.  Set to a
       value indicating the positioning of the negative_sign  for
       a  negative  formatted  monetary  quantity.   The value of
       n_sign_posn  is  interpreted  according   to   the   rules
       described under p_sign_posn.

   FFiilleess
       LC_MONETARY  database  for  locale LC_NUMERIC database for
       locale

   RReettuurrnn vvaalluueess
       localeconv returns a pointer to the filled-in object.  The
       structure  pointed to by the return value may be overwrit-
       ten by a subsequent call to localeconv.

UUssaaggee
       The following table illustrates the  rules  used  by  four
       countries to format monetary quantities.

                                                                2

localeconv(3C)                                     localeconv(3C)

       -----------------------------------------------------------------------
       Country       Positive format   Negative format   International format
       Italy         L.1.234           -L.1.234          ITL.1.234
       Netherlands   F 1.234,56        F -1.234,56       NLG 1.234,56
       Norway        kr1.234,56        kr1.234,56-       NOK 1.234,56
       Switzerland   SFrs.1,234.56     SFrs.1,234.56C    CHF 1,234.56

       For  these  four  countries, the respective values for the
       monetary members of the structure returned  by  localeconv
       are as follows:

       ----------------------------------------------------------------
                           Italy    Netherlands   Norway   Switzerland
       _ii_nn_tt____cc_uu_rr_rr____ss_yy_mm_bb_oo_ll     _""_II_TT_LL_.._""   _""_NN_LL_GG _""        _""_NN_OO_KK _""   _""_CC_HH_FF _""
       _cc_uu_rr_rr_ee_nn_cc_yy____ss_yy_mm_bb_oo_ll     _""_LL_.._""     _""_FF_""           _""_kk_rr_""     _""_SS_FF_rr_ss_.._""
       _mm_oo_nn____dd_ee_cc_ii_mm_aa_ll____pp_oo_ii_nn_tt   _""_""       _""_,,_""           _""_,,_""      _""_.._""
       _mm_oo_nn____tt_hh_oo_uu_ss_aa_nn_dd_ss____ss_ee_pp   _""_.._""      _""_.._""           _""_.._""      _""_,,_""
       _mm_oo_nn____gg_rr_oo_uu_pp_ii_nn_gg        _""_\\_33_""     _""_\\_33_""          _""_\\_33_""     _""_\\_33_""
       _pp_oo_ss_ii_tt_ii_vv_ee____ss_ii_gg_nn       _""_""       _""_""            _""_""       _""_""
       _nn_ee_gg_aa_tt_ii_vv_ee____ss_ii_gg_nn       _""_--_""      _""_--_""           _""_--_""      _""_CC_""
       _ii_nn_tt____ff_rr_aa_cc____dd_ii_gg_ii_tt_ss     _00        _22             _22        _22
       _ff_rr_aa_cc____dd_ii_gg_ii_tt_ss         _00        _22             _22        _22
       _pp____cc_ss____pp_rr_ee_cc_ee_dd_ee_ss       _11        _11             _11        _11
       _pp____ss_ee_pp____bb_yy____ss_pp_aa_cc_ee      _00        _11             _00        _00
       _nn____cc_ss____pp_rr_ee_cc_ee_dd_ee_ss       _11        _11             _11        _11
       _nn____ss_ee_pp____bb_yy____ss_pp_aa_cc_ee      _00        _11             _00        _00
       _pp____ss_ii_gg_nn____pp_oo_ss_nn         _11        _11             _11        _11
       _nn____ss_ii_gg_nn____pp_oo_ss_nn         _11        _44             _22        _22

RReeffeerreenncceess

                                                                3

