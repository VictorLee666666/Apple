

strftime(3C)                                         strftime(3C)

SSyynnooppssiiss
       #include <time.h>

       size_t  strftime(char *s, size_t maxsize, const char *for-
       mat,
           const struct tm *timeptr);

       int cftime(char  *s,  const  char  *format,  const  time_t
       *clock);

       int  ascftime(char *s, const char *format, const struct tm
       *timeptr);

DDeessccrriippttiioonn
       strftime, ascftime, and cftime place characters  into  the
       array  pointed to by s as controlled by the string pointed
       to by format.  The format string consists of zero or  more
       directives  and ordinary characters.  All ordinary charac-
       ters (including the terminating null character) are copied
       unchanged into the array.  For strftime, no more than max-
       size characters are placed into the array.

       For cftime and asctime, if format is (char *)0,  then  the
       locale's default format is used.  If the environment vari-
       able CFTIME is defined and nonempty, it  is  used  as  the
       default format; otherwise %N is used.

       Each  directive  is  replaced by appropriate characters as
       described by the following list.  The appropriate  charac-
       ters  are  determined  by the LC_TIME category of the pro-
       gram's locale and by the values contained in the structure
       pointed  to  by  timeptr for strftime and ascftime, and by
       the time represented by clock for cftime.

       same as %  abbreviated  weekday  name  full  weekday  name
       abbreviated month name full month name basic date and time
       representation number of the century  (00  -  99)  day  of
       month  (01 - 31) date as %m/%d/%y day of month (1-31; sin-
       gle digits are preceded by a blank) a  modifier  character
       used  in  association  with certain conversion specifiers;
       see below.  abbreviated month name.  hour (00 -  23)  hour
       (01  - 12) day number of year (001 - 366) month number (01
       - 12) minute (00 - 59) same as new-line date and time rep-
       resentation as used by date.  a modifier character used in
       association with certain conversion specifiers; see below.
       equivalent  of either AM or PM 12 hour time (including %p)
       same as %H:%M seconds (00 - 61), allows for  leap  seconds
       same  as  a  tab  same as %H:%M:%S weekday number (1 - 7),
       Monday = 1 week number of year (00 - 53),  Sunday  is  the
       first day of week 1 week number of the year weekday number
       (0 - 6), Sunday = 0 week number of year (00 - 53),  Monday
       is  the first day of week 1 locale's appropriate date rep-
       resentation locale's appropriate time representation  year
       within  century (00 - 99) year as ccyy (for example, 1986)

                                                                1

strftime(3C)                                         strftime(3C)

       time zone name or no characters if no time zone exists

       The difference between %U and %W  lies  in  which  day  is
       counted  as  the first of the week.  Week number 01 is the
       first week in January starting with a Sunday for %U  or  a
       Monday  for %W.  Week number 00 contains those days before
       the first Sunday or Monday  in  January  for  %U  and  %W,
       respectively.

       For  %V,  if  the  week containing January 1st has four or
       more days in the new year, it is week 1; otherwise, it  is
       week 53 of the preceding year.

   MMooddiiffiieedd ccoonnvveerrssiioonn ssppeecciiffiieerrss
       O modifies the behavior of the following conversion speci-
       fiers.  The decimal value is generated using the  locale's
       alternate  digit  symbols.   the  day  of the month, using
       alternative digit symbols filled as  needed  with  leading
       zeros  if  available;  otherwise, filled with spaces.  the
       day of the month, using alternative digit  symbols  filled
       with  leading spaces as needed.  the hour (24 hour clock),
       using  alternative  digit  symbols.   the  hour  (12  hour
       clock),  using alternative digit symbols.  the month using
       alternative digit symbols.  the minutes using  alternative
       digit  symbols.   the seconds using alternative digit sym-
       bols.  the weekday as a  number  using  alternative  digit
       symbols  (Monday  = 1).  the week number using alternative
       digit symbols (see rules for %U).  the week  number  using
       alternative digit symbols (see rules for %V).  the weekday
       as a number using alternative digit symbols (Sunday =  0).
       the week number using alternative digit symbols (see rules
       for %W).  the year  (offset  from  %C)  using  alternative
       digit symbols.

       E  also  modifies the behavior of the following conversion
       specifiers.  An Era-specific value is generated instead of
       the  normal  value.ile.   Era-specific  representation for
       date and time, as in Era-specific representation  for  the
       name  of the base year (period).  Era-specific representa-
       tion for the date.  Era-specific  representation  for  the
       time.  the offset from %E in the locale's alternative rep-
       resentation (year only).  the full alternative year repre-
       sentation.

       If  the  alternative format or specification for the above
       specifiers does not exist  for  the  current  locale,  the
       behavior  will be as if the unmodified specifier was used.

   SSeelleeccttiinngg tthhee oouuttppuutt''ss llaanngguuaaggee
       By default, the output of strftime, cftime,  and  ascftime
       appear  as in the C locale.  The user can request that the
       output of strftime, cftime, or ascftime be in  a  specific
       language  by  setting  the  locale for category LC_TIME in

                                                                2

strftime(3C)                                         strftime(3C)

       setlocale.

   TTiimmeezzoonnee
       The timezone is taken from  the  environment  variable  TZ
       [see for a description of TZ].

   RReettuurrnn vvaalluueess
       strftime , cftime, and ascftime return the number of char-
       acters placed into the array pointed to by s not including
       the   terminating  null  character.   Otherwise,  zero  is
       returned and the contents of the array are  indeterminate.
       If  more  than  maxsize  characters would have been placed
       into the array, strftime returns zero and the  array  con-
       tent  is  indeterminate.  If strftime, cftime, or ascftime
       overrun the size of the array, the behavior is  undefined.

   FFiilleess
       file containing locale-specific date and time information

UUssaaggee
       The  example  illustrates  the  use of strftime.  It shows
       what the string in str would look like  if  the  structure
       pointed  to  by tmptr contains the values corresponding to
       Thursday, August 28,  1986  at  12:44:36  in  New  Jersey.
       strftime(str, strsize, "%A %b %d %j", tmptr)

       This results in str containing Thursday Aug 28 240, in the
       C locale.

       For the following Era  related  definitions  for  LC_TIME:
       era_d_fmt "%EY%mgatsu%dnichi (%a)" era_d_fmt "The alterna-
       tive  time  format  is  %h  (%S)   in   %EC"   era_d_t_fmt
       "%EY%mgatsu%dnichi  (%a)  %T"  era "+:2:1990/01/01:+*:Hei-
       sei:%EC%Eynen";
             "+:1:1989/01/08:1989/12/31:Heisei:%ECgannen";
             "+:2:1927/01/01:1989/01/07:Shouwa:%EC%Eynen";
             "+:1:1926/12/25:1926/12/31:Shouwa:%ECgannen";
             "+:2:1913/01/01:1926/12/24:Taishou:%EC%Eynen";
             "+:1:1912/07/30:1912/12/31:Taishou:%ECgannen";
             "+:2:1869/01/01:1912/07/29:Meiji:%EC%Eynen";
             "+:1:1868/09/08:1868/12/31:Meiji:%ECgannen";
             "-:1868:1868/09/07:-*: :%Ey"

       For August 1st 1912, with the LC_TIME locale category  set
       as above: strftime(str, strsize, "%Ey", tmptr);

       would  result  in str containing "01".  strftime(str, str-
       size, "%Ey %EC %Ex", tmptr);

       would result  in  str  containing  "Taishougannen  Taishou
       Taishougannen08gatsu01nichi  (Sun)".   strftime(str,  str-
       size, "%EX", tmptr);

       would result  in  str  containing  "The  alternative  time

                                                                3

strftime(3C)                                         strftime(3C)

       format is Aug (01) in Taishou".

RReeffeerreenncceess
NNoottiicceess
       cftime and ascftime are obsolete.  strftime should be used
       instead.

                                                                4

