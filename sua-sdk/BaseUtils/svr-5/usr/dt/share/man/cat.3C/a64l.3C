

a64l(3C)                                                 a64l(3C)

SSyynnooppssiiss
       #include <stdlib.h>

       long a64l(const char *s);

       char *l64a(long l);

       char *l64a_r(long l, char *ret, size_t buflen);

DDeessccrriippttiioonn
       These  functions  are  used  to maintain numbers stored in
       base-64 ASCII characters.  These characters define a nota-
       tion  by  which  long integers can be represented by up to
       six characters; each character represents a ``digit'' in a
       radix-64 notation.

       The  characters used to represent ``digits'' are .  for 0,
       / for 1, 0 through 9 for 2-11, A through Z for 12-37,  and
       a through z for 38-63.

       a64l  takes  a pointer to a null-terminated base-64 repre-
       sentation and returns a corresponding long value.  If  the
       string  pointed to by s contains more than six characters,
       a64l will use the first six.

       a64l scans the character string from left  to  right  with
       the  least  significant  digit  on the left, decoding each
       character as a 6-bit radix-64 number.

       l64a takes a long argument and returns a  pointer  to  the
       corresponding  base-64 representation.  If the argument is
       0, l64a returns a pointer to a null string.

       l64a_r stores the base-64 representation of l in the  user
       supplied  buffer  ret of size buflen.  It returns ret upon
       successful completion.

   EErrrroorrss
       If l is zero, l64a_r returns NULL.

NNoottiicceess
       The value returned by l64a is  a  pointer  into  a  static
       buffer,  the  contents  of  which  are overwritten by each
       call.

       Use  the  reentrant  function  l64a_r  for  multi-threaded
       applications.

                                                                1

