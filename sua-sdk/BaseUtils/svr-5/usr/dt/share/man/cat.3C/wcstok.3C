

wcstok(3C)                                             wcstok(3C)

SSyynnooppssiiss
       #include <wchar.h>

       wchar_t  *wcstok(wchar_t  *ws1,const  wchar_t *ws2,wchar_t
       **savept);

DDeessccrriippttiioonn
       wcstok splits the wide  string  pointed  to  by  ws1  into
       tokens  delimited  by  a  wide character found in the wide
       string pointed to by ws2.   savept  points  to  a  wchar_t
       pointer  provided  by  the  caller, in which wcstok stores
       information it needs to continue processing  a  particular
       wide string.

       ws1  points  to a wide string on the first call to wcstok,
       and is a null pointer on subsequent  calls  for  the  same
       wide  string.   When  ws1  is  a  null  pointer, the value
       pointed to by saveptr is that set by the previous call  to
       wcstok  for the same wide string.  Otherwise, the incoming
       value of the object pointed to by savept is ignored.

       On the first call, wcstok  searches  for  the  first  wide
       character  which does not occur in the wide string pointed
       to by ws2.  This wide character, if found, is  the  begin-
       ning of the first token.  If no appropriate wide character
       is found, wcstok returns a null pointer, and there are  no
       tokens in the wide string.

       Starting  at the first wide character of the token, wcstok
       searches for a wide character which does occur in the wide
       string  pointed to by ws2.  If an appropriate wide charac-
       ter is found, it becomes the end  of  the  token,  and  is
       overwritten  by  a null wide character.  The current token
       extends to the end of the wide string pointed to by ws1 if
       no appropriate wide character is found.  A null pointer is
       returned by any  subsequent  searches  of  the  same  wide
       string.

       wcstok  uses  the  pointer  pointed to by saveptr to store
       enough information for subsequent calls to start searching
       just  past  the  end  of  the  token  (if  any) previously
       returned.

       ws2 can point to  a  different  wide  character  separator
       string for each call.

   RReettuurrnn vvaalluueess
       On  success,  wcstok  returns  a pointer to the first wide
       character of a token.  On failure, when no token is found,
       the function will return a null pointer.

RReeffeerreenncceess
NNoottiicceess
       If  the  feature  test macro _XOPEN_SOURCE is defined then

                                                                1

wcstok(3C)                                             wcstok(3C)

       the   following    synopsis    is    available:    wchar_t
       *wcstok(wchar_t *ws1, const wchar_t *ws2);

       Because  this  definition uses an internal wchar_t pointer
       for savept, there can be only one  active  wide  character
       string being split at a time.

                                                                2

