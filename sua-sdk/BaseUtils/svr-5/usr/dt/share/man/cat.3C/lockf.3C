

lockf(3C)                                               lockf(3C)

SSyynnooppssiiss
       #include <unistd.h>

       int lockf (int fildes, int function, off_t size);

       int lockf64 (int fildes, int function, off64_t size);

DDeessccrriippttiioonn
       lockf  locks  sections  of  a file.  Advisory or mandatory
       write locks depend on the mode bits of the file; see Other
       processes  that try to lock the locked file section either
       get an error or go to sleep  until  the  resource  becomes
       unlocked.   All  the  locks for a process are removed when
       the process terminates.  See for  more  information  about
       record locking.

       fildes  is  an  open file descriptor.  The file descriptor
       must have O_WRONLY or O_RDWR permission to establish locks
       with this function call.

       function  is  a control value that specifies the action to
       be taken.  The permissible values for function are defined
       in unistd.h as follows:

       #define F_ULOCK  0  /* unlock previously locked section */
       #define F_LOCK   1  /* lock section for exclusive use */
       #define F_TLOCK  2  /* test & lock section for exclusive use */
       #define F_TEST   3  /* test section for other locks */

       All  other  values  of  function  are  reserved for future
       extensions and will result  in  an  error  return  if  not
       implemented.

       F_TEST  is  used to detect if a lock by another process is
       present on the specified section.  F_LOCK and F_TLOCK both
       lock  a  section  of  a  file if the section is available.
       F_ULOCK removes locks from a section of the file.

       size is the number of contiguous bytes  to  be  locked  or
       unlocked.  The resource to be locked or unlocked starts at
       the current offset in the file and extends forward  for  a
       positive  size  and backward for a negative size (the pre-
       ceding bytes up to but not including the current  offset).
       If  size  is  zero,  the  section  from the current offset
       through the largest file offset is locked (that  is,  from
       the  current offset through the present or any future end-
       of-file).  An area need not be allocated to the file to be
       locked as such locks may exist past the end-of-file.

       The  sections  locked with F_LOCK or F_TLOCK may, in whole
       or in part, contain or be contained by a previously locked
       section  for  the  same  process.  Locked sections will be
       unlocked starting at the point of the offset through  size
       bytes  or  to  the end of file if size is (off_t) 0.  When

                                                                1

lockf(3C)                                               lockf(3C)

       this occurs, or if this occurs in adjacent  sections,  the
       sections  are  combined  into  a  single  section.  If the
       request requires that a new element be added to the  table
       of  active  locks and this table is already full, an error
       is returned, and the new section is not locked.

       F_LOCK and F_TLOCK requests  differ  only  by  the  action
       taken if the resource is not available.  F_LOCK will cause
       the calling process to sleep until the resource is  avail-
       able.   F_TLOCK will cause the function to return a -1 and
       set errno to EACCES if the section is  already  locked  by
       another process.

       F_ULOCK  requests may, in whole or in part, release one or
       more locked sections controlled by the process.  When sec-
       tions  are  not fully released, the remaining sections are
       still locked by the process.  Releasing the center section
       of  a locked section requires an additional element in the
       table of active locks.  If this table is full, an errno is
       set  to EDEADLK and the requested section is not released.

       A potential for deadlock occurs if a process controlling a
       locked resource is put to sleep by requesting another pro-
       cess's locked resource.  Thus calls to lockf or fcntl scan
       for  a  deadlock before sleeping on a locked resource.  An
       error return is made if sleeping on  the  locked  resource
       would cause a deadlock.

       Sleeping  on  a  resource  is interrupted with any signal.
       The alarm system call may be used  to  provide  a  timeout
       facility in applications that require this facility.

   RReettuurrnn vvaalluueess
       On success, lockf and lockf64 return 0.  On failure, lockf
       and lockf64 return -1 and set errno to indicate the error.

   EErrrroorrss
       lockf  will fail if one or more of the following are true:
       The offset of the first byte, or if  the  size  is  not  0
       (zero),  then the last byte, in the requested section can-
       not be represented correctly in an object of type off_t.

       lockf and lockf64 will fail if one or more of the  follow-
       ing  are true: fildes is not a valid open descriptor.  cmd
       is F_TLOCK or F_TEST and the section is already locked  by
       another  process.   cmd  is  F_LOCK  and  a deadlock would
       occur.  cmd is F_LOCK, F_TLOCK, or F_ULOCK and the  number
       of entries in the lock table would exceed the number allo-
       cated on the system.  fildes is on a  remote  machine  and
       the link to that machine is no longer active.  function is
       not one of F_LOCK, F_TLOCK, F_TEST, or F_UNLOCK,  or  size
       plus the current file offset is less than 0 (zero).

                                                                2

lockf(3C)                                               lockf(3C)

RReeffeerreenncceess
NNoottiicceess
       Unexpected  results may occur in processes that do buffer-
       ing in the user address  space.   The  process  may  later
       read/write  data  that  is/was  locked.   The standard I/O
       package is the most common source of unexpected buffering.

       Because  in  the  future the variable errno will be set to
       EAGAIN rather than EACCES when a  section  of  a  file  is
       already  locked  by  another process, portable application
       programs should expect and test for either value.

   CCoonnssiiddeerraattiioonnss ffoorr llaarrggee ffiillee ssuuppppoorrtt
       lockf64 supports large files, but is  otherwise  identical
       to lockf.  For details on programming for large file capa-
       ble applications, see on intro(2).

                                                                3

