

siginterrupt(3C)                                 siginterrupt(3C)

SSyynnooppssiiss
       #include <signal.h>

       int siginterrupt(int sig, int flag);

DDeessccrriippttiioonn
       The  siginterrupt  function  is used to change the restart
       behaviour when a function is interrupted by the  specified
       signal.

       If  the  flag  is  true  (1),  then  system  calls will be
       restarted if they are interrupted by the specified  signal
       and no data has been transferred yet.

       If  the flag is false (0), then restarting of system calls
       is disabled.  If a system call is interrupted by the spec-
       ified  signal and no data has been transferred, the system
       call will return -1 with errno set to EINTR.   Interrupted
       system  calls  that  have  started  transferring data will
       return the amount of data actually transferred.

       Issuing a siginterrupt call during the execution of a sig-
       nal handler will cause the new action to take place on the
       next signal to be caught.

RReettuurrnn vvaalluueess
       A 0 value indicates that the call succeeded.  A  -1  value
       indicates that an invalid signal number has been supplied.

EErrrroorrss
       The siginterrupt function will fail if: The  sig  argument
       is not a valid signal number.

CCoommppaattiibbiilliittyy
       The  siginterrupt  function  supports  programs written to
       historical system  interfaces.   A  portable  application,
       when  being  written  or  rewritten,  should  use with the
       SA_RESTART flag instead of siginterrupt.

RReeffeerreenncceess
SSttaannddaarrddss ccoonnffoorrmmaannccee
       This routine conforms  to  X/Open  System  Interfaces  and
       Headers, Issue 4, Version 2.

                                                                1

