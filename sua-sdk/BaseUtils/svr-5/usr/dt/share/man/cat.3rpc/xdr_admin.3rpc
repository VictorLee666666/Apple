

xdr_admin(3rpc)                                   xdr_admin(3rpc)

SSyynnooppssiiss
       cc [options] file -lnsl #include <rpc/xdr.h>

       u_int xdr_getpos(const XDR *xdrs);

       long *xdr_inline(XDR *xdrs; const int len);

       bool_t xdrrec_eof(XDR *xdrs);

       bool_t xdr_setpos(XDR *xdrs, const u_int pos);

DDeessccrriippttiioonn
       XDR library routines allow C programmers to describe arbi-
       trary data structures in  a  machine-independent  fashion.
       Protocols  such  as remote procedure calls (RPC) use these
       routines to describe the format of the data.

       These routines deal specifically with  the  management  of
       the XDR stream.

   RRoouuttiinneess
       See  for  the definition of the XDR data structure.  u_int
       xdr_getpos(const XDR *xdrs); A macro that invokes the get-
       position  routine  associated  with  the XDR stream, xdrs.
       The routine returns an unsigned integer,  which  indicates
       the  position of the XDR byte stream.  A desirable feature
       of XDR streams is that simple arithmetic works  with  this
       number, although the XDR stream instances need not guaran-
       tee this.  Therefore, applications written for portability
       should  not depend on this feature.  long * xdr_inline(XDR
       *xdrs, const int len); A macro that  invokes  the  in-line
       routine associated with the XDR stream, xdrs.  The routine
       returns a pointer to a contiguous piece  of  the  stream's
       buffer;  len  is  the  byte  length of the desired buffer.
       Note: pointer is cast to long *.

       Note: xdr_inline may return NULL (0) if it cannot allocate
       a  contiguous  piece  of a buffer.  Therefore the behavior
       may vary among stream instances; it exists for the sake of
       efficiency,   and  applications  written  for  portability
       should not depend on this feature.  bool_t  xdrrec_eof(XDR
       *xdrs);  This  routine can be invoked only on streams cre-
       ated by xdrrec_create.  After consuming the  rest  of  the
       current  record  in  the stream, this routine returns 1 if
       the  stream  has  no  more  input,  0  otherwise.   bool_t
       xdr_setpos(XDR  *xdrs,  const  u_int  pos);  A  macro that
       invokes the set position routine associated with  the  XDR
       stream  xdrs.   The  parameter  pos  is  a  position value
       obtained from xdr_getpos.  This routine returns 1  if  the
       XDR stream was repositioned, and 0 otherwise.

       Note:  it  is  difficult  to  reposition some types of XDR
       streams, so this routine may fail with one type of  stream
       and succeed with another.  Therefore, applications written

                                                                1

xdr_admin(3rpc)                                   xdr_admin(3rpc)

       for portability should not depend on this feature.

RReeffeerreenncceess

                                                                2

