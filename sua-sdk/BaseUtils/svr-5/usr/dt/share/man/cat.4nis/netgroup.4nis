

netgroup(4nis)                                     netgroup(4nis)

SSyynnooppssiiss
       /etc/netgroup

DDeessccrriippttiioonn
       The  netgroup  file  defines network wide groups, used for
       permission  checking  when  doing  remote  mounts,  remote
       logins, and remote shells.

FFiilleess
       Corresponding  NIS  map containing group names, user names
       and host names.  The host name is  the  key  in  the  map.
       Same  as  netgroup.byhost, except key is user name instead
       of host name.  Same  as  netgroup.byhost,  except  key  is
       group name instead of host name.

UUssaaggee
       For remote mounts, the information in the netgroup file is
       used to classify machines; for remote  logins  and  remote
       shells, it is used to classify users.

       Each line of the netgroup file defines a group and has the
       form: groupname member  ...   where  a  member  is  either
       another  group  name,  or  a  triple: (hostname, username,
       domainname) Any of these three fields  can  be  empty,  in
       which case it signifies a wild card.  For example, univer-
       sal (,,) defines a group to which everyone belongs.

       The following netgroup entry defines a group consisting of
       the  users  church, turing or godel on any of the machines
       lambda    or    enigma     in     the     domain     calc:
       trusted   (lambda,church,calc)        (enigma,turing,calc)
       (,godel,calc) The domainname  field  must  either  be  the
       local  domain  name  or empty for the netgroup entry to be
       used.  Note that this field does not limit the netgroup or
       provide  security.   The  domainname  field  refers to the
       domain in which the triple is valid,  not  to  the  domain
       containing the trusted host.

       A  gateway  machine  should  be  listed under all possible
       hostnames by which it may be recognized:  wan  (gateway,,)
       (gateway-ebb,,)

       Field names that begin with something other than a letter,
       digit or underscore (such as ``-'') work in precisely  the
       opposite  fashion.   For  example,  consider the following
       entries: machines  (analysis,-,diffeng)  (synthsis,-,diff-
       eng) people   (-,babbage,diffeng) (-,lovelace,diffeng)

       The  hosts  analysis  and  synthsis  belong  to  the group
       machines in the domain diffeng, but  no  users  belong  to
       them.  Similarly, the users babbage and lovelace belong to
       the group people in the domain diffeng,  but  no  machines
       belong to them.

                                                                1

netgroup(4nis)                                     netgroup(4nis)

       When  the  Network Information Service (NIS) is in use, it
       references the NIS maps  netgroup.byhost,  netgroup.byuser
       or netgroup on the NIS server instead of /etc/netgroup.

WWaarrnniinnggss
       The  triple  (,,  domain)  allows  all  users and machines
       trusted access, and has the  same  effect  as  the  triple
       (,,).   To  correctly restrict access to a specific set of
       members, use the  hostname  and  username  fields  of  the
       triple.

RReeffeerreenncceess

                                                                2

