

MACIOC_PROMISC(7mdi)                         MACIOC_PROMISC(7mdi)

DDeessccrriippttiioonn
       This ioctl requests that the MDI driver enable promiscuous
       mode on the adapter.   Enabling  promiscuous  mode  means:
       Network  frames bound for any MAC address are received and
       passed to the MDI consumer.  Runt frames are received  and
       passed  to the MDI consumer.  Promiscuous mode is disabled
       when the MDI device is  opened,  and  is  reset  when  the
       device  is  closed; see and Multiple opens of the same MDI
       device are not permitted; this means  that  only  one  MDI
       consumer  (for  example,  a  user  application  or  kernel
       driver) at a time can access the  the  device.   Normally,
       the  daemon opens the MDI device when the system goes into
       multiuser mode.

       Promiscuous mode support is optional for MDI drivers.  Set
       the  PROMISCUOUS parameter in the driver's file(s) to true
       if the driver implements promiscuous mode, or to false  if
       it does not.  Use the utility in interactive mode with the
       promiscuous command  to  determine  if  installed  network
       devices support promiscuous mode.

       This ioctl can only be sent directly to the MDI device; it
       will not be passed from the module (/dev/netX) to the  MDI
       driver (/dev/mdi).

       The MACIOC_PROMISC ioctl is valid only if the issuing pro-
       cess is root or has equivalent credentials; drivers should
       call  to  determine  if  the  accompanying  privileges are
       appropriate.  Promiscuous mode can be useful  for  network
       troubleshooting;  network monitors and other tools rely on
       promiscuous mode.  However, its use can  pose  significant
       security  risks, particularly when root integrity has been
       compromised.   For  this  reason,  modifications  to   MDI
       drivers  concerning  promiscuous  mode  are not supported.
       For  more  information,  see  CERT   (Computer   Emergency
       Response  Team)  Coordination Center advisories concerning
       network     monitoring     attacks,     available      at:
       http://www.cert.org/

   PPaarraammeetteerrss
       None; no ioctl data mblk is associated with this ioctl.

   DDaattaa rreettuurrnneedd
       None.

   VVeerrssiioonn aapppplliiccaabbiilliittyy
       1, 2, 2.1

   RReeffeerreenncceess

                                                                1

