

whois(1tcp)                                           whois(1tcp)

SSyynnooppssiiss
       /usr/bin/whois [-h host] identifier

DDeessccrriippttiioonn
       The whois command searches for an Internet directory entry
       for  an  identifier  which  is  either  a  name  (such  as
       ``Smith'') or a handle (such as ``SRI-NIC'').  The default
       is  for  whois   to   search   the   Internet   directory,
       whois.internic.net.

UUssaaggee
       For users who do not have direct access to Internet, whois
       provides the -h option, which allows users  to  specify  a
       host from which to request information.

       To  force  a  name-only  search,  precede  the name with a
       period; to force a handle-only search, precede the  handle
       with an exclamation point.

       To  search  for a group or organization entry, precede the
       argument with ``*'' (an asterisk).  The entire  membership
       list of the group will be displayed with the record.

       You can use an exclamation point and asterisk, or a period
       and asterisk together.

       Note that Domain Name Services needs to be  configured  on
       the  system,  at  least  as  a client, before whois can be
       used, so that the Internet domain name nic.ddn.mil can  be
       resolved.

EExxaammpplleess
       The  command  whois  Smith  looks  for  the name or handle
       SMITH.

       The command whois !SRI-NIC looks for  the  handle  SRI-NIC
       only.

       The  command  whois  .Smith,  John looks for the name JOHN
       SMITH only.

       Adding ``. . .''  to the  name  or  handle  argument  will
       match  anything  from  that  point; that is, ZU . . . will
       match ZUL, ZUM, and so on.

                                                                1

