

tftp(1tcp)                                             tftp(1tcp)

SSyynnooppssiiss
       tftp [host [port]]

DDeessccrriippttiioonn
       The  tftp  command  is  the user interface to the Internet
       TFTP (Trivial File Transfer Protocol), which allows  users
       to transfer files to and from a remote machine.

UUssaaggee
       The remote host, and optionally the port, may be specified
       on the command line, in which case tftp uses host  as  the
       default  host for future transfer (see the connect command
       below).

       host may be a resolvable domain name, an IPv4 address,  or
       an IPv6 address.  port must be specified as an integer.

   CCoommmmaannddss
       Once  tftp is running, it issues the prompt tftp> and rec-
       ognizes the following commands: Set the host (and  option-
       ally  port)  for transfers.  The TFTP protocol, unlike the
       FTP protocol, does not maintain connections between trans-
       fers; thus, the connect command does not actually create a
       connection, but merely remembers what host is to  be  used
       for  transfers.   You  do not have to use the connect com-
       mand; the remote host can be specified as part of the  get
       or  put  commands.   Set the mode for transfers; transfer-
       mode may be one of ascii or binary.  The default is ascii.
       Transfer  a  file,  or  a  set  of files, to the specified
       remote file or directory.  The destination can be  in  one
       of  two  forms:  a filename on the remote host if the host
       has already been  specified,  or  a  string  of  the  form
       host:filename  to  specify both a host and filename at the
       same time.  If the latter form is used, the specified host
       becomes  the default for future transfers.  If the remote-
       directory form is used, the remote host is assumed  to  be
       running the

       system.   Get  a file or set of files (three or more) from
       the specified remote sources.  source can be in one of two
       forms:  a  filename  on  the  remote  host if the host has
       already been specified, or a string of the form host:file-
       name to specify both a host and filename at the same time.
       If the latter  form  is  used,  the  last  host  specified
       becomes  the default for future transfers.  Exit tftp.  An
       EOF also exits.  Toggle verbose mode.  Toggle packet trac-
       ing.  Show current status.  Set the per-packet retransmis-
       sion timeout, in  seconds.   Set  the  total  transmission
       timeout, in seconds.  Shorthand for mode ascii.  Shorthand
       for mode binary.  Print help information.

WWaarrnniinnggss
       Because there is no user-login or  validation  within  the
       TFTP  protocol,  many remote sites restrict file access in

                                                                1

tftp(1tcp)                                             tftp(1tcp)

       various ways.  Approved methods for file access  are  spe-
       cific  to  each  site,  and therefore cannot be documented
       here.

       When using the get command to transfer multiple files from
       a remote host, three or more files must be specified.  The
       command returns an error message if  only  two  files  are
       specified.

RReeffeerreenncceess

                                                                2

