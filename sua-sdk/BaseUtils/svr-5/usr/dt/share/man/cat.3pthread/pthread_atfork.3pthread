

pthread_atfork(3pthread)                 pthread_atfork(3pthread)

SSyynnooppssiiss
       cc [options] -Kthread file

       #include <sys/types.h> #include <unistd.h>

       int   pthread_atfork(void  (*prepare)(void),  void  (*par-
       ent)(void),
           void (*child)(void));

DDeessccrriippttiioonn
       pthread_atfork declares fork handlers to be called  before
       and  after  in the context of the thread that called fork.
       The prepare fork handler is called before fork  processing
       commences.   The  parent  fork handle is called after fork
       processing completes in the  parent  process.   The  child
       fork  handler is called after fork processing completes in
       the child process.  If no handling is desired  at  one  or
       more of these three points, the corresponding fork handler
       address(es) may be set to NULL.

       The order of calls to pthread_atfork is significant.   The
       parent  and child fork handlers are called in the order in
       which they were established by  calls  to  pthread_atfork.
       The  prepare  fork  handlers  are  called  in the opposite
       order.

RReettuurrnn vvaalluueess
       Upon successful completion, pthread_atfork returns a value
       of  zero.  Otherwise, an error number is returned to indi-
       cate the error.

DDiiaaggnnoossttiiccss
       If the following  condition  is  detected,  pthread_atfork
       will  return  the  corresponding value: insufficient table
       space exists to record the fork handler addresses

RReeffeerreenncceess
SSttaannddaarrddss ccoommpplliiaannccee
       The Single UNIX Specification, Version 2; The Open  Group.

                                                                1

