

nlsrequest(3N)                                     nlsrequest(3N)

SSyynnooppssiiss
       cc [options] file -lnls -lnsl #include <listen.h>

       int nlsrequest (int fd, char *service_code);

       extern int _nlslog; extern char *_nlsrmsg;

DDeessccrriippttiioonn
       Given  a  virtual circuit to a listener process (fd) and a
       service code of a server process, nlsrequest  formats  and
       sends  a  service  request  message to the remote listener
       process  requesting  that  it  start  the  given  service.
       nlsrequest waits for the remote listener process to return
       a service request response message, which is  made  avail-
       able  to  the  caller  in the static, null terminated data
       buffer  pointed  to  by  _nlsrmsg.   The  service  request
       response  message includes a success or failure code and a
       text message.  The entire message is printable.

FFiilleess
RReettuurrnn vvaalluueess
       The success or failure code is  the  integer  return  code
       from  nlsrequest.   Zero indicates success, other negative
       values indicate  nlsrequest  failures  as  follows:  Error
       encountered by nlsrequest, see

       Positive  values  are error return codes from the listener
       process.  Mnemonics for these codes are defined  in  <lis-
       ten.h>.   Request message not interpretable.  Request ser-
       vice code unknown.  Service code known, but currently dis-
       abled.

       If non-null, _nlsrmsg contains a pointer to a static, null
       terminated character buffer containing the service request
       response  message.   Note  that both _nlsrmsg and the data
       buffer are overwritten by each call to nlsrequest.

       If _nlslog is non-zero, nlsrequest prints  error  messages
       on stderr.  Initially, _nlslog is zero.

RReeffeerreenncceess
NNoottiicceess
       nlsrequest cannot always be certain that the remote server
       process has been  successfully  started.   In  this  case,
       nlsrequest  returns with no indication of an error and the
       caller will receive notification of a disconnect event via
       a  T_LOOK  error before or during the first t_snd or t_rcv
       call.

                                                                1

