

DmiListComponentsByClass(3dmi)     DmiListComponentsByClass(3dmi)

SSyynnooppssiiss
       cc  [options] file -llibdmionc # remote calls cc [options]
       file -llibdmi2api # local calls #include <dmi2srv.h>

       DmiErrorStatus_t   DMI_API   DmiListComponentsByClass    (
                 DmiHandle_t         handle,
                 DmiRequestMode_t    requestMode,
                 DmiUnsigned_t       maxCount,
                 DmiBoolean_t        getPragma,
                 DmiBoolean_t        getDescription,
                 DmiId_t             compId,
                 DmiString_t *        className,           DmiAt-
       tributeValues_t  *  keyList,            DmiComponentList_t
       **    reply );

DDeessccrriippttiioonn
       The  DmiListComponentsByClass  function  lists  components
       that match specified criteria.  This command  is  used  to
       determine  if  a  component  contains a certain group or a
       certain row in a table.  A filter condition may be that  a
       component contains a specified group class name or that it
       contains a specific row in a specific group.   The  caller
       may  choose  not  to retrieve the component description by
       setting the value getDescription to false.  The caller may
       choose  not  to  retrieve the pragma string by setting the
       value of getPragma to false.

       The maxCount, requestMode, and compId parameters allow the
       caller to control the information returned by the DMI Ser-
       vice Provider.  When the requestMode is DMI_UNIQUE, compId
       specifies the first component requested (or only component
       if maxCount is one).  When the  requestMode  is  DMI_NEXT,
       compId   specifies  the  component  just  before  the  one
       requested.  When  requestMode  is  DMI_FIRSTs,  compId  is
       unused.

       To  control the amount of information returned, the caller
       sets maxCount to something other than zero.   The  Service
       Provider  must  honor this limit on the amount of informa-
       tion returned.  When maxCount is zero the Service Provider
       returns  information  for  all  components, subject to the
       constraints imposed by requestMode and compId.

   PPaarraammeetteerrss
       (Input) An open session handle (Input) Unique,  first,  or
       next  (Input)  Maximum  number  to  return,  or  0 for all
       (Input) Get optional pragma string ?  (Input) Get optional
       component description (Input) Component to start with (see
       requestMode) (Input) Group  class  name  string  to  match
       (Input)  Group row keys to match, or null (Output) List of
       components

                                                                1

DmiListComponentsByClass(3dmi)     DmiListComponentsByClass(3dmi)

RReettuurrnn vvaalluueess
       For a description of return values, see

NNoottiicceess
       Portions of this page are derived from material for  which
       the  copyright owner is the Desktop Management Task Force.
       The material is reprinted with permission.  See  copyright
       page for a full statement of rights and permissions.

                                                                2

