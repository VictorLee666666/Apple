

cond_wait(3synch)                               cond_wait(3synch)

SSyynnooppssiiss
       cc [options] -Kthread file

       #include <synch.h>

       int cond_wait(cond_t *cond, mutex_t *mutex);

DDeessccrriippttiioonn
       cond_wait blocks the calling thread at the condition vari-
       able pointed to by cond to wait for the  occurrence  of  a
       condition.  The calling thread must lock the mutual exclu-
       sion lock (mutex)  pointed  to  by  mutex  before  calling
       cond_wait, otherwise the behavior is unpredictable.

       cond_wait  automatically  releases the mutex, and waits on
       the condition variable cond.  When the condition  is  sig-
       naled  cond_wait  reacquires  the mutex and returns to the
       caller.  The wait can also be  interrupted  by  a   system
       signal, in which case mutex is reacquired, the signal han-
       dler is called, and cond_wait returns EINTR.

       The calling thread can resume execution when the condition
       is  signaled or broadcast, or when interrupted.  The logi-
       cal condition should be checked on  return,  as  a  return
       might not have been caused by a change in the condition.

   PPaarraammeetteerrss
       pointer to the condition variable to wait for pointer to a
       locked mutex

   ccoonndd ppaarraammeetteerr
       The condition variable denoted  by  cond  must  previously
       have been initialized (see cond_init(3synch)).

   mmuutteexx ppaarraammeetteerr
       mutex  is  a mutual exclusion variable protecting a shared
       resource associated with the condition represented by  the
       condition  variable,  cond.   The calling thread must lock
       mutex before calling cond_wait, otherwise the behavior  is
       unpredictable.

UUssaaggee
       See  the  description  of  how  to use condition variables
       under USAGE on

       Because the condition can change between the time the con-
       dition is signaled, and the mutex is relocked, the calling
       thread must always recheck the condition upon return  from
       cond_wait.

RReettuurrnn vvaalluueess
       cond_wait returns zero for success and an error number for
       failure.

                                                                1

cond_wait(3synch)                               cond_wait(3synch)

   EErrrroorrss
       If any of the following conditions is detected,  cond_wait
       fails  and  returns  the corresponding value: the wait was
       interrupted by a  system signal invalid argument specified

RReeffeerreenncceess

                                                                2

