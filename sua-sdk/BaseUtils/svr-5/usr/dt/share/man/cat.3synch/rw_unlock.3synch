

rw_unlock(3synch)                               rw_unlock(3synch)

SSyynnooppssiiss
       cc [options] -Kthread file

       #include <synch.h>

       int rw_unlock(rwlock_t *lock);

DDeessccrriippttiioonn
       rw_unlock   releases   a   reader-writer  lock  previously
       acquired by rw_rdlock, rw_wrlock, rw_tryrdlock, or rw_try-
       wrlock.   The  behavior  differs  according to whether the
       caller is a reader  or  a  writer:  When  a  writer  calls
       rw_unlock,  the  lock  is  unlocked.   When a reader calls
       rw_unlock, the reader count is decremented.  If the reader
       count  is zero, rw_unlock unlocks the lock, otherwise, the
       lock is not unlocked.

       When rw_unlock unlocks the lock, the first waiter  (reader
       or  writer)  is  activated.   If the thread activated is a
       reader, all subsequent readers are activated  (up  to  the
       next  writer  or  end  of  queue) and the count of readers
       holding the lock is  changed  to  reflect  this.   If  the
       thread  activated  is a writer, no other threads are acti-
       vated and the lock is marked as being held by a writer.

       lock  must   previously   have   been   initialized   (see
       rwlock_init(3synch)).

   PPaarraammeetteerrss
       pointer to the lock to be released

RReettuurrnn vvaalluueess
       rw_unlock returns zero for success and an error number for
       failure.

   EErrrroorrss
       If any the following  conditions  is  detected,  rw_unlock
       fails  and  returns the corresponding value: invalid argu-
       ment specified lock not locked

RReeffeerreenncceess

                                                                1

