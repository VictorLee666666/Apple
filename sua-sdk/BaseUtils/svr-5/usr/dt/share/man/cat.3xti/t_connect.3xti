

t_connect(3xti)                                   t_connect(3xti)

SSyynnooppssiiss
       cc [options] file -lnsl #include <xti.h>

       int t_connect(int fd, struct t_call *sndcall struct t_call
       *rcvcall)

DDeessccrriippttiioonn
       This TLI/XTI routine enables a transport user to request a
       connection to the specified destination transport user.

       This  function  is  a service of connection-mode transport
       providers and is supported only if the  provider  returned
       service  type T_COTS or T_COTS_ORD on t_open or t_getinfo.

       sndcall specifies  information  needed  by  the  transport
       provider  to  establish a connection and rcvcall specifies
       information that is associated with the newly  established
       connection.

   PPaarraammeetteerrss
       the  file  descriptor for the transport endpoint where the
       connection will be  established.   points  to  the  t_call
       structure  used to identify the transport user sending the
       connection indication.  points  to  the  t_call  structure
       used  to identify the transport user that will receive the
       connection indication.

   SSttrruuccttuurree ddeeffiinniittiioonnss
       The pointers sndcall and rcvcall refer to a t_call  struc-
       ture  that  contains  the following members: struct netbuf
       addr;     /* address              */  struct  netbuf  opt;
       /* options              */ struct netbuf udata;    /* user
       data            */  int  sequence;            /*  sequence
       number       */  netbuf  is  described in In sndcall, addr
       specifies the protocol address of the  destination  trans-
       port  user, opt presents any protocol-specific information
       that might be needed  by  the  transport  provider,  udata
       points  to  optional  user  data that may be passed to the
       destination transport user  during  connection  establish-
       ment, and sequence has no meaning for this function.

       On  return  in  rcvcall, addr returns the protocol address
       associated with the  responding  transport  endpoint,  opt
       presents any protocol-specific information associated with
       the connection, udata points to optional  user  data  that
       may  be  returned by the destination transport user during
       connection establishment, and sequence has no meaning  for
       this function.

       The  opt argument implies no structure on the options that
       may be passed to the transport  provider.   These  options
       are  specific  to the underlying protocol of the transport
       provider.  The user may choose not to  negotiate  protocol
       options  by  setting  the  len field of opt to 0.  In this

                                                                1

t_connect(3xti)                                   t_connect(3xti)

       case, the provider may use default options.

       The udata argument enables the caller to pass user data to
       the  destination transport user and receive user data from
       the  destination  user  during  connection  establishment.
       However,  the amount of user data must not exceed the lim-
       its supported by the transport provider as returned in the
       connect field of the info argument of t_open or t_getinfo.
       If the len (see netbuf in field of udata is 0 in  sndcall,
       no data will be sent to the destination transport user.

       On return, the addr, opt, and udata fields of rcvcall will
       be updated to reflect values associated with  the  connec-
       tion.   Thus, the maxlen (see the description of netbuf in
       field of each argument must be  set  before  issuing  this
       function  to  indicate  the maximum size of the buffer for
       each.  However, rcvcall may be  NULL,  in  which  case  no
       information is given to the user on return from t_connect.

   SSttaattee ttrraannssiittiioonnss
       On entry, T_IDLE; T_OUTCON or  TDATAXFER  (successful)  or
       T_IDLE  (failed)  on exit. If t_connect fails with a TLOOK
       or TNODATA error, a change of state may occur.

FFiilleess
       X/Open Transport Interface Library (shared object) Network
       Services Library (shared object)

UUssaaggee
       By  default,  t_connect  executes in synchronous mode, and
       will wait  for  the  destination  user's  response  before
       returning  control to the local user.  A successful return
       (that is, return value of 0) indicates that the  requested
       connection  has  been established.  However, if O_NONBLOCK
       is set (via t_open or fcntl), t_connect executes in  asyn-
       chronous mode.  In this way, the function simply initiates
       the connection establishment procedure by sending  a  con-
       nect  request  to  the destination transport user, and may
       fail with t_error set to TNODATA.

       Also, in the case of the TCP protocol, the peer  TCP,  and
       not the peer transport user, confirms the connection.  One
       consequence of this fact is that the t_connect can  return
       success, even though the remote server process may (later)
       call t_snddis, rather than  t_accept,  thus  aborting  the
       connection.

RReettuurrnn vvaalluueess
       t_connect  returns  0  on  success  and  -1 on failure and
       t_errno is set to indicate the error.

   EErrrroorrss
       On failure, t_errno may be set to one  of  the  following:
       The   specified  file  descriptor  does  not  refer  to  a

                                                                2

t_connect(3xti)                                   t_connect(3xti)

       transport endpoint.  The function was issued in the  wrong
       sequence.   O_NONBLOCK  was set (by t_open or cntl) so the
       function executed in asynchronous  mode.   Therefore,  the
       connection  establishment  procedure was successfully exe-
       cuted, but the function did not wait for a  response  from
       the remote user.  The specified protocol address was in an
       incorrect format or contained  invalid  information.   The
       specified  protocol options were in an incorrect format or
       contained invalid information.  The amount  of  user  data
       specified  was  not  within  the  bounds  supported by the
       transport provider as returned in the connect field of the
       info  argument  of t_open or t_getinfo.  The user does not
       have permission to use the specified address  or  options.
       The  number  of  bytes  (maxlen) allocated for an incoming
       argument is greater than zero but not sufficient to  store
       the  value  of  that argument.  If executed in synchronous
       mode, the provider's state, as seen by the  user,  changes
       to  T_DATAXFER,  and the connect indication information to
       be returned in  rcvcall  is  discarded.   An  asynchronous
       event  has occurred on the transport endpoint specified by
       fd and requires immediate attention.  This function is not
       supported  by the underlying transport provider.  A system
       error has occurred during execution of this function.  The
       specified  connection  already  exists, and this transport
       user does not support multiple connections with  the  same
       pair of local and remote addresses.  A communication prob-
       lem has been detected  with  the  transport  provider  and
       there  is  no other value of t_errno to describe the error
       condition.

RReeffeerreenncceess

                                                                3

