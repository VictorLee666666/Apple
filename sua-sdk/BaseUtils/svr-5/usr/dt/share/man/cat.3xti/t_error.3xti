

t_error(3xti)                                       t_error(3xti)

SSyynnooppssiiss
       cc [options] file -lnsl #include <xti.h>

       int t_error(char *errmsg);

DDeessccrriippttiioonn
       This  function  is a TLI/XTI local management routine used
       to generate a message  under  error  conditions.   t_error
       writes a message on the standard error file describing the
       last error encountered during a call to  a  TLI/XTI  func-
       tion.

       The argument string errmsg is user supplied and may be set
       to give context to the error.   The  message  returned  by
       t_error  prints in the following format: the user-supplied
       error message followed by a colon and the standard  trans-
       port  function  error  message  for the current value con-
       tained in t_errno.

   PPaarraammeetteerrss
       a user-supplied error message that gives  context  to  the
       error.   index  to a user-specified message array.  points
       to the array of user-supplied  message  strings.   maximum
       number of messages in the user-specified message array.

   SSttaattee ttrraannssiittiioonnss
       t_error may be issued from any valid state except T_UNINIT
       and has no effect on the entry state at exit.

FFiilleess
       X/Open Transport Interface Library (shared object) Network
       Services Library (shared object)

UUssaaggee
       On return, t_errno is set only when an error occurs and is
       not cleared on subsequent successful calls.

       If the returned value of t_errno has been set to  TSYSERR,
       t_error will also print the standard error message for the
       current value contained in errno (see

RReettuurrnn vvaalluueess
       Upon completion, a value of 0 is returned.  No errors  are
       defined.

RReeffeerreenncceess
EExxaammpplleess
       Following a t_connect function call, which might fail on a
       transport endpoint fd2 because a bad address was detected,
       a  call to t_error might be issued to check for a possible
       failure: t_error("t_connect failed on fd2"); If the t_con-
       nect  fails,  t_errno is set to the appropriate value, and
       the diagnostic message would print as: t_connect failed on
       fd2:  Incorrect  transport  address format where t_connect

                                                                1

t_error(3xti)                                       t_error(3xti)

       failed on fd2 tells the  user  which  function  failed  on
       which  transport endpoint, and Incorrect transport address
       format identifies the specific error that occurred.

                                                                2

