

fwprintf(3S)                                         fwprintf(3S)

SSyynnooppssiiss
       #include <wchar.h>

       int fwprintf(FILE *strm, const wchar_t *format, .../* args
       */);

       int swprintf(wchar_t *s,  size_t  maxsize,  const  wchar_t
       *format,      .../* args */);

       int wprintf(const wchar_t *format, .../* args */);

       #include <stdarg.h>

       int  vfwprintf(FILE  *strm,  const wchar_t *format, va_arg
       args);

       int vswprintf(wchar_t *s, size_t  maxsize,  const  wchar_t
       *format,      va_arg args);

       int vwprintf(const wchar_t *format, va_arg args);

DDeessccrriippttiioonn
       Each of these functions converts, formats, and outputs its
       args under control of the wide  character  string  format.
       Each function returns the number of wide/multibyte charac-
       ters transmitted (not including the terminating null  wide
       character in the case of swprintf and vswprintf) or a neg-
       ative value if an output error was encountered.

       fwprintf and vfwprint place multibyte output on strm.

       wprintf and vwprintf place multibyte output on  the  stan-
       dard output stream stdout.

       swprintf  and  vswprintf place wide character output, fol-
       lowed by a null wide character (\0), in  consecutive  wide
       characters  starting at s, limited to no more than maxsize
       wide characters.  If more  than  maxsize  wide  characters
       were requested, the output array will contain exactly max-
       size wide characters, with a null wide character being the
       last  (when  maxsize  is  nonzero);  a  negative  value is
       returned.

       The ``v'' functions take their arguments through the  sin-
       gle va_arg object passed.  See

       The  format consists of zero or more ordinary wide charac-
       ters (not %) which are directly copied to the output,  and
       zero  or  more conversion specifications, each of which is
       introduced by the a % and results in the fetching of  zero
       or more associated args.

       Each  conversion specification takes the following general
       form and sequence: %[pos$][flags][width][.prec][size]fmt

                                                                1

fwprintf(3S)                                         fwprintf(3S)

       An optional entry, consisting of one or more decimal  dig-
       its  followed  by a $ character, that specifies the number
       of the next arg to access.  The first arg (just after for-
       mat) is numbered 1.  If this entry is not present, the arg
       following the most recently used  arg  will  be  accessed.
       Zero  or  more  wide characters that change the meaning of
       the conversion specification.   The  flag  characters  and
       their  meanings  are: The result of the conversion will be
       left-justified within the field.  (It will be right-justi-
       fied  if  this  flag  is  not specified.)  The result of a
       signed conversion will always begin with a sign (+ or  -).
       (It  will  begin with a sign only when a negative value is
       converted if this flag is not specified.)   If  the  first
       wide character of a signed conversion is not a sign, or if
       a signed conversion results in no wide characters, a space
       will  be prefixed to the result.  If the space and + flags
       both appear, the space flag will be ignored.  The value is
       to be converted to an alternate form, depending on the fmt
       wide character: The result will contain  a  decimal  point
       wide  character, even if no digits follow.  (Normally, the
       decimal point wide character is only  present  when  frac-
       tional  digits  are produced.)  A nonzero result will have
       0b or 0B prefixed to it.  Trailing zero digits will not be
       removed from the result, as they normally are.  The preci-
       sion is increased (only when necessary) to force a zero as
       the first digit.  A nonzero result will have 0x or 0X pre-
       fixed to it.

       For other conversions, the behavior is undefined.  For all
       numeric  conversions  (a, A, e, E, f, F, g, G, b, B, d, i,
       o, u, x and X), leading zeros (following any indication of
       sign or base) are used to pad to the field width; no space
       padding is performed.  If the 0 and - flags  both  appear,
       the  0 flag will be ignored.  For the integer numeric con-
       versions (b, B, d, i, o, u, x and X), if  a  precision  is
       specified,  the 0 flag will be ignored.  For other conver-
       sions, the behavior is  undefined.   (an  apostrophe)  The
       nonfractional  portion  of the result of a decimal numeric
       conversion (d, i, u, f, F, g and G) will be grouped by the
       current  locale's thousands' separator wide character.  An
       optional entry that consists of either one or more decimal
       digits, or an asterisk (*), or an asterisk followed by one
       or more decimal digits and a $.  It specifies the  minimum
       field  width: If the converted value has fewer wide/multi-
       byte characters than the field width, it  will  be  padded
       (with  space  by  default)  on  the left or right (see the
       above flags description) to the field width.  An  optional
       entry  that consists of a period (.)  that precedes either
       zero or more decimal digits, or an  asterisk  (*),  or  an
       asterisk  followed  by one or more decimal digits and a $.
       It specifies a value that depends on the fmt wide  charac-
       ter:  It  specifies the number of fractional digits (those
       after the decimal point wide character).  For the hexadec-
       imal  floating  conversions  (a  and  A),  the  number  of

                                                                2

fwprintf(3S)                                         fwprintf(3S)

       fractional digits is just sufficient to produce  an  exact
       representation  of  the  value  (trailing  zero digits are
       removed); for the other conversions, the default number of
       fractional  digits  is 6.  It specifies the minimum number
       of digits to appear.  The default minimum number of digits
       is 1.  It specifies the maximum number of significant dig-
       its.  The default number of significant digits is  6.   It
       specifies  the maximum number of wide/multibyte characters
       to output.  The default is to take all elements up to  the
       null terminator (the entire string).

       If  only  a period is specified, the precision is taken to
       be zero.  For other conversions,  the  behavior  is  unde-
       fined.   An  optional hh, h, l (ell),ll, L, j, t or z that
       specifies other than the default argument type,  depending
       on the fmt character: The default argument type is double;
       an l is ignored for compatibility with the scanf functions
       (a  float  arg will have been promoted to double); an L ll
       causes a long double arg to  be  converted.   The  default
       argument  type  is unsigned int; an hh causes the unsigned
       int arg to be narrowed to unsigned char before conversion;
       an  h  causes  the  unsigned  int  arg  to  be narrowed to
       unsigned short before conversion; an l causes an  unsigned
       long  arg  to  be converted; an L or ll causes an unsigned
       long long arg to be converted.  The default argument  type
       is  int  which  is  converted to a wide character as if by
       calling btowc before output; an l causes a wchar_t arg  to
       be  output.   lc is a synonym for C.  The default argument
       type is int; an hh causes the int arg to  be  narrowed  to
       signed  char before conversion; an h causes the int arg to
       be narrowed to short before conversion; an l causes a long
       arg  to be converted; an L or ll causes a long long arg to
       be converted.  The default argument  type  is  pointer  to
       int;  an  h  changes  it to be a pointer to short, an l to
       pointer to long, and L or ll to pointer to long long.  The
       default  argument  type  is pointer the first element of a
       character array; an l changes it to be a  pointer  to  the
       first  element of a wchar_t array.  ls is a synonym for S.

       If a size appears other than in  these  combinations,  the
       behavior   is  undefined.   A  conversion  wide  character
       (described below) that shows the type of conversion to  be
       applied.

       When  a field width or precision includes an asterisk (*),
       an int arg supplies the width or precision value,  and  is
       said  to be ``indirect''.  A negative indirect field width
       value is taken as a - flag followed by  a  positive  field
       width.   A negative indirect precision value will be taken
       as zero.   When  an  indirect  field  width  or  precision
       includes  a  $,  the  decimal digits similarly specify the
       number of the arg that supplies the field width or  preci-
       sion.   Otherwise,  an int arg following the most recently
       used arg will be accessed for the indirect field width, or

                                                                3

fwprintf(3S)                                         fwprintf(3S)

       precision, or both, in that order; the arg to be converted
       immediately follows these.  Thus, if a conversion specifi-
       cation  includes  pos$  as well as a $-less indirect field
       width, or precision, or both, pos is taken to be the  num-
       ber  of the int arg used for the first $-less indirection,
       not the arg to be converted.

       When numbered argument specifications are used, specifying
       the  Nth  argument  requires  that all the preceding argu-
       ments, from the first to  the  (N-1)th,  be  specified  at
       least once, in a consistent way, in the format string.

       The conversion wide characters and their meanings are: The
       floating arg is converted to hexadecimal floating notation
       in  the  style [-]0xh.hhhp+-d.  The binary exponent of the
       converted value (d) is one or more  decimal  digits.   The
       number  of fractional hexadecimal digits h is equal to the
       precision.  If the precision is missing, the  result  will
       have  just  enough  digits to represent the value exactly.
       The value is rounded when fewer fractional digits is spec-
       ified.   If  the  precision  is zero and the # flag is not
       specified, no decimal point wide character  appears.   The
       single digit to the left of the decimal point character is
       nonzero for normal values.   The  A  conversion  specifier
       produces  a  value with 0X and P instead of 0x and p.  The
       unsigned integer arg is converted to  unsigned  binary  (b
       and  B),  unsigned  octal  (o),  unsigned  decimal (u), or
       unsigned hexadecimal notation (x and X).  The x conversion
       uses the letters abcdef and the X conversion uses the let-
       ters ABCDEF.  The precision specifies the  minimum  number
       of  digits  to appear; if the value being converted can be
       represented in fewer digits,  it  will  be  expanded  with
       leading zeros.  The default precision is 1.  The result of
       converting a zero value with a precision  of  zero  is  no
       wide  characters.   A j causes an uintmax_t argument to be
       converted; a t causes an argument with the  unsigned  type
       corresponding  to  ptrdiff_t to be converted; a z causes a
       size_t argument to be converted.  The integer arg is  con-
       verted to a wide character as if by calling btowc, and the
       resulting wide character is output.   The  wide  character
       wchar_t  arg  is  output.  The integer arg is converted to
       signed decimal.  The precision specifies the minimum  num-
       ber  of digits to appear; if the value being converted can
       be represented in fewer digits, it will be  expanded  with
       leading zeros.  The default precision is 1.  The result of
       converting a zero value with a precision  of  zero  is  no
       characters.   The  floating  arg is converted to the style
       [-]d.ddde+-dd, where there is one digit before the decimal
       point  character  (which  is  nonzero  if  the argument is
       nonzero) and the number of digits after it is equal to the
       precision.  If the precision is missing, it is taken as 6;
       if the precision is zero and the # flag is not  specified,
       no  decimal  point  wide  character appears.  The value is
       rounded to  the  appropriate  number  of  digits.   The  E

                                                                4

fwprintf(3S)                                         fwprintf(3S)

       conversion  wide  character  will  produce a number with E
       instead of  e  introducing  the  exponent.   The  exponent
       always  contains  at  least  two  digits.  If the value is
       zero, the exponent is zero.  The floating arg is converted
       to  decimal  notation  in  the style [-]ddd.ddd, where the
       number of fractional digits  is  equal  to  the  precision
       specification.   If  the precision is missing, it is taken
       as 6; if the precision is zero and the # flag is not spec-
       ified, no decimal point wide character appears.  If a dec-
       imal point wide character  appears,  at  least  one  digit
       appears  before it.  The value is rounded to the appropri-
       ate number of digits.  The floating arg  is  converted  in
       style  e  or f (or in style E or F in the case of a G con-
       version wide character), with the precision specifying the
       number  of  significant digits.  If the precision is zero,
       it is taken as one.  The style used depends on  the  value
       converted;  style  e (or E) will be used only if the expo-
       nent resulting from the conversion  is  less  than  -4  or
       greater  than  or  equal to the precision.  Trailing zeros
       are removed from the fractional part of the result; a dec-
       imal  point  wide character appears only if it is followed
       by a digit.  The arg is taken to be a pointer to an  inte-
       ger  into  which  is  written the number of wide/multibyte
       characters output so far by this  call.   No  argument  is
       converted.  The arg is taken to be a pointer to void.  The
       value of the pointer is converted to an sequence of print-
       able  wide  characters, which matches those read by the %p
       conversion of the functions.  The arg is  taken  to  be  a
       pointer  to  the  first element of an array of characters.
       Multibyte characters from the array are output up to  (but
       not  including)  a terminating null character; if a preci-
       sion is specified, no more than that  many  wide/multibyte
       characters are output.  If a precision is not specified or
       is greater than the size of the array, the array must con-
       tain  a  terminating  null character.  (A null pointer for
       arg will yield undefined results.)  The arg is taken to be
       a  pointer  to  the  first element of an array of wchar_t.
       Wide characters from the string are output  until  a  null
       wide character is encountered or the number of wide/multi-
       byte characters given by the precision wide would be  sur-
       passed.   If the precision specification is missing, it is
       taken  to  be  infinite.   In  no  case  will  a   partial
       wide/multibyte  character be output.  Output a %; no argu-
       ment is converted.

       If the form of the conversion specification does not match
       any  of the above, the results of the conversion are unde-
       fined.  Similarly, the results are undefined if there  are
       insufficient  args  for  the  format.   If  the  format is
       exhausted while args remain, the excess args are  ignored.

       If a floating-point value represents an infinity, the out-
       put is [+-]inf, where inf is infinity or INFINITY when the
       field  width  or  precision  is  at least 8 and inf or INF

                                                                5

fwprintf(3S)                                         fwprintf(3S)

       otherwise, the uppercase versions used only for a  capitol
       conversion wide character.  Output of the sign follows the
       rules described above.

       If a floating-point value has the internal  representation
       for  a  NaN  (not-a-number),  the  output is [+-]nan[(m)].
       Depending on the conversion character,  nan  is  similarly
       either  nan  or  NAN.   If the represented NaN matches the
       architecture's default, no (m) will be output.   Otherwise
       m  represents the bits from the significand in hexadecimal
       with abcdef or ABCDEF used, depending on the case  of  the
       conversion wide character.  Output of the sign follows the
       rules described above.

       Otherwise, the locale's decimal point wide character  will
       be  used to introduce the fractional digits of a floating-
       point value.

       A nonexistent or small field width does not cause  trunca-
       tion  of  a  field; if the result of a conversion is wider
       than the field width, the field is expanded to contain the
       conversion  result.   Multibyte  characters  generated  on
       streams (stdout or strm) are printed as if the putc  func-
       tion had been called repeatedly.

   EErrrroorrss
       These  functions return the number of wide/multibyte char-
       acters transmitted (not counting the terminating null wide
       character  for  swprintf and vswprintf), or return a nega-
       tive value if an error was encountered.

UUssaaggee
       To print a date and time in the  form  ``Sunday,  July  3,
       10:02,'' where weekday and month are pointers to null-ter-
       minated      strings:      wprintf(L"%s, %s %i, %d:%.2d",
               weekday, month, day, hour, min);

       To      print      -n      to     5     decimal     places:
       wprintf(L"pi = %.5f", 4 * atan(1.0));

       The following two calls to wprintf both produce  the  same
       result  of  10 10 00300 10:  wprintf(L"%d %1$d %.*d %1$d",
       10, 5, 300); wprintf(L"%d  %1$d  %3$.*2$d  %1$d",  10,  5,
       300);

       The  following shows a simple use of vfwprintf, a function
       that  writes  formatted  output  to  stderr  by   default.
       #include <stdarg.h> #include <stdio.h>

       void errwprintf(FILE *fp, const wchar_t *fmt, ...)  {
           va_list ap;

           va_start(ap, fmt);
           if (fp == 0)

                                                                6

fwprintf(3S)                                         fwprintf(3S)

               fp = stderr;
           (void)vfwprintf(fp, fmt, ap);
           va_end(ap); }

RReeffeerreenncceess

                                                                7

