

fprintf(3S)                                           fprintf(3S)

SSyynnooppssiiss
       #include <stdio.h>

       int  fprintf(FILE  *strm,  const  char *format, .../* args
       */);

       int printf(const char *format, .../* args */);

       int snprintf(char *s, size_t maxsize, const char  *format,
       .../* args */);

       int sprintf(char *s, const char *format, .../* args */);

       #include <stdarg.h>

       int  vfprintf(FILE  *strm,  const  char  *format,  va_list
       args);

       int vprintf(const char *format, va_list args);

       int vsnprintf(char *s, size_t maxsize, const char *format,
       va_list args);

       int vsprintf(char *s, const char *format, va_list args);

DDeessccrriippttiioonn
       Each of these functions converts, formats, and outputs its
       args under control of the character string  format.   Each
       function returns the number of characters transmitted (not
       including the terminating null character in  the  case  of
       snprintf,  sprintf,  vsnprintf and vsprintf) or a negative
       value if an output error was encountered.

       fprintf and vfprintf place output on strm.

       printf and vprintf place output  on  the  standard  output
       stream stdout.

       sprintf  and  vsprintf  place  output,  followed by a null
       character (\0), in consecutive bytes starting at s.  It is
       the  caller's responsibility to ensure that enough storage
       is available.

       snprintf  (vsnprintf)  behaves  like  sprintf  (vsprintf),
       except  that  no  more  than maxsize characters are placed
       into the array, including the terminating null  character.
       If more than maxsize characters were requested, the output
       array will contain exactly maxsize characters, with a null
       character being the last (when maxsize is nonzero); a neg-
       ative value is returned.

       The ``v'' functions take their arguments through the  sin-
       gle va_list object passed.  See

                                                                1

fprintf(3S)                                           fprintf(3S)

       The  format  consists  of zero or more ordinary characters
       (not %) which are directly copied to the output, and  zero
       or more conversion specifications, each of which is intro-
       duced by the a % and results in the fetching  of  zero  or
       more associated args.

       Each  conversion specification takes the following general
       form and sequence: %[pos$][flags][width][.prec][size]fmt

       An optional entry, consisting of one or more decimal  dig-
       its  followed  by a $ character, that specifies the number
       of the next arg to access.  The first arg (just after for-
       mat) is numbered 1.  If this entry is not present, the arg
       following the most recently used  arg  will  be  accessed.
       Zero  or  more  characters  that change the meaning of the
       conversion specification.  The flag characters  and  their
       meanings  are:  The result of the conversion will be left-
       justified within the field.  (It will  be  right-justified
       if  this  flag  is not specified.)  The result of a signed
       conversion will always begin with a sign (+  or  -).   (It
       will  begin with a sign only when a negative value is con-
       verted if this flag is not specified.)  If the first char-
       acter of a signed conversion is not a sign, or if a signed
       conversion results in no characters, a space will be  pre-
       fixed  to  the  result.   If  the  space  and + flags both
       appear, the space flag will be ignored.  The value  is  to
       be  converted  to  an alternate form, depending on the fmt
       character: The result will contain a decimal point charac-
       ter,  even  if  no  digits follow.  (Normally, the decimal
       point character is only present when fractional digits are
       produced.)   A  nonzero result will have 0b or 0B prefixed
       to it.  Trailing zero digits will not be removed from  the
       result,  as they normally are.  The precision is increased
       (only when necessary) to force a zero as the first  digit.
       A nonzero result will have 0x or 0X prefixed to it.

       For other conversions, the behavior is undefined.  For all
       numeric conversions (a, A, e, E, f, F, g, G, b, B,  d,  i,
       o, u, x and X), leading zeros (following any indication of
       sign or base) are used to pad to the field width; no space
       padding  is  performed.  If the 0 and - flags both appear,
       the 0 flag will be ignored.  For the integer numeric  con-
       versions  (b,  B,  d, i, o, u, x and X), if a precision is
       specified, the 0 flag will be ignored.  For other  conver-
       sions,  the  behavior  is  undefined.  (an apostrophe) The
       nonfractional portion of the result of a  decimal  numeric
       conversion (d, i, u, f, F, g and G) will be grouped by the
       current  locale's  thousands'  separator  character.    An
       optional entry that consists of either one or more decimal
       digits, or an asterisk (*), or an asterisk followed by one
       or  more decimal digits and a $.  It specifies the minimum
       field width: If the converted value has  fewer  characters
       than  the  field  width,  it will be padded (with space by
       default) on  the  left  or  right  (see  the  above  flags

                                                                2

fprintf(3S)                                           fprintf(3S)

       description)  to  the field width.  An optional entry that
       consists of a period (.)  that  precedes  either  zero  or
       more  decimal  digits,  or an asterisk (*), or an asterisk
       followed by one or more decimal digits and a $.  It speci-
       fies  a value that depends on the fmt character: It speci-
       fies the number of fractional digits (those after the dec-
       imal  point character).  For the hexadecimal floating con-
       versions (a and A), the number  of  fractional  digits  is
       just  sufficient to produce an exact representation of the
       value (trailing zero digits are removed);  for  the  other
       conversions, the default number of fractional digits is 6.
       It specifies the minimum number of digits to appear.   The
       default  minimum  number of digits is 1.  It specifies the
       maximum number of significant digits.  The default  number
       of significant digits is 6.  It specifies the maximum num-
       ber of bytes to output.  The default is to take  all  ele-
       ments up to the null terminator (the entire string).

       If  only  a period is specified, the precision is taken to
       be zero.  For other conversions,  the  behavior  is  unde-
       fined.   An  optional hh, h, l (ell),ll, L, j, t or z that
       specifies other than the default argument type,  depending
       on the fmt character: The default argument type is double;
       an l is ignored for compatibility with the scanf functions
       (a  float  arg will have been promoted to double); an L or
       ll causes a long double arg to be converted.  The  default
       argument  type  is unsigned int; an hh causes the unsigned
       int arg to be narrowed to unsigned char before conversion;
       an  h  causes  the  unsigned  int  arg  to  be narrowed to
       unsigned short before conversion; an l causes an  unsigned
       long  arg  to  be converted; an L or ll causes an unsigned
       long long arg to be converted.  The default argument  type
       is  int  which is narrowed to unsigned char before output;
       an l causes a wchar_t arg to be converted (to a  multibyte
       character).   lc is a synonym for C.  The default argument
       type is int; an hh causes the int arg to  be  narrowed  to
       signed  char before conversion; an h causes the int arg to
       be narrowed to short before conversion; an l causes a long
       arg  to  be converted; n L or ll causes a long long arg to
       be converted.  The default argument  type  is  pointer  to
       int;  an  h  changes  it to be a pointer to short, an l to
       pointer to long, and L or ll to pointer to long long.  The
       default  argument  type  is pointer the first element of a
       character array; an l changes it to be a  pointer  to  the
       first  element of a wchar_t array.  ls is a synonym for S.

       If a size appears other than in  these  combinations,  the
       behavior  is undefined.  A conversion character (described
       below) that shows the type of conversion to be applied.

       When a field width or precision includes an asterisk  (*),
       an  int  arg supplies the width or precision value, and is
       said to be ``indirect''.  A negative indirect field  width
       value  is  taken  as a - flag followed by a positive field

                                                                3

fprintf(3S)                                           fprintf(3S)

       width.  A negative indirect precision value will be  taken
       as  zero.   When  an  indirect  field  width  or precision
       includes a $, the decimal  digits  similarly  specify  the
       number  of the arg that supplies the field width or preci-
       sion.  Otherwise, an int arg following the  most  recently
       used arg will be accessed for the indirect field width, or
       precision, or both, in that order; the arg to be converted
       immediately follows these.  Thus, if a conversion specifi-
       cation includes pos$ as well as a  $-less  indirect  field
       width,  or precision, or both, pos is taken to be the num-
       ber of the int arg used for the first $-less  indirection,
       not the arg to be converted.

       When numbered argument specifications are used, specifying
       the Nth argument requires that  all  the  preceding  argu-
       ments,  from  the  first  to  the (N-1)th, be specified at
       least once, in a consistent way, in the format string.

       The conversion characters  and  their  meanings  are:  The
       floating arg is converted to hexadecimal floating notation
       in the style [-]0xh.hhhp+-d.  The binary exponent  of  the
       converted  value  (d)  is one or more decimal digits.  The
       number of fractional hexadecimal digits h is equal to  the
       precision.   If  the precision is missing, the result will
       have just enough digits to represent  the  value  exactly.
       The value is rounded when fewer fractional digits is spec-
       ified.  If the precision is zero and the  #  flag  is  not
       specified, no decimal point character appears.  The single
       digit to the  left  of  the  decimal  point  character  is
       nonzero  for  normal  values.   The A conversion specifier
       produces a value with 0X and P instead of 0x and  p.   The
       unsigned  integer  arg  is converted to unsigned binary (b
       and B), unsigned  octal  (o),  unsigned  decimal  (u),  or
       unsigned hexadecimal notation (x and X).  The x conversion
       uses the letters abcdef and the X conversion uses the let-
       ters  ABCDEF.   The precision specifies the minimum number
       of digits to appear; if the value being converted  can  be
       represented  in  fewer  digits,  it  will be expanded with
       leading zeros.  The default precision is 1.  The result of
       converting  a  zero  value  with a precision of zero is no
       characters.  A j causes an uintmax_t argument to  be  con-
       verted; a t causes an argument with the unsigned type cor-
       responding to ptrdiff_t to be  converted;  a  z  causes  a
       size_t  argument to be converted.  The integer arg is con-
       verted to an unsigned char, and the resulting character is
       output.   The wide character wchar_t arg is converted into
       a multibyte character and output.  The integer arg is con-
       verted  to  signed  decimal.   The precision specifies the
       minimum number of digits to appear;  if  the  value  being
       converted  can  be represented in fewer digits, it will be
       expanded with leading zeros.  The default precision is  1.
       The  result of converting a zero value with a precision of
       zero is no characters.  The floating arg is  converted  to
       the  style  [-]d.ddde+-dd, where there is one digit before

                                                                4

fprintf(3S)                                           fprintf(3S)

       the decimal point character (which is nonzero if the argu-
       ment  is  nonzero)  and  the  number of digits after it is
       equal to the precision.  If the precision is  missing,  it
       is  taken as 6; if the precision is zero and the # flag is
       not specified, no decimal point  character  appears.   The
       value is rounded to the appropriate number of digits.  The
       E conversion  character  will  produce  a  number  with  E
       instead  of  e  introducing  the  exponent.   The exponent
       always contains at least two  digits.   If  the  value  is
       zero, the exponent is zero.  The floating arg is converted
       to decimal notation in the  style  [-]ddd.ddd,  where  the
       number  of  fractional  digits  is  equal to the precision
       specification.  If the precision is missing, it  is  taken
       as 6; if the precision is zero and the # flag is not spec-
       ified, no decimal point character appears.  If  a  decimal
       point character appears, at least one digit appears before
       it.  The value is rounded to  the  appropriate  number  of
       digits.  The floating arg is converted in style e or f (or
       in style E or F in the case of a G conversion  character),
       with  the  precision  specifying the number of significant
       digits.  If the precision is zero, it  is  taken  as  one.
       The style used depends on the value converted; style e (or
       E) will be used only if the exponent  resulting  from  the
       conversion is less than -4 or greater than or equal to the
       precision.  Trailing zeros are removed from the fractional
       part of the result; a decimal point character appears only
       if it is followed by a digit.  The arg is taken  to  be  a
       pointer  to an integer into which is written the number of
       characters output so far by this  call.   No  argument  is
       converted.  The arg is taken to be a pointer to void.  The
       value of the pointer is converted to an sequence of print-
       able  characters,  which matches those read by the %p con-
       version of the functions.   The  arg  is  taken  to  be  a
       pointer  to  the  first element of an array of characters.
       Characters from the array  are  written  up  to  (but  not
       including) a terminating null character; if a precision is
       specified, no more than that many characters are  written.
       If  a  precision  is  not specified or is greater than the
       size of the array, the array must  contain  a  terminating
       null  character.  (A null pointer for arg will yield unde-
       fined results.)  The arg is taken to be a pointer  to  the
       first  element  of  an  array of wchar_t.  Wide characters
       from the string are converted into  multibyte  characters,
       and  output  until a null wide character is encountered or
       the number of bytes given by the precision wide  would  be
       surpassed.   If the precision specification is missing, it
       is taken to be infinite.  In no case will a partial multi-
       byte character be output.  Output a %; no argument is con-
       verted.

       If the form of the conversion specification does not match
       any  of the above, the results of the conversion are unde-
       fined.  Similarly, the results are undefined if there  are
       insufficient  args  for  the  format.   If  the  format is

                                                                5

fprintf(3S)                                           fprintf(3S)

       exhausted while args remain, the excess args are  ignored.

       If a floating-point value represents an infinity, the out-
       put is [+-]inf, where inf is infinity or INFINITY when the
       field width or precision is at least 8 and inf or INF oth-
       erwise, the uppercase versions used  only  for  a  capitol
       conversion  character.   Output  of  the  sign follows the
       rules described above.

       If a floating-point value has the internal  representation
       for  a  NaN  (not-a-number),  the  output is [+-]nan[(m)].
       Depending on the conversion character,  nan  is  similarly
       either  nan  or  NAN.   If the represented NaN matches the
       architecture's default, no (m) will be output.   Otherwise
       m  represents the bits from the significand in hexadecimal
       with abcdef or ABCDEF used, depending on the case  of  the
       conversion  character.   Output  of  the  sign follows the
       rules described above.

       Otherwise, the locale's decimal point  character  will  be
       used  to  introduce  the  fractional digits of a floating-
       point value.

       A nonexistent or small field width does not cause  trunca-
       tion  of  a  field; if the result of a conversion is wider
       than the field width, the field is expanded to contain the
       conversion  result.  Characters generated on streams (std-
       out or strm) are printed as if the putc function had  been
       called repeatedly.

   RReettuurrnn vvaalluueess
       These  functions return the number of characters transmit-
       ted (not  counting  the  terminating  null  character  for
       sprintf,  vsprintf,  snprintf  and vsnprintf), or return a
       negative value if an error was encountered.

       The functions fprintf, printf, vfprintf, and vprintf  fail
       if  either the stream is unbuffered or the stream's buffer
       needed to be flushed and an attempt was made to  write  at
       or beyond the offset maximum.

UUssaaggee
       To  print  a  date  and time in the form ``Sunday, July 3,
       10:02,'' where weekday and month are pointers to null-ter-
       minated       strings:       printf("%s, %s %i, %d:%.2d",
               weekday, month, day, hour, min);

       To     print     -n     to      5      decimal      places:
       printf("pi = %.5f", 4 * atan(1.0));

       The  following  two  calls to printf both produce the same
       result of 10 10 00300 10: printf("%d %1$d %.*d %1$d",  10,
       5, 300); printf("%d %1$d %3$.*2$d %1$d", 10, 5, 300);

                                                                6

fprintf(3S)                                           fprintf(3S)

       The  following  shows a simple use of vfprintf, a function
       that  writes  formatted  output  to  stderr  by   default.
       #include <stdarg.h> #include <stdio.h>

       void errprintf(FILE *fp, const char *fmt, ...)  {
           va_list ap;

           va_start(ap, fmt);
           if (fp == 0)
               fp = stderr;
           (void)vfprintf(fp, fmt, ap);
           va_end(ap); }

RReeffeerreenncceess

                                                                7

