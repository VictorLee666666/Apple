

Intro(3pam_spi)                                   Intro(3pam_spi)

DDeessccrriippttiioonn
       The  Pluggable  Authentication Modules (PAM) API forms the
       basis of the  single  sign-on  service  specified  by  the
       X/Open  Single  Sign-On  Service (XSSO) specification pub-
       lished by The Open Group.  It is independent of the mecha-
       nisms used for authentication.  Such authentication mecha-
       nisms are implemented as service modules that  conform  to
       the  system  programming  provider interface that is docu-
       mented in this manual page section.

       Service modules may also call the PAM API  functions  that
       are documented in the manual pages.

       The  manual  pages  in  this section (3pam_spi) define the
       functional interface that service modules  should  present
       to  the PAM subsystem.  Typically, a module is implemented
       as a shared object library that can be referenced from the
       PAM  configuration  file to provide authentication service
       for an application program.  The functions can be  divided
       into  five  groups  according  to  the  operation  that is
       required: An authentication service module must  implement
       the  following functions to authenticate a user and to set
       their credentials:
           Authenticate  a   user
           (that is, verify their
           identity) in the  pri-
           mary sign-on domain
           Authenticate a user in
           a  secondary   sign-on
           domain based on infor-
           mation recovered  from
           a mapping module
           Set the credentials of
           an authenticated user
       An account management service module  must  implement  the
       following  function to check that a user may use a sign-on
       service:
           Verify that a user may
           access their account
       A  session  management  service  module must implement the
       following functions to start and  end  a  session  for  an
       authenticated user:
           Start a user session
           Terminate  a user ses-
           sion
       A password management service module  must  implement  the
       following  function  to  change  a  user's password (their
       authentication token):
           Update a user's  pass-
           word
       A  mapping  service  module  must  implement the following
       functions to obtain and update user names and passwords in
       secondary  sign-on domains that are associated with a user
       in the primary sign-on domain:

                                                                1

Intro(3pam_spi)                                   Intro(3pam_spi)

           Get a user name for  a
           secondary      sign-on
           domain  from   locally
           cached information
           Set  a user name for a
           secondary      sign-on
           domain  in  the  local
           cache
           Get a user's  password
           for  a secondary sign-
           on domain from locally
           cached information
           Set  a user's password
           for a secondary  sign-
           on domain in the local
           cache

RReeffeerreenncceess

                                                                2

