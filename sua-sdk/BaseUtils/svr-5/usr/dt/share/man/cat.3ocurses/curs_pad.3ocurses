

curs_pad(3ocurses)                             curs_pad(3ocurses)

SSyynnooppssiiss
       cc [flag . . .] file -locurses [library . . .]

       #include <ocurses.h>

       WINDOW *newpad(int nlines, int ncols); WINDOW *subpad(WIN-
       DOW *orig, int nlines, int ncols,       int  begin_y,  int
       begin_x); int prefresh(WINDOW *pad, int pminrow, int pmin-
       col,      int sminrow, int smincol, int smaxrow, int smax-
       col); int pnoutrefresh(WINDOW *pad, int pminrow, int pmin-
       col,      int sminrow, int smincol, int smaxrow, int smax-
       col);   int   pechochar(WINDOW   *pad,   chtype  ch);  int
       pechowchar(WINDOW *pad, chtype wch);

DDeessccrriippttiioonn
       The newpad routine creates and returns a pointer to a  new
       pad data structure with the given number of lines, nlines,
       and columns, ncols.  A pad is like a window,  except  that
       it  is  not necessarily associated with a viewable part of
       the screen.  Automatic refreshes  of  pads  (for  example,
       from  scrolling  or echoing of input) do not occur.  It is
       not legal to call wrefresh with a pad as an argument;  the
       routines   prefresh   or  pnoutrefresh  should  be  called
       instead.  Note  that  these  routines  require  additional
       parameters  to specify the part of the pad to be displayed
       and the location on the screen to be used for the display.

       The subpad routine creates and returns a pointer to a sub-
       window within a  pad  with  the  given  number  of  lines,
       nlines,  and  columns,  ncols.   Unlike subwin, which uses
       screen coordinates, the window is  at  position  (begin_x,
       begin_y)  on the pad.  The window is made in the middle of
       the window orig, so that changes made to one window affect
       both  windows.   During  the  use of this routine, it will
       often be necessary to call touchwin or touchline  on  orig
       before calling prefresh.

       The  prefresh  and  pnoutrefresh routines are analogous to
       wrefresh and wnoutrefresh except that they relate to  pads
       instead  of windows.  The additional parameters are needed
       to indicate what part of the pad and screen are  involved.
       pminrow  and pmincol specify the upper left-hand corner of
       the rectangle to be displayed in the pad.  sminrow,  smin-
       col, smaxrow, and smaxcol specify the edges of the rectan-
       gle to be displayed on the screen.  The  lower  right-hand
       corner of the rectangle to be displayed in the pad is cal-
       culated from the screen coordinates, since the  rectangles
       must  be  the same size.  Both rectangles must be entirely
       contained within their  respective  structures.   Negative
       values  of  pminrow,  pmincol,  sminrow,  or  smincol  are
       treated as if they were zero.

       The pechochar routine is functionally equivalent to a call
       to  addch  followed by a call to refresh, a call to waddch

                                                                1

curs_pad(3ocurses)                             curs_pad(3ocurses)

       followed by a call to wrefresh, or a call to  waddch  fol-
       lowed  by  a  call to prefresh.  The knowledge that only a
       single character is being output is taken into  considera-
       tion  and, for non-control characters, a considerable per-
       formance gain  might  be  seen  by  using  these  routines
       instead of their
       equivalents.   In the case of pechochar, the last location
       of the pad on the screen is reused for  the  arguments  to
       prefresh.

       The  pechowchar  routine  is  functionally equivalent to a
       call to addwch followed by a call to refresh,  a  call  to
       waddwch  followed by a call to wrefresh, or a call to wad-
       dwch followed by a call to prefresh.

   RReettuurrnn vvaalluueess
       Routines that return an integer return  ERR  upon  failure
       and  an  integer value other than ERR upon successful com-
       pletion.

       Routines that return pointers return NULL on error.

RReeffeerreenncceess
NNoottiicceess
       The  header  file  ocurses.h  automatically  includes  the
       header files stdio.h and unctrl.h.

       Note that pechochar may be a macro.

                                                                2

