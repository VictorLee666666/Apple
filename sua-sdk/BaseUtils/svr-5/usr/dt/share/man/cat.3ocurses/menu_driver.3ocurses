

menu_driver(3ocurses)                       menu_driver(3ocurses)

SSyynnooppssiiss
       cc [flag . . .] file -lmenu -locurses [library . . .]

       #include <menu.h>

       int menu_driver(MENU *menu, int c);

DDeessccrriippttiioonn
       menu_driver  is  the workhorse of the menus subsystem.  It
       checks to determine whether the  character  c  is  a  menu
       request  or data.  If c is a request, the menu driver exe-
       cutes the request and reports the result.  If c is data (a
       printable  ASCII  character),  it enters the data into the
       pattern buffer and tries to find a matching item.   If  no
       match is found, the menu driver deletes the character from
       the pattern buffer and returns E_NO_MATCH.  If the charac-
       ter  is  not  recognized, the menu driver assumes it is an
       application-defined command and returns E_UNKNOWN_COMMAND.

       Menu driver requests:
       -----------------------------------------------------------------------
       REQ_LEFT_ITEM       Move left to an item.
       REQ_RIGHT_ITEM      Move right to an item.
       REQ_UP_ITEM         Move up to an item.
       REQ_DOWN_ITEM       Move down to an item.

              REQ_SCR_ULINE       Scroll up a line.
       REQ_SCR_DLINE       Scroll down a line.
       REQ_SCR_DPAGE       Scroll up a page.
       REQ_SCR_UPAGE       Scroll down a page.

              REQ_FIRST_ITEM      Move to the first item.
       REQ_LAST_ITEM       Move to the last item.
       REQ_NEXT_ITEM       Move to the next item.
       REQ_PREV_ITEM       Move to the previous item.

              REQ_TOGGLE_ITEM     Select/de-select an item.
       REQ_CLEAR_PATTERN   Clear the menu pattern buffer.
       REQ_BACK_PATTERN    Delete the previous character from pattern buffer.
       REQ_NEXT_MATCH      Move the next matching item.
       REQ_PREV_MATCH      Move to the previous matching item.

 vvaalluuee
       menu_driver returns one of the following:
       -------------------------------------------------------------------------
       E_OK                -   The routine returned successfully.
       E_SYSTEM_ERROR      -   System error.
       E_BAD_ARGUMENT      -   An incorrect argument was passed to the routine.
       E_BAD_STATE         -   The routine was called from an initialization or
                               termination function.
       E_NOT_POSTED        -   The menu has not been posted.
       E_UNKNOWN_COMMAND   -   An  unknown  request  was  passed  to  the  menu
                               driver.

                                                                1

menu_driver(3ocurses)                       menu_driver(3ocurses)

       E_NO_MATCH          -   The character failed to match.
       E_NOT_SELECTABLE    -   The item cannot be selected.
       E_REQUEST_DENIED    -   The menu driver could not process the request.

NNoottiicceess
       Application defined commands should be defined relative to
       (greater than) MAX_COMMAND, the maximum value of a request
       listed above.

       The  header  file menu.h automatically includes the header
       files eti.h and ocurses.h.

RReeffeerreenncceess

                                                                2

