

netstat(1Mtcp)                                     netstat(1Mtcp)

SSyynnooppssiiss
       netstat  [  -AagiLnrsu ] [ -f address_family ] [ -I inter-
       face ] [ -M corefile ] [ -N system ] [ -p protocol_name] [
       -w interval ] [ [ interval ] [ system ] [ corefile ] ]

DDeessccrriippttiioonn
       The  netstat command symbolically displays the contents of
       various network-related data structures.  The options have
       the following meanings: Show the kernel virtual address of
       any associated protocol control blocks  or  routing  table
       entries;  used for debugging.  Show the state of all sock-
       ets or routing table entries; sockets used by server  pro-
       cesses  and link-level routing table entries are not shown
       in the default  display.   Limit  statistics  and  control
       block displays to address_family.  The only address_family
       values currently supported are inet and unix.  Show multi-
       cast  routing  statistics.   Show the state of the network
       interfaces.  Show  interface  state  for  interface  only.
       Display  Global  Multiprocessor  Locking  Statistics.  Use
       corefile as the system core image instead of  the  default
       /dev/kmem  (used  with -A option).  Show network addresses
       as numbers  (normally  netstat  interprets  addresses  and
       attempts to display them symbolically).  Use system as the
       system namelist instead of the default /unix (used with -A
       option).   Limit  statistics and control block displays to
       protocol-name, for example, tcp.  Show the routing  table.
       Show per-protocol statistics.  Equivalent to specifying -f
       unix.  Show statistics regarding  packet  traffic  on  the
       configured  network interfaces, sampling and displaying an
       update every interval seconds.   The  arguments  interval,
       system,  and  corefile  are  the old syntax alternative to
       specifying the -w, -N, and -M options.

   AAddddrreessss aanndd ppoorrtt ffoorrmmaattss
       In all displays, address formats are of the form host.port
       or  network (if a socket's address specifies a network but
       no specific host address).  When  known,  host  addresses,
       network addresses, and port numbers are displayed symboli-
       cally.   The  symbolic  name  for  a  network  address  is
       obtained  from the database /etc/networks, from the Domain
       Name Service (DNS) resolver, or from NIS, depending on the
       configuration  specified  in /etc/netconfig.  The symbolic
       name for a host address  is  obtained  from  the  database
       /etc/hosts,  from  the Domain Name Service (DNS) resolver,
       or from NIS, depending on the configuration  specified  in
       /etc/netconfig.   The symbolic name for a port is obtained
       from the database /etc/services or from NIS.   If  a  sym-
       bolic name for an address or port is unknown, or if the -n
       option is specified, the address is printed in the  Inter-
       net  ``dot format'' (refer to for more information regard-
       ing this format) and the port is identified by its number.
       Unspecified,  or  ``wildcard,'' addresses and ports appear
       as *.

                                                                1

netstat(1Mtcp)                                     netstat(1Mtcp)

   DDiissppllaayy ffoorrmmaattss
       There are a number of display formats,  depending  on  the
       information presented.

       The  default display appears when netstat is invoked with-
       out any options.   This  display,  about  active  sockets,
       shows  the  local  and  remote addresses, send and receive
       queue sizes (in bytes), protocol, and, as appropriate, the
       internal state of the protocol.

       The  following  states  may  be displayed for TCP sockets:
       Closed. The socket  is  not  being  used.   Listening  for
       incoming  connections.  (Usually at server end.)  Actively
       trying to establish connection.  (Usually at client  end.)
       Initial  synchronization  of  the  connection  under  way.
       (Usually at server end.)  Connection has been established.
       Remote  shut down; waiting for the socket to close.  (Usu-
       ally at server  end.)   Remote  shut  down,  then  closed;
       awaiting   acknowledgement.    (Usually  at  server  end.)
       Socket closed;  shutting  down  connection.   (Usually  at
       client  end.)   Socket  closed;  waiting for shutdown from
       remote.  (Usually at client  end.)   Closed,  then  remote
       shutdown;  awaiting  acknowledgement.   (Usually at client
       end.)  Wait after close for  remote  shutdown  retransmis-
       sion.   (Usually at client end.)  The -i interface display
       provides a table of cumulative statistics regarding  pack-
       ets  transferred,  errors,  and  collisions.   The network
       address (currently Internet specific) of the interface and
       the  maximum  transmission  unit  (``mtu'')  are also dis-
       played.  If the -a flag is used in conjunction with the -i
       flag,  information  about multicast addresses will also be
       displayed.

       The -r  routing  table  display  indicates  the  available
       routes  and their status.  Each route consists of a desti-
       nation host or network and a gateway to use in  forwarding
       packets.   The  Flags  field  shows the state of the route
       (``U'' if ``up''), and whether the route is to  a  gateway
       (``G'').  Direct routes are automatically created for each
       interface attached to the  local  host.   The  Refs  field
       gives  the  current  number  of  active uses of the route.
       Connection-oriented protocols normally hold on to a single
       route  for the duration of a connection, while connection-
       less protocols obtain a route then discard  it.   The  Use
       field provides a count of the number of packets sent using
       that route.  The Interface  field  indicates  the  network
       interface utilized for the route.

       If  the  -a  option  is  used  in  conjunction with the -r
       option, link-level (ARP) routes will be displayed as  well
       as  regular IP-level routes.  In addition, some flags that
       are normally suppressed (M, N, P) will be displayed.

       A link-level entry for which no valid  link-level  address

                                                                2

netstat(1Mtcp)                                     netstat(1Mtcp)

       currently exists is listed as ``incomplete''.

       The  complete list of flags that may be shown in the rout-
       ing table display are: The route is  currently  marked  as
       ``losing''  because the kernel has detected a transmission
       problem.  The route is a ``cloning'' route via  an  inter-
       face.   New  routes  for  specific  destinations  will  be
       derived from this route.  The route  was  created  as  the
       result  of  an  ICMP redirect message being received.  The
       route is to a gateway.  The route is to a host.  The route
       has  associated  link-level  information,  such  as an ARP
       entry.  The route has been modified  since  its  creation,
       possibly  due to a redirect.  The Path MTU discovery algo-
       rithm has discovered a new MTU for this route.   Path  MTU
       discovery  is being performed on this route.  The route is
       marked as ``reject'' to prevent any traffic  from  flowing
       to this destination.  The route was statically configured.
       The route is up.  The route is resolved  externally  by  a
       user-level  process.  This is not supported in the current
       implementation.  The -w display consists of a column  sum-
       marizing  information for a default single interface and a
       column summarizing information for  all  interfaces.   The
       default  single  interface  may be changed by specifying a
       different interface using the -I option.  The  first  line
       of each screen of information contains a summary since the
       system was last rebooted.  Subsequent lines of output show
       values accumulated over the preceding interval.

DDiiaaggnnoossttiiccss
       Interface statistics are dependent on the link driver.  If
       it does not attach itself to the ifstats structure in  the
       kernel  or  support  the DL_GETSTATS ioctl, the message No
       Statistics Available will be printed for that interface.

RReeffeerreenncceess
NNoottiicceess
       Use of the old syntax for specifying interval, system, and
       corefile  (that  is. without a preceding argument) is dis-
       couraged, as support for this may disappear in the future.

                                                                3

