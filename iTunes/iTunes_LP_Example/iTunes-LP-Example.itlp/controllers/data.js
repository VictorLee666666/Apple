var appData = {
  feature : { XID : "TEST:uuid:B5ADF718-D4C8-473F-BFAF-B86570A233DF", title : "Music", artist : "Artist" },
  audioLoop : { src : "audio/intro.m4a", loop : false },
  numberOfPhotos : 10,
  songs : [
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" },
    { XID : "TEST:uuid:0C09B444-F52C-43D5-9B4A-272020F6CB3B" }
  ],
  videos : [
    { string : "Music Video 1", src: 'music_video_1.m4v', duration: '02:41' },
    { string : "Music Video 2", src: 'music_video_2.m4v', duration: '01:55' }
  ]
};